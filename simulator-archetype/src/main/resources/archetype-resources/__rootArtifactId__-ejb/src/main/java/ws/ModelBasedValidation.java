#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.ws;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.soap.SOAPException;

import net.ihe.gazelle.validation.model.ValidatorDescription;
import net.ihe.gazelle.validation.ws.AbstractModelBasedValidation;

import org.jboss.seam.annotations.Name;

// do not change those annotations, they are useful to keep a consistency between all validation services 
@Stateless
@Name("ModelBasedValidationWS")
@WebService(name = "ModelBasedValidationWS", 
		serviceName = "ModelBasedValidationWSService", 
		portName = "ModelBasedValidationWSPort", 
		targetNamespace = "http://ws.mb.validator.gazelle.ihe.net")
public class ModelBasedValidation extends AbstractModelBasedValidation {

	/**
	 * Returns the list of available validators through this web service
	 * @return
	 * @throws SOAPException
	 */
	@WebMethod
	@WebResult(name = "Validators")
	public List<String> getListOfValidators(@WebParam(name="descriminator") String descriminator) throws SOAPException {
		return null;
	}

	/**
	 * Validates a document against the given validator and returned a structured result
	 * @param document
	 * @param validator
	 * @return
	 * @throws SOAPException
	 */
	@WebMethod
	@WebResult(name = "DetailedResult")
	public String validateDocument(
			@WebParam(name = "document") String document,
			@WebParam(name = "validator") String validator)
			throws SOAPException {
		return "this validation service is not yet ready to be used ...";
	}
	
	@Override
	protected String executeValidation(String document, ValidatorDescription validator) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ValidatorDescription getValidatorByOidOrName(String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected List<ValidatorDescription> getValidatorsForDescriminator(String descriminator) {
		// TODO Auto-generated method stub
		return null;
	}

}

#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_name', '${rootArtifactId}');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_time_zone', 'UTC+01');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'svs_repository_url', 'http://gazelle.ihe.net');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'documentation_url', 'http://gazelle.ihe.net');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_url', 'http://localhost:8080/${rootArtifactId}');
INSERT INTO app_configuration(id, variable, value) values (nextval('app_configuration_id_seq'), 'cas_url', 'https://gazelle.ihe.net/cas');
INSERT INTO app_configuration(id, variable, value) values (nextval('app_configuration_id_seq'), 'application_works_without_cas', 'true');
INSERT INTO app_configuration(id, variable, value) values (nextval('app_configuration_id_seq'), 'ip_login', 'true');
INSERT INTO app_configuration(id, variable, value) values (nextval('app_configuration_id_seq'), 'ip_login_admin', '.*');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_issue_tracker_url', 'link to issue tracker');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_release_notes_url', 'link to release notes');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'time_zone', 'Europe/Paris');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'message_permanent_link', 'http://localhost:8080/${rootArtifactId}/messages/messageDisplay.seam?id=');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'NUMBER_OF_ITEMS_PER_PAGE', '20');
insert into app_configuration (id, variable, value) values (nextval('app_configuration_id_seq'), 'contact_email', 'dev@contact.com');
insert into app_configuration (id, variable, value) values (nextval('app_configuration_id_seq'), 'contact_name', 'Developer');
insert into app_configuration (id, variable, value) values (nextval('app_configuration_id_seq'), 'contact_title', 'Application administrator');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'gazelle_sts_url', 'https://gazelle.ihe.net/picketlink-sts');
INSERT INTO app_configuration(id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'restrict_access_to_messages', 'false');


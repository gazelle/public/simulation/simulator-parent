package net.ihe.gazelle.simulator.message.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>CompanyDetails class.</p>
 *
 * @author abe
 * @version 1.0: 19/02/18
 */

@Name("companyDetails")
@Entity
@Table(name = "cmn_company_details", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "company_keyword"))
@SequenceGenerator(name = "cmn_company_details_sequence", sequenceName = "cmn_company_details_id_seq", allocationSize = 1)
public class CompanyDetails implements Serializable{

    @Id
    @NotNull
    @GeneratedValue(generator = "cmn_company_details_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "company_keyword")
    private String companyKeyword;

    @OneToMany(mappedBy = "companyDetails", targetEntity = IPAddress.class, cascade = CascadeType.ALL)
    private List<IPAddress> ipList;

    public CompanyDetails(){

    }

    public String getCompanyKeyword() {
        return companyKeyword;
    }

    public void setCompanyKeyword(String companyKeyword) {
        this.companyKeyword = companyKeyword;
    }

    public List<IPAddress> getIpList() {
        if (ipList == null){
            ipList = new ArrayList<IPAddress>();
        }
        return ipList;
    }

    public void setIpList(List<IPAddress> ipList) {
        this.ipList = ipList;
    }

    public Integer getId() {
        return id;
    }

    public void addIpToList(IPAddress ip) {
        getIpList().add(ip);
    }
}

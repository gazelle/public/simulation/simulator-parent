/**
 * Classes for configuring HL7 Version 2 and Version 3 connection end points.
 *
 */
package net.ihe.gazelle.simulator.common.configuration.HL7.model;
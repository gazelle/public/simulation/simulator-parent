package net.ihe.gazelle.simulator.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Structure used to build the 'Message' part of the response
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Message")
public class TestReportDataMessage {
	// request or response
	@XmlElement(name = "Type")
	private String type;
	// MSH-9, action ...
	@XmlElement(name = "MessageType")
	private String messageType;
	// PASSED, FAILED or null
	@XmlElement(name = "ValidationResult")
	private String validationResult;
	// actor keyword of the sender
	@XmlElement(name = "Actor")
	private String actorKeyword;
	// if needed, id of the message in the database
	@XmlElement(name = "Id")
	private String messageId;

	/**
	 * <p>Constructor for TestReportDataMessage.</p>
	 *
	 * @param type a {@link java.lang.String} object.
	 * @param messageType a {@link java.lang.String} object.
	 * @param validationResult a {@link java.lang.String} object.
	 * @param actorKeyword a {@link java.lang.String} object.
	 * @param messageId a {@link java.lang.String} object.
	 */
	public TestReportDataMessage(String type, String messageType, String validationResult, String actorKeyword,
			String messageId) {
		this.type = type;
		this.messageType = messageType;
		this.validationResult = validationResult;
		this.actorKeyword = actorKeyword;
		this.messageId = messageId;
	}
	
	/**
	 * <p>Constructor for TestReportDataMessage.</p>
	 */
	public TestReportDataMessage(){
		
	}

	/**
	 * <p>Getter for the field <code>type</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getType() {
		return type;
	}

	/**
	 * <p>Getter for the field <code>messageType</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMessageType() {
		return messageType;
	}

	/**
	 * <p>Getter for the field <code>validationResult</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getValidationResult() {
		return validationResult;
	}

	/**
	 * <p>Getter for the field <code>actorKeyword</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getActorKeyword() {
		return actorKeyword;
	}

	/**
	 * <p>Getter for the field <code>messageId</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMessageId() {
		return messageId;
	}
}

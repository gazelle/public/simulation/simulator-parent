package net.ihe.gazelle.simulator.samlassertion.common;

import net.ihe.gazelle.simulator.common.utils.XmlUtil;
import net.ihe.gazelle.simulator.samlassertion.common.SignatureException;
import org.apache.commons.io.IOUtils;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.transforms.Transforms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStore.TrustedCertificateEntry;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Signs elements inside a DOM document.
 *
 * @author Gabriel Landais
 * @version $Id: $Id
 */
public class SignatureUtil {

    private static final Logger LOG = LoggerFactory.getLogger(SignatureUtil.class);

    private static final String NS_DSIG = "http://www.w3.org/2000/09/xmldsig#";

    private KeyStore keyStore;

    private Map<String, PublicKey> publicKeys;

    private String keyAlias = null;

    private String keyPassword = null;

    static {
        org.apache.xml.security.Init.init();
    }

    /**
     * <p>main.</p>
     *
     * @param args an array of {@link String} objects.
     * @throws Exception if any.
     */
    public static void main(String[] args) {
        try {
            SignatureUtil signatureUtil = new SignatureUtil("/usr/local/jboss/simu.jks", "gazelle", "tomcat", "gazelle");
            signatureUtil.exportPublicKey("/tmp/simu.valid.jks", "gazelle", "urn:idp:demo");
        } catch (Exception e) {
            LOG.error("Unexpected error", e);
        }
    }

    /**
     * Instantiates a new signature tool.
     *
     * @param keystorePath the keystore path
     * @param keyStorePass the key store pass
     * @throws SignatureException XML signature exception
     */
    public SignatureUtil(String keystorePath, String keyStorePass) throws SignatureException {
        this(keystorePath, keyStorePass, null, null);
    }

    /**
     * <p>Constructor for SignatureUtil.</p>
     *
     * @param keystorePath     a {@link String} object.
     * @param keyStorePass     a {@link String} object.
     * @param certificateAlias a {@link String} object.
     * @param keyAlias         a {@link String} object.
     * @param keyPassword      a {@link String} object.
     * @throws SignatureException if any.
     */
    @Deprecated
    public SignatureUtil(String keystorePath, String keyStorePass, String certificateAlias, String keyAlias,
                         String keyPassword) throws SignatureException {
        this(keystorePath, keyStorePass, keyAlias, keyPassword);
    }

    /**
     * <p>Constructor for SignatureUtil.</p>
     *
     * @param keystorePath a {@link String} object.
     * @param keyStorePass a {@link String} object.
     * @param keyAlias     a {@link String} object.
     * @param keyPassword  a {@link String} object.
     * @throws SignatureException if any.
     */
    public SignatureUtil(String keystorePath, String keyStorePass, String keyAlias, String keyPassword)
            throws SignatureException {
        super();
        setUpKeyStore(keystorePath, keyStorePass);
        this.keyAlias = keyAlias;
        this.keyPassword = keyPassword;
    }

    /**
     * Sets the up key store.
     *
     * @param keystorePath     the keystore path
     * @param keyStorePass     the key store pass
     * @param certificateAlias the certificate alias
     * @param keyAlias         the key alias
     * @param keyPass          the key pass
     * @throws SignatureException XML signature exception
     */
    private void setUpKeyStore(String keystorePath, String keyStorePass) throws SignatureException {
        InputStream is = null;
        try {
            // Try the file method
            File file = new File(keystorePath);
            is = new FileInputStream(file);
            keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(is, keyStorePass.toCharArray());

            publicKeys = new HashMap<String, PublicKey>();

            try {
                for (Enumeration<String> aliases = keyStore.aliases(); aliases.hasMoreElements(); ) {
                    String alias = aliases.nextElement();
                    Certificate certificate = keyStore.getCertificate(alias);
                    if (certificate != null) {
                        publicKeys.put(alias, certificate.getPublicKey());
                    }
                }
            } catch (Exception e) {
                throw new SignatureException("Failed to load public keys: " + e.getMessage());
            }

        } catch (Exception e) {
            throw new SignatureException(e);
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    /**
     * <p>signSOAPBody.</p>
     *
     * @param document a {@link String} object.
     * @return a {@link String} object.
     * @throws SignatureException if any.
     */
    public String signSOAPBody(String document) throws SignatureException {
        return document;
    }

    /**
     * <p>signSOAPBodyOld.</p>
     *
     * @param document a {@link String} object.
     * @return a {@link String} object.
     * @throws SignatureException if any.
     */
    public String signSOAPBodyOld(String document) throws SignatureException {
        try {
            Document documentDOM = XmlUtil.parse(document);
            Document signedDocument = signSOAPBody(documentDOM);
            return XmlUtil.outputDOM(signedDocument);
        } catch (Exception e) {
            throw new SignatureException(e);
        }
    }

    /**
     * Signs body of a SOAP message.
     *
     * @param document the document
     * @return the document
     * @throws SignatureException XML signature exception
     */
    public Document signSOAPBody(Document document) throws SignatureException {
        return document;
    }

    /**
     * <p>signSOAPBodyOld.</p>
     *
     * @param document a {@link Document} object.
     * @return a {@link Document} object.
     * @throws SignatureException if any.
     */
    public Document signSOAPBodyOld(Document document) throws SignatureException {
        Element bodyElement = null;

        Element envelope = document.getDocumentElement();
        NodeList childNodes = envelope.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            if (childNodes.item(i) instanceof Element) {
                Element element = (Element) childNodes.item(i);
                String localName = element.getTagName();
                if (isBody(localName)) {
                    bodyElement = element;
                }
            }
        }

        if (bodyElement != null) {
            return sign(document, bodyElement);
        } else {
            return document;
        }
    }

    private boolean isBody(String localName) {
        if ("Body".equals(localName)) {
            return true;
        } else if (localName != null && localName.endsWith(":Body")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Signs an element.
     *
     * @param document          the document
     * @param elementToBeSigned the element to be signed
     * @return the document
     * @throws SignatureException XML signature exception
     */
    public Document sign(Document document, Element elementToBeSigned) throws SignatureException {

        if (keyStore == null || keyAlias == null || keyPassword == null) {
            throw new SignatureException("Invalid keystore");
        }
        Certificate certificate = null;
        PrivateKey privateKey = null;
        try {
            privateKey = (PrivateKey) keyStore.getKey(keyAlias, keyPassword.toCharArray());
            if (privateKey == null) {
                throw new SignatureException("Invalid keystore : no private key");
            }
            // TODO check with chained certificates?
            certificate = keyStore.getCertificate(keyAlias);
        } catch (Exception e) {
            throw new SignatureException("Invalid keystore : failed to load credentials: " + e.getMessage());
        }

        try {
            String id = elementToBeSigned.getAttribute("ID");
            if (id == null || "".equals(id)) {
                id = UUID.randomUUID().toString();
                elementToBeSigned.setAttribute("ID", id);
            }

            Document result = XmlUtil.formatDocument(document);

            XMLSignature xmlSignature = new XMLSignature(result, "", "http://www.w3.org/2000/09/xmldsig#rsa-sha1",
                    "http://www.w3.org/2001/10/xml-exc-c14n#");
            // add it before sign, as an enveloped signature
            Element signatureElement = xmlSignature.getElement();
            appendSignature(result, id, signatureElement);

            Transforms transforms = new Transforms(result);
            transforms.addTransform("http://www.w3.org/2000/09/xmldsig#enveloped-signature");
            transforms.addTransform("http://www.w3.org/2001/10/xml-exc-c14n#");
            xmlSignature.addDocument("#" + id, transforms);

            if (certificate != null && certificate instanceof X509Certificate) {
                xmlSignature.addKeyInfo((X509Certificate) certificate);
            }

            xmlSignature.sign(privateKey);

            // String txtAssertion = XmlUtil.outputDOM(result);
            // log.info("===============================");
            // log.info(txtAssertion);
            // log.info("===============================");
            // log.info(validate(txtAssertion));
            // log.info("===============================");

            return result;
        } catch (Exception e) {
            throw new SignatureException("Failed to sign: " + e.getMessage());
        }

    }

    private void appendSignature(Document document, String id, Element signatureElement) {
        Node nextSibling = null;

        Element elementToBeSigned = getElement(document, id);

        if (elementToBeSigned != null) {
            NodeList childNodes = elementToBeSigned.getChildNodes();
            if (childNodes != null) {
                for (int i = 0; i < childNodes.getLength(); i++) {
                    if (childNodes.item(i) instanceof Element) {
                        String localName = ((Element) childNodes.item(i)).getLocalName();
                        if ("Subject".equals(localName)) {
                            nextSibling = childNodes.item(i);
                        }
                    }
                }
            }
            if (nextSibling == null) {
                elementToBeSigned.appendChild(signatureElement);
            } else {
                elementToBeSigned.insertBefore(signatureElement, nextSibling);
            }
        }
    }

    private Element getElement(Node node, String id) {
        if (node instanceof Element) {
            Element testElement = (Element) node;
            if (testElement.hasAttribute("ID") && testElement.getAttribute("ID").equals(id)) {
                return testElement;
            }
        }
        NodeList childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Element testElement = getElement(childNodes.item(i), id);
            if (testElement != null) {
                return testElement;
            }
        }
        return null;
    }

    /**
     * <p>validate.</p>
     *
     * @param xml   a {@link String} object.
     * @param alias a {@link String} object.
     * @return a boolean.
     * @throws SignatureException if any.
     */
    public boolean validate(String xml, String alias) throws SignatureException {
        if (keyStore == null || publicKeys == null || publicKeys.size() == 0) {
            throw new SignatureException("Invalid keystore");
        }

        Collection<PublicKey> publicKeysSet = null;

        if (alias == null) {
            publicKeysSet = publicKeys.values();
        } else {
            publicKeysSet = new ArrayList<PublicKey>();
            PublicKey publicKey = publicKeys.get(alias);
            if (publicKey != null) {
                publicKeysSet.add(publicKey);
            }
        }

        if (publicKeysSet == null || publicKeysSet.size() == 0) {
            throw new SignatureException("Alias " + alias + " not fund in truststore");
        }

        // Rebuild the document
        Document doc;
        try {
            doc = XmlUtil.parse(xml);
        } catch (Exception e) {
            throw new SignatureException("Failed to parse XML: " + e.getMessage());
        }

        NodeList signatures = doc.getElementsByTagNameNS(NS_DSIG, "Signature");
        for (int i = 0; i < signatures.getLength(); i++) {
            boolean signatureValid = false;
            Element signatureElement = (Element) signatures.item(i);
            XMLSignature xmlSignature;
            try {
                xmlSignature = new XMLSignature(signatureElement, "");
            } catch (Exception e) {
                throw new SignatureException("Failed to create signature: " + e.getMessage());
            }

            for (PublicKey publicKey : publicKeysSet) {
                try {
                    if (xmlSignature.checkSignatureValue(publicKey)) {
                        signatureValid = true;
                    }
                } catch (Exception e) {
                    throw new SignatureException("Failed to check with a public key: " + e.getMessage());
                }
            }

            if (!signatureValid) {
                return false;
            }
        }
        return true;
    }

    private void exportPublicKey(String destKeyStore, String destKeyPass, String destAlias) throws Exception {
        Certificate certificate = keyStore.getCertificate(keyAlias);
        TrustedCertificateEntry trustedCertificateEntry = new TrustedCertificateEntry(certificate);

        KeyStore ks = KeyStore.getInstance("JKS", "SUN");

        File keystoreFile = new File(destKeyStore);
        if (!keystoreFile.exists()) {
            ks.load(null, destKeyPass.toCharArray());
            ks.store(new FileOutputStream(destKeyStore), destKeyPass.toCharArray());
        }
        ks.load(new FileInputStream(destKeyStore), destKeyPass.toCharArray());

        ks.setEntry(destAlias, trustedCertificateEntry, null);

        ks.store(new FileOutputStream(destKeyStore), destKeyPass.toCharArray());
    }
}

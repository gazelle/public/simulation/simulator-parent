
/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.tf.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.utils.DatabaseUtil;

import javax.validation.constraints.NotNull;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;


@Entity
/**
 * <p>ActorIntegrationProfileOption class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("actorIntegrationProfileOption")
@Table(name = "tf_actor_integration_profile_option", schema = "public",	uniqueConstraints = @UniqueConstraint(columnNames = {"actor_integration_profile_id", "integration_profile_option_id"}))
@SequenceGenerator(name = "tf_actor_integration_profile_option_sequence",sequenceName = "tf_actor_integration_profile_option_id_seq", allocationSize=1)
public class ActorIntegrationProfileOption implements java.io.Serializable,Comparable<ActorIntegrationProfileOption> {

    /** Serial ID version of this object */
    private static final long serialVersionUID = 1L;

    /** Logger */
    @Logger
    private static Log log;



    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy=GenerationType.SEQUENCE, 
            generator="tf_actor_integration_profile_option_sequence")
            private Integer id;


    @ManyToOne
    @JoinColumn(name="actor_integration_profile_id" , nullable=false)
    private ActorIntegrationProfile actorIntegrationProfile;

    @ManyToOne
    @JoinColumn(name="integration_profile_option_id")	
    private IntegrationProfileOption integrationProfileOption;

    /**
     * <p>Constructor for ActorIntegrationProfileOption.</p>
     */
    public ActorIntegrationProfileOption() {

    }

    /**
     * <p>Constructor for ActorIntegrationProfileOption.</p>
     *
     * @param actorIntegrationProfile a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfile} object.
     * @param integrationProfileOption a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption} object.
     */
    public ActorIntegrationProfileOption(ActorIntegrationProfile actorIntegrationProfile,IntegrationProfileOption integrationProfileOption) {
        this.integrationProfileOption = integrationProfileOption;
        this.actorIntegrationProfile=actorIntegrationProfile;

    }	

    /**
     * <p>Constructor for ActorIntegrationProfileOption.</p>
     *
     * @param inActorIntegrationProfileOption a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfileOption} object.
     */
    public ActorIntegrationProfileOption(ActorIntegrationProfileOption inActorIntegrationProfileOption ) {

        if  (  inActorIntegrationProfileOption != null )
        { 
            if ( inActorIntegrationProfileOption.getActorIntegrationProfile() != null ) actorIntegrationProfile = new ActorIntegrationProfile ( inActorIntegrationProfileOption.getActorIntegrationProfile() ) ; 
            if ( inActorIntegrationProfileOption.getIntegrationProfileOption() != null ) integrationProfileOption = new IntegrationProfileOption( inActorIntegrationProfileOption.getIntegrationProfileOption() ) ; 
        }

    }  

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() { return id; }

    /**
     * <p>Getter for the field <code>integrationProfileOption</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption} object.
     */
    public IntegrationProfileOption getIntegrationProfileOption() {
        return integrationProfileOption;
    }
    /**
     * <p>Setter for the field <code>integrationProfileOption</code>.</p>
     *
     * @param integrationProfileOption a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption} object.
     */
    public void setIntegrationProfileOption(
            IntegrationProfileOption integrationProfileOption) {
        this.integrationProfileOption = integrationProfileOption;
    }

    /**
     * <p>Getter for the field <code>actorIntegrationProfile</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfile} object.
     */
    public ActorIntegrationProfile getActorIntegrationProfile() {
        return actorIntegrationProfile;
    }
    /**
     * <p>Setter for the field <code>actorIntegrationProfile</code>.</p>
     *
     * @param actorIntegrationProfile a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfile} object.
     */
    public void setActorIntegrationProfile(ActorIntegrationProfile actorIntegrationProfile) {
        this.actorIntegrationProfile = actorIntegrationProfile;
    }




    /**
     * <p>hashCode.</p>
     *
     * @return a int.
     */
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((actorIntegrationProfile == null) ? 0 : actorIntegrationProfile.hashCode());
        result = prime * result + ((integrationProfileOption == null) ? 0 : integrationProfileOption.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ActorIntegrationProfileOption other = (ActorIntegrationProfileOption) obj;
        if (actorIntegrationProfile == null)
        {
            if (other.actorIntegrationProfile != null)
                return false;
        }
        else if (!actorIntegrationProfile.equals(other.actorIntegrationProfile))
            return false;
        if (integrationProfileOption == null)
        {
            if (other.integrationProfileOption != null)
                return false;
        }
        else if (!integrationProfileOption.equals(other.integrationProfileOption))
            return false;
        return true;
    }


    /**
     * <p>listAllActorIntegrationProfileOption.</p>
     *
     * @return a {@link java.util.List} object.
     */
    @SuppressWarnings("unchecked")
    public static List<ActorIntegrationProfileOption> listAllActorIntegrationProfileOption()
    {
        EntityManager em = EntityManagerService.provideEntityManager();
        Query query = em.createQuery("SELECT aipo from ActorIntegrationProfileOption aipo order by aipo.actorIntegrationProfile.integrationProfile.keyword");
        return query.getResultList() ;
    }


    /**
     * <p>toString.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String toString()
    {
        if ( getActorIntegrationProfile() != null)
        {
            return getActorIntegrationProfile().getActor().getName() +" with " + getActorIntegrationProfile().getIntegrationProfile().getKeyword() + ( getIntegrationProfileOption() != null ? " and option " + getIntegrationProfileOption().getKeyword() : "" ) ;  
        }
        else return "Aip from Aipo is null";


    }
    /**
     * <p>compareTo.</p>
     *
     * @param o a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfileOption} object.
     * @return a int.
     */
    public int compareTo(ActorIntegrationProfileOption o) {
        return this.getActorIntegrationProfile().getIntegrationProfile().getKeyword().compareToIgnoreCase(o.getActorIntegrationProfile().getIntegrationProfile().getKeyword());
    }	

    /**
     * <p>findActorIntegrationProfileOption.</p>
     *
     * @param actorIntegrationProfileOption a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfileOption} object.
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfileOption} object.
     */
    public static ActorIntegrationProfileOption findActorIntegrationProfileOption(ActorIntegrationProfileOption actorIntegrationProfileOption){
        EntityManager  em = ( EntityManager) Component.getInstance("entityManager") ;
        Query q = em.createQuery("SELECT aipo " +
                "FROM ActorIntegrationProfileOption aipo " +
                "WHERE aipo.actorIntegrationProfile.actor.keyword = :inActorKeyword " +
                "AND aipo.actorIntegrationProfile.integrationProfile.keyword=:inIntegrationProfileKeyword " +
        "AND aipo.integrationProfileOption.keyword=:inAIPKeyword") ;
        q.setParameter("inActorKeyword", actorIntegrationProfileOption.getActorIntegrationProfile().getActor().getKeyword() ) ;
        q.setParameter("inIntegrationProfileKeyword", actorIntegrationProfileOption.getActorIntegrationProfile().getIntegrationProfile().getKeyword() ) ;
        q.setParameter("inAIPKeyword", actorIntegrationProfileOption.getIntegrationProfileOption().getKeyword() ) ;
        List<ActorIntegrationProfileOption> result = q.getResultList() ;
        if(result.size()>0)
            return result.get(0);
        return null;
    }

    /**
     * <p>mergeActorIntegrationProfileOption.</p>
     *
     * @param inActorIntegrationProfileOption a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfileOption} object.
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfileOption} object.
     */
    public static ActorIntegrationProfileOption mergeActorIntegrationProfileOption(ActorIntegrationProfileOption inActorIntegrationProfileOption){
        if(inActorIntegrationProfileOption!=null){
            EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
            ActorIntegrationProfileOption actorIntegrationProfileOption=ActorIntegrationProfileOption.findActorIntegrationProfileOption(inActorIntegrationProfileOption);
            if(actorIntegrationProfileOption==null){
                ActorIntegrationProfile actorIntegrationProfile=ActorIntegrationProfile.mergeActorIntegrationProfile(inActorIntegrationProfileOption.getActorIntegrationProfile());
                if(actorIntegrationProfile!=null){
                    //log.info("actorIntegrationProfile.id=="+actorIntegrationProfile.getId());
                    IntegrationProfileOption integrationProfileOption=IntegrationProfileOption.mergeIntegrationProfileOption(inActorIntegrationProfileOption.getIntegrationProfileOption());
                    if(integrationProfileOption!=null){
                        //log.info("integrationProfileOption.id=="+integrationProfileOption.getId());
                        actorIntegrationProfileOption=new ActorIntegrationProfileOption();
                        actorIntegrationProfileOption.setActorIntegrationProfile(actorIntegrationProfile);
                        actorIntegrationProfileOption.setIntegrationProfileOption(integrationProfileOption);
                        actorIntegrationProfileOption=entityManager.merge(actorIntegrationProfileOption);
                        //log.info("actorIntegrationProfileOption.id=="+actorIntegrationProfileOption.getId());
                    }
                }
            }
            return actorIntegrationProfileOption;
        }
        return null;
    }

    /**
     * <p>getActorIntegrationProfileOptionFiltered.</p>
     *
     * @param inActorIntegrationProfile a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfile} object.
     * @param inIntegrationProfile a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile} object.
     * @param inIntegrationProfileOption a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption} object.
     * @param inActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @return a {@link java.util.List} object.
     */
    public static List<ActorIntegrationProfileOption> getActorIntegrationProfileOptionFiltered(  ActorIntegrationProfile inActorIntegrationProfile    ,
            IntegrationProfile inIntegrationProfile              ,
            IntegrationProfileOption inIntegrationProfileOption  , 
            Actor inActor                                          ){
        ActorIntegrationProfileOption a = null ;
        return getClassFromAIPOFiltered("select tp FROM", a ,  inActorIntegrationProfile, inIntegrationProfile, inIntegrationProfileOption, inActor) ;
    }

    @SuppressWarnings("unchecked")
    private static <T> List<T> getClassFromAIPOFiltered( String selectString , T inClassToUse,  ActorIntegrationProfile inActorIntegrationProfile    ,
            IntegrationProfile inIntegrationProfile              ,
            IntegrationProfileOption inIntegrationProfileOption  , 
            Actor inActor                                          ){
        EntityManager em = EntityManagerService.provideEntityManager();
        Query query ;
        StringBuffer queryString = new StringBuffer() ;
        HashSet < String> prefixes = new HashSet<String>();
        HashSet < String> prefixesUsed = new HashSet<String>();
        HashMap<String , Object > mapOfParameters = new HashMap<String, Object>() ;

        if ( inActorIntegrationProfile != null )
        {
            DatabaseUtil.addANDorWHERE(queryString) ;
            prefixes.add("ActorIntegrationProfileOption tp") ;
            queryString.append( "tp.actorIntegrationProfile = :inActorIntegrationProfile " );
            mapOfParameters.put("inActorIntegrationProfile", inActorIntegrationProfile ) ;    
        }

        if ( inIntegrationProfile != null )
        {
            DatabaseUtil.addANDorWHERE(queryString) ;   
            prefixes.add("ActorIntegrationProfileOption tp") ;
            queryString.append( "tp.actorIntegrationProfile.integrationProfile = :inIntegrationProfile " );
            mapOfParameters.put("inIntegrationProfile", inIntegrationProfile  ) ;       
        }
        if ( inIntegrationProfileOption != null )
        {
            DatabaseUtil.addANDorWHERE(queryString) ;
            prefixes.add("ActorIntegrationProfileOption tp") ;
            queryString.append( "tp.integrationProfileOption = :inIntegrationProfileOption" );
            mapOfParameters.put("inIntegrationProfileOption", inIntegrationProfileOption ) ;

        }
        if ( inActor != null )
        {
            DatabaseUtil.addANDorWHERE(queryString) ;
            prefixes.add("ActorIntegrationProfileOption tp") ;
            queryString.append( "tp.actorIntegrationProfile.actor = :inActor" );
            mapOfParameters.put("inActor", inActor ) ;
        }

        List<String> listOfPrefixes = new ArrayList<String>( prefixes ) ;

        StringBuffer firstPartOfQuery = new StringBuffer() ;

        if ( queryString.toString().trim().length() == 0 ) return null ;



        for ( int i= 0 ; i< listOfPrefixes.size() ; i++  )
        {
            if ( i == 0 ) 
            {
                firstPartOfQuery.append( selectString + " " ) ;
            } 

            if ( !prefixesUsed.contains(listOfPrefixes.get(i)) )
            {
                firstPartOfQuery.append( listOfPrefixes.get(i) ) ;

                if ( prefixesUsed.size() > 0 ) firstPartOfQuery.append(" , "  ) ;
                prefixesUsed.add( listOfPrefixes.get(i) ) ;
            }

        }

        queryString.insert(0, firstPartOfQuery ) ;

        if ( queryString.toString().trim().length() == 0 ) return null ;

        query = em.createQuery(queryString.toString()) ;


        List<String> listOfParameters =  new ArrayList<String>( mapOfParameters.keySet() ) ;
        for ( String param : listOfParameters )
        {
            query.setParameter( param , mapOfParameters.get(param) ) ;
        }

        return  (List<T>)query.getResultList() ; 

    }



}


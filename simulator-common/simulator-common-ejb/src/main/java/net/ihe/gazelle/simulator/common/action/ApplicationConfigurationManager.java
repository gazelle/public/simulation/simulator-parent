/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.action;

import java.io.Serializable;

import javax.ejb.Remove;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  <b>Class Description :  </b>ApplicationConfigurationManager<br><br>
 * This class contains the methods which are called in the footer modal panels
 *
 * @author                	Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version                	1.0 - 2010, October 15th
 */

@Name("applicationConfigurationManager")
@AutoCreate
@Scope(ScopeType.APPLICATION)
@GenerateInterface("ApplicationConfigurationManagerLocal")
public class ApplicationConfigurationManager implements Serializable, ApplicationConfigurationManagerLocal{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7817765881624341837L;
	private static Logger log = LoggerFactory.getLogger(ApplicationConfigurationManager.class);
	
	private String releaseNoteUrl;
	private String documentation;
	private Boolean worksWithoutCas;
	private String applicationUrl;
	private String casUrl;
	private Boolean ipLogin;
	private String ipLoginAdmin;
	
	/**
	 * <p>destroy.</p>
	 */
	@Remove
	@Destroy
	public void destroy() {

	}

	/**
	 * <p>resetApplicationConfiguration.</p>
	 */
	public void resetApplicationConfiguration()
	{
		releaseNoteUrl = null;
		documentation = null;
		worksWithoutCas = null;
		casUrl = null;
		applicationUrl = null;
		ipLogin = null;
		ipLoginAdmin = null;
	}
	

	/**
	 * <p>Getter for the field <code>releaseNoteUrl</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getReleaseNoteUrl() {
		if (releaseNoteUrl == null)
		{
			releaseNoteUrl = ApplicationConfiguration.getValueOfVariable("application_release_notes_url");
			if (releaseNoteUrl == null)
			{
				log.error("application_release_notes_url variable is missing in app_configuration table");
			}
		}
		return releaseNoteUrl;
	}

	/**
	 * <p>Getter for the field <code>documentation</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDocumentation(){
	    if (this.documentation == null){
	        this.documentation = ApplicationConfiguration.getValueOfVariable("documentation_url");
	    }
	    if (this.documentation == null){
	    	log.error("application_documentation variable is missing in app_configuration table");
	        this.documentation = "http://gazelle.ihe.net";
	    }
	    return this.documentation;
	}

	/**
	 * <p>isWorksWithoutCas.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isWorksWithoutCas() {
		if (worksWithoutCas == null)
		{
			String booleanAsString = ApplicationConfiguration.getValueOfVariable("application_works_without_cas");
			if (booleanAsString == null)
			{
				this.worksWithoutCas = false;
			}
			else
			{
				this.worksWithoutCas = Boolean.valueOf(booleanAsString);
			}
		}
		return worksWithoutCas;
	}
	
	/**
	 * <p>Getter for the field <code>applicationUrl</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getApplicationUrl() {
		if (applicationUrl == null){
			this.applicationUrl = ApplicationConfiguration.getValueOfVariable("application_url");
		}
		return applicationUrl;
	}

	/**
	 * <p>Getter for the field <code>casUrl</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCasUrl() {
		if (this.casUrl == null){
			this.casUrl = ApplicationConfiguration.getValueOfVariable("cas_url");
		}
		return casUrl;
	}

	/**
	 * <p>instance.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.action.ApplicationConfigurationManager} object.
	 */
	public static ApplicationConfigurationManager instance(){
		ApplicationConfigurationManager configurationManager = (ApplicationConfigurationManager) Component
				.getInstance("applicationConfigurationManager");
		return configurationManager;
	}

	/**
	 * <p>Getter for the field <code>ipLogin</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getIpLogin() {
		if (ipLogin == null)
		{
			String booleanAsString = ApplicationConfiguration.getValueOfVariable("ip_login");
			if (booleanAsString == null)
			{
				this.ipLogin = false;
			}
			else
			{
				this.ipLogin = Boolean.valueOf(booleanAsString);
			}
		}
		return ipLogin;
	}

	/**
	 * <p>Getter for the field <code>ipLoginAdmin</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getIpLoginAdmin() {
		if (this.ipLoginAdmin == null){
			this.ipLoginAdmin = ApplicationConfiguration.getValueOfVariable("ip_login_admin");
		}
		return ipLoginAdmin;
	}
	
	/**
	 * <p>isUserAllowedAsAdmin.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isUserAllowedAsAdmin(){
		return Identity.instance().isLoggedIn() && Identity.instance().hasRole("admin_role");
	}
}

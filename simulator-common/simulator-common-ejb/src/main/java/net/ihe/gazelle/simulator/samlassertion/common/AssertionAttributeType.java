package net.ihe.gazelle.simulator.samlassertion.common;

public interface AssertionAttributeType {

    /**
     * Gets the friendly name.
     *
     * @return the friendly name
     */
    String getFriendlyName();

    /**
     * Gets the name.
     *
     * @return the name
     */
    String getName();

    /**
     * Gets the type.
     *
     * @return the type
     */
    String getType();

    /**
     * Gets the xsi type.
     *
     * @return the xsi type
     */
    String getXsiType();

}

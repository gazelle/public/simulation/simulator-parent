/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.simulator.common.configuration.DICOM.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



import javax.validation.constraints.NotNull;

import org.jboss.seam.annotations.Name;


/**
 * Class doing nothing more than the abstract DicomConfiguration but allowing to differentiate it from the DicomSCUConfigurationClass
 *
 * @author jbmeyer
 * @version $Id: $Id
 */
@Entity
@Name("sopClass")
@Table(name = "cfg_sop_class", schema = "public")
@SequenceGenerator(name = "cfg_sop_class_sequence", sequenceName = "cfg_sop_class_id_seq", allocationSize=1)
public class SopClass implements Serializable
{


	/**
	 * 
	 */
	private static final long serialVersionUID = 114417097017L;

	/**
	 * 
	 */
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="cfg_sop_class_sequence")
	private Integer id ;

	@Column(name = "keyword",nullable=false)
	private String keyword  ;

	@Column(name = "name" )
	private String  name ;


	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId()
	{
		return id;
	}

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id)
	{
		this.id = id;
	}

	/**
	 * <p>Getter for the field <code>keyword</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getKeyword()
	{
		return keyword;
	}

	/**
	 * <p>Setter for the field <code>keyword</code>.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 */
	public void setKeyword(String keyword)
	{
		this.keyword = keyword;
	}


	/**
	 * <p>Getter for the field <code>name</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * <p>Setter for the field <code>name</code>.</p>
	 *
	 * @param name a {@link java.lang.String} object.
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * <p>Constructor for SopClass.</p>
	 */
	public SopClass()
	{

	}

	/**
	 * <p>Constructor for SopClass.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @param name a {@link java.lang.String} object.
	 */
	public SopClass( String keyword , String name ) 
	{
		this.keyword = keyword ;
		this.name    = name    ;
	}


	/**
	 * <p>hashCode.</p>
	 *
	 * @return a int.
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SopClass other = (SopClass) obj;
		if (keyword == null) {
			if (other.keyword != null)
				return false;
		} else if (!keyword.equals(other.keyword))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


}

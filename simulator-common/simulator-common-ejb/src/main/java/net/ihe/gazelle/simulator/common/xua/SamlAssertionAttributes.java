package net.ihe.gazelle.simulator.common.xua;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * The Class SamlAssertionAttributes.<br>
 * Handles the attributes specific for epSOS
 *
 * Use instead {@link net.ihe.gazelle.simulator.samlassertion.epsos.EpSOSAssertionAttributes}
 *
 * @author aberge
 * @version $Id: $Id
 */

@Deprecated
public class SamlAssertionAttributes implements Serializable{

	private static final String NS_XS = "http://www.w3.org/2001/XMLSchema";
	private static final String NS_XSI = "http://www.w3.org/2001/XMLSchema-instance";

	/** The Constant NS_SAML2. */
	private static final String NS_SAML2 = "urn:oasis:names:tc:SAML:2.0:assertion";

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4735418842276523676L;

	private HashMap<SamlAssertionAttributeType, List<String>> delegate;

	/**
	 * <p>Constructor for SamlAssertionAttributes.</p>
	 *
	 * @param isTRC a boolean.
	 */
	public SamlAssertionAttributes(boolean isTRC) {
		super();
		delegate = new HashMap<SamlAssertionAttributeType, List<String>>();
		if (isTRC) {
			setDefaultsTRC();
		} else {
			setDefaults();
		}
	}

	/**
	 * <p>Constructor for SamlAssertionAttributes.</p>
	 */
	public SamlAssertionAttributes() {
		super();
		delegate = new HashMap<SamlAssertionAttributeType, List<String>>();

		setDefaults();
	}

	private void setDefaultsTRC() {
		// Default values for simulators not setting values
		setXSPASubjectTRC("Patient ID");
		setXSPAPurposeOfUse("TREATMENT");
	}

	private void setDefaults() {
		// Default values for simulators not setting values
		setXSPASubject("Dr. Muller");
		setXSPAOrganization("Vienna AKH");
		setXSPAOrganizationId("urn:oid:1.2.3.4.5.6.7");
		setEpSOSHealthcareFacilityType("Resident Physician");
		setXSPARole("medical doctor");
		setXSPALocality("vienna-akh");
		setXSPAPurposeOfUse("TREATMENT");
		setHITSPClinicalSpeciality("UNKNOWN");

		getXSPAPermissionsHL7().add("urn:oasis:names:tc:xspa:1.0:subject:hl7:permission:PRD-006");
	}

	/**
	 * Gets the value.
	 *
	 * @param type
	 *            the type
	 * @return the value
	 */
	public String getValue(SamlAssertionAttributeType type) {
		if (delegate.get(type) == null) {
			return null;
		} else {
			if (delegate.get(type).size() == 0) {
				return null;
			} else {
				return delegate.get(type).get(0);
			}
		}
	}

	/**
	 * Gets the values.
	 *
	 * @param type
	 *            the type
	 * @return the values
	 */
	public List<String> getValues(SamlAssertionAttributeType type) {
		checkType(type);
		return delegate.get(type);
	}

	/**
	 * Sets the value.
	 *
	 * @param type
	 *            the type
	 * @param value
	 *            the value
	 */
	public void setValue(SamlAssertionAttributeType type, String value) {
		checkType(type);
		if (delegate.get(type).size() == 0) {
			delegate.get(type).add(value);
		} else {
			delegate.get(type).set(0, value);
		}
	}

	/**
	 * Check type.<br>
	 * Avoid NPE
	 * 
	 * @param type
	 *            the type
	 */
	private void checkType(SamlAssertionAttributeType type) {
		if (delegate.get(type) == null) {
			delegate.put(type, new ArrayList<String>(1));
		}
	}

	/**
	 * Gets the XSPA permissions HL7.
	 *
	 * @return the XSPA permissions HL7
	 */
	public List<String> getXSPAPermissionsHL7() {
		return getValues(SamlAssertionAttributeType.XSPAPermissionsHL7);
	}

	/**
	 * Gets the xSPA subject.
	 *
	 * @return the xSPA subject
	 */
	public String getXSPASubject() {
		return getValue(SamlAssertionAttributeType.XSPASubject);
	}

	/**
	 * <p>getXSPASubjectTRC.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getXSPASubjectTRC() {
		return getValue(SamlAssertionAttributeType.XSPASubjectTRC);
	}

	/**
	 * Sets the xSPA subject.
	 *
	 * @param xSPASubject
	 *            the new xSPA subject
	 */
	public void setXSPASubject(String xSPASubject) {
		setValue(SamlAssertionAttributeType.XSPASubject, xSPASubject);
	}

	/**
	 * <p>setXSPASubjectTRC.</p>
	 *
	 * @param xSPASubjectTRC a {@link java.lang.String} object.
	 */
	public void setXSPASubjectTRC(String xSPASubjectTRC) {
		setValue(SamlAssertionAttributeType.XSPASubjectTRC, xSPASubjectTRC);
	}

	/**
	 * Gets the xSPA role.
	 *
	 * @return the xSPA role
	 */
	public String getXSPARole() {
		return getValue(SamlAssertionAttributeType.XSPARole);
	}

	/**
	 * Sets the xSPA role.
	 *
	 * @param xSPARole
	 *            the new xSPA role
	 */
	public void setXSPARole(String xSPARole) {
		setValue(SamlAssertionAttributeType.XSPARole, xSPARole);
	}

	/**
	 * Gets the hITSP clinical speciality.
	 *
	 * @return the hITSP clinical speciality
	 */
	public String getHITSPClinicalSpeciality() {
		return getValue(SamlAssertionAttributeType.HITSPClinicalSpeciality);
	}

	/**
	 * Sets the hITSP clinical speciality.
	 *
	 * @param hITSPClinicalSpeciality
	 *            the new hITSP clinical speciality
	 */
	public void setHITSPClinicalSpeciality(String hITSPClinicalSpeciality) {
		setValue(SamlAssertionAttributeType.HITSPClinicalSpeciality, hITSPClinicalSpeciality);
	}

	/**
	 * Gets the xSPA organization.
	 *
	 * @return the xSPA organization
	 */
	public String getXSPAOrganization() {
		return getValue(SamlAssertionAttributeType.XSPAOrganization);
	}

	/**
	 * Sets the xSPA organization.
	 *
	 * @param xSPAOrganization
	 *            the new xSPA organization
	 */
	public void setXSPAOrganization(String xSPAOrganization) {
		setValue(SamlAssertionAttributeType.XSPAOrganization, xSPAOrganization);
	}

	/**
	 * Gets the xSPA organization id.
	 *
	 * @return the xSPA organization id
	 */
	public String getXSPAOrganizationId() {
		return getValue(SamlAssertionAttributeType.XSPAOrganizationId);
	}

	/**
	 * Sets the xSPA organization id.
	 *
	 * @param xSPAOrganizationId
	 *            the new xSPA organization id
	 */
	public void setXSPAOrganizationId(String xSPAOrganizationId) {
		setValue(SamlAssertionAttributeType.XSPAOrganizationId, xSPAOrganizationId);
	}

	/**
	 * Gets the ep sos healthcare facility type.
	 *
	 * @return the ep sos healthcare facility type
	 */
	public String getEpSOSHealthcareFacilityType() {
		return getValue(SamlAssertionAttributeType.epSOSHealthcareFacilityType);
	}

	/**
	 * Sets the ep sos healthcare facility type.
	 *
	 * @param epSOSHealthcareFacilityType
	 *            the new ep sos healthcare facility type
	 */
	public void setEpSOSHealthcareFacilityType(String epSOSHealthcareFacilityType) {
		setValue(SamlAssertionAttributeType.epSOSHealthcareFacilityType, epSOSHealthcareFacilityType);
	}

	/**
	 * Gets the xSPA purpose of use.
	 *
	 * @return the xSPA purpose of use
	 */
	public String getXSPAPurposeOfUse() {
		return getValue(SamlAssertionAttributeType.XSPAPurposeOfUse);
	}

	/**
	 * Sets the xSPA purpose of use.
	 *
	 * @param xSPAPurposeOfUse
	 *            the new xSPA purpose of use
	 */
	public void setXSPAPurposeOfUse(String xSPAPurposeOfUse) {
		setValue(SamlAssertionAttributeType.XSPAPurposeOfUse, xSPAPurposeOfUse);
	}

	/**
	 * Gets the xSPA locality.
	 *
	 * @return the xSPA locality
	 */
	public String getXSPALocality() {
		return getValue(SamlAssertionAttributeType.XSPALocality);
	}

	/**
	 * Sets the xSPA locality.
	 *
	 * @param xSPALocality
	 *            the new xSPA locality
	 */
	public void setXSPALocality(String xSPALocality) {
		setValue(SamlAssertionAttributeType.XSPALocality, xSPALocality);
	}

	/**
	 * Creates the element.
	 * 
	 * @param document
	 * 
	 * @return the element
	 */
	Element getDOMElement(String prefix) throws SignatureException {
		DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
		fabrique.setNamespaceAware(true);
		DocumentBuilder constructeur;
		try {
			constructeur = fabrique.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new SignatureException(e);
		}
		Document document = constructeur.newDocument();

		Element attributeStatement = document.createElementNS(NS_SAML2, prefix + "AttributeStatement");

		Set<java.util.Map.Entry<SamlAssertionAttributeType, List<String>>> entrySet = delegate.entrySet();
		for (java.util.Map.Entry<SamlAssertionAttributeType, List<String>> entry : entrySet) {
			SamlAssertionAttributeType key = entry.getKey();
			List<String> values = entry.getValue();

			Element attribute = document.createElementNS(NS_SAML2, prefix + "Attribute");
			attributeStatement.appendChild(attribute);

			attribute.setAttribute("FriendlyName", key.getFriendlyName());
			attribute.setAttribute("Name", key.getName());
			attribute.setAttribute("NameFormat", key.getType());

			for (String value : values) {
				Element valueElement = document.createElementNS(NS_SAML2, prefix + "AttributeValue");
				valueElement.setAttribute("xmlns:xs", NS_XS);
				valueElement.setAttribute("xmlns:xsi", NS_XSI);
				valueElement.setAttribute("xsi:type", key.getXsiType());
				attribute.appendChild(valueElement);
				valueElement.setTextContent(value);
			}

		}

		return attributeStatement;
	}

}

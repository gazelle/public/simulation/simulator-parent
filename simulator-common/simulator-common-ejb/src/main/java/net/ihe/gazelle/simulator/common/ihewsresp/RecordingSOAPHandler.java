package net.ihe.gazelle.simulator.common.ihewsresp;

import net.ihe.gazelle.hql.providers.EntityManagerProvider;
import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.services.GenericServiceLoader;
import net.ihe.gazelle.simulator.common.action.SimulatorEntityManagerProvider;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapMessageConverter;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstanceQuery;

import javax.persistence.EntityManager;
import javax.xml.namespace.QName;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.util.List;
import java.util.Set;

public class RecordingSOAPHandler implements SOAPHandler<SOAPMessageContext> {

    public static final String TRANSACTION_ID = "TRANSACTION_ID";
    public static final String PAYLOAD = "PAYLOAD";

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        recordMessage(context);
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean handleFault(SOAPMessageContext context) {
        recordMessage(context);
        return false;
    }

    /**
     * Handle the recording of the message by either copying the payload to the response object or passing the request payload in the context for it
     * to be initialized.
     *
     * @param context the message context
     */
    private void recordMessage(SOAPMessageContext context) {
        final Integer transactionId = (Integer) context.get(TRANSACTION_ID);
        if (transactionId != null) {
            final byte[] payload = SoapMessageConverter.soapMessageToByteArray(context.getMessage());

            setEntityManager();
            EntityManager entityManager = HibernateActionPerformer.ENTITYMANGER_THREADLOCAL.get();
            entityManager.getTransaction().begin();
            TransactionInstanceQuery query = new TransactionInstanceQuery(entityManager);
            query.id().eq(transactionId);
            TransactionInstance transactionInstance = query.getUniqueResult();
            transactionInstance.getResponse().setPayload(payload);
            entityManager.merge(transactionInstance);
            entityManager.getTransaction().commit();
            entityManager.close();
            HibernateActionPerformer.ENTITYMANGER_THREADLOCAL.remove();
        } else {
            context.put(PAYLOAD, SoapMessageConverter.soapMessageToByteArray(context.getMessage()));
            context.setScope(PAYLOAD, MessageContext.Scope.APPLICATION);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close(MessageContext context) {
    }

    /**
     * Set an {@link EntityManager} that is not retrieved from Seam for the {@link HibernateActionPerformer} to use.
     */
    private void setEntityManager() {
        HibernateActionPerformer.ENTITYMANGER_THREADLOCAL.remove();
        List<EntityManagerProvider> services = GenericServiceLoader.getServices(EntityManagerProvider.class);
        for (EntityManagerProvider service : services) {
            if (service instanceof SimulatorEntityManagerProvider) {
                HibernateActionPerformer.ENTITYMANGER_THREADLOCAL.set(service.provideEntityManager());
                HibernateActionPerformer.ENTITYMANGER_THREADLOCAL.get().getTransaction().isActive();
            }
        }
    }
}

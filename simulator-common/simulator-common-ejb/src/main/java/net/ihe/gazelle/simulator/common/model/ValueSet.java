package net.ihe.gazelle.simulator.common.model;

import net.ihe.gazelle.simulator.common.action.ValueSetProvider;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <b>Class Description : </b>ValueSet<br>
 * <br>
 *
 * OrderManager application uses the SVSSimulator to retrieve appropriate coded values. This class describes the value set used within the application.
 *
 * ValueSet possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id in the database</li>
 * <li><b>valueSetName</b> name of the value set used in the java code to retrieved it</li>
 * <li><b>valueSetOID</b> OID of the value set in the SVS repository</li>
 * </ul>
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project

 * @version 1.0 - 2011, November 17th
 */

@Entity
@Name("valueSet")
@XmlRootElement(name = "ValueSet")
@XmlAccessorType(XmlAccessType.FIELD)
@Table(name = "cmn_value_set", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "cmn_value_set_sequence", sequenceName = "cmn_value_set_id_seq", allocationSize = 1)
public class ValueSet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger log = LoggerFactory.getLogger(ValueSet.class);

	@Id
	@NotNull
	@GeneratedValue(generator = "cmn_value_set_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", nullable = false, unique = true)
	@XmlTransient
	private Integer id;

	@Column(name = "value_set_keyword")
	@XmlAttribute(name = "keyword")
	private String valueSetKeyword;

	@Column(name = "value_set_name")
	@XmlAttribute(name = "name")
	private String valueSetName;

	@XmlAttribute(name = "oid")
	@Column(name = "value_set_oid")
	private String valueSetOID;

	@XmlAttribute(name = "usage")
	@Column(name = "usage")
	private String usage;

	/**
	 * null = never checked
	 */
	@XmlAttribute(name = "accessible")
	@Column(name = "accessible")
	private Boolean accessible;

	@XmlAttribute(name = "lastCheck")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_check")
	private Date lastCheck;

	/**
	 * <p>Constructor for ValueSet.</p>
	 */
	public ValueSet() {

	}

	/**
	 * Returns a list of SVS concept from a given value set
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 */
	public List<SelectItem> getListOfConcepts(String keyword) {
		return ValueSetProvider.getInstance().getListOfConcepts(keyword);
	}

	/**
	 * For a given code from a given value set, returns the display name in the selected language
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @param code a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getDisplayNameForCode(String keyword, String code) {
		return ValueSetProvider.getInstance().getDisplayNameForCode(keyword, code);
	}

	/**
	 * Returns a code randomly extracted from the given value set
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getRandomCodeFromValueSet(String keyword) {
		return ValueSetProvider.getInstance().getRandomCodeFromValueSet(keyword);
	}

	/**
	 * <p>getValueSetIdForKeyword.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getValueSetIdForKeyword(String keyword) {
		ValueSetQuery query = new ValueSetQuery();
		query.valueSetKeyword().eq(keyword);
		try {
			return query.getUniqueResult().getValueSetOID();
		} catch (Exception e) {
			log.warn("No value set defined for keyword " + keyword);
			return null;
		}
	}

	/**
	 * Returns the entire concept for a given code
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @param code a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.model.Concept} object.
	 */
	public static Concept getConceptForCode(String keyword, String code) {
		return ValueSetProvider.getInstance().getConceptForCode(keyword, code, null);
	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * <p>Getter for the field <code>valueSetName</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getValueSetName() {
		return valueSetName;
	}

	/**
	 * <p>Setter for the field <code>valueSetName</code>.</p>
	 *
	 * @param valueSetName a {@link java.lang.String} object.
	 */
	public void setValueSetName(String valueSetName) {
		this.valueSetName = valueSetName;
	}

	/**
	 * <p>Getter for the field <code>valueSetOID</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getValueSetOID() {
		return valueSetOID;
	}

	/**
	 * <p>Setter for the field <code>valueSetOID</code>.</p>
	 *
	 * @param valueSetOID a {@link java.lang.String} object.
	 */
	public void setValueSetOID(String valueSetOID) {
		this.valueSetOID = valueSetOID;
	}

	/**
	 * <p>Getter for the field <code>usage</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUsage() {
		return usage;
	}

	/**
	 * <p>Setter for the field <code>usage</code>.</p>
	 *
	 * @param usage a {@link java.lang.String} object.
	 */
	public void setUsage(String usage) {
		this.usage = usage;
	}

	/**
	 * <p>Getter for the field <code>valueSetKeyword</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getValueSetKeyword() {
		return valueSetKeyword;
	}

	/**
	 * <p>Setter for the field <code>valueSetKeyword</code>.</p>
	 *
	 * @param valueSetKeyword a {@link java.lang.String} object.
	 */
	public void setValueSetKeyword(String valueSetKeyword) {
		this.valueSetKeyword = valueSetKeyword;
	}

	/**
	 * <p>Getter for the field <code>accessible</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getAccessible() {
		return accessible;
	}

	/**
	 * <p>Setter for the field <code>accessible</code>.</p>
	 *
	 * @param accessible a {@link java.lang.Boolean} object.
	 */
	public void setAccessible(Boolean accessible) {
		this.accessible = accessible;
	}

	/**
	 * <p>Getter for the field <code>lastCheck</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getLastCheck() {
		return lastCheck;
	}

	/**
	 * <p>Setter for the field <code>lastCheck</code>.</p>
	 *
	 * @param lastCheck a {@link java.util.Date} object.
	 */
	public void setLastCheck(Date lastCheck) {
		this.lastCheck = lastCheck;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof ValueSet)) {
			return false;
		}

		ValueSet valueSet = (ValueSet) o;

		if (valueSetKeyword != null ? !valueSetKeyword.equals(valueSet.valueSetKeyword) : valueSet.valueSetKeyword != null) {
			return false;
		}
		if (valueSetName != null ? !valueSetName.equals(valueSet.valueSetName) : valueSet.valueSetName != null) {
			return false;
		}
		if (valueSetOID != null ? !valueSetOID.equals(valueSet.valueSetOID) : valueSet.valueSetOID != null) {
			return false;
		}
		if (usage != null ? !usage.equals(valueSet.usage) : valueSet.usage != null) {
			return false;
		}
		if (accessible != null ? !accessible.equals(valueSet.accessible) : valueSet.accessible != null) {
			return false;
		}
		return lastCheck != null ? lastCheck.equals(valueSet.lastCheck) : valueSet.lastCheck == null;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		int result = valueSetKeyword != null ? valueSetKeyword.hashCode() : 0;
		result = 31 * result + (valueSetName != null ? valueSetName.hashCode() : 0);
		result = 31 * result + (valueSetOID != null ? valueSetOID.hashCode() : 0);
		result = 31 * result + (usage != null ? usage.hashCode() : 0);
		result = 31 * result + (accessible != null ? accessible.hashCode() : 0);
		result = 31 * result + (lastCheck != null ? lastCheck.hashCode() : 0);
		return result;
	}
}

package net.ihe.gazelle.simulator.common.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

/**
 * <p>ContextualInformation class.</p>
 *
 * @author abderrazek boufahja
 * @version $Id: $Id
 */
@Entity
@Name("contextualInformation")
@Table(name = "gs_contextual_information", schema = "public")
@SequenceGenerator(name = "gs_contextual_information_sequence", sequenceName = "gs_contextual_information_id_seq", allocationSize = 1)
public class ContextualInformation implements Serializable, Comparable<ContextualInformation>{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gs_contextual_information_sequence")
	private Integer id;

	@Column(name = "label",nullable=false)
	private String label;

	@ManyToOne( cascade = { CascadeType.ALL })
    @JoinColumn(name= "path",nullable=false)
	private Path path;
	
	@Column(name = "value",nullable=true)
	private String value;
	
	@OneToMany(mappedBy = "contextualInformation", fetch = FetchType.LAZY)
    private List<ContextualInformationInstance> contextualInformationInstances;


	/**
	 * <p>Constructor for ContextualInformation.</p>
	 */
	public ContextualInformation(){

	}

	/**
	 * <p>Constructor for ContextualInformation.</p>
	 *
	 * @param label a {@link java.lang.String} object.
	 * @param path a {@link net.ihe.gazelle.simulator.common.model.Path} object.
	 */
	public ContextualInformation(String label,Path path){
		this.label=label;
		this.path=path;
	}

	/**
	 * <p>Constructor for ContextualInformation.</p>
	 *
	 * @param inContextualInformation a {@link net.ihe.gazelle.simulator.common.model.ContextualInformation} object.
	 */
	public ContextualInformation(ContextualInformation inContextualInformation){
		if(inContextualInformation.getLabel()!=null) this.label = inContextualInformation.getLabel();
		if(inContextualInformation.getPath() !=null) this.path  = inContextualInformation.getPath();
		if(inContextualInformation.getValue() !=null) this.value  = inContextualInformation.getValue();
	}
	
	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}


	/**
	 * <p>Getter for the field <code>contextualInformationInstances</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<ContextualInformationInstance> getContextualInformationInstances() {
        return contextualInformationInstances;
    }

    /**
     * <p>Setter for the field <code>contextualInformationInstances</code>.</p>
     *
     * @param contextualInformationInstances a {@link java.util.List} object.
     */
    public void setContextualInformationInstances(
            List<ContextualInformationInstance> contextualInformationInstances) {
        this.contextualInformationInstances = contextualInformationInstances;
    }

    /**
     * <p>Getter for the field <code>label</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
		return label;
	}

	/**
	 * <p>Setter for the field <code>label</code>.</p>
	 *
	 * @param label a {@link java.lang.String} object.
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * <p>Getter for the field <code>path</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.model.Path} object.
	 */
	public Path getPath() {
		return path;
	}

	/**
	 * <p>Setter for the field <code>path</code>.</p>
	 *
	 * @param path a {@link net.ihe.gazelle.simulator.common.model.Path} object.
	 */
	public void setPath(Path path) {
		this.path = path;
	}

	/**
	 * <p>compareTo.</p>
	 *
	 * @param o a {@link net.ihe.gazelle.simulator.common.model.ContextualInformation} object.
	 * @return a int.
	 */
	public int compareTo(ContextualInformation o) {
		return label.compareTo(o.label);
	}

	/**
	 * <p>Getter for the field <code>value</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <p>Setter for the field <code>value</code>.</p>
	 *
	 * @param value a {@link java.lang.String} object.
	 */
	public void setValue(String value) {
		this.value = value;
	}

	//------ hashcode and equals ----------------------------------------
	
	/**
	 * <p>hashCode.</p>
	 *
	 * @return a int.
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContextualInformation other = (ContextualInformation) obj;
		if (label == null) {
			if (other.label != null)
				return false;
		} else if (!label.equals(other.label))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	//------ to string ----------------------------------------
	
	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString() {
		return "ContextualInformation [id=" + id + ", label=" + label
				+ ", path=" + path + ", value=" + value + "]";
	}
	
	/**
	 * <p>getContextualInformationByCI.</p>
	 *
	 * @param ci a {@link net.ihe.gazelle.simulator.common.model.ContextualInformation} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.model.ContextualInformation} object.
	 */
	public static ContextualInformation getContextualInformationByCI(ContextualInformation ci){
	    if (ci == null) return null;
	    EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
        Query query=entityManager.createQuery("SELECT ci FROM ContextualInformation ci WHERE ci.label=:lab AND ci.value=:value");
        query.setParameter("lab", ci.label);
        query.setParameter("value", ci.value);
        List<ContextualInformation> list=query.getResultList();
        if (list != null){
            if(list.size()>0){
                for (ContextualInformation contextualInformation : list) {
                    if (contextualInformation.equals(ci)){
                        return contextualInformation;
                    }
                }
            }
        }
        if (ci.path != null){
            Path path = Path.getPathByKeyword(ci.getPath().getKeyword());
            if (path != null){
                ci.setPath(path);
            }
        }
        return ci;
	}
	
	
}

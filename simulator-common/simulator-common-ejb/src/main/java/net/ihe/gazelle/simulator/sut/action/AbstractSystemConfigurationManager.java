package net.ihe.gazelle.simulator.sut.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.simulator.sut.model.Usage;
import org.hibernate.exception.ConstraintViolationException;
import net.ihe.gazelle.ssov7.gum.client.interlay.filter.UserValueFormatter;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.util.List;

/**
 * <p>Abstract AbstractSystemConfigurationManager class.</p>
 *
 * @author                	Abderrazek Boufahja
 * @version $Id: $Id
 */
public abstract class AbstractSystemConfigurationManager<T extends SystemConfiguration> implements AbstractSystemConfigurationLocal<T>, QueryModifier<T> {


	private static final long serialVersionUID = 5935336351088175578L;
	private static Logger log = LoggerFactory
			.getLogger(AbstractSystemConfigurationManager.class);
	
	protected FilterDataModel<T> availableSystemConfigurations;
	protected T selectedSystemConfiguration;
	protected boolean displayListPanel = true;
	protected boolean displayEditPanel = false;
    protected Filter<T> filter;
    protected Usage filteredUsage;

	
	/**
	 * <p>Getter for the field <code>availableSystemConfigurations</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
	 */
	public FilterDataModel<T> getAvailableSystemConfigurations() {
		return new FilterDataModel<T>(getFilter()) {
            @Override
            protected Object getId(T t) {
                return t.getId();
            }
        };
	}

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<T> getFilter(){
        if (filter == null){
            filter = new Filter<T>(getHQLCriterionsForFilter());
			filter.getFormatters().put("owner", new UserValueFormatter(this.filter, "owner"));
        }
        return filter;
    }

    /**
     * <p>getHQLCriterionsForFilter.</p>
     *
     * @return a {@link net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter} object.
     */
    protected abstract HQLCriterionsForFilter<T> getHQLCriterionsForFilter();

    /**
     * <p>reset.</p>
     */
    public void reset(){
        getFilter().clear();
        filteredUsage = null;
    }

    /**
     * <p>Getter for the field <code>filteredUsage</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.sut.model.Usage} object.
     */
    public Usage getFilteredUsage() {
        return filteredUsage;
    }

    /**
     * <p>Setter for the field <code>filteredUsage</code>.</p>
     *
     * @param filteredUsage a {@link net.ihe.gazelle.simulator.sut.model.Usage} object.
     */
    public void setFilteredUsage(Usage filteredUsage) {
        this.filteredUsage = filteredUsage;
    }

    /**
     * <p>Getter for the field <code>selectedSystemConfiguration</code>.</p>
     *
     * @return a T object.
     */
    public T getSelectedSystemConfiguration() {
		return selectedSystemConfiguration;
	}

	/**
	 * <p>Setter for the field <code>selectedSystemConfiguration</code>.</p>
	 *
	 * @param selectedSystemConfiguration a T object.
	 */
	public void setSelectedSystemConfiguration(
			T selectedSystemConfiguration) {
		this.selectedSystemConfiguration = selectedSystemConfiguration;
	}

	/**
	 * <p>isDisplayListPanel.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isDisplayListPanel() {
		return displayListPanel;
	}

	/** {@inheritDoc} */
	public void setDisplayListPanel(boolean displayListPanel) {
		this.displayListPanel = displayListPanel;
	}

	/**
	 * <p>isDisplayEditPanel.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isDisplayEditPanel() {
		return displayEditPanel;
	}

	/** {@inheritDoc} */
	public void setDisplayEditPanel(boolean displayEditPanel) {
		this.displayEditPanel = displayEditPanel;
	}
	

	/**
	 * <p>copySelectedConfiguration.</p>
	 *
	 * @param inConfiguration a T object.
	 */
	public abstract void copySelectedConfiguration(T inConfiguration);
	
	/**
	 * <p>initialize.</p>
	 */
	@Create
	public void initialize(){
		displayListPanel = true;
		displayEditPanel = false;
	}

	/**
	 * <p>destroy.</p>
	 */
	@Destroy
	@Remove
	public void destroy() {

	}
	
	/**
	 * <p>addConfiguration.</p>
	 */
	public abstract void addConfiguration();

	/**
	 * <p>displayAllConfigurations.</p>
	 */
	public void displayAllConfigurations() {
		displayEditPanel = false;
		displayListPanel = true;
		selectedSystemConfiguration = null;
	}

	/**
	 * <p>editSelectedConfiguration.</p>
	 */
	public void editSelectedConfiguration() {
		displayEditPanel = true;
		displayListPanel = false;
	}

	/**
	 * <p>editSelectedConfiguration.</p>
	 *
	 * @param inConfiguration a T object.
	 */
	public void editSelectedConfiguration(T inConfiguration) {
		selectedSystemConfiguration = inConfiguration;
		editSelectedConfiguration();
	}

	/**
	 * <p>saveCurrentConfiguration.</p>
	 */
	public void saveCurrentConfiguration(){
		if (this.selectedSystemConfiguration == null){
			return;
		}
		EntityManager em = EntityManagerService.provideEntityManager();
		if (Identity.instance().isLoggedIn()){
			this.selectedSystemConfiguration.setOwner(Identity.instance().getCredentials().getUsername());
		}
		try{
			this.selectedSystemConfiguration = em.merge(this.selectedSystemConfiguration);
			em.flush();
			FacesMessages.instance().add(StatusMessage.Severity.INFO,"Your configuration is saved.");
			this.back();
		}catch(ConstraintViolationException cve){
			FacesMessages.instance().add(StatusMessage.Severity.WARN,"The name " + this.selectedSystemConfiguration.getName() + " is already used, please choose another one");
		}catch(PersistenceException pve){
			if (this.selectedSystemConfiguration.getName() == null)
				FacesMessages.instance().add(StatusMessage.Severity.ERROR,"The name of your configuration cannot be null, please fill the appropriate field and save again");
		}catch(Exception e){
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to save configuration : " + e.getMessage());
		}
		displayAllConfigurations();
	}
	
	/**
	 * <p>deleteSelectedSystemConfiguration.</p>
	 *
	 * @param classs a {@link java.lang.Class} object.
	 */
	public void deleteSelectedSystemConfiguration(Class<T> classs){
		if (this.selectedSystemConfiguration != null){
			if (this.selectedSystemConfiguration.getId() != null){
				EntityManager em = EntityManagerService.provideEntityManager();
				this.selectedSystemConfiguration = em.find(classs, this.selectedSystemConfiguration.getId());
				em.remove(this.selectedSystemConfiguration);
				em.flush();
			}
		}
		reset();
	}
	
	/**
	 * <p>getPossibleListUsages.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public abstract List<Usage> getPossibleListUsages();
	
	/**
	 * <p>back.</p>
	 */
	public void back(){
		selectedSystemConfiguration = null;
		displayAllConfigurations();
	}
	
	/**
	 * <p>isOwnerOfConfig.</p>
	 *
	 * @param sc a {@link net.ihe.gazelle.simulator.sut.model.SystemConfiguration} object.
	 * @return a boolean.
	 */
	public boolean isOwnerOfConfig(SystemConfiguration sc){
		boolean res = false;
		if (sc != null){
			if (sc.getOwner() != null && Identity.instance().getCredentials().getUsername() != null && 
					Identity.instance().getCredentials().getUsername().equals(sc.getOwner())){
				res = true;
			}
		}
		return res;
	}
	
}

package net.ihe.gazelle.simulator.common.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;

import javax.validation.constraints.NotNull;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;


@Entity
/**
 * <p>Message class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("message")
@Table(name = "gs_message", schema = "public")
@SequenceGenerator(name = "gs_message_sequence", sequenceName = "gs_message_id_seq", allocationSize = 1)
public class Message implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 5093985974251904654L;
	
	
	//   attributes ////////////////////////////////////////////////////////////////////////////
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="gs_message_sequence")
	private Integer id;

	
	@Column(name="message_content")
	private byte[] messageContent;

	@Column(name="message_type_id")
	private String messageType;


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="time_stamp")
	private Date timeStamp;
	
	@ManyToOne
	@JoinColumn(name="transaction_id")
	private Transaction transaction;
	
	@ManyToOne
	@JoinColumn(name = "test_instance_participants_sender_id")
	private TestInstanceParticipants testInstanceParticipantsSender;
	
	@ManyToOne
	@JoinColumn(name = "test_instance_participants_receiver_id")
	private TestInstanceParticipants testInstanceParticipantsReceiver;


	//   constructors ////////////////////////////////////////////////////////////////////////////
	
	/**
	 * <p>Constructor for Message.</p>
	 */
	public Message(){
	}
	
	/**
	 * <p>Constructor for Message.</p>
	 *
	 * @param messageContent an array of byte.
	 * @param messageType a {@link java.lang.String} object.
	 * @param timeStamp a {@link java.util.Date} object.
	 * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 * @param testInstanceParticipantsSender a {@link net.ihe.gazelle.simulator.common.model.TestInstanceParticipants} object.
	 * @param testInstanceParticipantsReceiver a {@link net.ihe.gazelle.simulator.common.model.TestInstanceParticipants} object.
	 */
	public Message(byte[] messageContent, String messageType, Date timeStamp,
			Transaction transaction,
			TestInstanceParticipants testInstanceParticipantsSender,
			TestInstanceParticipants testInstanceParticipantsReceiver) {
		super();
		this.messageContent = messageContent;
		this.messageType = messageType;
		this.timeStamp = timeStamp;
		this.transaction = transaction;
		this.testInstanceParticipantsSender = testInstanceParticipantsSender;
		this.testInstanceParticipantsReceiver = testInstanceParticipantsReceiver;
	}
	
	

	//   attributes ////////////////////////////////////////////////////////////////////////////

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * <p>Setter for the field <code>transaction</code>.</p>
	 *
	 * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	/**
	 * <p>Getter for the field <code>transaction</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 */
	public Transaction getTransaction() {
		return transaction;
	}

	/**
	 * <p>Getter for the field <code>timeStamp</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getTimeStamp() {
		return timeStamp;
	}

	/**
	 * <p>Setter for the field <code>timeStamp</code>.</p>
	 *
	 * @param timeStamp a {@link java.util.Date} object.
	 */
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * <p>Getter for the field <code>messageType</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMessageType() {
		return messageType;
	}

	/**
	 * <p>Setter for the field <code>messageType</code>.</p>
	 *
	 * @param messageType a {@link java.lang.String} object.
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}


	/**
	 * <p>Getter for the field <code>messageContent</code>.</p>
	 *
	 * @return an array of byte.
	 */
	public byte[] getMessageContent() {
		return messageContent;
	}
	/**
	 * <p>Setter for the field <code>messageContent</code>.</p>
	 *
	 * @param messageContent an array of byte.
	 */
	public void setMessageContent(byte[] messageContent) {
		this.messageContent = messageContent;
	}
	/**
	 * <p>Getter for the field <code>testInstanceParticipantsSender</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.model.TestInstanceParticipants} object.
	 */
	public TestInstanceParticipants getTestInstanceParticipantsSender() {
		return testInstanceParticipantsSender;
	}
	/**
	 * <p>Setter for the field <code>testInstanceParticipantsSender</code>.</p>
	 *
	 * @param testInstanceParticipantsSender a {@link net.ihe.gazelle.simulator.common.model.TestInstanceParticipants} object.
	 */
	public void setTestInstanceParticipantsSender(
			TestInstanceParticipants testInstanceParticipantsSender) {
		this.testInstanceParticipantsSender = testInstanceParticipantsSender;
	}
	/**
	 * <p>Getter for the field <code>testInstanceParticipantsReceiver</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.model.TestInstanceParticipants} object.
	 */
	public TestInstanceParticipants getTestInstanceParticipantsReceiver() {
		return testInstanceParticipantsReceiver;
	}
	/**
	 * <p>Setter for the field <code>testInstanceParticipantsReceiver</code>.</p>
	 *
	 * @param testInstanceParticipantsReceiver a {@link net.ihe.gazelle.simulator.common.model.TestInstanceParticipants} object.
	 */
	public void setTestInstanceParticipantsReceiver(
			TestInstanceParticipants testInstanceParticipantsReceiver) {
		this.testInstanceParticipantsReceiver = testInstanceParticipantsReceiver;
	}
	
	//   equals and hashcode ////////////////////////////////////////////////////////////////////////////

	/**
	 * <p>hashCode.</p>
	 *
	 * @return a int.
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(messageContent);
		result = prime * result
				+ ((messageType == null) ? 0 : messageType.hashCode());
		result = prime
				* result
				+ ((testInstanceParticipantsReceiver == null) ? 0
						: testInstanceParticipantsReceiver.hashCode());
		result = prime
				* result
				+ ((testInstanceParticipantsSender == null) ? 0
						: testInstanceParticipantsSender.hashCode());
		result = prime * result
				+ ((timeStamp == null) ? 0 : timeStamp.hashCode());
		result = prime * result
				+ ((transaction == null) ? 0 : transaction.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		if (!Arrays.equals(messageContent, other.messageContent))
			return false;
		if (messageType == null) {
			if (other.messageType != null)
				return false;
		} else if (!messageType.equals(other.messageType))
			return false;
		if (testInstanceParticipantsReceiver == null) {
			if (other.testInstanceParticipantsReceiver != null)
				return false;
		} else if (!testInstanceParticipantsReceiver
				.equals(other.testInstanceParticipantsReceiver))
			return false;
		if (testInstanceParticipantsSender == null) {
			if (other.testInstanceParticipantsSender != null)
				return false;
		} else if (!testInstanceParticipantsSender
				.equals(other.testInstanceParticipantsSender))
			return false;
		if (timeStamp == null) {
			if (other.timeStamp != null)
				return false;
		} else if (!timeStamp.equals(other.timeStamp))
			return false;
		if (transaction == null) {
			if (other.transaction != null)
				return false;
		} else if (!transaction.equals(other.transaction))
			return false;
		return true;
	}

	
	//   methods ////////////////////////////////////////////////////////////////////////////

	/**
	 * <p>addANDorWHERE.</p>
	 *
	 * @param currentQueryString a {@link java.lang.StringBuffer} object.
	 * @return a {@link java.lang.StringBuffer} object.
	 */
	public static StringBuffer addANDorWHERE( StringBuffer currentQueryString  )
	{
		if ( currentQueryString.indexOf("WHERE") > 0 )
			return currentQueryString.append(" AND " ) ;
		else 
			return currentQueryString.append(" WHERE " ) ;

	}
	
	/**
	 * <p>getStringValue.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getStringValue(){
		String msgHL7 = new String(this.messageContent);
		return msgHL7;
	}
	
	/**
	 * <p>getListOfAllMessage.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	@SuppressWarnings("unchecked")
	public static List<Message> getListOfAllMessage(){
		EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
		Query queryContact = entityManager.createQuery("SELECT msg From Message msg");
		return (List<Message>)queryContact.getResultList();
	}
	
	/**
	 * <p>getMessageFiltred.</p>
	 *
	 * @param inMessageType a {@link java.lang.String} object.
	 * @param inTestInstanceParticipantReceiver a {@link net.ihe.gazelle.simulator.common.model.TestInstanceParticipants} object.
	 * @param inTestInstanceParticipantSender a {@link net.ihe.gazelle.simulator.common.model.TestInstanceParticipants} object.
	 * @param inTransaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 * @param inTestInstance a {@link net.ihe.gazelle.simulator.common.model.TestInstance} object.
	 * @return a {@link java.util.List} object.
	 */
	public static List<Message> getMessageFiltred(String inMessageType, 
												  TestInstanceParticipants inTestInstanceParticipantReceiver,
												  TestInstanceParticipants inTestInstanceParticipantSender,
												  Transaction inTransaction,
												  TestInstance inTestInstance){
		EntityManager em = EntityManagerService.provideEntityManager();
		Query query ;
		StringBuffer queryString = new StringBuffer() ;
		HashSet < String> prefixes = new HashSet<String>();
		HashSet < String> prefixesUsed = new HashSet<String>();
		HashMap<String ,String > mapOfJoin = new HashMap<String, String>() ;
		HashMap<String , Object > mapOfParameters = new HashMap<String, Object>() ;
		
		
		if (inMessageType != null){
			addANDorWHERE(queryString) ;
			prefixes.add("Message mess");
			queryString.append( "mess.messageType = :inMessageType" );
			mapOfParameters.put("inMessageType", inMessageType);
		}
		
		if (inTestInstanceParticipantSender != null){
			addANDorWHERE(queryString) ;
			prefixes.add("Message mess");
			queryString.append( "mess.testInstanceParticipantsSender = :inTestInstanceParticipantSender" );
			mapOfParameters.put("inTestInstanceParticipantSender", inTestInstanceParticipantSender);
		}
		
		if (inTestInstanceParticipantReceiver != null){
			addANDorWHERE(queryString) ;
			prefixes.add("Message mess");
			queryString.append( "mess.testInstanceParticipantsReceiver = :inTestInstanceParticipantReceiver" );
			mapOfParameters.put("inTestInstanceParticipantReceiver", inTestInstanceParticipantReceiver);
		}
		
		if (inTransaction != null){
			addANDorWHERE(queryString) ;
			prefixes.add("Message mess");
			queryString.append( "mess.transaction = :inTransaction" );
			mapOfParameters.put("inTransaction", inTransaction);
		}
		
		if (inTestInstance != null){
			addANDorWHERE(queryString) ;
			prefixes.add("Message mess");
			prefixes.add("TestInstance ti");
			mapOfJoin.put("TestInstance ti", "join ti.testInstanceParticipants tip" ) ;
			queryString.append( "mess.testInstanceParticipantsReceiver=tip and ti=:inTestInstance" );
			mapOfParameters.put("inTestInstance", inTestInstance);
		}
		
		prefixes.add("Message mess");
		
		List<String> listOfPrefixes = new ArrayList<String>( prefixes ) ;
		StringBuffer firstPartOfQuery = new StringBuffer() ;

		for ( int i= 0 ; i< listOfPrefixes.size() ; i++  )
		{
			if ( i == 0 ) 
			{
				firstPartOfQuery.append("SELECT distinct mess FROM "    ) ;
			}

			if ( !prefixesUsed.contains(listOfPrefixes.get(i)) )
			{
				if ( prefixesUsed.size() > 0 ) firstPartOfQuery.append(" , "  ) ;

				firstPartOfQuery.append( listOfPrefixes.get(i) ) ;
				if (mapOfJoin.containsKey(listOfPrefixes.get(i)))
					firstPartOfQuery.append(" " + mapOfJoin.get(listOfPrefixes.get(i)) +" " ) ;
				prefixesUsed.add( listOfPrefixes.get(i) ) ;
			}
		}

		queryString.insert(0, firstPartOfQuery ) ;
		if ( queryString.toString().trim().length() == 0 ) return null ;
		query = em.createQuery(queryString.toString()) ;
		List<String> listOfParameters =  new ArrayList<String>( mapOfParameters.keySet() ) ;
		for ( String param : listOfParameters ){
			query.setParameter( param , mapOfParameters.get(param) ) ;
		}
		List<Message> listOfObjectFile = ( List<Message> )query.getResultList(); 
		return listOfObjectFile ;
	}

}

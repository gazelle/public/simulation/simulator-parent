/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.configuration.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Entity
/**
 * <p>Host class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("host")
@Table(name = "cfg_host", schema = "public")
@SequenceGenerator(name = "cfg_host_sequence", sequenceName = "cfg_host_id_seq", allocationSize = 1)
public class Host implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 124392223397281644L;
	
	/** Constant <code>LOG</code> */
	@Logger
    public static  Log log;

	// ~ Attributes
	// ////////////////////////////////////////////////////////////////////////////////////////////////////

	/** Id attribute! */
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cfg_host_sequence")
	private Integer id;

	/**
	 * hostname attribute ,
	 */
	@Column(name = "hostname")
	@NotNull
	private String hostname;

	/** Ip of the System configuration */
	@Column(name = "ip" )
	private String ip;


	/** Comments on this configuration */
	@Column(name = "comment")
	private String comment;

	/** Alias of the configuration available */
	@Column(name = "alias")
	private String alias;

	/**
	 * Creates a new Host object.
	 */
	public Host()
	{
	}

	/**
	 * Creates a new Host object.
	 *
	 * @param inHostname hostname for this configuration
	 */
	public Host(final String inHostname)
	{
		this.hostname = inHostname;

	}

	/**
	 * <p>Constructor for Host.</p>
	 *
	 * @param inHost a {@link net.ihe.gazelle.simulator.common.configuration.model.Host} object.
	 */
	public Host(Host inHost){
		if (  inHost.getAlias()     		!= null ) this.alias         	= inHost.getAlias();
		if (  inHost.getComment()    	!= null ) this.comment         	= inHost.getComment();
		if (  inHost.getHostname()    	!= null ) this.hostname        	= inHost.getHostname();
		if (  inHost.getIp()  			!= null ) this.ip         		= inHost.getIp();
	}

	/**
	 * get the Index of the configuration
	 *
	 * @return the index
	 */
	public Integer getId()
	{
		return id ;
	}

	/**
	 * set the index of the configuration
	 *
	 * @param inId a {@link java.lang.Integer} object.
	 */
	public void setIndex(Integer inId)
	{
		this.id =  inId ;
	}

	/**
	 * get the hostname of the configuration
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getHostname()
	{
		return hostname;
	}

	/**
	 * set the hostname of the configuration
	 *
	 * @param inHostname a {@link java.lang.String} object.
	 */
	public void setHostname(String inHostname)
	{
		this.hostname = inHostname;
	}

	/**
	 * get the Ip of the configuration
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getIp()
	{
		return ip;
	}

	/**
	 * set the Ip for the configuration
	 *
	 * @param inIp a {@link java.lang.String} object.
	 */
	public void setIp(String inIp)
	{
		this.ip = inIp;
	}


	/**
	 * get the comment inserted for this configuration
	 *
	 * @return comment
	 */
	public String getComment()
	{
		return comment;
	}

	/**
	 * set the comment for this configuration
	 *
	 * @param inComment a {@link java.lang.String} object.
	 */
	public void setComment(String inComment)
	{
		this.comment = inComment;
	}

	/**
	 * Get the alias of this configuration
	 *
	 * @return alias
	 */
	public String getAlias()
	{
		return alias;
	}

	/**
	 * set an alias for this configuration
	 *
	 * @param inAlias a {@link java.lang.String} object.
	 */
	public void setAlias(String inAlias){
		this.alias = inAlias;
	}

    /**
     * <p>hashCode.</p>
     *
     * @return a int.
     */
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((alias == null) ? 0 : alias.hashCode());
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result
                + ((hostname == null) ? 0 : hostname.hashCode());
        result = prime * result + ((ip == null) ? 0 : ip.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Host other = (Host) obj;
        if (alias == null) {
            if (other.alias != null)
                return false;
        } else if (!alias.equals(other.alias))
            return false;
        if (comment == null) {
            if (other.comment != null)
                return false;
        } else if (!comment.equals(other.comment))
            return false;
        if (hostname == null) {
            if (other.hostname != null)
                return false;
        } else if (!hostname.equals(other.hostname))
            return false;
        if (ip == null) {
            if (other.ip != null)
                return false;
        } else if (!ip.equals(other.ip))
            return false;
        return true;
    }

    /**
     * <p>getHostByHostname.</p>
     *
     * @param hostname a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.Host} object.
     */
    public static Host getHostByHostname(String hostname){
		EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
		Query query=entityManager.createQuery("SELECT host " +
				"FROM Host host " +
				"WHERE host.hostname='"+hostname+"' ");
		List<Host> result=query.getResultList();
		if(result.size()>0)
			return result.get(0);
		return null;
	}
	/**
	 * <p>mergeHost.</p>
	 *
	 * @param inHost a {@link net.ihe.gazelle.simulator.common.configuration.model.Host} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.Host} object.
	 */
	public static Host mergeHost(Host inHost){
		if(inHost!=null){
			EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
			Host host=getHostByHostname(inHost.getHostname());
			if (inHost.equals(host)) {
			    return host;
			}
			inHost=entityManager.merge(inHost);
			entityManager.flush();
			return inHost;
		}
		return null;
	}

    /**
     * <p>toString.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String toString() {
        return "Host [id=" + id + ", hostname=" + hostname + ", ip=" + ip
                + ", comment=" + comment + ", alias=" + alias + "]";
    }
	
	
}

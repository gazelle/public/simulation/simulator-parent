package net.ihe.gazelle.simulator.common.utils;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.model.Concept;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.persistence.EntityManager;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>SVSConsumer class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class SVSConsumer {

    private static final ClientExecutor CLIENT_EXECUTOR = new ApacheHttpClient4Executor();


    private static Logger log = LoggerFactory.getLogger(SVSConsumer.class);

    /**
     * <p>getRandomConceptFromValueSet.</p>
     *
     * @param lang a {@link java.lang.String} object.
     * @param valueSetId a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.simulator.common.model.Concept} object.
     */
    public static Concept getRandomConceptFromValueSet(String valueSetId, String lang) {
        return getRandomConceptFromValueSet(valueSetId, lang, null);
    }

    /**
     * <p>getRandomConceptFromValueSet.</p>
     *
     * @param valueSetId a {@link java.lang.String} object.
     * @param lang a {@link java.lang.String} object.
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     * @return a {@link net.ihe.gazelle.simulator.common.model.Concept} object.
     */
    public static Concept getRandomConceptFromValueSet(String valueSetId, String lang, EntityManager entityManager) {
        if (valueSetId == null || valueSetId.isEmpty()) {
            return null;
        }
        String svsRepository = getSVSRepositoryUrl(entityManager);
        ClientRequest request = new ClientRequest(svsRepository.concat("/RetrieveValueSetForSimulator"), CLIENT_EXECUTOR);
        request.queryParameter("id", valueSetId);
        request.followRedirects(true);
        if (lang != null && !lang.isEmpty()) {
            request.queryParameter("lang", lang);
        }
        request.queryParameter("random", "true");
        return parseResponse(request);
    }

    private static Concept parseResponse(ClientRequest request) {
        ClientResponse<String> response = null;
        Concept concept = null;
        try {
            response = request.get(String.class);
            if (response.getStatus() == 200) {
                String xmlContent = response.getEntity();
                Document document = XmlUtil.parse(xmlContent);
                NodeList concepts = document.getElementsByTagName("Concept");
                if (concepts.getLength() > 0) {
                    Node conceptNode = concepts.item(0);
                    NamedNodeMap attributes = conceptNode.getAttributes();
                    concept = parseAttributes(attributes);
                }
            }
        } catch (Exception e) {
            log.error("Cannot parse response from SVSSimulator:" + e.getMessage());
        } finally {
            if (response != null) {
                response.releaseConnection();
            }
        }
        return concept;
    }

    /**
     * <p>getConceptsListFromValueSet.</p>
     *
     * @param valueSetId a {@link java.lang.String} object.
     * @param lang a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public static List<Concept> getConceptsListFromValueSet(String valueSetId, String lang) {
        if (valueSetId == null || valueSetId.isEmpty()) {
            return null;
        }
        String svsRepository = getSVSRepositoryUrl(null);
        ClientRequest request = new ClientRequest(svsRepository.concat("/RetrieveValueSetForSimulator"), CLIENT_EXECUTOR);
        request.queryParameter("id", valueSetId);
        request.followRedirects(true);
        if (lang != null && !lang.isEmpty()) {
            request.queryParameter("lang", lang);
        }
        ClientResponse<String> response = null;
        List<Concept> conceptsList = null;
        try {
            response = request.get(String.class);
            if (response.getStatus() == 200) {
                String xmlContent = response.getEntity();
                Document document = XmlUtil.parse(xmlContent);
                NodeList concepts = document.getElementsByTagName("Concept");
                if (concepts.getLength() > 0) {
                    conceptsList = new ArrayList<Concept>();
                    for (int index = 0; index < concepts.getLength(); index++) {
                        Node conceptNode = concepts.item(index);
                        NamedNodeMap attributes = conceptNode.getAttributes();
                        conceptsList.add(parseAttributes(attributes));
                    }
                    Concept.sort(conceptsList);
                }
            }
        } catch (Exception e) {
            log.error("Cannot retrieve concept list" + e.getMessage());
        } finally {
            if (response != null) {
                response.releaseConnection();
            }
        }
        return conceptsList;
    }

    /**
     * Returns the displayName attribute associate to a given code from a given value set
     *
     * @param valueSetId a {@link java.lang.String} object.
     * @param lang a {@link java.lang.String} object.
     * @param code a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getDisplayNameForGivenCode(String valueSetId, String lang, String code) {
        Concept concept = getConceptForCode(valueSetId, lang, code);
        if (concept != null) {
            return concept.getDisplayName();
        } else {
            return null;
        }
    }

    /**
     * <p>getConceptForCode.</p>
     *
     * @param valueSetId a {@link java.lang.String} object.
     * @param lang a {@link java.lang.String} object.
     * @param code a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.simulator.common.model.Concept} object.
     */
    public static Concept getConceptForCode(String valueSetId, String lang, String code) {
        if (valueSetId == null || valueSetId.isEmpty()) {
            return null;
        }
        if (code == null || code.isEmpty()) {
            return null;
        }
        String svsRepository = getSVSRepositoryUrl(null);
        ClientRequest request = new ClientRequest(svsRepository.concat("/RetrieveValueSetForSimulator"), CLIENT_EXECUTOR);
        request.queryParameter("id", valueSetId);
        request.followRedirects(true);
        if (lang != null && !lang.isEmpty()) {
            request.queryParameter("lang", lang);
        }
        request.queryParameter("code", code);
        return parseResponse(request);
    }

    /**
     * <p>getCodeSetAsXmlString.</p>
     *
     * @param valueSetId a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getCodeSetAsXmlString(String valueSetId) {
        if (valueSetId == null || valueSetId.isEmpty()) {
            return null;
        }
        String svsRepository = getSVSRepositoryUrl(null);
        ClientRequest request = new ClientRequest(svsRepository.concat("/RetrieveValueSetForSimulator"), CLIENT_EXECUTOR);
        request.queryParameter("id", valueSetId);
        request.followRedirects(true);
        String value = null;
        ClientResponse<String> response = null;
        try {
            response = request.get(String.class);
            if (response.getStatus() == 200) {
                value = response.getEntity();
            }
        } catch (Exception e) {
            log.error("Cannot get code set as xml:" + e.getMessage());
        } finally {
            if (response != null) {
                response.releaseConnection();
            }
        }
        return value;
    }

    /**
     * @param entityManager
     *
     * @return
     */
    private static String getSVSRepositoryUrl(EntityManager entityManager) {
        // if the entityManager is not provided we use the one managed by Seam
        if (entityManager == null) {
            entityManager = EntityManagerService.provideEntityManager();
        }
        String svsRepository = ApplicationConfiguration.getValueOfVariable("svs_repository_url", entityManager);
        if (svsRepository == null) {
            ApplicationConfiguration pref = new ApplicationConfiguration("svs_repository_url", "http://gazelle.ihe.net");
            entityManager.merge(pref);
            entityManager.flush();
            svsRepository = pref.getValue();
        }
        return svsRepository;
    }

    private static Concept parseAttributes(NamedNodeMap attributes) {
        String code = null;
        String displayName = null;
        String codeSystem = null;
        String codeSystemName = null;
        Node attribute;
        if ((attribute = attributes.getNamedItem("code")) != null) {
            code = attribute.getTextContent();
        }
        if ((attribute = attributes.getNamedItem("displayName")) != null) {
            displayName = attribute.getTextContent();
        }
        if ((attribute = attributes.getNamedItem("codeSystem")) != null) {
            codeSystem = attribute.getTextContent();
        }
        if ((attribute = attributes.getNamedItem("codeSystemName")) != null) {
            codeSystemName = attribute.getTextContent();
        }
        return new Concept(code, displayName, codeSystem, codeSystemName);
    }

    /**
     * <p>checkAvailability.</p>
     *
     * @param valueSetOID a {@link java.lang.String} object.
     * @return a boolean.
     * @throws java.lang.Exception if any.
     */
    public static boolean checkAvailability(String valueSetOID) throws Exception {
        String svsRepository = getSVSRepositoryUrl(null);
        ClientRequest request = new ClientRequest(svsRepository.concat("/RetrieveValueSetForSimulator"), CLIENT_EXECUTOR);
        request.queryParameter("id", valueSetOID);
        request.followRedirects(true);
        ClientResponse<String> response = null;
        try {
            response = request.get(String.class);
        }catch (Exception e){
            throw new Exception(valueSetOID + ": " + e.getMessage());
        }finally {
            if (response != null) {
                response.releaseConnection();
            }
        }
        if (response != null) {
            return response.getStatus() == Response.Status.OK.getStatusCode();
        } else {
            return false;
        }
    }
}

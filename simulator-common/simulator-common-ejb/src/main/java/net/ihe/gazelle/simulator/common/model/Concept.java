package net.ihe.gazelle.simulator.common.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * <p>Concept class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class Concept implements Serializable, Comparable<Concept>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4518318071899080800L;
	
	private String code;
	private String displayName;
	private String codeSystem;
	private String codeSystemName;
	
	/**
	 * <p>Constructor for Concept.</p>
	 */
	public Concept()
	{
		
	}
	
	/**
	 * <p>Constructor for Concept.</p>
	 *
	 * @param inCode a {@link java.lang.String} object.
	 * @param inDisplayName a {@link java.lang.String} object.
	 * @param inCodeSystem a {@link java.lang.String} object.
	 */
	public Concept(String inCode, String inDisplayName, String inCodeSystem)
	{
		this.code = inCode;
		this.displayName = inDisplayName;
		this.codeSystem = inCodeSystem;
		this.codeSystemName = null;
	}
	
	/**
	 * <p>Constructor for Concept.</p>
	 *
	 * @param inCode a {@link java.lang.String} object.
	 * @param inDisplayName a {@link java.lang.String} object.
	 * @param inCodeSystem a {@link java.lang.String} object.
	 * @param inCodeSystemName a {@link java.lang.String} object.
	 */
	public Concept(String inCode, String inDisplayName, String inCodeSystem, String inCodeSystemName){
		this.code = inCode;
		this.displayName = inDisplayName;
		this.codeSystem = inCodeSystem;
		this.codeSystemName = inCodeSystemName;
	}

	/**
	 * <p>Getter for the field <code>code</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * <p>Setter for the field <code>code</code>.</p>
	 *
	 * @param code a {@link java.lang.String} object.
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * <p>Getter for the field <code>displayName</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * <p>Setter for the field <code>displayName</code>.</p>
	 *
	 * @param displayName a {@link java.lang.String} object.
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * <p>Getter for the field <code>codeSystem</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCodeSystem() {
		return codeSystem;
	}

	/**
	 * <p>Setter for the field <code>codeSystem</code>.</p>
	 *
	 * @param codeSystem a {@link java.lang.String} object.
	 */
	public void setCodeSystem(String codeSystem) {
		this.codeSystem = codeSystem;
	}
	
	/**
	 * <p>sort.</p>
	 *
	 * @param concepts a {@link java.util.List} object.
	 */
	public static void sort(List<Concept> concepts)
	{
		Collections.sort(concepts);
	}

	/** {@inheritDoc} */
	@Override
	public int compareTo(Concept o) {
		return this.getDisplayName().compareToIgnoreCase(o.getDisplayName());
	}

	/**
	 * <p>Getter for the field <code>codeSystemName</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCodeSystemName() {
		return codeSystemName;
	}

	/**
	 * <p>Setter for the field <code>codeSystemName</code>.</p>
	 *
	 * @param codeSystemName a {@link java.lang.String} object.
	 */
	public void setCodeSystemName(String codeSystemName) {
		this.codeSystemName = codeSystemName;
	}
	
}

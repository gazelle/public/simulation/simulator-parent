package net.ihe.gazelle.simulator.report;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <p>TestReportData class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "TestReport")
public class TestReportData {
	
	/**
	 * Test identifier, can be the identifier of the message in the database
	 */
	@XmlElement(name = "TestId")
	protected String testId;
	/**
	 * If several kind of test are available in the simulator, the kind of test we are looking for
	 */
	@XmlElement(name = "Test")
	protected String test;
	/**
	 * date of test execution
	 */
	@XmlElement(name = "Date")
	protected Date date;
	/**
	 * Who was the sender of the first message
	 */
	@XmlElement(name = "Source")
	protected String source;
	/**
	 * Who received the message (and eventually sent ack)
	 */
	@XmlElement(name = "Target")
	protected String target;
	/**
	 * HL7v2 MSA-1, HTTP response code ...
	 */
	@XmlElement(name = "AcknowledgementCode")
	protected String acknowledgmentCode;
	/**
	 * Where to find the log
	 */
	@XmlElement(name = "URL")
	protected String logUrl;
	/**
	 * The list of messages sent and received during the test
	 */
	@XmlElement(name = "Message")
	protected List<TestReportDataMessage> messages;
	
	@XmlElement(name="Tool")
	protected TestReportTool tool;

	/**
	 * Default constructor
	 */
	public TestReportData() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * <p>Constructor for TestReportData.</p>
	 *
	 * @param testId a {@link java.lang.String} object.
	 * @param test a {@link java.lang.String} object.
	 */
	public TestReportData(String testId, String test) {
		this.test = test;
		this.testId = testId;
	}
	

	/**
	 **********************************************
	 * GETTERS and SETTERS
	 ***********************************************
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getTest() {
		return test;
	}

	/**
	 * <p>Setter for the field <code>test</code>.</p>
	 *
	 * @param test a {@link java.lang.String} object.
	 */
	public void setTest(String test) {
		this.test = test;
	}

	/**
	 * <p>Getter for the field <code>date</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * <p>Setter for the field <code>date</code>.</p>
	 *
	 * @param date a {@link java.util.Date} object.
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * <p>Getter for the field <code>source</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getSource() {
		return source;
	}

	/**
	 * <p>Setter for the field <code>source</code>.</p>
	 *
	 * @param source a {@link java.lang.String} object.
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * <p>Getter for the field <code>target</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * <p>Setter for the field <code>target</code>.</p>
	 *
	 * @param target a {@link java.lang.String} object.
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 * <p>Getter for the field <code>acknowledgmentCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getAcknowledgmentCode() {
		return acknowledgmentCode;
	}

	/**
	 * <p>Setter for the field <code>acknowledgmentCode</code>.</p>
	 *
	 * @param acknowledgmentCode a {@link java.lang.String} object.
	 */
	public void setAcknowledgmentCode(String acknowledgmentCode) {
		this.acknowledgmentCode = acknowledgmentCode;
	}

	/**
	 * <p>Setter for the field <code>testId</code>.</p>
	 *
	 * @param testId a {@link java.lang.String} object.
	 */
	public void setTestId(String testId) {
		this.testId = testId;
	}

	/**
	 * <p>Getter for the field <code>testId</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getTestId() {
		return testId;
	}

	/**
	 * <p>Getter for the field <code>logUrl</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLogUrl() {
		return logUrl;
	}

	/**
	 * <p>Setter for the field <code>logUrl</code>.</p>
	 *
	 * @param logUrl a {@link java.lang.String} object.
	 */
	public void setLogUrl(String logUrl) {
		this.logUrl = logUrl;
	}

	/**
	 * <p>Setter for the field <code>messages</code>.</p>
	 *
	 * @param messages a {@link java.util.List} object.
	 */
	public void setMessages(List<TestReportDataMessage> messages) {
		this.messages = messages;
	}

	/**
	 * <p>Getter for the field <code>messages</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<TestReportDataMessage> getMessages() {
		return messages;
	}
	
	/**
	 * <p>Getter for the field <code>tool</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.report.TestReportTool} object.
	 */
	public TestReportTool getTool(){
		return this.tool;
	}
	
	/**
	 * <p>Setter for the field <code>tool</code>.</p>
	 *
	 * @param inTool a {@link net.ihe.gazelle.simulator.report.TestReportTool} object.
	 */
	public void setTool(TestReportTool inTool){
		this.tool = inTool;
	}

	/**
	 * Builds the XML to return to TM
	 *
	 * @return a {@link java.lang.String} object.
	 * @throws javax.xml.bind.JAXBException if any.
	 */
	public String toXml() throws JAXBException{
		JAXBContext jc = JAXBContext.newInstance(TestReportData.class);
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		StringWriter writer = new StringWriter();
		m.marshal(this, writer);
		return writer.toString();
	}

}

package net.ihe.gazelle.simulator.common.ihewsinit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class SoapMessageConverter {

    private static Logger LOGGER = LoggerFactory.getLogger(SoapMessageConverter.class);

    /**
     * Convert a {@link SOAPMessage} to a byte array.
     *
     * @param message the message to convert.
     * @return the byte array.
     */
    public static byte[] soapMessageToByteArray(SOAPMessage message){
        return soapMessageToString(message).getBytes(StandardCharsets.UTF_8);
    }

    /**
     * Convert a {@link SOAPMessage} to a String.
     *
     * @param message the message ton convert.
     * @return the literal representation of the {@link SOAPMessage}
     */
    private static String soapMessageToString(SOAPMessage message) {
        String result = null;

        if (message != null) {
            ByteArrayOutputStream outputStream = null;
            try {
                outputStream = new ByteArrayOutputStream();
                message.writeTo(outputStream);
                result = outputStream.toString(StandardCharsets.UTF_8.name());
            } catch (Exception e) {
                LOGGER.error("Error writing to OutputStream while converting SOAP Message to byte[]!", e);
            } finally {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException ioe) {
                        LOGGER.error("Error closing OutputStream while converting SOAP Message to byte[]!", ioe);
                    }
                }
            }
        }
        return result;
    }
}

package net.ihe.gazelle.simulator.common.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.security.Identity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>ApplicationConfiguration class.</p>
 *
 * @author Abderrazek Boufahja > INRIA Rennes IHE development Project
 * @version $Id: $Id
 */
@Entity
@Name("applicationConfiguration")
@Table(name = "app_configuration", schema = "public")
@SequenceGenerator(name = "app_configuration_sequence", sequenceName = "app_configuration_id_seq", allocationSize = 1)
public class ApplicationConfiguration implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5444985974251904654L;

    // attributes ////////////////////////////////////////////////////////////////////////////

    /**
     * Id of this object
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_configuration_sequence")
    private Integer id;

    @Column(name = "variable", unique = true)
    private String variable;

    @Column(name = "value")
    private String value;

    // constructors ////////////////////////////////////////////////////////////////////////////

    /**
     * <p>Constructor for ApplicationConfiguration.</p>
     */
    public ApplicationConfiguration() {
    }

    /**
     * <p>Constructor for ApplicationConfiguration.</p>
     *
     * @param variable a {@link java.lang.String} object.
     * @param value    a {@link java.lang.String} object.
     */
    public ApplicationConfiguration(String variable, String value) {
        super();
        this.variable = variable;
        this.value = value;
    }

    // getters and setters //////////////////////////////////////////////////////////////////////

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>variable</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getVariable() {
        return variable;
    }

    /**
     * <p>Setter for the field <code>variable</code>.</p>
     *
     * @param variable a {@link java.lang.String} object.
     */
    public void setVariable(String variable) {
        this.variable = variable;
    }

    /**
     * <p>Getter for the field <code>value</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getValue() {
        return value;
    }

    /**
     * <p>Setter for the field <code>value</code>.</p>
     *
     * @param value a {@link java.lang.String} object.
     */
    public void setValue(String value) {
        this.value = value;
    }

    // hashcode and equals //////////////////////////////////////////////////////////////////////

    /**
     * <p>hashCode.</p>
     *
     * @return a int.
     */
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        result = prime * result + ((variable == null) ? 0 : variable.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ApplicationConfiguration other = (ApplicationConfiguration) obj;
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!value.equals(other.value)) {
            return false;
        }
        if (variable == null) {
            if (other.variable != null) {
                return false;
            }
        } else if (!variable.equals(other.variable)) {
            return false;
        }
        return true;
    }

    // methods ///////////////////////////////////////////////////////////////////////////////////

    /**
     * <p>getValueOfVariable.</p>
     *
     * @param variable a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getValueOfVariable(String variable) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        return getValueOfVariable(variable, entityManager);
    }

    /**
     * <p>getValueOfVariable.</p>
     *
     * @param variable      a {@link java.lang.String} object.
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getValueOfVariable(String variable, EntityManager entityManager) {
        ApplicationConfigurationQuery query = new ApplicationConfigurationQuery(
                new HQLQueryBuilder<ApplicationConfiguration>(entityManager, ApplicationConfiguration.class));
        query.variable().eq(variable);
        ApplicationConfiguration conf = query.getUniqueResult();
        if (conf == null) {
            return null;
        } else {
            return conf.getValue();
        }
    }

    /**
     * <p>getApplicationConfigurationByVariable.</p>
     *
     * @param variable a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.simulator.common.model.ApplicationConfiguration} object.
     */
    public static ApplicationConfiguration getApplicationConfigurationByVariable(String variable) {
        ApplicationConfigurationQuery query = new ApplicationConfigurationQuery();
        query.variable().eq(variable);
        return query.getUniqueResult();
    }

    /**
     * <p>getApplicationConfigurationByVariableOrNew.</p>
     *
     * @param variable a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.simulator.common.model.ApplicationConfiguration} object.
     */
    public static ApplicationConfiguration getApplicationConfigurationByVariableOrNew(String variable) {
        ApplicationConfiguration res = getApplicationConfigurationByVariable(variable);
        if (res != null) {
            return res;
        }
        return new ApplicationConfiguration(variable, "");
    }

    /**
     * <p>getBooleanValue.</p>
     *
     * @param variable a {@link java.lang.String} object.
     * @return true only if the value of the variable fixed to "true"
     */
    public static Boolean getBooleanValue(String variable) {
        String val = getValueOfVariable(variable);
        if (val == null) {
            return false;
        }
        try {
            return Boolean.valueOf(val);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * <p>loginByIP.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String loginByIP() {
        Identity identity = (Identity) Component.getInstance("org.jboss.seam.security.identity");
        if ("loggedIn".equals(identity.login())) {
            return "/home.xhtml";
        } else {
            return null;
        }
    }

}

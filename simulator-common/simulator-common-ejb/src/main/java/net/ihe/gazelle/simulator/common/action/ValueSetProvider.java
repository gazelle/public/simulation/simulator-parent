package net.ihe.gazelle.simulator.common.action;

import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.simulator.common.model.ValueSet;
import net.ihe.gazelle.simulator.common.utils.SVSConsumer;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.international.LocaleSelector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Random;

/**
 * <p>ValueSetProvider class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class ValueSetProvider {

    private static final Random RANDOM = new Random();
    private static Logger log = LoggerFactory.getLogger(ValueSetProvider.class);

    private static final ValueSetProvider SINGLETON = new ValueSetProvider();

    /**
     * <p>getInstance.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.action.ValueSetProvider} object.
     */
    public static ValueSetProvider getInstance(){
        return SINGLETON;
    }

    /**
     * <p>reset.</p>
     */
    public void reset(){
        valueSets = null;
        knownValues = null;
    }

    private Map<String, List<SelectItem>> valueSets;

    private Map<String, Map<String, Concept>> knownValues;

    /**
     * <p>getListOfConcepts.</p>
     *
     * @param key a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public List<SelectItem> getListOfConcepts(String key) {
        String language = LocaleSelector.instance().getLanguage();
        String langKey = language.concat(key);
        if (valueSets == null) {
            valueSets = new HashMap<String, List<SelectItem>>();
        } else if (valueSets.containsKey(langKey)) {
            return valueSets.get(langKey);
        }

        String oid = ValueSet.getValueSetIdForKeyword(key);
        if (oid != null) {
            List<SelectItem> items = new ArrayList<SelectItem>();
            String please;
            try {
                please = ResourceBundle.instance().getString("gazelle.simulator.PleaseSelect");
            } catch (MissingResourceException e) {
                please = "Please select ...";
            }
            items.add(new SelectItem(null, please));
            try {
                List<Concept> concepts = SVSConsumer.getConceptsListFromValueSet(oid, language);
                if (concepts != null) {
                    for (Concept concept : concepts) {
                        items.add(new SelectItem(concept.getCode(), concept.getDisplayName()));
                    }
                }
            } catch (Exception e) {
                log.warn("An error occurred when retrieving value set with oid: " + oid);
            }
            valueSets.put(langKey, items);
            return items;
        } else {
            return null;
        }

    }

    /**
     * <p>getConceptForCode.</p>
     *
     * @param key a {@link java.lang.String} object.
     * @param code a {@link java.lang.String} object.
     * @param language a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.simulator.common.model.Concept} object.
     */
    public Concept getConceptForCode(String key, String code, String language) {
        String langKey;
        if (language != null) {
            langKey = language.concat(key);
        } else {
            langKey = key;
        }
        if (knownValues == null) {
            knownValues = new HashMap<String, Map<String, Concept>>();
        } else if (knownValues.containsKey(langKey) && knownValues.get(langKey).containsKey(code)) {
            return knownValues.get(langKey).get(code);
        }
        String oid = ValueSet.getValueSetIdForKeyword(key);
        if (oid != null) {
            Concept concept = SVSConsumer.getConceptForCode(oid, language, code);
            if (concept != null) {
                if (knownValues.containsKey(langKey)) {
                    knownValues.get(langKey).put(code, concept);
                } else {
                    Map<String, Concept> concepts = new HashMap<String, Concept>();
                    concepts.put(code, concept);
                    knownValues.put(langKey, concepts);
                }
                return concept;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * <p>getRandomCodeFromValueSet.</p>
     *
     * @param key a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public String getRandomCodeFromValueSet(String key) {
        String language = LocaleSelector.instance().getLanguage();
        String langKey = language.concat(key);
        String code = null;
        if ((valueSets != null) && valueSets.containsKey(langKey) && valueSets.get(langKey).size() > 1) {
            int index = RANDOM.nextInt(valueSets.get(langKey).size());
            // first value of the value set (List<SelectItem>) is the null object
            if (index == 0) {
                index = 1;
            }
            code = (String) valueSets.get(langKey).get(index).getValue();
        } else {
            List<SelectItem> valueSet = getListOfConcepts(key);
            if ((valueSet != null) && (valueSet.size() > 1)) {
                int index = RANDOM.nextInt(valueSet.size());
                if (index == 0) {
                    index = 1;
                }
                code = (String) valueSet.get(index).getValue();
            }
        }
        return code;
    }

    /**
     * <p>getDisplayNameForCode.</p>
     *
     * @param key a {@link java.lang.String} object.
     * @param code a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public String getDisplayNameForCode(String key, String code) {
        String language = LocaleSelector.instance().getLanguage();
        Concept concept = getConceptForCode(key, code, language);
        if (concept != null) {
            return concept.getDisplayName();
        } else {
            return code;
        }
    }


    /**
     * creates the string to put in the field of type CE by using SVS to retrieve the coded information
     *
     * @param code a {@link java.lang.String} object.
     * @param valueSetName a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public String buildCEElementFromCode(String code, String valueSetName) {
        Concept concept = getConceptForCode(valueSetName, code, LocaleSelector.instance().getLanguage());
        if (concept != null) {
            StringBuilder ce = new StringBuilder(code);
            ce.append('^');
            ce.append(concept.getDisplayName());
            ce.append('^');
            if ((concept.getCodeSystemName() != null) && !concept.getCodeSystemName().isEmpty()) {
                ce.append(concept.getCodeSystemName());
            } else {
                ce.append(concept.getCodeSystem());
            }
            return ce.toString();
        } else {
            return code;
        }
    }

}

/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.tf.model;

//JPA imports
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Entity
/**
 * <p>Transaction class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("transaction")
@Table(name = "tf_transaction", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
"keyword"}))
@SequenceGenerator(name = "tf_transaction_sequence", sequenceName = "tf_transaction_id_seq", allocationSize=1)
@XmlAccessorType(XmlAccessType.FIELD)

public class Transaction extends TFObject implements java.io.Serializable,Comparable<Transaction> {

	/** Serial ID version of this object */
	private static final long serialVersionUID = -5994534457658398088L;

	@Logger
	private static Log log;	

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tf_transaction_sequence")
	@XmlTransient
	private Integer id;

	//	Constructors
	/**
	 * <p>Constructor for Transaction.</p>
	 */
	public Transaction() {}

	/**
	 * <p>Constructor for Transaction.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @param name a {@link java.lang.String} object.
	 * @param description a {@link java.lang.String} object.
	 */
	public Transaction(String keyword, String name, String description) {
		this.keyword = keyword;
		this.name = name;
		this.description = description;
	}


	// *********************************************************
	//  Getters and Setters : setup the DB columns properties
	// *********************************************************


	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() 
	{
		return this.id;
	}
	
	// *********************************************************
	// Overrides for equals and hash code methods
	// *********************************************************

	/**
	 * <p>hashCode.</p>
	 *
	 * @return a int.
	 */
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (description == null)
		{
			if (other.description != null)
				return false;
		}
		else if (!description.equals(other.description))
			return false;
		if (keyword == null)
		{
			if (other.keyword != null)
				return false;
		}
		else if (!keyword.equals(other.keyword))
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		}
		else if (!name.equals(other.name))
			return false;

		return true;
	}

	/**
	 * <p>compareTo.</p>
	 *
	 * @param o a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 * @return a int.
	 */
	public int compareTo(Transaction o) {
		return this.getKeyword().compareToIgnoreCase(o.getKeyword());
	}

 	/**
 	 * <p>ListAllTransactions.</p>
 	 *
 	 * @return a {@link java.util.List} object.
 	 */
 	public static List<Transaction> ListAllTransactions()
	{
		TransactionQuery query = new TransactionQuery();
		query.keyword().order(true);
		return query.getList();
	}
 	
	/**
	 * <p>GetTransactionByKeyword.</p>
	 *
	 * @param inKeyword a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 */
	public static  Transaction GetTransactionByKeyword(String inKeyword )
	{
		if (inKeyword == null){
            return null;
        } else {
            TransactionQuery query = new TransactionQuery();
            query.keyword().like(inKeyword, HQLRestrictionLikeMatchMode.EXACT);
            return query.getUniqueResult();
        }
	}
	
	
	/**
	 * <p>getTransactionByKeywordAndCreateIfItDoesntExist.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @param name a {@link java.lang.String} object.
	 * @param em a {@link javax.persistence.EntityManager} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 */
	public static Transaction getTransactionByKeywordAndCreateIfItDoesntExist(String keyword, String name,  EntityManager em)
	{
		//LOG.info("Transaction ******************* keyword :" + keyword);
		Transaction transaction = GetTransactionByKeyword(keyword, em);
		if (transaction == null)
		{
			//LOG.info("Creation of a new transaction in the tf_transaction Table");
			Transaction newTransaction = new Transaction(keyword,name,null);
			mergeTransaction(newTransaction);
			return newTransaction;
		}
		else
		{
			//LOG.info("getTransactionByKeywordAndCreateIfItDoesntExist ******************* transaction.getId :" + transaction.getId());
			return transaction;		
		}
			
	}
	
	
	
	/**
	 * <p>getTransactionByKeywordAndCreateIfItDoesntExist.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @param name a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 */
	public static Transaction getTransactionByKeywordAndCreateIfItDoesntExist(String keyword, String name)
	{
		EntityManager  em = ( EntityManager) Component.getInstance("entityManager") ;
		Transaction transaction = GetTransactionByKeyword(keyword, em);
		if (transaction == null)
		{
			//LOG.info("Creation of a new transaction in the tf_transaction Table");
			Transaction newTransaction = new Transaction(keyword,name,null);
			mergeTransaction(newTransaction);
			return newTransaction;
		}
		else
		{
			//LOG.info("getTransactionByKeywordAndCreateIfItDoesntExist ******************* transaction.getId :" + transaction.getId());
			return transaction;		
		}
			
	}
	
	
	/**
	 * <p>GetTransactionByKeyword.</p>
	 *
	 * @param inKeyword a {@link java.lang.String} object.
	 * @param em a {@link javax.persistence.EntityManager} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 */
	public static  Transaction GetTransactionByKeyword(String inKeyword, EntityManager em )
    {
        if (inKeyword == null){
            return null;
        } else {
            TransactionQuery query = new TransactionQuery(em);
            query.keyword().like(inKeyword, HQLRestrictionLikeMatchMode.EXACT);
            return query.getUniqueResult();
        }
    }

	/**
	 * <p>mergeTransaction.</p>
	 *
	 * @param inTransaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 */
	public static Transaction mergeTransaction(Transaction inTransaction){
		if(inTransaction!=null){
			Transaction transaction=Transaction.GetTransactionByKeyword(inTransaction.getKeyword());
			if(transaction==null){
				EntityManager  entityManager = ( EntityManager) Component.getInstance("entityManager") ;
				transaction=entityManager.merge(inTransaction);
				entityManager.flush();
			}
			return transaction;
		}
		return null;
	}
	
	/**
	 * <p>getTransactionById.</p>
	 *
	 * @param transactionIdString a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 */
	public static Transaction getTransactionById(String transactionIdString){
        if (transactionIdString == null){
            return null;
        } else {
            TransactionQuery query = new TransactionQuery();

            try{
                Integer transactionId = Integer.parseInt(transactionIdString);
                query.id().eq(transactionId);
                return query.getUniqueResult();
            }catch (NumberFormatException e){
                log.error(transactionIdString + "does not represent a valid positive integer");
                return null;
            }

        }
	}
	
	

}

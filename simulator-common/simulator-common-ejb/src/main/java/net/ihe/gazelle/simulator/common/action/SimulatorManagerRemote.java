package net.ihe.gazelle.simulator.common.action;

import java.util.List;

import javax.xml.soap.SOAPException;

import net.ihe.gazelle.simulator.common.model.ConfigurationForWS;
import net.ihe.gazelle.simulator.common.model.ContextualInformationInstance;
import net.ihe.gazelle.simulator.common.model.Message;
import net.ihe.gazelle.simulator.common.model.OIDConfiguration;
import net.ihe.gazelle.simulator.common.model.TestInstanceParticipants;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
/**
 * <p>SimulatorManagerRemote interface.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public interface SimulatorManagerRemote {

	/**
	 * The purpose of this method is to inform the simulator that gazelle is starting a new test instance.
	 * The simulator is participating to that test instance.
	 *
	 * @param testInstanceId the id of the test instance
	 * @return true in case everything is ok. False otherwise.
	 */
	public boolean startTestInstance(String testInstanceId);



	/**
	 * The purpose of this method is to inform the simulator that gazelle stopped a test instance.
	 * The simulator is participating to that test instance.
	 *
	 * @param testInstanceId the id of the test instance
	 * @return true in case everything is ok. False otherwise.
	 */
	public boolean stopTestInstance(String testInstanceId);

	/**
	 * The purpose of this method is to inform the simulator that gazelle stopped a test instance.
	 * The simulator is participating to that test instance.
	 *
	 * @param testInstanceId the id of the test instance
	 * @return true in case everything is ok. False otherwise.
	 */
	public boolean deleteTestInstance(String testInstanceId);


	/**
	 * The purpose of this method is to inform the simulator of the configuration of the other participants to the given test instance.
	 * Other participants can SUT or Simulators.
	 *  Arguments are the id of the test instance and a list of configuration parameters for each Actor/Integration Profile/Option combination participating to the test.
	 *
	 * @param testInstanceId the id of the test instance
	 * @param oidList list of oid to be used on messages
	 * @return true in case everything is ok. False otherwise.
	 * @param testInstanceParticipantsList a {@link java.util.List} object.
	 * @throws javax.xml.soap.SOAPException if any.
	 */
	public boolean setTestPartnerConfigurations(String testInstanceId,
												List<TestInstanceParticipants> testInstanceParticipantsList,
												List<OIDConfiguration> oidList) throws SOAPException;



	/**
	 * The method sends a message to a specified test partner.
	 *
	 * @param testInstanceId the id of the test instance
	 * @return  the id of the sent message.
	 * @param serverTestInstanceParticipantsId a {@link java.lang.String} object.
	 * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 * @param messageType a {@link java.lang.String} object.
	 * @param responderConfiguration a {@link net.ihe.gazelle.simulator.common.model.ConfigurationForWS} object.
	 * @param listContextualInformationInstanceInput a {@link java.util.List} object.
	 * @param listContextualInformationInstanceOutput a {@link java.util.List} object.
	 * @throws javax.xml.soap.SOAPException if any.
	 */
	public ResultSendMessage sendMessage(String testInstanceId,
							  String serverTestInstanceParticipantsId,
							  Transaction transaction,String messageType, 
                              ConfigurationForWS responderConfiguration,
                              List<ContextualInformationInstance> listContextualInformationInstanceInput,
							  List<ContextualInformationInstance> listContextualInformationInstanceOutput)throws SOAPException;


	/**
	 * The method notifies if the simulator receive a message.
	 *
	 * @param testInstanceId the id of the test instance
	 * @return  the message_id if the simulator did receive a message corresponding to the criteria, Null otherwise
	 * @param testInstanceParticipantsId a {@link java.lang.String} object.
	 * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 * @param messageType a {@link java.lang.String} object.
	 */
	public String confirmMessageReception(String testInstanceId,
										  String testInstanceParticipantsId,
										  Transaction transaction,String messageType);

	/**
	 * The method returns the message content for the given messageId
	 * 
	 * @param messageId the id of the message to be returned
	 * @return the message content which id is provided as input
	 */
	//public Message getMessage(String messageId);
	
	
	/**
	 * the method returns the list of messages as viewed by the simulator, that were exchanged during the test instance.
	 *
	 * @param testInstanceId a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 */
	public List<Message> getMessagesForTestInstance(String testInstanceId);
}

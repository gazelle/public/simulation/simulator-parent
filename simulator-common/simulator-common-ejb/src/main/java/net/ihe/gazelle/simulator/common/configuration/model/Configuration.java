/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.simulator.common.configuration.model;


import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.simulator.common.tf.model.Actor;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;



@Entity
/**
 * <p>Configuration class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("configuration")
@Table(name = "cfg_configuration", schema = "public")
@SequenceGenerator(name = "cfg_configuration_sequence", sequenceName = "cfg_configuration_id_seq", allocationSize = 1)
public class Configuration implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Constant <code>LOG</code> */
	@Logger
	public static  Log log;


	/** Id attribute! */
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cfg_configuration_sequence")
	private Integer id;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="host_id")
	private Host host ;

	@Column(name="comment")
	private String comment ;

	@Column(name="is_secured")
	private Boolean isSecured ;

	/** If the configuration has been approved */
	@Column(name = "approved", nullable = false)
	private Boolean isApproved = false;

	/**
	 * <p>Constructor for Configuration.</p>
	 */
	public Configuration()
	{

	}
	
	/**
	 * <p>Constructor for Configuration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 */
	public Configuration(Configuration inConfiguration){
	    if (inConfiguration.getIsApproved() != null)  this.isApproved            = inConfiguration.getIsApproved();
	    if (inConfiguration.getIsSecured() != null)   this.isSecured   = inConfiguration.getIsSecured();
	    if (inConfiguration.getComment() != null)     this.comment     = inConfiguration.getComment();
	    if (inConfiguration.getHost() != null) this.host = inConfiguration.getHost();
	    
	}

	/**
	 * <p>Constructor for Configuration.</p>
	 *
	 * @param inHost a {@link net.ihe.gazelle.simulator.common.configuration.model.Host} object.
	 * @param inActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 * @param inComment a {@link java.lang.String} object.
	 * @param isSecured a {@link java.lang.Boolean} object.
	 */
	public Configuration( Host inHost , Actor inActor , String inComment , Boolean isSecured  )
	{

		host              = inHost              ;
		comment           = inComment           ;
		this.isSecured    = isSecured           ;

	}
	/**
	 * <p>Constructor for Configuration.</p>
	 *
	 * @param inActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 * @param inComment a {@link java.lang.String} object.
	 * @param isSecured a {@link java.lang.Boolean} object.
	 */
	public Configuration(  Actor inActor  ,  String inComment , Boolean isSecured  )
	{
		comment           = inComment           ;
		this.isSecured    = isSecured           ;

	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId()
	{
		return id;
	}


	/**
	 * <p>Getter for the field <code>host</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.Host} object.
	 */
	public Host getHost()
	{
		return host;
	}

	/**
	 * <p>Setter for the field <code>host</code>.</p>
	 *
	 * @param host a {@link net.ihe.gazelle.simulator.common.configuration.model.Host} object.
	 */
	public void setHost(Host host)
	{
		this.host = host;
	}

	/**
	 * <p>Getter for the field <code>isSecured</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getIsSecured()
	{
		return isSecured;
	}

	/**
	 * <p>Setter for the field <code>isSecured</code>.</p>
	 *
	 * @param isSecured a {@link java.lang.Boolean} object.
	 */
	public void setIsSecured(Boolean isSecured)
	{
		this.isSecured = isSecured;
	}

	/**
	 * <p>Getter for the field <code>comment</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getComment()
	{
		return comment;
	}

	/**
	 * <p>Setter for the field <code>comment</code>.</p>
	 *
	 * @param comment a {@link java.lang.String} object.
	 */
	public void setComment(String comment)
	{
		this.comment = comment;
	}

	/**
	 * <p>Getter for the field <code>isApproved</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getIsApproved() {
        return isApproved;
    }

    /**
     * <p>Setter for the field <code>isApproved</code>.</p>
     *
     * @param isApproved a {@link java.lang.Boolean} object.
     */
    public void setIsApproved(Boolean isApproved) {
        this.isApproved = isApproved;
    }
	
	
	/**
	 * <p>hashCode.</p>
	 *
	 * @return a int.
	 */
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result + ((host == null) ? 0 : host.hashCode());
		result = prime * result + ((isApproved == null) ? 0 : isApproved.hashCode());
		result = prime * result + ((isSecured == null) ? 0 : isSecured.hashCode());
		return result;
	}
	/** {@inheritDoc} */
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Configuration other = (Configuration) obj;
		if (comment == null)
		{
			if (other.comment != null)
				return false;
		}
		else if (!comment.equals(other.comment))
			return false;
		if (host == null)
		{
			if (other.host != null)
				return false;
		}
		else if (!host.equals(other.host))
			return false;
		if (isApproved == null)
		{
			if (other.isApproved != null)
				return false;
		}
		else if (!isApproved.equals(other.isApproved))
			return false;
		if (isSecured == null)
		{
			if (other.isSecured != null)
				return false;
		}
		else if (!isSecured.equals(other.isSecured))
			return false;
		return true;
	}

	/**
	 * <p>getConfigurationByHost.</p>
	 *
	 * @param inHost a {@link net.ihe.gazelle.simulator.common.configuration.model.Host} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 */
	public static Configuration getConfigurationByHost(Host inHost){
		EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
		Query query=entityManager.createQuery("SELECT conf " +
				"FROM Configuration conf " +
		"WHERE conf.host=:inHost");
		query.setParameter("inHost",inHost);
		List<Configuration> result=query.getResultList();
		if(result.size()>0)
			return result.get(0);
		return null;
	}

	/**
	 * <p>mergeConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 */
	public static Configuration mergeConfiguration(Configuration inConfiguration){
	    log.info("-- configuration --");
		if(inConfiguration!=null){
			EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
			if(inConfiguration.getHost()!=null){
				Host host=Host.mergeHost(inConfiguration.getHost());
				entityManager.flush();
				inConfiguration.setHost(host);
				Configuration conf = Configuration.getConfigurationByHost(host);
				if (inConfiguration.equals(conf)){
				    log.info("configuration exist !");
				    return conf;
				}
				log.info("configuration d'ont exist !");
				inConfiguration=entityManager.merge(inConfiguration);
			    entityManager.flush();
				return inConfiguration;
			}
		}
		return null;
	}
}


package net.ihe.gazelle.simulator.common.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.simulator.common.utils.DatabaseUtil;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import javax.validation.constraints.NotNull;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;




@Entity
/**
 * <p>TestInstanceParticipants class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("testInstanceParticipants")
@Table(name = "gs_test_instance_participants", schema = "public",uniqueConstraints = @UniqueConstraint(columnNames = {"server_aipo_id", "server_test_instance_participants_id"}))
@SequenceGenerator(name = "gs_test_instance_participants_sequence", sequenceName = "gs_test_instance_participants_id_seq", allocationSize = 1)
public class TestInstanceParticipants implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9105097522928936153L;


	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="gs_test_instance_participants_sequence")
	private Integer id;

	@ManyToOne
	@JoinColumn(name="aipo_id")
	private ActorIntegrationProfileOption actorIntegrationProfileOption;

	@ManyToOne
	@JoinColumn(name="system_id")
	private System system;

	@Column(name="server_test_instance_participants_id",nullable=false,unique=true)
	private String serverTestInstanceParticipantsId;


	@Column(name="server_aipo_id",nullable=false)
	private String serverAipoId;
	
	@ManyToMany(fetch=FetchType.LAZY)
	@Fetch(FetchMode.SUBSELECT)
	@JoinTable(name="gs_test_instance_test_instance_participants", 
	        joinColumns=@JoinColumn(name="test_instance_participants_id"), 
	        inverseJoinColumns=@JoinColumn(name="test_instance_id") ) 
	private List<TestInstance> testInstances;
	
	/**
	 * <p>Constructor for TestInstanceParticipants.</p>
	 */
	public TestInstanceParticipants(){
	    
	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
		this.id = id;
	}


	/**
	 * <p>Getter for the field <code>testInstances</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<TestInstance> getTestInstances() {
        return testInstances;
    }

    /**
     * <p>Setter for the field <code>testInstances</code>.</p>
     *
     * @param testInstances a {@link java.util.List} object.
     */
    public void setTestInstances(List<TestInstance> testInstances) {
        this.testInstances = testInstances;
    }

    /**
     * <p>Getter for the field <code>serverTestInstanceParticipantsId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getServerTestInstanceParticipantsId() {
		return serverTestInstanceParticipantsId;
	}

	/**
	 * <p>Setter for the field <code>serverTestInstanceParticipantsId</code>.</p>
	 *
	 * @param serverTestInstanceParticipantsId a {@link java.lang.String} object.
	 */
	public void setServerTestInstanceParticipantsId(
			String serverTestInstanceParticipantsId) {
		this.serverTestInstanceParticipantsId = serverTestInstanceParticipantsId;
	}

	/**
	 * <p>Getter for the field <code>serverAipoId</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getServerAipoId() {
		return serverAipoId;
	}

	/**
	 * <p>Setter for the field <code>serverAipoId</code>.</p>
	 *
	 * @param serverAipoId a {@link java.lang.String} object.
	 */
	public void setServerAipoId(String serverAipoId) {
		this.serverAipoId = serverAipoId;
	}

	/**
	 * <p>Getter for the field <code>actorIntegrationProfileOption</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfileOption} object.
	 */
	public ActorIntegrationProfileOption getActorIntegrationProfileOption() {
		return actorIntegrationProfileOption;
	}

	/**
	 * <p>Setter for the field <code>actorIntegrationProfileOption</code>.</p>
	 *
	 * @param actorIntegrationProfileOption a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfileOption} object.
	 */
	public void setActorIntegrationProfileOption(
			ActorIntegrationProfileOption actorIntegrationProfileOption) {
		this.actorIntegrationProfileOption = actorIntegrationProfileOption;
	}

	/**
	 * <p>Getter for the field <code>system</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.model.System} object.
	 */
	public System getSystem() {
		return system;
	}

	/**
	 * <p>Setter for the field <code>system</code>.</p>
	 *
	 * @param system a {@link net.ihe.gazelle.simulator.common.model.System} object.
	 */
	public void setSystem(System system) {
		this.system = system;
	}

	/**
	 * <p>getTestInstanceParticipantsByServerIds.</p>
	 *
	 * @param inServerAipoId a {@link java.lang.String} object.
	 * @param inServerTestInstanceParticipantsId a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.model.TestInstanceParticipants} object.
	 */
	public static TestInstanceParticipants getTestInstanceParticipantsByServerIds(String inServerAipoId,String inServerTestInstanceParticipantsId){
		EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
		Query query=entityManager.createQuery("SELECT tip FROM TestInstanceParticipants tip WHERE tip.serverAipoId=:inServerAipoId AND tip.serverTestInstanceParticipantsId=:inServerTestInstanceParticipantsId");
		query.setParameter("inServerAipoId", inServerAipoId);
		query.setParameter("inServerTestInstanceParticipantsId", inServerTestInstanceParticipantsId);
		List<TestInstanceParticipants> list=query.getResultList();
		if(list.size()>0){
			return list.get(0);
		}
		return null;
	}
	
	/**
	 * <p>getTestInstanceParticipants.</p>
	 *
	 * @param inServerTestInstanceParticipantsId a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.model.TestInstanceParticipants} object.
	 */
	public static TestInstanceParticipants getTestInstanceParticipants(String inServerTestInstanceParticipantsId){
		EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
		Query query=entityManager.createQuery("SELECT tip FROM TestInstanceParticipants tip WHERE tip.serverTestInstanceParticipantsId=:inServerTestInstanceParticipantsId");
		query.setParameter("inServerTestInstanceParticipantsId", inServerTestInstanceParticipantsId);
		List<TestInstanceParticipants> list=query.getResultList();
		if(list.size()>0){
			return list.get(0);
		}
		return null;
	}
	
	/**
	 * <p>getAllTestInstanceParticipants.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public static List<TestInstanceParticipants> getAllTestInstanceParticipants(){
		EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
		Query query=entityManager.createQuery("SELECT tip FROM TestInstanceParticipants tip");
		List<TestInstanceParticipants> list=query.getResultList();
		return list;
	}

	/**
	 * <p>getTestInstanceParticipantsFiltered.</p>
	 *
	 * @param inTestInstance a {@link net.ihe.gazelle.simulator.common.model.TestInstance} object.
	 * @return a {@link java.util.List} object.
	 */
	public static List<TestInstanceParticipants> getTestInstanceParticipantsFiltered(TestInstance inTestInstance){
		EntityManager em = EntityManagerService.provideEntityManager();
		Query query ;
		StringBuffer queryString = new StringBuffer() ;
		HashSet < String> prefixes = new HashSet<String>();
		HashSet < String> prefixesUsed = new HashSet<String>();
		HashMap<String ,String > mapOfJoin = new HashMap<String, String>() ;
		HashMap<String , Object > mapOfParameters = new HashMap<String, Object>() ;

		if ( inTestInstance != null )
		{
			DatabaseUtil.addANDorWHERE(queryString) ;  
			prefixes.add("TestInstanceParticipants tip") ;
			prefixes.add("TestInstance ti") ;
			mapOfJoin.put("TestInstance ti", "join ti.testInstanceParticipants titip" ) ;
			queryString.append( "titip=tip AND ti=:inTestInstance" );
			mapOfParameters.put("inTestInstance", inTestInstance);
		}
		
		List<String> listOfPrefixes = new ArrayList<String>( prefixes ) ;
		StringBuffer firstPartOfQuery = new StringBuffer() ;

		for ( int i= 0 ; i< listOfPrefixes.size() ; i++  )
		{
			if ( i == 0 ) 
			{
				firstPartOfQuery.append("SELECT distinct tip FROM "    ) ;
			}

			if ( !prefixesUsed.contains(listOfPrefixes.get(i)) )
			{
				if ( prefixesUsed.size() > 0 ) firstPartOfQuery.append(" , "  ) ;

				firstPartOfQuery.append( listOfPrefixes.get(i) ) ;
				if (mapOfJoin.containsKey(listOfPrefixes.get(i)))
					firstPartOfQuery.append(" " + mapOfJoin.get(listOfPrefixes.get(i)) +" " ) ;

				prefixesUsed.add( listOfPrefixes.get(i) ) ;
			}
		}

		queryString.insert(0, firstPartOfQuery ) ;
		if ( queryString.toString().trim().length() == 0 ) return null ;
		query = em.createQuery(queryString.toString()) ;
		List<String> listOfParameters =  new ArrayList<String>( mapOfParameters.keySet() ) ;
		for ( String param : listOfParameters )
		{
			query.setParameter( param , mapOfParameters.get(param) ) ;
		}
		List<TestInstanceParticipants> listOfTestInstanceParticipants = ( List<TestInstanceParticipants> )  query.getResultList() ; 
		return listOfTestInstanceParticipants ;
	}

    /**
     * <p>hashCode.</p>
     *
     * @return a int.
     */
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((actorIntegrationProfileOption == null) ? 0
                        : actorIntegrationProfileOption.hashCode());
        result = prime * result
                + ((serverAipoId == null) ? 0 : serverAipoId.hashCode());
        result = prime
                * result
                + ((serverTestInstanceParticipantsId == null) ? 0
                        : serverTestInstanceParticipantsId.hashCode());
        result = prime * result + ((system == null) ? 0 : system.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TestInstanceParticipants other = (TestInstanceParticipants) obj;
        if (actorIntegrationProfileOption == null) {
            if (other.actorIntegrationProfileOption != null)
                return false;
        } else if (!actorIntegrationProfileOption
                .equals(other.actorIntegrationProfileOption))
            return false;
        if (serverAipoId == null) {
            if (other.serverAipoId != null)
                return false;
        } else if (!serverAipoId.equals(other.serverAipoId))
            return false;
        if (serverTestInstanceParticipantsId == null) {
            if (other.serverTestInstanceParticipantsId != null)
                return false;
        } else if (!serverTestInstanceParticipantsId
                .equals(other.serverTestInstanceParticipantsId))
            return false;
        if (system == null) {
            if (other.system != null)
                return false;
        } else if (!system.equals(other.system))
            return false;
        return true;
    }
	
	
	
}

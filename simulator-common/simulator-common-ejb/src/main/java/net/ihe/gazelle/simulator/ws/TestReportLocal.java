package net.ihe.gazelle.simulator.ws;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.jboss.resteasy.annotations.providers.jaxb.XmlHeader;


@Local
/**
 * <p>TestReportLocal interface.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Path("/")
public interface TestReportLocal{

	/**
	 * Sends back the report of the test
	 *
	 * @param testId : id of the test (as given in the permanent URL)
	 * @param test : if test results are not stored in a unique database, we need to know where to find the test if the id is not enough do to so
	 * @return a formatted XML file
	 * @param req a {@link javax.servlet.http.HttpServletRequest} object.
	 */
	@GET
	@Path("/GetReport")
	@Produces("application/xml")
	@Wrapped
	@XmlHeader("<?xml-stylesheet type=\"text/xsl\" href=\"/xsl/testReport/testReport.xsl\"?>")
	public Response getReport(
			@QueryParam("id") String testId,
			@QueryParam("test") String test,
			@Context HttpServletRequest req);
	
	/**
	 * <p>hello.</p>
	 *
	 * @param req a {@link javax.servlet.http.HttpServletRequest} object.
	 * @return a {@link javax.ws.rs.core.Response} object.
	 */
	@GET
	@Path("/Hello")
	@Produces("text/plain")
	public Response hello(@Context HttpServletRequest req);
}

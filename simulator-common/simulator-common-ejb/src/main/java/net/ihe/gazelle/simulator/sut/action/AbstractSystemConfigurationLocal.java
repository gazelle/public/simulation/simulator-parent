package net.ihe.gazelle.simulator.sut.action;

import java.util.List;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.simulator.sut.model.Usage;

/**
 * <p>AbstractSystemConfigurationLocal interface.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public interface AbstractSystemConfigurationLocal<T extends SystemConfiguration> {

	/**
	 * <p>getAvailableSystemConfigurations.</p>
	 *
	 * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
	 */
	public FilterDataModel<T> getAvailableSystemConfigurations();

    /**
     * <p>getFilter.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<T> getFilter();

	/**
	 * <p>getSelectedSystemConfiguration.</p>
	 *
	 * @return a T object.
	 */
	public T getSelectedSystemConfiguration();

	/**
	 * <p>setSelectedSystemConfiguration.</p>
	 *
	 * @param selectedSystemConfiguration a T object.
	 */
	public void setSelectedSystemConfiguration(T selectedSystemConfiguration);

	/**
	 * <p>isDisplayListPanel.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isDisplayListPanel();

	/**
	 * <p>setDisplayListPanel.</p>
	 *
	 * @param displayListPanel a boolean.
	 */
	public void setDisplayListPanel(boolean displayListPanel);

	/**
	 * <p>isDisplayEditPanel.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isDisplayEditPanel();

	/**
	 * <p>setDisplayEditPanel.</p>
	 *
	 * @param displayEditPanel a boolean.
	 */
	public void setDisplayEditPanel(boolean displayEditPanel);

	/**
	 * <p>copySelectedConfiguration.</p>
	 *
	 * @param inConfiguration a T object.
	 */
	public void copySelectedConfiguration(T inConfiguration);

	/**
	 * <p>destroy.</p>
	 */
	public void destroy();

	/**
	 * <p>addConfiguration.</p>
	 */
	public void addConfiguration();

	/**
	 * <p>displayAllConfigurations.</p>
	 */
	public void displayAllConfigurations();

	/**
	 * <p>editSelectedConfiguration.</p>
	 */
	public void editSelectedConfiguration();

	/**
	 * <p>editSelectedConfiguration.</p>
	 *
	 * @param inConfiguration a T object.
	 */
	public void editSelectedConfiguration(T inConfiguration);

	/**
	 * <p>saveCurrentConfiguration.</p>
	 */
	public void saveCurrentConfiguration();

	/**
	 * <p>back.</p>
	 */
	public void back();
	
	/**
	 * <p>deleteSelectedSystemConfiguration.</p>
	 */
	public void deleteSelectedSystemConfiguration();
	
	/**
	 * <p>getPossibleListUsages.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public abstract List<Usage> getPossibleListUsages();

}

package net.ihe.gazelle.simulator.message.model;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.security.Identity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>IPAddress class.</p>
 *
 * @author abe
 * @version 1.0: 23/02/18
 */

@Entity
@Name("ipAddress")
@Table(name = "cmn_ip_address", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "cmn_ip_address_sequence", sequenceName = "cmn_ip_address_id_seq", allocationSize = 1)
public class IPAddress implements Serializable {

    @Id
    @GeneratedValue(generator = "cmn_ip_address_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "value")
    private String value;

    @ManyToOne(targetEntity = CompanyDetails.class)
    @JoinColumn(name = "company_details_id")
    private CompanyDetails companyDetails;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "added_on")
    private Date addedOn;

    @Column(name = "added_by")
    private String addedBy;

    public IPAddress(){

    }

    public IPAddress(CompanyDetails companyDetails, String value){
        this.companyDetails = companyDetails;
        this.value = value;
        this.addedOn = new Date();
        if (Identity.instance().isLoggedIn()){
            this.addedBy = Identity.instance().getCredentials().getUsername();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public CompanyDetails getCompanyDetails() {
        return companyDetails;
    }

    public void setCompanyDetails(CompanyDetails companyDetails) {
        this.companyDetails = companyDetails;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }
}

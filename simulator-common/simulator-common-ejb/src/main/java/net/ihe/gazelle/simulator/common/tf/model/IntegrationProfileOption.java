
/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.tf.model;

// JPA imports
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import javax.validation.constraints.NotNull;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;


@Entity
/**
 * <p>IntegrationProfileOption class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("integrationProfileOption")
@Table(name = "tf_integration_profile_option", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@SequenceGenerator(name = "tf_integration_profile_option_sequence", sequenceName = "tf_integration_profile_option_id_seq", allocationSize=1)
public class IntegrationProfileOption extends TFObject implements java.io.Serializable , Comparable<IntegrationProfileOption>  {

	/** Serial ID version of this object */
	private static final long serialVersionUID = -410911131541283760L;



	//	Attributes (existing in database as a column)
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tf_integration_profile_option_sequence")
	private Integer id;


	/**
	 * <p>Constructor for IntegrationProfileOption.</p>
	 */
	public IntegrationProfileOption() {
	}


	/**
	 * <p>Constructor for IntegrationProfileOption.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @param name a {@link java.lang.String} object.
	 * @param description a {@link java.lang.String} object.
	 */
	public IntegrationProfileOption(String keyword, String name, String description) {
		this.keyword = keyword;
		this.name = name;
		this.description = description;
	}

	/**
	 * <p>Constructor for IntegrationProfileOption.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @param name a {@link java.lang.String} object.
	 */
	public IntegrationProfileOption( String keyword, String name) {
		this.keyword = keyword;
		this.name = name;
	}


	/**
	 * <p>Constructor for IntegrationProfileOption.</p>
	 *
	 * @param oldOption a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption} object.
	 */
	public IntegrationProfileOption( IntegrationProfileOption oldOption )
	{
		if (  oldOption.getName() != null )
			this.name             = oldOption.getName()          ;
		if ( oldOption.getKeyword() != null )
			this.keyword         = new String ( oldOption.getKeyword()     ) ;
		if ( oldOption.getDescription() != null )
			this.description     = new String ( oldOption.getDescription() ) ;

	}

	// *********************************************************
	//  Getters and Setters : setup the DB columns properties
	// *********************************************************

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return this.id;
	}


	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString()
	{
		return keyword+"-"+name ;    
	}


	/**
	 * <p>findIntegrationProfileOptionWithKeyword.</p>
	 *
	 * @param keywordName a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption} object.
	 */
	public static IntegrationProfileOption findIntegrationProfileOptionWithKeyword( String keywordName )
	{
		return IntegrationProfileOption.TFObjectByKeyword(IntegrationProfileOption.class, keywordName) ;
	}

	/**
	 * <p>compareTo.</p>
	 *
	 * @param o a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption} object.
	 * @return a int.
	 */
	public int compareTo(IntegrationProfileOption o)
	{
		return keyword.compareTo(o.keyword) ;
	}


	/**
	 * <p>GetAllIntegrationProfileOptions.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public static List<IntegrationProfileOption> GetAllIntegrationProfileOptions()
	{
		return IntegrationProfileOption.ListAllTFObjects(IntegrationProfileOption.class) ;
	}

	/**
	 * <p>mergeIntegrationProfileOption.</p>
	 *
	 * @param inIntegrationProfileOption a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption} object.
	 */
	public static IntegrationProfileOption mergeIntegrationProfileOption(IntegrationProfileOption inIntegrationProfileOption){
		if(inIntegrationProfileOption!=null){
			EntityManager  entityManager = ( EntityManager) Component.getInstance("entityManager") ;
			IntegrationProfileOption integrationProfileOption=IntegrationProfileOption.findIntegrationProfileOptionWithKeyword(inIntegrationProfileOption.getKeyword());
			if(integrationProfileOption==null){
				integrationProfileOption=entityManager.merge(inIntegrationProfileOption);
			}
			return integrationProfileOption;
		}
		return null;
	}
}

package net.ihe.gazelle.simulator.common.action;

import net.ihe.gazelle.simulator.common.model.Home;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * <p>HomeManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("homeManagerBean")
@Scope(ScopeType.PAGE)
public class HomeManager implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1111092307583013570L;

	private Home selectedHome = null;
	private boolean mainContentEditMode;
	private boolean editTitle = false;

	/**
	 * <p>Getter for the field <code>selectedHome</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.model.Home} object.
	 */
	public Home getSelectedHome() {
		return selectedHome;
	}

	/**
	 * <p>isEditTitle.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isEditTitle() {
		return editTitle;
	}

	/**
	 * <p>Setter for the field <code>editTitle</code>.</p>
	 *
	 * @param editTitle a boolean.
	 */
	public void setEditTitle(boolean editTitle) {
		this.editTitle = editTitle;
	}

	/**
	 * <p>Setter for the field <code>selectedHome</code>.</p>
	 *
	 * @param selectedHome a {@link net.ihe.gazelle.simulator.common.model.Home} object.
	 */
	public void setSelectedHome(Home selectedHome) {
		this.selectedHome = selectedHome;
	}

	/**
	 * <p>isMainContentEditMode.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isMainContentEditMode() {
		return mainContentEditMode;
	}

	/**
	 * <p>Setter for the field <code>mainContentEditMode</code>.</p>
	 *
	 * @param mainContentEditMode a boolean.
	 */
	public void setMainContentEditMode(boolean mainContentEditMode) {
		this.mainContentEditMode = mainContentEditMode;
	}

	/**
	 * <p>initializeHome.</p>
	 */
	@Create
	public void initializeHome() {
		selectedHome = Home.getHomeForSelectedLanguage();
	}

	/**
	 * <p>editMainContent.</p>
	 */
	public void editMainContent() {
		mainContentEditMode = true;
	}

	/**
	 * <p>save.</p>
	 */
	public void save() {
		selectedHome = selectedHome.save();
		mainContentEditMode = false;
		editTitle = false;
	}

	/**
	 * <p>cancel.</p>
	 */
	public void cancel() {
		mainContentEditMode = false;
		editTitle = false;
	}

	/**
	 * <p>changeLanguage.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String changeLanguage() {
		ExternalContext ex = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ex.getRequest();
		selectedHome = Home.getHomeForSelectedLanguage();
		return request.getRequestURI().toString();
	}
}

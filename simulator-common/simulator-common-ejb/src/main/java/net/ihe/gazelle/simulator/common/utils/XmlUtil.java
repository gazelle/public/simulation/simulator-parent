package net.ihe.gazelle.simulator.common.utils;

import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.c14n.InvalidCanonicalizerException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * <p>XmlUtil class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class XmlUtil {

	private static final Charset UTF_8 = StandardCharsets.UTF_8;

	static {
		org.apache.xml.security.Init.init();
	}

	/**
	 * <p>outputDOM.</p>
	 *
	 * @param node a {@link org.w3c.dom.Node} object.
	 * @return a {@link java.lang.String} object.
	 * @throws org.apache.xml.security.c14n.CanonicalizationException if any.
	 * @throws java.io.IOException if any.
	 * @throws org.apache.xml.security.c14n.InvalidCanonicalizerException if any.
	 */
	public static String outputDOM(Node node) throws CanonicalizationException, IOException, InvalidCanonicalizerException {
		return new String(outputDOMAsBytes(node), UTF_8);
	}

	/**
	 * <p>outputDOMAsBytes.</p>
	 *
	 * @param node a {@link org.w3c.dom.Node} object.
	 * @return an array of byte.
	 * @throws org.apache.xml.security.c14n.InvalidCanonicalizerException if any.
	 * @throws org.apache.xml.security.c14n.CanonicalizationException if any.
	 * @throws java.io.IOException if any.
	 */
	public static byte[] outputDOMAsBytes(Node node) throws InvalidCanonicalizerException, CanonicalizationException, IOException {

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			OutputStream os = new BufferedOutputStream(bos);
			os.write(Canonicalizer.getInstance(Canonicalizer.ALGO_ID_C14N_WITH_COMMENTS).canonicalizeSubtree(node));
			os.flush();
			return bos.toByteArray();

	}

	/**
	 * <p>formatDocument.</p>
	 *
	 * @param document a {@link org.w3c.dom.Document} object.
	 * @return a {@link org.w3c.dom.Document} object.
	 * @throws java.lang.Exception if any.
	 */
	public static Document formatDocument(Document document) throws Exception {
		String formatedDocument = outputDOM(document);
		return parse(formatedDocument);
	}

	/**
	 * <p>parse.</p>
	 *
	 * @param document a {@link java.lang.String} object.
	 * @return a {@link org.w3c.dom.Document} object.
	 * @throws java.lang.Exception if any.
	 */
	public static Document parse(String document) throws Exception {
		ByteArrayInputStream bais = new ByteArrayInputStream(document.getBytes(UTF_8));

		// Rebuild the document
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		builderFactory.setNamespaceAware(true);

		DocumentBuilder builder = builderFactory.newDocumentBuilder();
		Document documentDOM = builder.parse(bais);
		bais.close();
		return documentDOM;
	}

}

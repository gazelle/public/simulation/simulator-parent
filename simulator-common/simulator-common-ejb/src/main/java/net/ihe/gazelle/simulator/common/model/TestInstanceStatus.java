package net.ihe.gazelle.simulator.common.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import javax.validation.constraints.NotNull;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

/**
 * <p>TestInstanceStatus class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Status")
@Entity
@Name("testInstanceStatus")
@Table(name = "gs_test_instance_status", schema = "public")
@SequenceGenerator(name = "gs_test_instance_status_sequence", sequenceName = "gs_test_instance_status_id_seq", allocationSize = 1)
public class TestInstanceStatus implements Serializable{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /** Constant <code>STATUS_STARTED_STRING="started"</code> */
    public final static String STATUS_STARTED_STRING         = "started"            ;
    /** Constant <code>STATUS_COMPLETED_STRING="completed"</code> */
    public final static String STATUS_COMPLETED_STRING       = "completed"          ;

    private static TestInstanceStatus STATUS_STARTED             ;
    private static TestInstanceStatus STATUS_COMPLETED           ;
    
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="gs_test_instance_status_sequence")
    private Integer id;

    @XmlElement(name="Keyword")
    @Column(name="keyword")
    private String keyword;

    @Column(name="label_to_display")
    private String labelToDisplay ;

    @Column(name="description")
    private String description;
    
    @OneToMany(mappedBy = "testInstanceStatus", fetch = FetchType.LAZY)
    private List<TestInstance> testInstances;
    
    //--------- constructors ------------------
    
    /**
     * <p>Constructor for TestInstanceStatus.</p>
     */
    public TestInstanceStatus(){}
    
    /**
     * <p>Constructor for TestInstanceStatus.</p>
     *
     * @param tis a {@link net.ihe.gazelle.simulator.common.model.TestInstanceStatus} object.
     */
    public TestInstanceStatus(TestInstanceStatus tis){
        if (tis != null){
            this.description = tis.description;
            this.keyword = tis.keyword;
            this.labelToDisplay = tis.labelToDisplay;
        }
    }

    /**
     * <p>Constructor for TestInstanceStatus.</p>
     *
     * @param keyword a {@link java.lang.String} object.
     * @param labelToDisplay a {@link java.lang.String} object.
     * @param description a {@link java.lang.String} object.
     */
    public TestInstanceStatus(String keyword, String labelToDisplay,
            String description) {
        this.keyword = keyword;
        this.labelToDisplay = labelToDisplay;
        this.description = description;
    }

    
    
    //--------- getters and setters -----------
    
    /**
     * <p>getSTATUS_STARTED.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.model.TestInstanceStatus} object.
     */
    public static TestInstanceStatus getSTATUS_STARTED() {
        if (STATUS_STARTED == null){
            STATUS_STARTED = TestInstanceStatus.getStatusByKeyword(STATUS_STARTED_STRING);
        }
        return STATUS_STARTED;
    }

    /**
     * <p>getSTATUS_COMPLETED.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.model.TestInstanceStatus} object.
     */
    public static TestInstanceStatus getSTATUS_COMPLETED() {
        if (STATUS_COMPLETED == null){
            STATUS_COMPLETED = TestInstanceStatus.getStatusByKeyword(STATUS_COMPLETED_STRING);
        }
        return STATUS_COMPLETED;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>keyword</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * <p>Setter for the field <code>keyword</code>.</p>
     *
     * @param keyword a {@link java.lang.String} object.
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * <p>Getter for the field <code>labelToDisplay</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    /**
     * <p>Setter for the field <code>labelToDisplay</code>.</p>
     *
     * @param labelToDisplay a {@link java.lang.String} object.
     */
    public void setLabelToDisplay(String labelToDisplay) {
        this.labelToDisplay = labelToDisplay;
    }

    /**
     * <p>Getter for the field <code>description</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>Setter for the field <code>description</code>.</p>
     *
     * @param description a {@link java.lang.String} object.
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /**
     * <p>Getter for the field <code>testInstances</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TestInstance> getTestInstances() {
        return testInstances;
    }

    /**
     * <p>Setter for the field <code>testInstances</code>.</p>
     *
     * @param testInstances a {@link java.util.List} object.
     */
    public void setTestInstances(List<TestInstance> testInstances) {
        this.testInstances = testInstances;
    }
    
    //--------- equals ------------------------
    
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
        result = prime * result
                + ((labelToDisplay == null) ? 0 : labelToDisplay.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TestInstanceStatus other = (TestInstanceStatus) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (keyword == null) {
            if (other.keyword != null)
                return false;
        } else if (!keyword.equals(other.keyword))
            return false;
        if (labelToDisplay == null) {
            if (other.labelToDisplay != null)
                return false;
        } else if (!labelToDisplay.equals(other.labelToDisplay))
            return false;
        return true;
    }
    
  //--------- methods -----------------------
    
    /**
     * <p>getStatusByKeyword.</p>
     *
     * @param keyword a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.simulator.common.model.TestInstanceStatus} object.
     */
    public static TestInstanceStatus getStatusByKeyword(String keyword){
        EntityManager em = (EntityManager)Component.getInstance("entityManager") ;
        Query query = em.createQuery("FROM TestInstanceStatus status where status.keyword=:key");
        query.setParameter("key",keyword ) ;
        List <TestInstanceStatus> tps = (List <TestInstanceStatus>)query.getResultList(); 
        
        if ( (tps == null) || ( tps.size() != 1) )
        {
            return null;
        }
        
        return tps.get(0)   ;
        
    }

}

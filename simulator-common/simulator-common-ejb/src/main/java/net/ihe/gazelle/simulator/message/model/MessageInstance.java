package net.ihe.gazelle.simulator.message.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * <b>Class description</b>: MessageInstance
 *
 * This class is used to store the request/response sent/received in an instance of a transaction between a SUT and the simulator
 *
 * @author 	Anne-Gaëlle Bergé / IHE Europe
 */

@Entity
@Name("messageInstance")
@Table(name="cmn_message_instance", schema="public", uniqueConstraints=@UniqueConstraint(columnNames="id"))
@SequenceGenerator(name="cmn_message_instance_sequence", sequenceName="cmn_message_instance_id_seq", allocationSize=1)
public class MessageInstance implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8457580323530563536L;

	@Id
	@NotNull
	@GeneratedValue(generator="cmn_message_instance_sequence", strategy=GenerationType.SEQUENCE)
	@Column(name="id", nullable=false, unique=true)
	private Integer id;
	
	@JoinColumn(name="issuing_actor")
	@ManyToOne(fetch=FetchType.EAGER)
	private Actor issuingActor;
	
	@Column(name="validation_status")
	private String validationStatus;
	
	@Column(name="validation_detailed_result")
	private byte[] validationDetailedResult;

	@Column(name="content")
	private byte[] content;
	
	@Column(name="type")
	private String type;
	
	@Column(name="issuer")
	private String issuer;

	@Column(name = "issuer_ip_address")
	private String issuerIpAddress;

	@Column(name = "payload")
	private byte[] payload;

	@OneToMany(targetEntity = MessageInstanceMetadata.class, mappedBy = "messageInstance")
	@Cascade(CascadeType.MERGE)
	private List<MessageInstanceMetadata> metadataList;

	/**
	 * <p>Constructor for MessageInstance.</p>
	 */
	public MessageInstance()
	{
		
	}

	/**
	 * <p>Getter for the field <code>metadataList</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<MessageInstanceMetadata> getMetadataList() {
		return metadataList;
	}

	/**
	 * <p>Setter for the field <code>metadataList</code>.</p>
	 *
	 * @param metadataList a {@link java.util.List} object.
	 */
	public void setMetadataList(List<MessageInstanceMetadata> metadataList) {
		this.metadataList = metadataList;
	}

	/**
	 * <p>Getter for the field <code>issuingActor</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public Actor getIssuingActor() {
		return issuingActor;
	}

	/**
	 * <p>Setter for the field <code>issuingActor</code>.</p>
	 *
	 * @param issuingActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public void setIssuingActor(Actor issuingActor) {
		this.issuingActor = issuingActor;
	}

	/**
	 * <p>Getter for the field <code>validationStatus</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getValidationStatus() {
		return validationStatus;
	}

	/**
	 * <p>Setter for the field <code>validationStatus</code>.</p>
	 *
	 * @param validationStatus a {@link java.lang.String} object.
	 */
	public void setValidationStatus(String validationStatus) {
		this.validationStatus = validationStatus;
	}

	/**
	 * <p>Getter for the field <code>validationDetailedResult</code>.</p>
	 *
	 * @return an array of byte.
	 */
	public byte[] getValidationDetailedResult() {
		return validationDetailedResult;
	}

	/**
	 * <p>Setter for the field <code>validationDetailedResult</code>.</p>
	 *
	 * @param validationDetailedResult an array of byte.
	 */
	public void setValidationDetailedResult(byte[] validationDetailedResult) {
		this.validationDetailedResult = validationDetailedResult;
	}

	/**
	 * <p>Getter for the field <code>content</code>.</p>
	 *
	 * @return an array of byte.
	 */
	public byte[] getContent()
	{
		return this.content;
	}

	/**
	 * <p>isEmpty.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isEmpty(){
		return (content == null || type == null);
	}
	
	/**
	 * <p>getContentAsString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getContentAsString() {
		if (this.content == null) {
			return null;
		}
		else {
			return new String(this.content, StandardCharsets.UTF_8);
		}
	}

	/**
	 * <p>Setter for the field <code>content</code>.</p>
	 *
	 * @param content an array of byte.
	 */
	public void setContent(byte[] content) {
		this.content = content;
	}
	
	/**
	 * <p>Setter for the field <code>content</code>.</p>
	 *
	 * @param content a {@link java.lang.String} object.
	 */
	public void setContent(String content)
	{
		if (content == null) {
			this.content = null;
		}
		else {
			setContent(content.getBytes(StandardCharsets.UTF_8));
		}
	}

    /**
     * Get full message with the soap envelope
     *
     * @return message as byte
     */
	public byte[] getPayload() {
		return payload;
	}

    /**
     * Set the full message with the soap envelope
     *
     * @param payload
     */
	public void setPayload(byte[] payload) {
		this.payload = payload;
	}

	/**
	 * <p>Getter for the field <code>type</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getType() {
		return type;
	}

	/**
	 * <p>Setter for the field <code>type</code>.</p>
	 *
	 * @param type a {@link java.lang.String} object.
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * <p>Getter for the field <code>issuer</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getIssuer() {
		return issuer;
	}

	/**
	 * <p>Setter for the field <code>issuer</code>.</p>
	 *
	 * @param issuer a {@link java.lang.String} object.
	 */
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}

	public String getIssuerIpAddress() {
		return issuerIpAddress;
	}

	public void setIssuerIpAddress(String issuerIpAddress) {
		this.issuerIpAddress = issuerIpAddress;
	}

	public MessageInstance save() {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		MessageInstance result = entityManager.merge(this);
		entityManager.flush();
		return result;
	}
}

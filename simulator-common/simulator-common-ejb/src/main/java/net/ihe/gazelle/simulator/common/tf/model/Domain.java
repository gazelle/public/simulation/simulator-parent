package net.ihe.gazelle.simulator.common.tf.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import javax.validation.constraints.NotNull;

import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import org.jboss.seam.annotations.Name;

@Entity
/**
 * <p>Domain class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("domain")
@Table(name="tf_domain", schema="public", uniqueConstraints=@UniqueConstraint(columnNames="keyword"))
@SequenceGenerator(name="tf_domain_sequence", sequenceName="tf_domain_id_seq", allocationSize=1)
public class Domain extends TFObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6652832577180218448L;

	@Id
	@GeneratedValue(generator="tf_domain_sequence", strategy=GenerationType.SEQUENCE)
	@NotNull
	@Column(name="id", nullable=false, unique=true)
	private Integer id;
	
	@ManyToMany
	@JoinTable(name="tf_domain_integration_profiles", 
			joinColumns=@JoinColumn(name="domain_id"),
			inverseJoinColumns=@JoinColumn(name="integration_profile_id"),
			uniqueConstraints=@UniqueConstraint(columnNames={"integration_profile_id","domain_id"}))
	private List<IntegrationProfile> integrationProfiles;
	
	/**
	 * <p>Constructor for Domain.</p>
	 */
	public Domain()
	{
		
	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * <p>Getter for the field <code>integrationProfiles</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<IntegrationProfile> getIntegrationProfiles() {
		return integrationProfiles;
	}

	/**
	 * <p>Setter for the field <code>integrationProfiles</code>.</p>
	 *
	 * @param integrationProfiles a {@link java.util.List} object.
	 */
	public void setIntegrationProfiles(List<IntegrationProfile> integrationProfiles) {
		this.integrationProfiles = integrationProfiles;
	}
	
	/**
	 * <p>listAllDomains.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public static List<Domain> listAllDomains()
	{
		DomainQuery query = new DomainQuery();
		query.keyword().order(true);
		return query.getList();
	}
	
	/**
	 * <p>getDomainByKeyword.</p>
	 *
	 * @param inKeyword a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
	 */
	public static Domain getDomainByKeyword(String inKeyword)
	{
		if (inKeyword == null){
            return null;
        } else {
            DomainQuery query = new DomainQuery();
            query.keyword().like(inKeyword, HQLRestrictionLikeMatchMode.EXACT);
            return query.getUniqueResult();
        }
	}
	
	/**
	 * <p>getDomainByKeyword.</p>
	 *
	 * @param inKeyword a {@link java.lang.String} object.
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
	 */
	public static Domain getDomainByKeyword(String inKeyword, EntityManager entityManager)
	{
        if (inKeyword == null){
            return null;
        } else {
            DomainQuery query = new DomainQuery(entityManager);
            query.keyword().like(inKeyword, HQLRestrictionLikeMatchMode.EXACT);
            return query.getUniqueResult();
        }
	}
}

package net.ihe.gazelle.simulator.message.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;

import javax.persistence.EntityManager;

/**
 * <p>MessageInstanceDAO class.</p>
 *
 * @author aberge
 * @version 1.0: 05/10/17
 */

public class MessageInstanceDAO {

    public static MessageInstance getMessageInstanceByMessageIdAndIssuer(String messageID, String issuer){
        MessageInstanceQuery query = new MessageInstanceQuery();
        query.addRestriction(HQLRestrictions.and(
                query.metadataList().label().eqRestriction(MessageInstanceMetadata.MESSAGE_ID_LABEL),
                query.metadataList().value().eqRestriction(messageID)
        ));
        query.issuer().eqRestriction(issuer);
        return query.getUniqueResult();
    }

    public static MessageInstance save(MessageInstance messageInstance) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        MessageInstance result = entityManager.merge(messageInstance);
        entityManager.flush();
        return result;
    }


}

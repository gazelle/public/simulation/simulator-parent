package net.ihe.gazelle.simulator.common.tf.model;

/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HQLQueryBuilder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import org.jboss.seam.annotations.Name;

/**
 *  <b>Class Description :  </b>AffinityDomain<br><br>
 * This class describes the affinity domain.
 *
 * AffinityDomain possesses the following attributes:
 * <ul>
 * <li><b>id</b> id of the instance in the database</li>
 * <li><b>labelToDisplay</b> The name which will be displayed in the GUI</li>
 * <li><b>profile</b> The integration profile the affinity domain refers to (typically it would be XCA or XDS-b)</li>
 * <li><b>keyword</b> Keyword describing the object</li>
 * <li><b>supportedTransactions</b> The list of transactions which can be performed in this affinity domain</li>
 * </ul>
 *
 * @author                	Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version                	1.0 - 2010, November 18th
 */

@Entity
@Name("affinityDomain")
@Table(name="affinity_domain", schema="public")
@XmlAccessorType(XmlAccessType.FIELD)
@SequenceGenerator(name="affinity_domain_sequence", sequenceName="affinity_domain_id_seq", allocationSize=1)
public class AffinityDomain implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@GeneratedValue(generator="affinity_domain_sequence", strategy=GenerationType.SEQUENCE)
	@Column(name="id")
	@XmlTransient
	private Integer id;
	
	@Column(name="label_to_display")
	private String labelToDisplay;
	
	@Column(name="profile")
	private String profile;
	
	@Column(name="keyword", unique = true)
	private String keyword;
	
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name="affinity_domain_transactions", 
			joinColumns=@JoinColumn(name="affinity_domain_id"),
			inverseJoinColumns=@JoinColumn(name="transaction_id"),
			uniqueConstraints=@UniqueConstraint(columnNames={"affinity_domain_id", "transaction_id"}))
	private List<Transaction> supportedTransactions;
	
	
	/**
	 * Constructor
	 */
	public AffinityDomain()
	{
		
	}

	/**
	 * Getters and Setters
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}


	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * <p>Getter for the field <code>labelToDisplay</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	@FilterLabel
	public String getLabelToDisplay() {
		return labelToDisplay;
	}


	/**
	 * <p>Setter for the field <code>labelToDisplay</code>.</p>
	 *
	 * @param labelToDisplay a {@link java.lang.String} object.
	 */
	public void setLabelToDisplay(String labelToDisplay) {
		this.labelToDisplay = labelToDisplay;
	}

	/**
	 * <p>Getter for the field <code>keyword</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getKeyword() {
		return keyword;
	}


	/**
	 * <p>Setter for the field <code>keyword</code>.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}


	/**
	 * <p>Setter for the field <code>profile</code>.</p>
	 *
	 * @param profile a {@link java.lang.String} object.
	 */
	public void setProfile(String profile) {
		this.profile = profile;
	}

	/**
	 * <p>Getter for the field <code>profile</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getProfile() {
		return profile;
	}

	/**
	 * <p>Getter for the field <code>supportedTransactions</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<Transaction> getSupportedTransactions() {
		return supportedTransactions;
	}


	/**
	 * <p>Setter for the field <code>supportedTransactions</code>.</p>
	 *
	 * @param supportedTransactions a {@link java.util.List} object.
	 */
	public void setSupportedTransactions(List<Transaction> supportedTransactions) {
		this.supportedTransactions = supportedTransactions;
	}
	

	/**
	 * Returns the list of affinity domains stored in database according the given profile.
	 * If given profile is null, returns ALL the affinity domains contained in database
	 *
	 * @param inProfile : profile for which we need the affinity domains (XCA or XDS-b)
	 * @return null if none found
	 */
	public static List<AffinityDomain> listAllAffinityDomainsByProfile(String inProfile)
	{
		AffinityDomainQuery query = new AffinityDomainQuery();
		if (inProfile != null && !inProfile.isEmpty()){
		query.profile().eq(inProfile);
		}
		query.labelToDisplay().order(true);
		return query.getListNullIfEmpty();
	}
	
	/**
	 * <p>listAllAffinityDomains.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public static List<AffinityDomain> listAllAffinityDomains(){
		AffinityDomainQuery query = new AffinityDomainQuery();
		query.labelToDisplay().order(true);
		return query.getList();
	}

	/**
	 * hashCode and equals methods
	 *
	 * @param key a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.AffinityDomain} object.
	 */
	public static AffinityDomain getAffinityDomainByKeyword(String key){
		AffinityDomainQuery aa = new AffinityDomainQuery();
		aa.keyword().eq(key);
		return aa.getUniqueResult();
	}

	/**
	 * <p>getAffinityDomainByKeyword.</p>
	 *
	 * @param key a {@link java.lang.String} object.
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.AffinityDomain} object.
	 */
	public static AffinityDomain getAffinityDomainByKeyword(String key, EntityManager entityManager){
		AffinityDomainQuery query = new AffinityDomainQuery(new HQLQueryBuilder<AffinityDomain>(entityManager, AffinityDomain.class));
		query.keyword().eq(key);
		return query.getUniqueResult();
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AffinityDomain other = (AffinityDomain) obj;
		if (keyword == null) {
			if (other.keyword != null)
				return false;
		} else if (!keyword.equals(other.keyword))
			return false;
		return true;
	}

}

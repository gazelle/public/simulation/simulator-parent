package net.ihe.gazelle.simulator.ws;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.ejb.Local;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Local
/**
 * <p>ValueSetCheckerApi interface.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Path("/valueSets")
public interface ValueSetCheckerApi {

    /**
     * <p>checkAllValueSets.</p>
     *
     * @return a {@link javax.ws.rs.core.Response} object.
     */
    @GET
    @Path("/checkAll")
    @Produces("application/xml")
    @Wrapped
    //@XmlHeader("<?xml-stylesheet type=\"text/xsl\" href=\"/xsl/testReport/testReport.xsl\"?>")
    javax.ws.rs.core.Response checkAllValueSets();

    /**
     * <p>checkValueSet.</p>
     *
     * @param keyword a {@link java.lang.String} object.
     * @return a {@link javax.ws.rs.core.Response} object.
     */
    @GET
    @Path("/check")
    @Produces("plain/text")
    javax.ws.rs.core.Response checkValueSet(@QueryParam("keyword") String keyword);
}

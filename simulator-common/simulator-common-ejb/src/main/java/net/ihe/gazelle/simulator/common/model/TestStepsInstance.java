package net.ihe.gazelle.simulator.common.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration;
import net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCUConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2InitiatorConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3InitiatorConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration;
import net.ihe.gazelle.simulator.common.configuration.model.SyslogConfiguration;
import net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration;

import javax.validation.constraints.NotNull;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Entity
/**
 * <p>TestStepsInstance class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("testStepsInstance")
@Table(name = "gs_test_steps_instance", schema = "public")
@SequenceGenerator(name = "gs_test_steps_instance_sequence", sequenceName = "gs_test_steps_instance_id_seq", allocationSize = 1)
public class TestStepsInstance implements Serializable{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @SuppressWarnings("unused")
    @Logger
    private static Log log;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="gs_test_steps_instance_sequence")
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name= "testInstance_id")
    private TestInstance testInstance;
    
    @ManyToOne
    @JoinColumn(name = "hl7v2_initiator_config_id")
    private HL7V2InitiatorConfiguration hL7V2InitiatorConfiguration;
    
    @ManyToOne
    @JoinColumn(name = "hl7v2_responder_config_id")
    private HL7V2ResponderConfiguration hL7V2ResponderConfiguration;
    
    @ManyToOne
    @JoinColumn(name = "hl7v3_initiator_config_id")
    private HL7V3InitiatorConfiguration hL7V3InitiatorConfiguration;
    
    @ManyToOne
    @JoinColumn(name = "hl7v3_responder_config_id")
    private HL7V3ResponderConfiguration hL7V3ResponderConfiguration;
    
    @ManyToOne
    @JoinColumn(name = "dicom_scp_config_id")
    private DicomSCPConfiguration dicomSCPConfiguration;
    
    @ManyToOne
    @JoinColumn(name = "dicom_scu_config_id")
    private DicomSCUConfiguration dicomSCUConfiguration;
    
    @ManyToOne
    @JoinColumn(name = "web_service_config_id")
    private WebServiceConfiguration webServiceConfiguration;
    
    @ManyToOne
    @JoinColumn(name = "syslog_config_id")
    private SyslogConfiguration syslogConfiguration;
    
    @OneToMany(mappedBy = "testStepsInstance", fetch = FetchType.LAZY)
    private List<ContextualInformationInstance> contextualInformationInstances;
    
    
    /**
     * <p>Constructor for TestStepsInstance.</p>
     */
    public TestStepsInstance(){}
    
    
    /**
     * <p>Getter for the field <code>contextualInformationInstances</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<ContextualInformationInstance> getContextualInformationInstances() {
        return contextualInformationInstances;
    }


    /**
     * <p>Setter for the field <code>contextualInformationInstances</code>.</p>
     *
     * @param contextualInformationInstances a {@link java.util.List} object.
     */
    public void setContextualInformationInstances(
            List<ContextualInformationInstance> contextualInformationInstances) {
        this.contextualInformationInstances = contextualInformationInstances;
    }

    

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }


    /**
     * <p>Getter for the field <code>testInstance</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.model.TestInstance} object.
     */
    public TestInstance getTestInstance() {
        return testInstance;
    }

    /**
     * <p>Setter for the field <code>testInstance</code>.</p>
     *
     * @param testInstance a {@link net.ihe.gazelle.simulator.common.model.TestInstance} object.
     */
    public void setTestInstance(TestInstance testInstance) {
        this.testInstance = testInstance;
    }




    /**
     * <p>Getter for the field <code>hL7V2InitiatorConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2InitiatorConfiguration} object.
     */
    public HL7V2InitiatorConfiguration gethL7V2InitiatorConfiguration() {
        return hL7V2InitiatorConfiguration;
    }



    /**
     * <p>Setter for the field <code>hL7V2InitiatorConfiguration</code>.</p>
     *
     * @param hL7V2InitiatorConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2InitiatorConfiguration} object.
     */
    public void sethL7V2InitiatorConfiguration(
            HL7V2InitiatorConfiguration hL7V2InitiatorConfiguration) {
        this.hL7V2InitiatorConfiguration = hL7V2InitiatorConfiguration;
    }



    /**
     * <p>Getter for the field <code>hL7V2ResponderConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration} object.
     */
    public HL7V2ResponderConfiguration gethL7V2ResponderConfiguration() {
        return hL7V2ResponderConfiguration;
    }



    /**
     * <p>Setter for the field <code>hL7V2ResponderConfiguration</code>.</p>
     *
     * @param hL7V2ResponderConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration} object.
     */
    public void sethL7V2ResponderConfiguration(
            HL7V2ResponderConfiguration hL7V2ResponderConfiguration) {
        this.hL7V2ResponderConfiguration = hL7V2ResponderConfiguration;
    }



    /**
     * <p>Getter for the field <code>hL7V3InitiatorConfiguration</code>.</p>
     *
     * @return a net$ihe$gazelle$simulator$common$configuration$HL7$model$HL7V3InitiatorConfiguration object.
     */
    public HL7V3InitiatorConfiguration gethL7V3InitiatorConfiguration() {
        return hL7V3InitiatorConfiguration;
    }



    /**
     * <p>Setter for the field <code>hL7V3InitiatorConfiguration</code>.</p>
     *
     * @param hL7V3InitiatorConfiguration a net$ihe$gazelle$simulator$common$configuration$HL7$model$HL7V3InitiatorConfiguration object.
     */
    public void sethL7V3InitiatorConfiguration(
            HL7V3InitiatorConfiguration hL7V3InitiatorConfiguration) {
        this.hL7V3InitiatorConfiguration = hL7V3InitiatorConfiguration;
    }



    /**
     * <p>Getter for the field <code>hL7V3ResponderConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration} object.
     */
    public HL7V3ResponderConfiguration gethL7V3ResponderConfiguration() {
        return hL7V3ResponderConfiguration;
    }



    /**
     * <p>Setter for the field <code>hL7V3ResponderConfiguration</code>.</p>
     *
     * @param hL7V3ResponderConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration} object.
     */
    public void sethL7V3ResponderConfiguration(
            HL7V3ResponderConfiguration hL7V3ResponderConfiguration) {
        this.hL7V3ResponderConfiguration = hL7V3ResponderConfiguration;
    }



    /**
     * <p>Getter for the field <code>dicomSCPConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration} object.
     */
    public DicomSCPConfiguration getDicomSCPConfiguration() {
        return dicomSCPConfiguration;
    }



    /**
     * <p>Setter for the field <code>dicomSCPConfiguration</code>.</p>
     *
     * @param dicomSCPConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration} object.
     */
    public void setDicomSCPConfiguration(DicomSCPConfiguration dicomSCPConfiguration) {
        this.dicomSCPConfiguration = dicomSCPConfiguration;
    }



    /**
     * <p>Getter for the field <code>dicomSCUConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCUConfiguration} object.
     */
    public DicomSCUConfiguration getDicomSCUConfiguration() {
        return dicomSCUConfiguration;
    }



    /**
     * <p>Setter for the field <code>dicomSCUConfiguration</code>.</p>
     *
     * @param dicomSCUConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCUConfiguration} object.
     */
    public void setDicomSCUConfiguration(DicomSCUConfiguration dicomSCUConfiguration) {
        this.dicomSCUConfiguration = dicomSCUConfiguration;
    }



    /**
     * <p>Getter for the field <code>webServiceConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration} object.
     */
    public WebServiceConfiguration getWebServiceConfiguration() {
        return webServiceConfiguration;
    }



    /**
     * <p>Setter for the field <code>webServiceConfiguration</code>.</p>
     *
     * @param webServiceConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration} object.
     */
    public void setWebServiceConfiguration(
            WebServiceConfiguration webServiceConfiguration) {
        this.webServiceConfiguration = webServiceConfiguration;
    }



    /**
     * <p>Getter for the field <code>syslogConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.SyslogConfiguration} object.
     */
    public SyslogConfiguration getSyslogConfiguration() {
        return syslogConfiguration;
    }



    /**
     * <p>Setter for the field <code>syslogConfiguration</code>.</p>
     *
     * @param syslogConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.SyslogConfiguration} object.
     */
    public void setSyslogConfiguration(SyslogConfiguration syslogConfiguration) {
        this.syslogConfiguration = syslogConfiguration;
    }



    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((dicomSCPConfiguration == null) ? 0 : dicomSCPConfiguration
                        .hashCode());
        result = prime
                * result
                + ((dicomSCUConfiguration == null) ? 0 : dicomSCUConfiguration
                        .hashCode());
        result = prime
                * result
                + ((hL7V2InitiatorConfiguration == null) ? 0
                        : hL7V2InitiatorConfiguration.hashCode());
        result = prime
                * result
                + ((hL7V2ResponderConfiguration == null) ? 0
                        : hL7V2ResponderConfiguration.hashCode());
        result = prime
                * result
                + ((hL7V3InitiatorConfiguration == null) ? 0
                        : hL7V3InitiatorConfiguration.hashCode());
        result = prime
                * result
                + ((hL7V3ResponderConfiguration == null) ? 0
                        : hL7V3ResponderConfiguration.hashCode());
        result = prime
                * result
                + ((syslogConfiguration == null) ? 0 : syslogConfiguration
                        .hashCode());
        result = prime * result
                + ((testInstance == null) ? 0 : testInstance.hashCode());
        result = prime
                * result
                + ((webServiceConfiguration == null) ? 0
                        : webServiceConfiguration.hashCode());
        return result;
    }



    /** {@inheritDoc} */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TestStepsInstance other = (TestStepsInstance) obj;
        if (dicomSCPConfiguration == null) {
            if (other.dicomSCPConfiguration != null)
                return false;
        } else if (!dicomSCPConfiguration.equals(other.dicomSCPConfiguration))
            return false;
        if (dicomSCUConfiguration == null) {
            if (other.dicomSCUConfiguration != null)
                return false;
        } else if (!dicomSCUConfiguration.equals(other.dicomSCUConfiguration))
            return false;
        if (hL7V2InitiatorConfiguration == null) {
            if (other.hL7V2InitiatorConfiguration != null)
                return false;
        } else if (!hL7V2InitiatorConfiguration
                .equals(other.hL7V2InitiatorConfiguration))
            return false;
        if (hL7V2ResponderConfiguration == null) {
            if (other.hL7V2ResponderConfiguration != null)
                return false;
        } else if (!hL7V2ResponderConfiguration
                .equals(other.hL7V2ResponderConfiguration))
            return false;
        if (hL7V3InitiatorConfiguration == null) {
            if (other.hL7V3InitiatorConfiguration != null)
                return false;
        } else if (!hL7V3InitiatorConfiguration
                .equals(other.hL7V3InitiatorConfiguration))
            return false;
        if (hL7V3ResponderConfiguration == null) {
            if (other.hL7V3ResponderConfiguration != null)
                return false;
        } else if (!hL7V3ResponderConfiguration
                .equals(other.hL7V3ResponderConfiguration))
            return false;
        if (syslogConfiguration == null) {
            if (other.syslogConfiguration != null)
                return false;
        } else if (!syslogConfiguration.equals(other.syslogConfiguration))
            return false;
        if (testInstance == null) {
            if (other.testInstance != null)
                return false;
        } else if (!testInstance.equals(other.testInstance))
            return false;
        if (webServiceConfiguration == null) {
            if (other.webServiceConfiguration != null)
                return false;
        } else if (!webServiceConfiguration
                .equals(other.webServiceConfiguration))
            return false;
        return true;
    }



    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "TestStepsInstance [id=" + id + ", testInstance=" + testInstance
                + ", hL7V2InitiatorConfiguration="
                + hL7V2InitiatorConfiguration
                + ", hL7V2ResponderConfiguration="
                + hL7V2ResponderConfiguration
                + ", hL7V3InitiatorConfiguration="
                + hL7V3InitiatorConfiguration
                + ", hL7V3ResponderConfiguration="
                + hL7V3ResponderConfiguration + ", dicomSCPConfiguration="
                + dicomSCPConfiguration + ", dicomSCUConfiguration="
                + dicomSCUConfiguration + ", webServiceConfiguration="
                + webServiceConfiguration + ", syslogConfiguration="
                + syslogConfiguration + "]";
    }






}

package net.ihe.gazelle.simulator.samlassertion.epsos;

import net.ihe.gazelle.simulator.samlassertion.common.AbstractAssertionAttributes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EpSOSAssertionAttributes extends AbstractAssertionAttributes<EpSOSAssertionAttributeType> {

    private Map<EpSOSAssertionAttributeType, List<String>> delegate;

    /**
     * <p>Constructor for EpSOSAssertionAttributes.</p>
     *
     * @param isTRC a boolean.
     */
    public EpSOSAssertionAttributes(boolean isTRC) {
        super();
        delegate = new HashMap<>();
        if (isTRC) {
            setDefaultsTRC();
        } else {
            setDefaults();
        }
    }

    /**
     * <p>Constructor for EpSOSAssertionAttributes.</p>
     */
    public EpSOSAssertionAttributes() {
        super();
        delegate = new HashMap<>();

        setDefaults();
    }

    private void setDefaultsTRC() {
        // Default values for simulators not setting values
        setXSPASubjectTRC("Patient ID");
        setXSPAPurposeOfUse("TREATMENT");
    }

    private void setDefaults() {
        // Default values for simulators not setting values
        setXSPASubject("Dr. Muller");
        setXSPAOrganization("Vienna AKH");
        setXSPAOrganizationId("urn:oid:1.2.3.4.5.6.7");
        setEpSOSHealthcareFacilityType("Resident Physician");
        setXSPARole("medical doctor");
        setXSPALocality("vienna-akh");
        setXSPAPurposeOfUse("TREATMENT");
        setHITSPClinicalSpeciality("UNKNOWN");

        getXSPAPermissionsHL7().add("urn:oasis:names:tc:xspa:1.0:subject:hl7:permission:PRD-006");
    }

    /**
     * Gets the XSPA permissions HL7.
     *
     * @return the XSPA permissions HL7
     */
    public List<String> getXSPAPermissionsHL7() {
        return getValues(EpSOSAssertionAttributeType.XSPAPermissionsHL7);
    }

    /**
     * Gets the xSPA subject.
     *
     * @return the xSPA subject
     */
    public String getXSPASubject() {
        return getValue(EpSOSAssertionAttributeType.XSPASubject);
    }

    /**
     * <p>getXSPASubjectTRC.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getXSPASubjectTRC() {
        return getValue(EpSOSAssertionAttributeType.XSPASubjectTRC);
    }

    /**
     * Sets the xSPA subject.
     *
     * @param xSPASubject the new xSPA subject
     */
    public void setXSPASubject(String xSPASubject) {
        setValue(EpSOSAssertionAttributeType.XSPASubject, xSPASubject);
    }

    /**
     * <p>setXSPASubjectTRC.</p>
     *
     * @param xSPASubjectTRC a {@link java.lang.String} object.
     */
    public void setXSPASubjectTRC(String xSPASubjectTRC) {
        setValue(EpSOSAssertionAttributeType.XSPASubjectTRC, xSPASubjectTRC);
    }

    /**
     * Gets the xSPA role.
     *
     * @return the xSPA role
     */
    public String getXSPARole() {
        return getValue(EpSOSAssertionAttributeType.XSPARole);
    }

    /**
     * Sets the xSPA role.
     *
     * @param xSPARole the new xSPA role
     */
    public void setXSPARole(String xSPARole) {
        setValue(EpSOSAssertionAttributeType.XSPARole, xSPARole);
    }

    /**
     * Gets the hITSP clinical speciality.
     *
     * @return the hITSP clinical speciality
     */
    public String getHITSPClinicalSpeciality() {
        return getValue(EpSOSAssertionAttributeType.HITSPClinicalSpeciality);
    }

    /**
     * Sets the hITSP clinical speciality.
     *
     * @param hITSPClinicalSpeciality the new hITSP clinical speciality
     */
    public void setHITSPClinicalSpeciality(String hITSPClinicalSpeciality) {
        setValue(EpSOSAssertionAttributeType.HITSPClinicalSpeciality, hITSPClinicalSpeciality);
    }

    /**
     * Gets the xSPA organization.
     *
     * @return the xSPA organization
     */
    public String getXSPAOrganization() {
        return getValue(EpSOSAssertionAttributeType.XSPAOrganization);
    }

    /**
     * Sets the xSPA organization.
     *
     * @param xSPAOrganization the new xSPA organization
     */
    public void setXSPAOrganization(String xSPAOrganization) {
        setValue(EpSOSAssertionAttributeType.XSPAOrganization, xSPAOrganization);
    }

    /**
     * Gets the xSPA organization id.
     *
     * @return the xSPA organization id
     */
    public String getXSPAOrganizationId() {
        return getValue(EpSOSAssertionAttributeType.XSPAOrganizationId);
    }

    /**
     * Sets the xSPA organization id.
     *
     * @param xSPAOrganizationId the new xSPA organization id
     */
    public void setXSPAOrganizationId(String xSPAOrganizationId) {
        setValue(EpSOSAssertionAttributeType.XSPAOrganizationId, xSPAOrganizationId);
    }

    /**
     * Gets the ep sos healthcare facility type.
     *
     * @return the ep sos healthcare facility type
     */
    public String getEpSOSHealthcareFacilityType() {
        return getValue(EpSOSAssertionAttributeType.epSOSHealthcareFacilityType);
    }

    /**
     * Sets the ep sos healthcare facility type.
     *
     * @param epSOSHealthcareFacilityType the new ep sos healthcare facility type
     */
    public void setEpSOSHealthcareFacilityType(String epSOSHealthcareFacilityType) {
        setValue(EpSOSAssertionAttributeType.epSOSHealthcareFacilityType, epSOSHealthcareFacilityType);
    }

    /**
     * Gets the xSPA purpose of use.
     *
     * @return the xSPA purpose of use
     */
    public String getXSPAPurposeOfUse() {
        return getValue(EpSOSAssertionAttributeType.XSPAPurposeOfUse);
    }

    /**
     * Sets the xSPA purpose of use.
     *
     * @param xSPAPurposeOfUse the new xSPA purpose of use
     */
    public void setXSPAPurposeOfUse(String xSPAPurposeOfUse) {
        setValue(EpSOSAssertionAttributeType.XSPAPurposeOfUse, xSPAPurposeOfUse);
    }

    /**
     * Gets the xSPA locality.
     *
     * @return the xSPA locality
     */
    public String getXSPALocality() {
        return getValue(EpSOSAssertionAttributeType.XSPALocality);
    }

    /**
     * Sets the xSPA locality.
     *
     * @param xSPALocality the new xSPA locality
     */
    public void setXSPALocality(String xSPALocality) {
        setValue(EpSOSAssertionAttributeType.XSPALocality, xSPALocality);
    }

    @Override
    public Map<EpSOSAssertionAttributeType, List<String>> getDelegate() {
        return delegate;
    }

}

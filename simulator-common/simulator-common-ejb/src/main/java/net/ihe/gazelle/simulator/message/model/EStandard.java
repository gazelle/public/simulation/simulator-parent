package net.ihe.gazelle.simulator.message.model;

/**
 * <b>Class Description : </b>EStandard<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 03/05/16
 */
public enum EStandard {

    HL7V2,
    HL7V3,
    XDS,
    FHIR_XML,
    FHIR_JSON,
    SVS,
    HPD,
    OTHER

    // this enum is used in database, append new values at the end
}

/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.configuration.HL7.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.configuration.model.Configuration;
import net.ihe.gazelle.simulator.common.model.TestInstance;
import net.ihe.gazelle.simulator.common.model.TestStepsInstance;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

@Entity
/**
 * <p>HL7V2ResponderConfiguration class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("HL7V2ResponderConfiguration")
@Table(name = "cfg_hl7_responder_configuration", schema = "public")
@SequenceGenerator(name = "cfg_hl7_responder_configuration_sequence", sequenceName = "cfg_hl7_responder_configuration_id_seq", allocationSize = 1)
public class HL7V2ResponderConfiguration extends AbstractHL7Configuration
{


    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cfg_hl7_responder_configuration_sequence")
    private Integer id;

    @Column(name="port")
    private Integer port ;

    @Column(name="port_out")
    private Integer portOut ;

    @Column(name="port_secure")
    private Integer portSecured ;

    @OneToMany(mappedBy = "hL7V2ResponderConfiguration", fetch = FetchType.LAZY)
    private List<TestStepsInstance> testStepsInstances;

    /**
     * <p>Getter for the field <code>testStepsInstances</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TestStepsInstance> getTestStepsInstances() {
        return testStepsInstances;
    }

    /**
     * <p>Setter for the field <code>testStepsInstances</code>.</p>
     *
     * @param testStepsInstances a {@link java.util.List} object.
     */
    public void setTestStepsInstances(List<TestStepsInstance> testStepsInstances) {
        this.testStepsInstances = testStepsInstances;
    }

    private HL7V2ResponderConfiguration()
    {
        super() ;
        setTypeOfConfiguration(HL7V2ResponderConfiguration.CONFIG_RESPONDER) ; 
    }

    /**
     * <p>Constructor for HL7V2ResponderConfiguration.</p>
     *
     * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
     */
    public HL7V2ResponderConfiguration( Configuration inConfiguration )
    {
        super(inConfiguration) ;
        setTypeOfConfiguration(HL7V2ResponderConfiguration.CONFIG_RESPONDER) ;  

    }
    /**
     * <p>Constructor for HL7V2ResponderConfiguration.</p>
     *
     * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
     * @param inPortIn a {@link java.lang.Integer} object.
     */
    public HL7V2ResponderConfiguration( Configuration inConfiguration , Integer inPortIn )
    {
        super(inConfiguration) ;
        port = inPortIn ;
        portOut = 0 ;
        setTypeOfConfiguration(HL7V2ResponderConfiguration.CONFIG_RESPONDER) ;  

    }
    /**
     * <p>Constructor for HL7V2ResponderConfiguration.</p>
     *
     * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
     * @param inPortIn a {@link java.lang.Integer} object.
     * @param inPortSecured a {@link java.lang.Integer} object.
     */
    public HL7V2ResponderConfiguration( Configuration inConfiguration , Integer inPortIn , Integer inPortSecured )
    {
        super(inConfiguration) ;
        port      = inPortIn       ;
        portSecured = inPortSecured  ;
        portOut     = 0              ;

        setTypeOfConfiguration(HL7V2ResponderConfiguration.CONFIG_RESPONDER) ;  


    }
    /**
     * <p>Constructor for HL7V2ResponderConfiguration.</p>
     *
     * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
     * @param inPortIn a {@link java.lang.Integer} object.
     * @param inPortSecure a {@link java.lang.Integer} object.
     * @param inSendingReceivingApplication a {@link java.lang.String} object.
     * @param inSendingReceivingFacility a {@link java.lang.String} object.
     * @param inAssigningAuthority a {@link java.lang.String} object.
     * @param inComments a {@link java.lang.String} object.
     */
    public HL7V2ResponderConfiguration(Configuration inConfiguration , Integer inPortIn , Integer inPortSecure , String inSendingReceivingApplication , String inSendingReceivingFacility , String inAssigningAuthority , String inComments)
    {
        super(inConfiguration , inSendingReceivingApplication, inSendingReceivingFacility, inAssigningAuthority, inComments) ;
        port  = inPortIn          ;
        portOut = 0                 ;
        portSecured = inPortSecure  ;

        setTypeOfConfiguration(HL7V2ResponderConfiguration.CONFIG_RESPONDER) ;


    }

    /**
     * <p>Constructor for HL7V2ResponderConfiguration.</p>
     *
     * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
     * @param inPortIn a {@link java.lang.Integer} object.
     * @param inSendingReceivingApplication a {@link java.lang.String} object.
     * @param inSendingReceivingFacility a {@link java.lang.String} object.
     * @param inAssigningAuthority a {@link java.lang.String} object.
     * @param inComments a {@link java.lang.String} object.
     */
    public HL7V2ResponderConfiguration(Configuration inConfiguration , Integer inPortIn   , String inSendingReceivingApplication , String inSendingReceivingFacility , String inAssigningAuthority , String inComments)
    {
        super(inConfiguration , inSendingReceivingApplication, inSendingReceivingFacility, inAssigningAuthority, inComments) ;
        port  = inPortIn   ;
        portOut = 0          ;

        setTypeOfConfiguration(HL7V2ResponderConfiguration.CONFIG_RESPONDER) ;  

    }
    
    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Getter for the field <code>port</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getPort()
    {
        return port;
    }


    /**
     * <p>Setter for the field <code>port</code>.</p>
     *
     * @param portIn a {@link java.lang.Integer} object.
     */
    public void setPort(Integer portIn)
    {
        this.port = portIn;
    }


    /**
     * <p>Getter for the field <code>portOut</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getPortOut()
    {
        return portOut;
    }


    /**
     * <p>Setter for the field <code>portOut</code>.</p>
     *
     * @param portOut a {@link java.lang.Integer} object.
     */
    public void setPortOut(Integer portOut)
    {
        this.portOut = portOut;
    }


    /**
     * <p>Getter for the field <code>portSecured</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getPortSecured()
    {
        return portSecured;
    }

    /**
     * <p>Setter for the field <code>portSecured</code>.</p>
     *
     * @param portSecured a {@link java.lang.Integer} object.
     */
    public void setPortSecured(Integer portSecured)
    {
        this.portSecured = portSecured;
    }

    /**
     * <p>hashCode.</p>
     *
     * @return a int.
     */
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((port == null) ? 0 : port.hashCode());
        result = prime * result + ((portOut == null) ? 0 : portOut.hashCode());
        result = prime * result + ((portSecured == null) ? 0 : portSecured.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        HL7V2ResponderConfiguration other = (HL7V2ResponderConfiguration) obj;
        if (port == null)
        {
            if (other.port != null)
                return false;
        }
        else if (!port.equals(other.port))
            return false;
        if (portOut == null)
        {
            if (other.portOut != null)
                return false;
        }
        else if (!portOut.equals(other.portOut))
            return false;
        if (portSecured == null)
        {
            if (other.portSecured != null)
                return false;
        }
        else if (!portSecured.equals(other.portSecured))
            return false;
        return true;
    }
    
    /**
     * <p>getHL7V2ResponderConfiguration.</p>
     *
     * @param hl7v3conf a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration} object.
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration} object.
     */
    public static HL7V2ResponderConfiguration getHL7V2ResponderConfiguration(HL7V2ResponderConfiguration hl7v3conf){
        EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
        Query query=entityManager.createQuery("SELECT hl7v3conf FROM HL7V2ResponderConfiguration hl7v3conf WHERE hl7v3conf.configuration=:configuration");
        query.setParameter("configuration",hl7v3conf.getConfiguration());
        List<HL7V2ResponderConfiguration> result=query.getResultList();
        if (result != null){
            for (HL7V2ResponderConfiguration ssc: result){
                if (ssc.equals(hl7v3conf)){
                    return ssc;
                }
            }
        }
        return null;
    }

    /**
     * <p>mergeHL7V2ResponderConfiguration.</p>
     *
     * @param inHL7V2ResponderConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration} object.
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration} object.
     */
    public static HL7V2ResponderConfiguration mergeHL7V2ResponderConfiguration(HL7V2ResponderConfiguration inHL7V2ResponderConfiguration) {
        if(inHL7V2ResponderConfiguration!=null){
            EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
            Configuration conf=Configuration.mergeConfiguration(inHL7V2ResponderConfiguration.getConfiguration());
            inHL7V2ResponderConfiguration.setConfiguration(conf);
            HL7V2ResponderConfiguration hl7v3conf = HL7V2ResponderConfiguration.getHL7V2ResponderConfiguration(inHL7V2ResponderConfiguration);
            if (hl7v3conf != null) return hl7v3conf;
            inHL7V2ResponderConfiguration=entityManager.merge(inHL7V2ResponderConfiguration);
            return inHL7V2ResponderConfiguration;
        }
        return null;
    }

    /**
     * <p>getHL7V2ResponderConfigurationFiltered.</p>
     *
     * @param inSystem a {@link net.ihe.gazelle.simulator.common.model.System} object.
     * @param ti a {@link net.ihe.gazelle.simulator.common.model.TestInstance} object.
     * @param tsi a {@link net.ihe.gazelle.simulator.common.model.TestStepsInstance} object.
     * @param em a {@link javax.persistence.EntityManager} object.
     * @return a {@link java.util.List} object.
     */
    public static List<HL7V2ResponderConfiguration> getHL7V2ResponderConfigurationFiltered(net.ihe.gazelle.simulator.common.model.System inSystem, 
            TestInstance ti, TestStepsInstance tsi ,EntityManager em){
        
        HQLQueryBuilder<HL7V2ResponderConfiguration> hbuild = new HQLQueryBuilder<HL7V2ResponderConfiguration>(em, HL7V2ResponderConfiguration.class);
        if (inSystem != null) hbuild.addEq("system", inSystem);
        if (tsi != null) hbuild.addEq("testStepsInstances", tsi);
        if (ti != null) hbuild.addEq("testStepsInstances.testInstance", ti);

       return hbuild.getList();
    }


}

package net.ihe.gazelle.simulator.samlassertion.epr;

import net.ihe.gazelle.simulator.samlassertion.common.AbstractAssertionAttributes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EPRAssertionAttributes extends AbstractAssertionAttributes<EPRAssertionAttributeType> {

    private Map<EPRAssertionAttributeType, List<String>> delegate;

    public EPRAssertionAttributes() {
        delegate = new HashMap<EPRAssertionAttributeType, List<String>>();
    }

    /**
     * Gets the Resource-id.
     *
     * @return the Resource-id
     */
    public String getResourceId() {
        return getValue(EPRAssertionAttributeType.RESOURCE_ID);
    }

    /**
     * Gets the Organization-id.
     *
     * @return the Organization-Id
     */
    public String getOrganizationId() {
        return getValue(EPRAssertionAttributeType.ORGANIZATION_ID);
    }

    /**
     * Gets the Role
     *
     * @return the Role.
     */
    public String getRole() {
        return getValue(EPRAssertionAttributeType.ROLE);
    }

    /**
     * Gets the Purpose of Use.
     *
     * @return the Purpose of use
     */
    public String getPurposeOfUse() {
        return getValue(EPRAssertionAttributeType.PURPOSE_OF_USE);
    }

    /**
     * Sets the Resource-id
     *
     * @param resourceId the new resourceId role .
     */
    public void setResourceId(String resourceId) {
        setValue(EPRAssertionAttributeType.RESOURCE_ID, resourceId);
    }

    /**
     * Sets the Organization-id
     *
     * @param organizationId the new organizationId role .
     */
    public void setOrganizationId(String organizationId) {
        setValue(EPRAssertionAttributeType.ORGANIZATION_ID, organizationId);
    }

    /**
     * Sets the Role
     *
     * @param role the new role .
     */
    public void setRole(String role) {
        setValue(EPRAssertionAttributeType.ROLE, role);
    }

    /**
     * Sets the Purpose of Use
     *
     * @param purposeOfUse the new purposeOfUse role .
     */
    public void setPurposeOfUse(String purposeOfUse) {
        setValue(EPRAssertionAttributeType.PURPOSE_OF_USE, purposeOfUse);
    }

    @Override
    public Map getDelegate() {
        return delegate;
    }
}

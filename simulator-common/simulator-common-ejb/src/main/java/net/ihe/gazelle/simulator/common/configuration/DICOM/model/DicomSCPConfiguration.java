/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.simulator.common.configuration.DICOM.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.configuration.model.Configuration;
import net.ihe.gazelle.simulator.common.model.TestInstance;
import net.ihe.gazelle.simulator.common.model.TestStepsInstance;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;


/**
 * Class doing nothing more than the abstract DicomConfiguration but allowing to differentiate it from the DicomSCUConfigurationClass
 *
 * @author jbmeyer
 * @version $Id: $Id
 */
@Entity
@Name("dicomSCPConfiguration")
@Table(name = "cfg_dicom_scp_configuration", schema = "public")
@SequenceGenerator(name = "cfg_dicom_scp_configuration_sequence", sequenceName = "cfg_dicom_scp_configuration_id_seq", allocationSize = 1)
public final class DicomSCPConfiguration extends AbstractDicomConfiguration
{


   /**
    * 
    */
   private static final long serialVersionUID = 1L;
   
   @Id
   @Column(name = "id", unique = true, nullable = false)
   @NotNull
   @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cfg_dicom_scp_configuration_sequence")
   private Integer id;
   
   @OneToMany(mappedBy = "dicomSCPConfiguration", fetch = FetchType.LAZY)
   private List<TestStepsInstance> testStepsInstances;

  
   /**
    * <p>Getter for the field <code>testStepsInstances</code>.</p>
    *
    * @return a {@link java.util.List} object.
    */
   public List<TestStepsInstance> getTestStepsInstances() {
    return testStepsInstances;
}

/**
 * <p>Setter for the field <code>testStepsInstances</code>.</p>
 *
 * @param testStepsInstances a {@link java.util.List} object.
 */
public void setTestStepsInstances(List<TestStepsInstance> testStepsInstances) {
    this.testStepsInstances = testStepsInstances;
}

/**
 * <p>Setter for the field <code>id</code>.</p>
 *
 * @param id a {@link java.lang.Integer} object.
 */
public void setId(Integer id) {
        this.id = id;
    }
    
    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }
    
    
    private DicomSCPConfiguration()
   {
      super (  ) ;
   }


   /**
    * <p>Constructor for DicomSCPConfiguration.</p>
    *
    * @param inAbstractDicomConf a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.AbstractDicomConfiguration} object.
    */
   public DicomSCPConfiguration(AbstractDicomConfiguration inAbstractDicomConf)
   {
      super(inAbstractDicomConf);
    
   }

   /**
    * <p>Constructor for DicomSCPConfiguration.</p>
    *
    * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
    */
   public DicomSCPConfiguration(Configuration inConfiguration )
   {
      super(inConfiguration);
    
   }
   
   /**
    * <p>Constructor for DicomSCPConfiguration.</p>
    *
    * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
    * @param inPort a int.
    */
   public DicomSCPConfiguration(Configuration inConfiguration, int inPort)
   {
      super(inConfiguration, inPort);
    
   }


   /**
    * <p>Constructor for DicomSCPConfiguration.</p>
    *
    * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
    * @param inPort a {@link java.lang.Integer} object.
    * @param inPortSecure a {@link java.lang.Integer} object.
    */
   public DicomSCPConfiguration(Configuration inConfiguration, Integer inPort, Integer inPortSecure)
   {
      super(inConfiguration, inPort, inPortSecure);
    
   }


   /**
    * <p>Constructor for DicomSCPConfiguration.</p>
    *
    * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
    * @param inAETitle a {@link java.lang.String} object.
    * @param inPort a {@link java.lang.Integer} object.
    * @param inPortSecure a {@link java.lang.Integer} object.
    * @param inSopClass a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.SopClass} object.
    * @param inTransfertRole a {@link java.lang.String} object.
    */
   public DicomSCPConfiguration(Configuration inConfiguration, String inAETitle, Integer inPort, Integer inPortSecure, SopClass inSopClass, String inTransfertRole)
   {
      super(inConfiguration, inAETitle, inPort, inPortSecure, inSopClass, inTransfertRole);
    
   }


   /**
    * <p>Constructor for DicomSCPConfiguration.</p>
    *
    * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
    * @param inAETitle a {@link java.lang.String} object.
    * @param inPort a {@link java.lang.Integer} object.
    * @param inPortSecure a {@link java.lang.Integer} object.
    */
   public DicomSCPConfiguration(Configuration inConfiguration, String inAETitle, Integer inPort, Integer inPortSecure)
   {
      super(inConfiguration, inAETitle, inPort, inPortSecure);
    
   }


   /**
    * <p>Constructor for DicomSCPConfiguration.</p>
    *
    * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
    * @param inAETitle a {@link java.lang.String} object.
    * @param inPort a {@link java.lang.Integer} object.
    */
   public DicomSCPConfiguration(Configuration inConfiguration, String inAETitle, Integer inPort)
   {
      super(inConfiguration, inAETitle, inPort);
    
   }


   /**
    * <p>Constructor for DicomSCPConfiguration.</p>
    *
    * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
    * @param inAETitle a {@link java.lang.String} object.
    * @param inSopClass a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.SopClass} object.
    * @param inTransfertRole a {@link java.lang.String} object.
    */
   public DicomSCPConfiguration(Configuration inConfiguration, String inAETitle, SopClass inSopClass, String inTransfertRole)
   {
      super(inConfiguration, inAETitle, inSopClass, inTransfertRole);
    
   }


   /**
    * <p>getDicomSCPConfiguration.</p>
    *
    * @param hl7v3conf a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration} object.
    * @return a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration} object.
    */
   public static DicomSCPConfiguration getDicomSCPConfiguration(DicomSCPConfiguration hl7v3conf){
       EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
       Query query=entityManager.createQuery("SELECT hl7v3conf FROM DicomSCPConfiguration hl7v3conf WHERE hl7v3conf.configuration=:configuration");
       query.setParameter("configuration",hl7v3conf.getConfiguration());
       List<DicomSCPConfiguration> result=query.getResultList();
       if (result != null){
           for (DicomSCPConfiguration ssc: result){
               if (ssc.equals(hl7v3conf)){
                   return ssc;
               }
           }
       }
       return null;
   }

   /**
    * <p>mergeDicomSCPConfiguration.</p>
    *
    * @param inDicomSCPConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration} object.
    * @return a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration} object.
    */
   public static DicomSCPConfiguration mergeDicomSCPConfiguration(DicomSCPConfiguration inDicomSCPConfiguration) {
       if(inDicomSCPConfiguration!=null){
           EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
           Configuration conf=Configuration.mergeConfiguration(inDicomSCPConfiguration.getConfiguration());
           inDicomSCPConfiguration.setConfiguration(conf);
           DicomSCPConfiguration hl7v3conf = DicomSCPConfiguration.getDicomSCPConfiguration(inDicomSCPConfiguration);
           if (hl7v3conf != null) return hl7v3conf;
           inDicomSCPConfiguration=entityManager.merge(inDicomSCPConfiguration);
           return inDicomSCPConfiguration;
       }
       return null;
   }
   
   /**
    * <p>getDicomSCPConfigurationFiltered.</p>
    *
    * @param inSystem a {@link net.ihe.gazelle.simulator.common.model.System} object.
    * @param ti a {@link net.ihe.gazelle.simulator.common.model.TestInstance} object.
    * @param tsi a {@link net.ihe.gazelle.simulator.common.model.TestStepsInstance} object.
    * @param em a {@link javax.persistence.EntityManager} object.
    * @return a {@link java.util.List} object.
    */
   public static List<DicomSCPConfiguration> getDicomSCPConfigurationFiltered(net.ihe.gazelle.simulator.common.model.System inSystem, 
           TestInstance ti, TestStepsInstance tsi ,EntityManager em){
       
       HQLQueryBuilder<DicomSCPConfiguration> hbuild = new HQLQueryBuilder<DicomSCPConfiguration>(em, DicomSCPConfiguration.class);
       if (inSystem != null) hbuild.addEq("system", inSystem);
       if (tsi != null) hbuild.addEq("testStepsInstances", tsi);
       if (ti != null) hbuild.addEq("testStepsInstances.testInstance", ti);

      return hbuild.getList();
   }
    
   
   
}

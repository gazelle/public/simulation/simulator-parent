package net.ihe.gazelle.simulator.common.ihewsinit;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.EStandard;
import net.ihe.gazelle.simulator.message.model.MessageInstanceMetadata;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.AddressingFeature;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.UUID;

/**
 * <p>Abstract SoapWebServiceClient class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class SoapWebServiceClient {

    private static Logger log = LoggerFactory.getLogger(SoapWebServiceClient.class);
    private String sutUrl;
    private String sutName;
    protected TransactionInstance transactionInstance;
    /** Constant <code>ERROR_MSG="Unable to marshall message"</code> */
    protected static final String ERROR_MSG = "Unable to marshall message";
    private SOAPFault fault;
    private SOAPMessage soapRequest;
    private SOAPMessage soapResponse;

    /**
     * <p>Constructor for SoapWebServiceClient.</p>
     */
    public SoapWebServiceClient() {
        transactionInstance = null;
    }

    /**
     * <p>sendOld.</p>
     *
     * @param clazz a {@link java.lang.Class} object.
     * @param xmlNodePresentation a {@link org.w3c.dom.Node} object.
     * @param soapAction a {@link java.lang.String} object.
     * @throws MalformedURLException if any.
     * @throws SOAPException if any.
     * @throws JAXBException if any.
     * @param <RESP> a RESP object.
     * @return a RESP object.
     */
    @Deprecated
    public <RESP> RESP sendOld(Class<RESP> clazz, Node xmlNodePresentation, String soapAction) {
        try {
            QName serviceQName = getServiceQName();
            QName portQName = getPortQName();
            URL endpointAddress = new URL(sutUrl);
            Service service = Service.create(endpointAddress, serviceQName);
            Dispatch<SOAPMessage> dispatch = service.createDispatch(portQName, SOAPMessage.class, Service.Mode.MESSAGE,
                    new AddressingFeature(true, true));
            dispatch.getRequestContext().put(BindingProvider.SOAPACTION_USE_PROPERTY, true);
            dispatch.getRequestContext().put(BindingProvider.SOAPACTION_URI_PROPERTY, soapAction);
            SOAPMessage request = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage();
            request.getSOAPBody().addDocument(xmlNodePresentation.getOwnerDocument());
            for (String entry : dispatch.getRequestContext().keySet()) {
                log.info(entry);
            }
            SOAPMessage response = dispatch.invoke(request);
            JAXBContext jc = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            JAXBElement<RESP> jaxbElement = unmarshaller.unmarshal(response.getSOAPBody().getFirstChild(), clazz);
            return jaxbElement.getValue();
        } catch (SOAPException e) {
            log.error(e.getMessage());
        } catch (MalformedURLException e) {
            log.error(e.getMessage());
        } catch (JAXBException e) {
            log.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * <p>sendWithXua.</p>
     *
     * @param clazz a {@link java.lang.Class} object.
     * @param xmlNodePresentation a {@link org.w3c.dom.Node} object.
     * @param soapAction a {@link java.lang.String} object.
     * @param credentialsUsername a {@link java.lang.String} object.
     * @param credentialsPassword a {@link java.lang.String} object.
     * @param <RESP> a RESP object.
     * @return a RESP object.
     */
    public <RESP> RESP sendWithXua(Class<RESP> clazz, Node xmlNodePresentation, String soapAction, String credentialsUsername, String credentialsPassword)
            throws SoapSendException {
        Element assertion = null;
        if (credentialsUsername != null) {
            assertion = getAssertionFromSTS(credentialsUsername, credentialsPassword);
        }
        return internalSend(clazz, xmlNodePresentation, soapAction, assertion);
    }

    /**
     * <p>getAssertionFromSTS.</p>
     *
     * @param username a {@link java.lang.String} object.
     * @param password a {@link java.lang.String} object.
     * @return a {@link org.w3c.dom.Element} object.
     */
    protected abstract Element getAssertionFromSTS(String username, String password);


    /**
     * <p>send.</p>
     *
     * @param clazz a {@link java.lang.Class} object.
     * @param xmlNodePresentation a {@link org.w3c.dom.Node} object.
     * @param soapAction a {@link java.lang.String} object.
     * @param <RESP> a RESP object.
     * @return a RESP object.
     */
    public <RESP> RESP send(Class<RESP> clazz, Node xmlNodePresentation, String soapAction) throws SoapSendException {
        return internalSend(clazz, xmlNodePresentation, soapAction, null);
    }

    private <RESP> RESP internalSend(Class<RESP> clazz, Node xmlNodePresentation, String soapAction, Element assertion) throws SoapSendException{
        try {
            URL endpoint = new URL(sutUrl);
            SOAPMessage msg = createSoapMessage();
            addAddressingHeader(soapAction, msg);
            if (assertion != null){
                appendAssertionToSoapHeader(msg, assertion);
            }
            return addBodyAndSendRequest(clazz, xmlNodePresentation, endpoint, msg);
        } catch (SOAPException | MalformedURLException | JAXBException e) {
            log.error("Error sending message : ", e);
            throw new SoapSendException(e);
        }
    }

    /**
     * <p>appendAssertionToSoapHeader.</p>
     *
     * @param msg a {@link javax.xml.soap.SOAPMessage} object.
     * @param assertion a {@link org.w3c.dom.Element} object.
     */
    protected abstract void appendAssertionToSoapHeader(SOAPMessage msg, Element assertion);

    private <RESP> RESP addBodyAndSendRequest(Class<RESP> clazz, Node xmlNodePresentation, URL endpoint, SOAPMessage msg) throws SOAPException, JAXBException {
        this.soapRequest = msg;
        msg.getSOAPBody().addDocument(xmlNodePresentation.getOwnerDocument());

        SOAPConnectionFactory sfc = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = sfc.createConnection();
        SOAPMessage response = connection.call(msg, endpoint);
        this.soapResponse = response;
        if (response.getSOAPBody() != null){
            if (response.getSOAPBody().hasFault()){
                fault = response.getSOAPBody().getFault();
                return null;
            } else if (response.getSOAPBody().hasChildNodes()){
                JAXBContext jc = JAXBContext.newInstance(clazz);
                Unmarshaller unmarshaller = jc.createUnmarshaller();
                Node childNode = getFirstElementChild(response.getSOAPBody().getChildNodes());
                if (childNode != null){
                    JAXBElement<RESP> jaxbElement = unmarshaller.unmarshal(childNode, clazz);
                    return jaxbElement.getValue();
                } else {
                    throw  new JAXBException("No child element found in SOAP Body !");
                }
            } else {
                return null;
            }
        }
        else {
            return null;
        }
    }

    protected Node getFirstElementChild(NodeList nodeList){
        for (int i = 0;i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                return node;
            }
        }
        return null;
    }

    /**
     * <p>Getter for the field <code>fault</code>.</p>
     *
     * @return a {@link javax.xml.soap.SOAPFault} object.
     */
    public SOAPFault getFault() {
        return fault;
    }

    private void addAddressingHeader(String soapAction, SOAPMessage msg) throws SOAPException {
        msg.getMimeHeaders().addHeader("Content-Type", "application/soap+xml");
        SOAPHeader soapHeader = msg.getSOAPHeader();
        soapHeader.addHeaderElement(new QName("http://www.w3.org/2005/08/addressing", "Action")).setValue(soapAction);
        soapHeader.addHeaderElement(new QName("http://www.w3.org/2005/08/addressing", "MessageID")).setValue(String.valueOf(UUID.randomUUID()));
        soapHeader.addHeaderElement(new QName("http://www.w3.org/2005/08/addressing", "To")).setValue(sutUrl);
        soapHeader.addHeaderElement(new QName("http://www.w3.org/2005/08/addressing", "ReplyTo")).addChildElement(new QName("Address")).setValue("http://www.w3.org/2005/08/addressing/anonymous");
    }

    private SOAPMessage createSoapMessage() throws SOAPException {
        MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
        return mf.createMessage();
    }

    /**
     * <p>getDomainForTransactionInstance.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public abstract Domain getDomainForTransactionInstance();

    /**
     * <p>getIssuerName.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public abstract String getIssuerName();

    /**
     * <p>saveTransactionInstance.</p>
     *
     * @param requestContent an array of byte.
     * @param responseContent an array of byte.
     * @param requestType a {@link java.lang.String} object.
     * @param responseType a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public TransactionInstance saveTransactionInstance(byte[] requestContent, byte[] responseContent,
                                                       String requestType, String responseType) {
        transactionInstance = new TransactionInstance();
        transactionInstance.setTimestamp(new Date());
        transactionInstance.setSimulatedActor(getSimulatedActor());
        transactionInstance.setTransaction(getSimulatedTransaction());
        transactionInstance.setDomain(getDomainForTransactionInstance());
        transactionInstance.getRequest().setType(requestType);
        transactionInstance.getRequest().setIssuer(getIssuerName());
        transactionInstance.getRequest().setContent(requestContent);
        transactionInstance.getRequest().setPayload(SoapMessageConverter.soapMessageToByteArray(soapRequest));
        transactionInstance.getRequest().setIssuingActor(getSimulatedActor());
        transactionInstance.getResponse().setIssuer(sutName);
        transactionInstance.getResponse().setIssuingActor(getSutActor());
        if (fault != null){
            responseType = "SOAP Fault";
        }
        transactionInstance.getResponse().setType(responseType);
        transactionInstance.getResponse().setContent(responseContent);
        transactionInstance.getResponse().setPayload(SoapMessageConverter.soapMessageToByteArray(soapResponse));
        transactionInstance.setStandard(getStandard());
        if (Identity.instance().isLoggedIn()) {
            transactionInstance.setCompanyKeyword(Identity.instance().getCredentials().getUsername());
        }
        transactionInstance = transactionInstance.save(EntityManagerService.provideEntityManager());
        if (fault != null){
            MessageInstanceMetadata faultCode = new MessageInstanceMetadata(transactionInstance.getResponse(), "Fault Code", fault.getFaultCode());
            faultCode.save();
            MessageInstanceMetadata faultReason = new MessageInstanceMetadata(transactionInstance.getResponse(), "Fault Reason", fault.getFaultString());
            faultReason.save();
        }
        return transactionInstance;
    }

    /**
     * <p>getServiceQName.</p>
     *
     * @return a {@link javax.xml.namespace.QName} object.
     */
    protected abstract QName getServiceQName();

    /**
     * <p>getPortQName.</p>
     *
     * @return a {@link javax.xml.namespace.QName} object.
     */
    protected abstract QName getPortQName();

    /**
     * <p>getSimulatedActor.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    protected abstract Actor getSimulatedActor();

    /**
     * <p>getSimulatedTransaction.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    protected abstract Transaction getSimulatedTransaction();

    /**
     * <p>getSutActor.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    protected abstract Actor getSutActor();

    /**
     * <p>getStandard.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.message.model.EStandard} object.
     */
    protected abstract EStandard getStandard();


    /**
     * <p>Getter for the field <code>transactionInstance</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public TransactionInstance getTransactionInstance() {
        return transactionInstance;
    }

    /**
     * <p>Setter for the field <code>sutUrl</code>.</p>
     *
     * @param sutUrl a {@link java.lang.String} object.
     */
    public void setSutUrl(String sutUrl) {
        this.sutUrl = sutUrl;
    }

    /**
     * <p>Setter for the field <code>sutName</code>.</p>
     *
     * @param sutName a {@link java.lang.String} object.
     */
    public void setSutName(String sutName) {
        this.sutName = sutName;
    }

    /**
     * <p>Getter for the field <code>sutUrl</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSutUrl() {
        return sutUrl;
    }
}

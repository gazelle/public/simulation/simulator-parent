package net.ihe.gazelle.simulator.common.action;

import java.util.Date;

import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Contexts;
import org.kohsuke.MetaInfServices;

/**
 * <p>SimulatorPreferenceProvider class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@MetaInfServices(PreferenceProvider.class)
@Name("simulatorPreferenceProvider")
@AutoCreate
public class SimulatorPreferenceProvider implements PreferenceProvider {

	/** {@inheritDoc} */
	@Override
	public int compareTo(PreferenceProvider o) {
		return getWeight().compareTo(o.getWeight());
	}

	/** {@inheritDoc} */
	@Override
	public Boolean getBoolean(String key) {
		String prefAsString = getString(key);
		if (prefAsString != null && !prefAsString.isEmpty()){
			return Boolean.valueOf(prefAsString);
		}else{
			return null;
		}
	}

	/** {@inheritDoc} */
	@Override
	public Date getDate(String arg0) {
		return null;
	}

	/** {@inheritDoc} */
	@Override
	public Integer getInteger(String key) {
		String prefAsString = getString(key);
		if (prefAsString != null && !prefAsString.isEmpty()){
			try{
				return Integer.decode(prefAsString);
			}catch(NumberFormatException e){
				return null;
			}
		}else{
			return null;
		}
	}

	/** {@inheritDoc} */
	@Override
	public Object getObject(Object arg0) {
		return null;
	}

	/** {@inheritDoc} */
	@Override
	public String getString(String key) {
		return ApplicationConfiguration.getValueOfVariable(key);
	}

	/** {@inheritDoc} */
	@Override
	public Integer getWeight() {
		if (Contexts.isApplicationContextActive()) {
			return -100;
		} else {
			return 100;
		}
	}

	/** {@inheritDoc} */
	@Override
	public void setBoolean(String arg0, Boolean arg1) {
		
	}

	/** {@inheritDoc} */
	@Override
	public void setDate(String arg0, Date arg1) {

	}

	/** {@inheritDoc} */
	@Override
	public void setInteger(String arg0, Integer arg1) {

	}

	/** {@inheritDoc} */
	@Override
	public void setObject(Object arg0, Object arg1) {

	}

	/** {@inheritDoc} */
	@Override
	public void setString(String arg0, String arg1) {
	}

}

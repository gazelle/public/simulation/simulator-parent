package net.ihe.gazelle.simulator.message.model;

import net.ihe.gazelle.hql.restrictions.HQLRestrictions;

/**
 * <p>TransactionInstanceDAO class.</p>
 *
 * @author aberge
 * @version 1.0: 05/10/17
 */

public class TransactionInstanceDAO {

    public static TransactionInstance getTransactionInstanceByMessageIdAndIssuer(String messageId, String issuer){
        TransactionInstanceQuery query = new TransactionInstanceQuery();
        query.addRestriction(HQLRestrictions.or(
                HQLRestrictions.and(query.request().metadataList().label().eqRestriction(MessageInstanceMetadata.MESSAGE_ID_LABEL),
                        query.request().metadataList().value().eqRestriction(messageId),
                        query.request().issuer().eqRestriction(issuer)),
                HQLRestrictions.and(query.response().metadataList().label().eqRestriction(MessageInstanceMetadata.MESSAGE_ID_LABEL),
                        query.response().metadataList().value().eqRestriction(messageId),
                        query.response().metadataList().value().eqRestriction(issuer))
        ));
        return query.getUniqueResult();
    }

    public static TransactionInstance getMockTransactionInstanceByMessageIdAndIssuer(int messageId, String issuer){
        TransactionInstanceQuery query = new TransactionInstanceQuery();
        query.addRestriction(HQLRestrictions.or(
                HQLRestrictions.and(
                        query.request().id().eqRestriction(messageId),
                        query.request().issuer().eqRestriction(issuer)),
                HQLRestrictions.and(
                        query.response().id().eqRestriction(messageId),
                        query.response().issuer().eqRestriction(issuer))
        ));
        return query.getUniqueResult();
    }
}

package net.ihe.gazelle.simulator.common.action;

import java.util.List;

import javax.ejb.Local;
import javax.faces.event.ValueChangeEvent;

import net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration;
import net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCUConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2InitiatorConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3InitiatorConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration;
import net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration;
import net.ihe.gazelle.simulator.common.model.TestInstance;
import net.ihe.gazelle.simulator.common.model.TestInstanceParticipants;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile;
import net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption;

import org.richfaces.model.TreeNodeImpl;


/**
 * <p>TestInstanceManagerLocal interface.</p>
 *
 * @author abderrazek boufahja
 * @version $Id: $Id
 */
@Local
public interface TestInstanceManagerLocal {
	
	
	/**
	 * <p>setSelectedTestInstance.</p>
	 *
	 * @param selectedTestInstance a {@link net.ihe.gazelle.simulator.common.model.TestInstance} object.
	 */
	public void setSelectedTestInstance(TestInstance selectedTestInstance);
	/**
	 * <p>getSelectedTestInstance.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.model.TestInstance} object.
	 */
	public TestInstance getSelectedTestInstance();
	/**
	 * <p>setSelectedTestInstanceParticipants.</p>
	 *
	 * @param selectedTestInstanceParticipants a {@link net.ihe.gazelle.simulator.common.model.TestInstanceParticipants} object.
	 */
	public void setSelectedTestInstanceParticipants(TestInstanceParticipants selectedTestInstanceParticipants);
	/**
	 * <p>getSelectedTestInstanceParticipants.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.model.TestInstanceParticipants} object.
	 */
	public TestInstanceParticipants getSelectedTestInstanceParticipants();
	/**
	 * <p>setSelectedIntegrationProfileOption.</p>
	 *
	 * @param selectedIntegrationProfileOption a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption} object.
	 */
	public void setSelectedIntegrationProfileOption(IntegrationProfileOption selectedIntegrationProfileOption);
    /**
     * <p>getSelectedIntegrationProfileOption.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption} object.
     */
    public IntegrationProfileOption getSelectedIntegrationProfileOption();
    /**
     * <p>setSelectedActor.</p>
     *
     * @param selectedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public void setSelectedActor(Actor selectedActor);
    /**
     * <p>getSelectedActor.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getSelectedActor();
    /**
     * <p>setSelectedIntegrationProfile.</p>
     *
     * @param selectedIntegrationProfile a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile} object.
     */
    public void setSelectedIntegrationProfile(IntegrationProfile selectedIntegrationProfile);
    /**
     * <p>getSelectedIntegrationProfile.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile} object.
     */
    public IntegrationProfile getSelectedIntegrationProfile();
	
	/**
	 * <p>getListOfPartnerTree.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.action.GazelleTreeNodeImpl} object.
	 */
	public GazelleTreeNodeImpl<Object> getListOfPartnerTree();
	/**
	 * <p>getListOfTestInstances.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<TestInstance> getListOfTestInstances();
	/**
	 * <p>initializeVariables.</p>
	 */
	public void initializeVariables();
	/**
	 * <p>optionChanged.</p>
	 *
	 * @param event a {@link javax.faces.event.ValueChangeEvent} object.
	 */
	public void optionChanged(ValueChangeEvent event);
	/**
	 * <p>getPossibleIntegrationProfiles.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<IntegrationProfile> getPossibleIntegrationProfiles();
	/**
	 * <p>getPossibleOptions.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<IntegrationProfileOption> getPossibleOptions();
	/**
	 * <p>getPossibleActors.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<Actor> getPossibleActors();
	/**
	 * <p>resetSelectionValues.</p>
	 *
	 * @param i a int.
	 */
	public void resetSelectionValues(int i);
	
	/**
	 * <p>destroy.</p>
	 */
	public void destroy();
    /**
     * <p>setSelectedWebServiceConfiguration.</p>
     *
     * @param selectedWebServiceConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration} object.
     */
    public void setSelectedWebServiceConfiguration(WebServiceConfiguration selectedWebServiceConfiguration);
    /**
     * <p>getSelectedWebServiceConfiguration.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration} object.
     */
    public WebServiceConfiguration getSelectedWebServiceConfiguration();
    /**
     * <p>setSelectedDicomSCUConfiguration.</p>
     *
     * @param selectedDicomSCUConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCUConfiguration} object.
     */
    public void setSelectedDicomSCUConfiguration(DicomSCUConfiguration selectedDicomSCUConfiguration);
    /**
     * <p>getSelectedDicomSCUConfiguration.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCUConfiguration} object.
     */
    public DicomSCUConfiguration getSelectedDicomSCUConfiguration();
    /**
     * <p>setSelectedDicomSCPConfiguration.</p>
     *
     * @param selectedDicomSCPConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration} object.
     */
    public void setSelectedDicomSCPConfiguration(DicomSCPConfiguration selectedDicomSCPConfiguration);
    /**
     * <p>getSelectedDicomSCPConfiguration.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration} object.
     */
    public DicomSCPConfiguration getSelectedDicomSCPConfiguration();
    /**
     * <p>setSelectedHL7V3ResponderConfiguration.</p>
     *
     * @param selectedHL7V3ResponderConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration} object.
     */
    public void setSelectedHL7V3ResponderConfiguration(HL7V3ResponderConfiguration selectedHL7V3ResponderConfiguration);
    /**
     * <p>getSelectedHL7V3ResponderConfiguration.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration} object.
     */
    public HL7V3ResponderConfiguration getSelectedHL7V3ResponderConfiguration();
    /**
     * <p>setSelectedHL7V3InitiatorConfiguration.</p>
     *
     * @param selectedHL7V3InitiatorConfiguration a net$ihe$gazelle$simulator$common$configuration$HL7$model$HL7V3InitiatorConfiguration object.
     */
    public void setSelectedHL7V3InitiatorConfiguration(HL7V3InitiatorConfiguration selectedHL7V3InitiatorConfiguration);
    /**
     * <p>getSelectedHL7V3InitiatorConfiguration.</p>
     *
     * @return a net$ihe$gazelle$simulator$common$configuration$HL7$model$HL7V3InitiatorConfiguration object.
     */
    public  HL7V3InitiatorConfiguration getSelectedHL7V3InitiatorConfiguration();
    /**
     * <p>setSelectedHL7V2ResponderConfiguration.</p>
     *
     * @param selectedHL7V2ResponderConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration} object.
     */
    public  void setSelectedHL7V2ResponderConfiguration(HL7V2ResponderConfiguration selectedHL7V2ResponderConfiguration);
    /**
     * <p>getSelectedHL7V2ResponderConfiguration.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration} object.
     */
    public  HL7V2ResponderConfiguration getSelectedHL7V2ResponderConfiguration();
    /**
     * <p>setSelectedHL7V2InitiatorConfiguration.</p>
     *
     * @param selectedHL7V2InitiatorConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2InitiatorConfiguration} object.
     */
    public  void setSelectedHL7V2InitiatorConfiguration(HL7V2InitiatorConfiguration selectedHL7V2InitiatorConfiguration);
    /**
     * <p>getSelectedHL7V2InitiatorConfiguration.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2InitiatorConfiguration} object.
     */
    public  HL7V2InitiatorConfiguration getSelectedHL7V2InitiatorConfiguration();
    
    /**
     * <p>isShowSystemProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowSystemProperties();
    /**
     * <p>setShowSystemProperties.</p>
     *
     * @param showSystemProperties a boolean.
     */
    public void setShowSystemProperties(boolean showSystemProperties);
    /**
     * <p>isShowHL7V2InitiatorProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowHL7V2InitiatorProperties();
    /**
     * <p>setShowHL7V2InitiatorProperties.</p>
     *
     * @param showHL7V2InitiatorProperties a boolean.
     */
    public void setShowHL7V2InitiatorProperties(boolean showHL7V2InitiatorProperties);
    /**
     * <p>isShowHL7V2ResponderProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowHL7V2ResponderProperties();
    /**
     * <p>setShowHL7V2ResponderProperties.</p>
     *
     * @param showHL7V2ResponderProperties a boolean.
     */
    public void setShowHL7V2ResponderProperties(boolean showHL7V2ResponderProperties);
    /**
     * <p>isShowHL7V3InitiatorProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowHL7V3InitiatorProperties();
    /**
     * <p>setShowHL7V3InitiatorProperties.</p>
     *
     * @param showHL7V3InitiatorProperties a boolean.
     */
    public void setShowHL7V3InitiatorProperties(boolean showHL7V3InitiatorProperties);
    /**
     * <p>isShowHL7V3ResponderProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowHL7V3ResponderProperties();
    /**
     * <p>setShowHL7V3ResponderProperties.</p>
     *
     * @param showHL7V3ResponderProperties a boolean.
     */
    public void setShowHL7V3ResponderProperties(boolean showHL7V3ResponderProperties);
    /**
     * <p>isShowDicomSCPProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowDicomSCPProperties();
    /**
     * <p>setShowDicomSCPProperties.</p>
     *
     * @param showDicomSCPProperties a boolean.
     */
    public void setShowDicomSCPProperties(boolean showDicomSCPProperties);
    /**
     * <p>isShowDicomSCUProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowDicomSCUProperties();
    /**
     * <p>setShowDicomSCUProperties.</p>
     *
     * @param showDicomSCUProperties a boolean.
     */
    public void setShowDicomSCUProperties(boolean showDicomSCUProperties);
    /**
     * <p>isShowWebServiceProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowWebServiceProperties();
    /**
     * <p>setShowWebServiceProperties.</p>
     *
     * @param showWebServiceProperties a boolean.
     */
    public void setShowWebServiceProperties(boolean showWebServiceProperties);
    
    /**
     * <p>initSelectedDicomSCPConfiguration.</p>
     *
     * @param dsc a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration} object.
     */
    public void initSelectedDicomSCPConfiguration(DicomSCPConfiguration dsc);
    /**
     * <p>initSelectedDicomSCUConfiguration.</p>
     *
     * @param dsc a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCUConfiguration} object.
     */
    public void initSelectedDicomSCUConfiguration(DicomSCUConfiguration dsc);
    /**
     * <p>initSelectedHL7V2InitiatorConfiguration.</p>
     *
     * @param dsc a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2InitiatorConfiguration} object.
     */
    public void initSelectedHL7V2InitiatorConfiguration(HL7V2InitiatorConfiguration dsc);
    /**
     * <p>initSelectedHL7V2ResponderConfiguration.</p>
     *
     * @param dsc a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration} object.
     */
    public void initSelectedHL7V2ResponderConfiguration(HL7V2ResponderConfiguration dsc);    
    /**
     * <p>initSelectedHL7V3InitiatorConfiguration.</p>
     *
     * @param dsc a net$ihe$gazelle$simulator$common$configuration$HL7$model$HL7V3InitiatorConfiguration object.
     */
    public void initSelectedHL7V3InitiatorConfiguration(HL7V3InitiatorConfiguration dsc);
    /**
     * <p>initSelectedHL7V3ResponderConfiguration.</p>
     *
     * @param dsc a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration} object.
     */
    public void initSelectedHL7V3ResponderConfiguration(HL7V3ResponderConfiguration dsc);
    /**
     * <p>initSelectedWebServiceConfiguration.</p>
     *
     * @param dsc a {@link net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration} object.
     */
    public void initSelectedWebServiceConfiguration(WebServiceConfiguration dsc);
    /**
     * <p>initSelectedTestInstanceParticipants.</p>
     *
     * @param tip a {@link net.ihe.gazelle.simulator.common.model.TestInstanceParticipants} object.
     */
    public void initSelectedTestInstanceParticipants(TestInstanceParticipants tip);
    
    /**
     * <p>initTestInstanceView.</p>
     */
    public void initTestInstanceView();

}

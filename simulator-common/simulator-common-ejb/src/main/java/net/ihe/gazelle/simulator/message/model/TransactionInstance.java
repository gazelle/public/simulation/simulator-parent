package net.ihe.gazelle.simulator.message.model;

import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.security.Identity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <b>Class description</b>: TransactionInstance
 *
 * This class represents an entity which is used to store an instance of a transaction (request + response)
 *
 * @author 	Anne-Gaëlle Bergé / IHE Europe

 */

@Entity
@Name("transactionInstance")
@Table(name="cmn_transaction_instance", schema="public", uniqueConstraints=@UniqueConstraint(columnNames="id"))
@SequenceGenerator(name="cmn_transaction_instance_sequence", sequenceName="cmn_transaction_instance_id_seq", allocationSize = 1)
public class TransactionInstance implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = -4930989278477898216L;

	@Id
	@GeneratedValue(generator="cmn_transaction_instance_sequence", strategy=GenerationType.SEQUENCE)
	@NotNull
	@Column(name="id", nullable=false, unique=true)
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="timestamp")
	private Date timestamp;

	@Column(name="is_visible")
	private Boolean visible;

	@JoinColumn(name="domain_id")
	@ManyToOne(fetch = FetchType.EAGER)
	private Domain domain;

	@JoinColumn(name="simulated_actor_id")
	@ManyToOne(fetch = FetchType.EAGER)
	private Actor simulatedActor;

	@JoinColumn(name="transaction_id")
	@ManyToOne(fetch=FetchType.EAGER)
	private Transaction transaction;

	@Enumerated(EnumType.STRING)
    @Column(name = "standard")
    private EStandard standard;

	@Column(name="company_keyword")
	private String companyKeyword;

	@OneToOne(targetEntity=MessageInstance.class, cascade=CascadeType.ALL)
	@JoinColumn(name="request_id")
	private MessageInstance request;

	@OneToOne(targetEntity=MessageInstance.class, cascade=CascadeType.ALL)
	@JoinColumn(name="response_id")
	private MessageInstance response;

	/**
	 * <p>Constructor for TransactionInstance.</p>
	 */
	public TransactionInstance()
	{
		this.visible = true;
		this.request = new MessageInstance();
		this.response = new MessageInstance();
	}

    /**
     * Save the object in the database
     *
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     * @return a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    @Transactional
	public TransactionInstance save(EntityManager entityManager)
	{
		TransactionInstance instance = entityManager.merge(this);
		entityManager.flush();
		return instance;
	}

	/**
	 * If a session context is active, use the company's keyword of the logged in user for future retrieval or filtering
	 */
	@PreUpdate
	@PrePersist
	public void setCompanyKeywordOnSaving(){
    	if (Contexts.isSessionContextActive() && Identity.instance().isLoggedIn()){
			this.companyKeyword = GazelleIdentityImpl.instance().getOrganisationKeyword();
		}
	}

    /**
     * <p>Getter for the field <code>standard</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.message.model.EStandard} object.
     */
    public EStandard getStandard() {
        return standard;
    }

    /**
     * <p>Setter for the field <code>standard</code>.</p>
     *
     * @param standard a {@link net.ihe.gazelle.simulator.message.model.EStandard} object.
     */
    public void setStandard(EStandard standard) {
        this.standard = standard;
    }

    /**
     * <p>Getter for the field <code>timestamp</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * <p>Setter for the field <code>timestamp</code>.</p>
	 *
	 * @param timestamp a {@link java.util.Date} object.
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * <p>Getter for the field <code>visible</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getVisible() {
		return visible;
	}

	/**
	 * <p>Setter for the field <code>visible</code>.</p>
	 *
	 * @param visible a {@link java.lang.Boolean} object.
	 */
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	/**
	 * <p>Getter for the field <code>domain</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
	 */
	public Domain getDomain() {
		return domain;
	}

	/**
	 * <p>Setter for the field <code>domain</code>.</p>
	 *
	 * @param domain a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
	 */
	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	/**
	 * <p>Getter for the field <code>simulatedActor</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public Actor getSimulatedActor() {
		return simulatedActor;
	}

	/**
	 * <p>Setter for the field <code>simulatedActor</code>.</p>
	 *
	 * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public void setSimulatedActor(Actor simulatedActor) {
		this.simulatedActor = simulatedActor;
	}

	/**
	 * <p>Getter for the field <code>transaction</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 */
	public Transaction getTransaction() {
		return transaction;
	}

	/**
	 * <p>Setter for the field <code>transaction</code>.</p>
	 *
	 * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	/**
	 * <p>Getter for the field <code>companyKeyword</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCompanyKeyword() {
		return companyKeyword;
	}

	/**
	 * <p>Setter for the field <code>companyKeyword</code>.</p>
	 *
	 * @param companyKeyword a {@link java.lang.String} object.
	 */
	public void setCompanyKeyword(String companyKeyword) {
		this.companyKeyword = companyKeyword;
	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * <p>Getter for the field <code>request</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.message.model.MessageInstance} object.
	 */
	public MessageInstance getRequest() {
		return request;
	}

	/**
	 * <p>Setter for the field <code>request</code>.</p>
	 *
	 * @param request a {@link net.ihe.gazelle.simulator.message.model.MessageInstance} object.
	 */
	public void setRequest(MessageInstance request) {
		this.request = request;
	}

	/**
	 * <p>Getter for the field <code>response</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.message.model.MessageInstance} object.
	 */
	public MessageInstance getResponse() {
		return response;
	}

	/**
	 * <p>Setter for the field <code>response</code>.</p>
	 *
	 * @param response a {@link net.ihe.gazelle.simulator.message.model.MessageInstance} object.
	 */
	public void setResponse(MessageInstance response) {
		this.response = response;
	}

	/**
	 * <p>getAckMessageType.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getAckMessageType(){
		return response.getType();
	}

	/**
	 * <p>getReceivedMessageContent.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getReceivedMessageContent(){
		return response.getContentAsString();
	}

    /**
     * <p>isSentFromSimulator.</p>
     *
     * @return a boolean.
     */
    public boolean isSentFromSimulator(){
        if (request != null && simulatedActor != null && request.getIssuingActor() != null) {
            return (request != null && simulatedActor.equals(request.getIssuingActor()));
        } else {
            return false;
        }
    }
}

/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.configuration.HL7.model;



import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import net.ihe.gazelle.simulator.common.configuration.model.AbstractConfiguration;
import net.ihe.gazelle.simulator.common.configuration.model.Configuration;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;


@MappedSuperclass
/**
 * <p>Abstract AbstractHL7Configuration class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class AbstractHL7Configuration extends AbstractConfiguration 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Logger */
	@SuppressWarnings("unused")
    @Logger
	private static Log log;
	
	/** Constant <code>CONFIG_INITIATOR="Initiator"</code> */
	@Transient
	public static String CONFIG_INITIATOR = "Initiator" ;
	/** Constant <code>CONFIG_RESPONDER="Responder"</code> */
	@Transient
	public static String CONFIG_RESPONDER = "Responder" ;

	@Transient
	private String typeOfConfiguration ;

	@Column(name="sending_receiving_application", nullable=false )
	private String sendingReceivingApplication ;

	@Column(name="sending_receiving_facility", nullable=false )
	private String sendingReceivingFacility ;

	/** assigningAuthority attribute */
	@Column(name = "assigning_authority")
	private String assigningAuthority;

	/**
	 * <p>Constructor for AbstractHL7Configuration.</p>
	 */
	protected AbstractHL7Configuration() { super(null); }


	/**
	 * <p>Constructor for AbstractHL7Configuration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param inSendingReceivingApplication a {@link java.lang.String} object.
	 * @param inSendingReceivingFacility a {@link java.lang.String} object.
	 * @param inAssigningAuthority a {@link java.lang.String} object.
	 * @param inComments a {@link java.lang.String} object.
	 */
	public AbstractHL7Configuration( Configuration inConfiguration , 
			String inSendingReceivingApplication , 
			String inSendingReceivingFacility    ,
			String inAssigningAuthority          ,
			String inComments)
	{
		super( inConfiguration , inComments ) ;

		sendingReceivingApplication   = inSendingReceivingApplication ;
		sendingReceivingFacility      = inSendingReceivingFacility    ;
		assigningAuthority            = inAssigningAuthority          ;

	}
	/**
	 * <p>Constructor for AbstractHL7Configuration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 */
	public AbstractHL7Configuration( Configuration inConfiguration )
	{
		super( inConfiguration ) ;
		sendingReceivingApplication   = ""  ;
		sendingReceivingFacility      = ""  ;
		assigningAuthority            = ""  ;
	}

	/**
	 * <p>Getter for the field <code>typeOfConfiguration</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getTypeOfConfiguration()
	{
		return typeOfConfiguration;
	}

	/**
	 * <p>Setter for the field <code>typeOfConfiguration</code>.</p>
	 *
	 * @param typeOfConfiguration a {@link java.lang.String} object.
	 */
	public void setTypeOfConfiguration(String typeOfConfiguration)
	{
		this.typeOfConfiguration = typeOfConfiguration;
	}

	/**
	 * <p>Getter for the field <code>sendingReceivingApplication</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getSendingReceivingApplication()
	{
		return sendingReceivingApplication;
	}

	/**
	 * <p>Setter for the field <code>sendingReceivingApplication</code>.</p>
	 *
	 * @param sendingReceivingApplication a {@link java.lang.String} object.
	 */
	public void setSendingReceivingApplication(String sendingReceivingApplication)
	{
		this.sendingReceivingApplication = sendingReceivingApplication;
	}

	/**
	 * <p>Getter for the field <code>sendingReceivingFacility</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getSendingReceivingFacility()
	{
		return sendingReceivingFacility;
	}

	/**
	 * <p>Setter for the field <code>sendingReceivingFacility</code>.</p>
	 *
	 * @param sendingReceivingFacility a {@link java.lang.String} object.
	 */
	public void setSendingReceivingFacility(String sendingReceivingFacility)
	{
		this.sendingReceivingFacility = sendingReceivingFacility;
	}

	/**
	 * <p>Getter for the field <code>assigningAuthority</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getAssigningAuthority()
	{
		return assigningAuthority;
	}

	/**
	 * <p>Setter for the field <code>assigningAuthority</code>.</p>
	 *
	 * @param assigningAuthority a {@link java.lang.String} object.
	 */
	public void setAssigningAuthority(String assigningAuthority)
	{
		this.assigningAuthority = assigningAuthority;
	}


	/**
	 * <p>hashCode.</p>
	 *
	 * @return a int.
	 */
	public int hashCode()
	{
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((assigningAuthority == null) ? 0 : assigningAuthority.hashCode());
		result = prime * result + ((sendingReceivingApplication == null) ? 0 : sendingReceivingApplication.hashCode());
		result = prime * result + ((sendingReceivingFacility == null) ? 0 : sendingReceivingFacility.hashCode());
		result = prime * result + ((typeOfConfiguration == null) ? 0 : typeOfConfiguration.hashCode());
		return result;
	}


	/** {@inheritDoc} */
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractHL7Configuration other = (AbstractHL7Configuration) obj;
		if (assigningAuthority == null)
		{
			if (other.assigningAuthority != null)
				return false;
		}
		else if (!assigningAuthority.equals(other.assigningAuthority))
			return false;
		if (sendingReceivingApplication == null)
		{
			if (other.sendingReceivingApplication != null)
				return false;
		}
		else if (!sendingReceivingApplication.equals(other.sendingReceivingApplication))
			return false;
		if (sendingReceivingFacility == null)
		{
			if (other.sendingReceivingFacility != null)
				return false;
		}
		else if (!sendingReceivingFacility.equals(other.sendingReceivingFacility))
			return false;
		if (typeOfConfiguration == null)
		{
			if (other.typeOfConfiguration != null)
				return false;
		}
		else if (!typeOfConfiguration.equals(other.typeOfConfiguration))
			return false;
		return true;
	}

}

package net.ihe.gazelle.simulator.common.ihewsinit;

/**
 * <p>SoapSendException class.</p>
 *
 * @author wbars
 * @version $Id: $Id
 */
public class SoapSendException extends Exception {

    private static final long serialVersionUID = -5100871848771290637L;

    /**
     * <p>Constructor for SignatureException.</p>
     */
    public SoapSendException() {
        super();
    }

    /**
     * <p>Constructor for SignatureException.</p>
     *
     * @param message a {@link String} object.
     * @param cause   a {@link Throwable} object.
     */
    public SoapSendException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * <p>Constructor for SignatureException.</p>
     *
     * @param message a {@link String} object.
     */
    public SoapSendException(String message) {
        super(message);
    }

    /**
     * <p>Constructor for SignatureException.</p>
     *
     * @param cause a {@link Throwable} object.
     */
    public SoapSendException(Throwable cause) {
            super(cause);
        }
}

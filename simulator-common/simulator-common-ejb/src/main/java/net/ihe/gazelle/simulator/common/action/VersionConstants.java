/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.action;

/**
 *  <b>Class Description :  </b>VersionConstants<br><br>
 * This class contains the values required to fill footer modal panels. Values are set at build time
 *
 * @author                	Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version                	1.0 - 2010, October 15th
 */
public final class VersionConstants {
	
	/** Constant <code>MAJOR_VERSION="@current.application.version@"</code> */
	public static final String MAJOR_VERSION = "@current.application.version@";
	/** Constant <code>VERSION="MAJOR_VERSION.concat(.@build.revision@)"</code> */
	public static final String VERSION = MAJOR_VERSION.concat(".@build.revision@");
	/** Constant <code>RELEASE_DATE="@last.updated@"</code> */
	public static final String RELEASE_DATE = "@last.updated@";
	/** Constant <code>APPLICATION_CONTRIBUTORS="@application.contributors@"</code> */
	public static final String APPLICATION_CONTRIBUTORS = "@application.contributors@";
}

/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.tf.converter;


import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.persistence.EntityManager;

import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.utils.ExceptionLogging;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.log.Log;

/**
 * <p>DomainConverter class.</p>
 *
 * @author abderrazek boufahja
 * @version $Id: $Id
 */
@BypassInterceptors
@Name("domainConverter")
@Converter(forClass=Domain.class)
public class DomainConverter implements javax.faces.convert.Converter, Serializable {
 
   @Logger
   private static Log log;

   /** Serial ID version of this object     */
   private static final long serialVersionUID = 1235343655242960L;
  
	/** {@inheritDoc} */
	public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
		if (value instanceof Domain)
		{
			Domain domain = (Domain) value;
			if ( domain.getId() == null ) return null ;
			return domain.getId().toString();
		}
		else
		{
			return null;
		}

	}


	/** {@inheritDoc} */
	@Transactional
	public Object getAsObject(FacesContext Context,
			UIComponent Component, String value) throws ConverterException {
			if (value != null)
		{
			try
			{
				Integer id = Integer.parseInt(value);
				if (id != null)
				{
				   EntityManager entityManager = (EntityManager)org.jboss.seam.Component.getInstance("entityManager") ;
					Domain d =  entityManager.find(Domain.class, id);
			
					return d ;

				}
			}
			catch (NumberFormatException e) {
            ExceptionLogging.logException(e, log);
			}
		}
		return null;
	}


}

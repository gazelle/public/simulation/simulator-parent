package net.ihe.gazelle.simulator.samlassertion.epr;

import net.ihe.gazelle.simulator.samlassertion.common.AssertionAttributeType;

public enum EPRAssertionAttributeType implements AssertionAttributeType {

    RESOURCE_ID("resource-id", "urn:oasis:names:tc:xacml:2.0:resource:resource-id",
            "urn:oasis:names:tc:SAML:2.0:attrname-format:uri", "xs:string"),

    ORGANIZATION_ID("organization-id", "urn:oasis:names:tc:xspa:1.0:subject:organization-id",
            "urn:oasis:names:tc:SAML:2.0:attrname-format:uri", "xs:string"),

    ROLE("role", "urn:oasis:names:tc:xacml:2.0:subject:role",
            "urn:oasis:names:tc:SAML:2.0:attrname-format:uri", "xs:string"),

    PURPOSE_OF_USE("purposeofuse", "urn:oasis:names:tc:xspa:1.0:subject:purposeofuse",
            "urn:oasis:names:tc:SAML:2.0:attrname-format:uri", "xs:string");

    /**
     * The friendly name.
     */
    protected String friendlyName;

    /**
     * The name.
     */
    protected String name;

    /**
     * The type.
     */
    protected String type;

    /**
     * The xsi type.
     */
    protected String xsiType;

    /**
     * Instantiates a new saml assertion attribute type.
     *
     * @param friendlyName the friendly name
     * @param name         the name
     * @param type         the type
     */
    EPRAssertionAttributeType(String friendlyName, String name, String type, String xsiType) {
        this.friendlyName = friendlyName;
        this.name = name;
        this.type = type;
        this.xsiType = xsiType;
    }

    /**
     * Gets the friendly name.
     *
     * @return the friendly name
     */
    public String getFriendlyName() {
        return friendlyName;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Gets the xsi type.
     *
     * @return the xsi type
     */
    public String getXsiType() {
        return xsiType;
    }


    /**
     * <p>byFriendlyName.</p>
     *
     * @param friendlyName a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.simulator.samlassertion.epr.EPRAssertionAttributeType} object.
     */
    public static EPRAssertionAttributeType byFriendlyName(String friendlyName) {
        EPRAssertionAttributeType result = null;
        for (EPRAssertionAttributeType assertionAttributeType : EPRAssertionAttributeType.values()) {
            if (assertionAttributeType.getFriendlyName().equals(friendlyName)) {
                result = assertionAttributeType;
            }
        }
        return result;
    }


}

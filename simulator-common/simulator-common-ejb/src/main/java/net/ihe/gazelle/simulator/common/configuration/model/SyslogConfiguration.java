/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.configuration.model;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.model.TestInstance;
import net.ihe.gazelle.simulator.common.model.TestStepsInstance;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;


@Entity
/**
 * <p>SyslogConfiguration class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("syslogConfiguration")
@Table(name = "cfg_syslog_configuration", schema = "public")
@SequenceGenerator(name = "cfg_syslog_configuration_sequence", sequenceName = "cfg_syslog_configuration_id_seq", allocationSize = 1)
public final class SyslogConfiguration extends AbstractConfiguration  
{

    

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cfg_syslog_configuration_sequence")
    private Integer id;


	@Column(name="port")
	private Integer port ;


	@OneToMany(mappedBy = "syslogConfiguration", fetch = FetchType.LAZY)
    private List<TestStepsInstance> testStepsInstances;

	// ~ Constructors
	// //////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }
    
    /**
     * <p>Getter for the field <code>testStepsInstances</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TestStepsInstance> getTestStepsInstances() {
        return testStepsInstances;
    }

    /**
     * <p>Setter for the field <code>testStepsInstances</code>.</p>
     *
     * @param testStepsInstances a {@link java.util.List} object.
     */
    public void setTestStepsInstances(List<TestStepsInstance> testStepsInstances) {
        this.testStepsInstances = testStepsInstances;
    }

	/**
	 * Creates a new SyslogConfiguration object.
	 */
	public SyslogConfiguration()
	{
		super() ;
	}

	/**
	 * <p>Constructor for SyslogConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 */
	public SyslogConfiguration( Configuration inConfiguration  )
	{
		super( inConfiguration ); 

	}

	/**
	 * <p>Constructor for SyslogConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param inPort a int.
	 */
	public SyslogConfiguration( Configuration inConfiguration , int inPort )
	{
		super( inConfiguration ); 
		port = inPort ;  
	}

	/**
	 * <p>Constructor for SyslogConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param inPort a {@link java.lang.Integer} object.
	 */
	public SyslogConfiguration( Configuration inConfiguration ,  Integer inPort )
	{
		super( inConfiguration );
		port        = inPort        ;   

	}

	// ~ Methods
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * <p>Getter for the field <code>port</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getPort()
	{
		return port;
	}

	/**
	 * <p>Setter for the field <code>port</code>.</p>
	 *
	 * @param port a {@link java.lang.Integer} object.
	 */
	public void setPort(Integer port)
	{
		this.port = port;
	}



	/**
	 * <p>hashCode.</p>
	 *
	 * @return a int.
	 */
	public int hashCode()
	{
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((port == null) ? 0 : port.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyslogConfiguration other = (SyslogConfiguration) obj;
		if (port == null)
		{
			if (other.port != null)
				return false;
		}
		else if (!port.equals(other.port))
			return false;

		return true;
	}

	/**
	 * <p>getSyslogConfiguration.</p>
	 *
	 * @param sysconf a {@link net.ihe.gazelle.simulator.common.configuration.model.SyslogConfiguration} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.SyslogConfiguration} object.
	 */
	public static SyslogConfiguration getSyslogConfiguration(SyslogConfiguration sysconf){
        EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
        Query query=entityManager.createQuery("SELECT sysconf FROM SyslogConfiguration sysconf WHERE sysconf.configuration=:configuration");
        query.setParameter("configuration",sysconf.getConfiguration());
        List<SyslogConfiguration> result=query.getResultList();
        if (result != null){
            for (SyslogConfiguration ssc: result){
                if (ssc.equals(sysconf)){
                    return ssc;
                }
            }
        }
        return null;
    }

    /**
     * <p>mergeSyslogConfiguration.</p>
     *
     * @param inSyslogConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.SyslogConfiguration} object.
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.SyslogConfiguration} object.
     */
    public static SyslogConfiguration mergeSyslogConfiguration(SyslogConfiguration inSyslogConfiguration) {
        if(inSyslogConfiguration!=null){
            EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
            Configuration conf=Configuration.mergeConfiguration(inSyslogConfiguration.getConfiguration());
            inSyslogConfiguration.setConfiguration(conf);
            SyslogConfiguration sysconf = SyslogConfiguration.getSyslogConfiguration(inSyslogConfiguration);
            if (sysconf != null) return sysconf;
            inSyslogConfiguration=entityManager.merge(inSyslogConfiguration);
            return inSyslogConfiguration;
        }
        return null;
    }
    
    /**
     * <p>getSyslogConfigurationFiltered.</p>
     *
     * @param inSystem a {@link net.ihe.gazelle.simulator.common.model.System} object.
     * @param ti a {@link net.ihe.gazelle.simulator.common.model.TestInstance} object.
     * @param tsi a {@link net.ihe.gazelle.simulator.common.model.TestStepsInstance} object.
     * @param em a {@link javax.persistence.EntityManager} object.
     * @return a {@link java.util.List} object.
     */
    public static List<SyslogConfiguration> getSyslogConfigurationFiltered(net.ihe.gazelle.simulator.common.model.System inSystem, 
            TestInstance ti, TestStepsInstance tsi ,EntityManager em){
        
        HQLQueryBuilder<SyslogConfiguration> hbuild = new HQLQueryBuilder<SyslogConfiguration>(em, SyslogConfiguration.class);
        if (inSystem != null) hbuild.addEq("system", inSystem);
        if (tsi != null) hbuild.addEq("testStepsInstances", tsi);
        if (ti != null) hbuild.addEq("testStepsInstances.testInstance", ti);

       return hbuild.getList();
    }


}

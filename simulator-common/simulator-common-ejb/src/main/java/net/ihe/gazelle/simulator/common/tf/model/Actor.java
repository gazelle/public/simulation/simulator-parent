/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.tf.model;

//JPA imports
import java.util.Comparator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import javax.validation.constraints.NotNull;

import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;



@Entity
/**
 * <p>Actor class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("actor")
@Table(name = "tf_actor", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@SequenceGenerator(name = "tf_actor_sequence", sequenceName = "tf_actor_id_seq", allocationSize=1)
public class Actor  extends TFObject implements java.io.Serializable ,   Comparable<Actor>, Comparator<Actor>{


	/** Serial ID version of this object */
	private static final long serialVersionUID = -450111131541283360L;

	@Logger
	private static Log log;	

	//	Attributes (existing in database as a column)

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tf_actor_sequence")
	private Integer id;
	
	/**
	 * indicates whether a SUT can create a configuration for this actor or not
	 */
	@Column(name="can_act_as_responder")
	private Boolean canActAsResponder;


	//	Constructors	
	/**
	 * <p>Constructor for Actor.</p>
	 */
	public Actor() {
	}

	/**
	 * <p>Constructor for Actor.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 * @param keyword a {@link java.lang.String} object.
	 * @param name a {@link java.lang.String} object.
	 */
	public Actor(Integer id, String keyword, String name) {
		this.keyword = keyword;
		this.name = name;
	}
	/**
	 * <p>Constructor for Actor.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @param name a {@link java.lang.String} object.
	 * @param description a {@link java.lang.String} object.
	 */
	public Actor(String keyword, String name, String description) {
		this.keyword = keyword;
		this.name = name;
		this.description = description;
	}

	/**
	 * <p>Constructor for Actor.</p>
	 *
	 * @param oldActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public Actor ( Actor oldActor )
	{
		if ( oldActor.getKeyword()!= null )
			this.keyword     = new String ( oldActor.getKeyword() ) ;
		if (  oldActor.getName() != null )
			this.name        = new String ( oldActor.getName()    ) ;
		if ( oldActor.getDescription() != null )
			this.description = new String ( oldActor.getDescription() ) ;

	}

	// *********************************************************
	//  Getters and Setters : setup the DB columns properties
	// *********************************************************



	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return this.id;
	}
	/**
	 * <p>Setter for the field <code>canActAsResponder</code>.</p>
	 *
	 * @param canActAsResponder a {@link java.lang.Boolean} object.
	 */
	public void setCanActAsResponder(Boolean canActAsResponder) {
		this.canActAsResponder = canActAsResponder;
	}

	/**
	 * <p>Getter for the field <code>canActAsResponder</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getCanActAsResponder() {
		return canActAsResponder;
	}

	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString(){
		return keyword + "-" + name ;
	}


	/**
	 * <p>compare.</p>
	 *
	 * @param o1 a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 * @param o2 a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 * @return a int.
	 */
	public int compare(Actor o1, Actor o2)
	{
		return  o1.keyword.compareToIgnoreCase( o2.keyword ) ;
	}

	/**
	 * <p>compareTo.</p>
	 *
	 * @param o a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 * @return a int.
	 */
	public int compareTo(Actor o)
	{
		return  keyword.compareToIgnoreCase( o.keyword ) ;
	}

	/**
	 * Find and retrieve the Actor object associated to a keyword
	 *
	 * @return Actor object
	 * @param keyword a {@link java.lang.String} object.
	 */
	public static Actor findActorWithKeyword (String keyword)
	{
		if ( (keyword == null) || (keyword.length() == 0))
		{
			return null;
		}

		ActorQuery query = new ActorQuery();
        query.keyword().like(keyword, HQLRestrictionLikeMatchMode.EXACT);
        return query.getUniqueResult();
	}
	
	/**
	 * <p>getAllActorActingAsResponder.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public static List<Actor> getAllActorActingAsResponder()
	{
		ActorQuery query = new ActorQuery();
		query.canActAsResponder().eq(true);
		query.name().order(true);
		return query.getListNullIfEmpty();
	}
	
	/**
	 * <p>findActorWithKeyword.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @param em a {@link javax.persistence.EntityManager} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public static Actor findActorWithKeyword ( String keyword, EntityManager  em)
	{
		if ( (keyword == null) || (keyword.length() == 0))
		{
			return null;
		}
        ActorQuery query = new ActorQuery(em);
        query.keyword().like(keyword, HQLRestrictionLikeMatchMode.EXACT);
        return query.getUniqueResult();
	}

	
	/**
	 * <p>getActorWithKeywordAndCreateIfItDoesntExist.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @param name a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public static Actor getActorWithKeywordAndCreateIfItDoesntExist(String keyword, String name)
	{
		Actor actor = findActorWithKeyword(keyword);
		if (actor == null)
		{
			log.info("Creation of a new actor in the tf_actor Table");
			Actor newActor = new Actor(keyword,name,null);
			mergeActor(newActor);
			return newActor;
		}
		else
			return actor;		
	}
	
	
	/**
	 * <p>getActorWithKeywordAndCreateIfItDoesntExist.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @param name a {@link java.lang.String} object.
	 * @param em a {@link javax.persistence.EntityManager} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public static Actor getActorWithKeywordAndCreateIfItDoesntExist(String keyword, String name,EntityManager  em)
	{
		Actor actor = findActorWithKeyword(keyword, em);
		if (actor == null)
		{
			log.info("Creation of a new actor in the tf_actor Table");
			Actor newActor = new Actor(keyword,name,null);
			mergeActor(newActor);
			return newActor;
		}
		else
			return actor;		
	}


	/**
	 * <p>listAllActors.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	@SuppressWarnings("unchecked")
	public static List<Actor> listAllActors()
	{
		ActorQuery query = new ActorQuery();
		query.keyword().order(true);
		return query.getListNullIfEmpty();
	}

	/**
	 * <p>mergeActor.</p>
	 *
	 * @param inActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public static Actor mergeActor(Actor inActor){
		if(inActor!=null){
			Actor actor=Actor.findActorWithKeyword(inActor.getKeyword());
			if(actor==null){
				EntityManager  entityManager = ( EntityManager) Component.getInstance("entityManager") ;
				actor=entityManager.merge(inActor);
				entityManager.flush();
			}
			return actor;
		}
		return null;
	}
	
	
}

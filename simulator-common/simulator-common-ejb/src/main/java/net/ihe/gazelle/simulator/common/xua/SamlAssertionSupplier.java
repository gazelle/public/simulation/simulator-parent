package net.ihe.gazelle.simulator.common.xua;

import net.ihe.gazelle.simulator.common.utils.XmlUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.picketlink.identity.federation.core.saml.v2.common.IDGenerator;
import org.picketlink.identity.federation.core.saml.v2.factories.SAMLAssertionFactory;
import org.picketlink.identity.federation.core.wstrust.WSTrustException;
import org.picketlink.identity.federation.core.wstrust.WSTrustUtil;
import org.picketlink.identity.federation.core.wstrust.plugins.saml.SAMLUtil;
import org.picketlink.identity.federation.core.wstrust.wrappers.Lifetime;
import org.picketlink.identity.federation.saml.v2.assertion.AdviceType;
import org.picketlink.identity.federation.saml.v2.assertion.AssertionType;
import org.picketlink.identity.federation.saml.v2.assertion.AudienceRestrictionType;
import org.picketlink.identity.federation.saml.v2.assertion.AuthnContextType;
import org.picketlink.identity.federation.saml.v2.assertion.AuthnStatementType;
import org.picketlink.identity.federation.saml.v2.assertion.ConditionsType;
import org.picketlink.identity.federation.saml.v2.assertion.KeyInfoConfirmationDataType;
import org.picketlink.identity.federation.saml.v2.assertion.NameIDType;
import org.picketlink.identity.federation.saml.v2.assertion.StatementAbstractType;
import org.picketlink.identity.federation.saml.v2.assertion.SubjectConfirmationType;
import org.picketlink.identity.federation.saml.v2.assertion.SubjectType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * <p>SamlAssertionSupplier class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class SamlAssertionSupplier {

    private static final Logger LOG = LoggerFactory.getLogger(SamlAssertionSupplier.class);
    private static final String NS_SAML2 = "urn:oasis:names:tc:SAML:2.0:assertion";

    // private static Logger log =
    // Logger.getLogger(SamlAssertionSupplier.class);

    /**
     * <p>main.</p>
     *
     * @param args an array of {@link java.lang.String} objects.
     * @throws java.lang.Exception if any.
     */
    public static void main(String[] args) {
        try {
            SamlAssertionAttributes attributes = null;

            SignatureUtil signatureUtil = new SignatureUtil("/opt/fr.jks", "password", "tomcat", "password");

            attributes = new SamlAssertionAttributes(false);
            Element assertion = getAssertion("Franz.Muller@AKH.Vienna.at", attributes, signatureUtil);
            saveToFile(assertion, "assert.xml");

            attributes = new SamlAssertionAttributes(true);
            Element assertionTRC = getAssertionTRC("Franz.Muller@AKH.Vienna.at", attributes, assertion, signatureUtil);
            saveToFile(assertionTRC, "assert.trc.xml");
        } catch (Exception e) {
            LOG.error("Unexpected error", e);
        }
    }

    private static void saveToFile(Element assertion, String fileName) throws Exception {
        byte[] bytes = XmlUtil.outputDOMAsBytes(assertion);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File(fileName));
            fos.write(bytes);
        } catch (IOException e) {
            throw e;
        } finally {
            if (fos != null) {
                IOUtils.closeQuietly(fos);
            }
        }
    }

    /**
     * <p>validateAssertion.</p>
     *
     * @param assertion     a {@link org.w3c.dom.Element} object.
     * @param signatureUtil a {@link net.ihe.gazelle.simulator.common.xua.SignatureUtil} object.
     * @return a boolean.
     * @throws java.lang.Exception if any.
     */
    public static boolean validateAssertion(Element assertion, SignatureUtil signatureUtil) throws Exception {
        String certAlias = null;

        Element issuerElement = null;

        NodeList childNodes = assertion.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            if (childNodes.item(i) instanceof Element) {
                Element element = (Element) childNodes.item(i);
                String localName = element.getTagName();
                if (isIssuer(localName)) {
                    issuerElement = element;
                }
            }
        }
        if (issuerElement != null) {
            certAlias = StringUtils.trimToEmpty(issuerElement.getTextContent());
        } else {
            throw new SignatureException("Unable to find issuer id");
        }

        return signatureUtil.validate(XmlUtil.outputDOM(assertion), certAlias);
    }

    private static boolean isIssuer(String localName) {
        if ("Issuer".equals(localName)) {
            return true;
        } else if (localName != null && localName.endsWith(":Issuer")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gets the assertion.
     *
     * @param nameID        the name id of the HCP
     * @param attributes    the attributes of the TRC (got by new
     *                      SamlAssertionAttributes(false))
     * @param signatureUtil the signature util
     * @return the assertion
     * @throws org.picketlink.identity.federation.core.wstrust.WSTrustException the wS trust exception
     */
    public static Element getAssertion(final String nameID, SamlAssertionAttributes attributes,
                                       SignatureUtil signatureUtil) throws WSTrustException {
        String authnContextClassRefValue = "urn:oasis:names:tc:SAML:2.0:ac:classes:X509";
        AdviceType advice = null;

        return getAssertionGeneric(nameID, attributes, signatureUtil, authnContextClassRefValue, advice);
    }

    /**
     * Gets the Treatment Realtionship Confirmation Assertion.
     *
     * @param nameID          the name id of the HCP
     * @param attributes      the attributes of the TRC (got by new
     *                        SamlAssertionAttributes(true))
     * @param sourceAssertion the source assertion (got with getAssertion)
     * @param signatureUtil   the signature util
     * @return the Treatment Realtionship Confirmation Assertion
     * @throws org.picketlink.identity.federation.core.wstrust.WSTrustException the wS trust exception
     */
    public static Element getAssertionTRC(final String nameID, SamlAssertionAttributes attributes,
                                          Element sourceAssertion, SignatureUtil signatureUtil) throws WSTrustException {
        String sourceAssertionId = getAssertionId(sourceAssertion);
        String authnContextClassRefValue = "urn:oasis:names:tc:SAML:2.0:ac:classes:PreviousSession";
        AdviceType advice = new AdviceType();
        JAXBElement<String> assertionIDRef = new JAXBElement<String>(new QName("urn:oasis:names:tc:SAML:2.0:assertion",
                "AssertionIDRef"), String.class, sourceAssertionId);
        advice.getAssertionIDRefOrAssertionURIRefOrAssertion().add(assertionIDRef);
        return getAssertionGeneric(nameID, attributes, signatureUtil, authnContextClassRefValue, advice);
    }

    private static Element getAssertionGeneric(final String nameID, SamlAssertionAttributes attributes,
                                               SignatureUtil signatureUtil, String authnContextClassRefValue, AdviceType advice)
            throws WSTrustException {
        // generate an id for the new assertion.
        String assertionID = IDGenerator.create("ID_");

        // lifetime and audience restrictions.
        Lifetime lifetime = WSTrustUtil.createDefaultLifetime(7200 * 1000);
        AudienceRestrictionType restriction = null;
        ConditionsType conditions = SAMLAssertionFactory.createConditions(lifetime.getCreated(), lifetime.getExpires(),
                restriction);

        String confirmationMethod = SAMLUtil.SAML2_SENDER_VOUCHES_URI;
        KeyInfoConfirmationDataType keyInfoDataType = null;

        SubjectConfirmationType subjectConfirmation = SAMLAssertionFactory.createSubjectConfirmation(null,
                confirmationMethod, keyInfoDataType);

        // create a subject using the caller principal or on-behalf-of
        // principal.
        NameIDType nameIDType = SAMLAssertionFactory.createNameID(null, null, nameID);
        nameIDType.setFormat("urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified");
        SubjectType subject = SAMLAssertionFactory.createSubject(nameIDType, subjectConfirmation);

        // create the attribute statements if necessary.
        List<StatementAbstractType> statements = null;

        // create the SAML assertion.
        NameIDType issuerID = SAMLAssertionFactory.createNameID(null, null, "urn:idp:demo");
        AssertionType assertion = SAMLAssertionFactory.createAssertion(assertionID, issuerID, lifetime.getCreated(),
                conditions, subject, statements);

        AuthnStatementType authnStatement = new AuthnStatementType();
        authnStatement.setAuthnInstant(lifetime.getCreated());
        authnStatement.setSessionNotOnOrAfter(lifetime.getExpires());
        AuthnContextType authnContext = new AuthnContextType();
        JAXBElement<String> authnContextClassRef = new JAXBElement<String>(new QName(
                "urn:oasis:names:tc:SAML:2.0:assertion", "AuthnContextClassRef"), String.class,
                authnContextClassRefValue);
        authnContext.getContent().add(authnContextClassRef);
        authnStatement.setAuthnContext(authnContext);
        assertion.getStatementOrAuthnStatementOrAuthzDecisionStatement().add(authnStatement);

        if (advice != null) {
            assertion.setAdvice(advice);
        }

        // convert the constructed assertion to element.
        Element assertionElement = null;
        try {
            assertionElement = SAMLUtil.toElement(assertion);
            // log.info("===============================");
            // log.info(XmlUtil.outputDOM(assertionElement));
        } catch (Exception e) {
            throw new WSTrustException("Failed to marshall SAMLV2 assertion: " + e.getMessage());
        }

        DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
        fabrique.setNamespaceAware(true);

        DocumentBuilder constructeur;
        try {
            constructeur = fabrique.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            throw new WSTrustException("Failed to sign assertion");
        }
        Document document = constructeur.newDocument();
        assertionElement = (Element) document.importNode(assertionElement, true);
        document.appendChild(assertionElement);

        try {
            Element attributesDOM = attributes.getDOMElement(getSAML2NSPrefix(assertionElement));
            assertionElement.appendChild((Element) document.importNode(attributesDOM, true));

            // log.info("===============================");
            // log.info(XmlUtil.outputDOM(attributesDOM));
            // log.info("===============================");
            // log.info(XmlUtil.outputDOM(document));

            document = signatureUtil.sign(document, document.getDocumentElement());
        } catch (SignatureException e) {
            e.printStackTrace();
            throw new WSTrustException("Failed to sign assertion");
        }

        return document.getDocumentElement();
    }

    private static String getAssertionId(Element sourceAssertion) {
        String id = sourceAssertion.getAttribute("ID");
        return id;
    }

    private static String getSAML2NSPrefix(Element assertionElement) {
        NamedNodeMap attributes = assertionElement.getAttributes();
        for (int i = 0; i < attributes.getLength(); i++) {
            Attr attribute = (Attr) attributes.item(i);
            // log.info(attribute.getName() + " = " + attribute.getValue());
            if (NS_SAML2.equals(attribute.getValue())) {
                String prefix = attribute.getName();
                if (prefix.equals("xmlns")) {
                    return "";
                } else {
                    int index = prefix.indexOf(':');
                    prefix = prefix.substring(index + 1);
                    return prefix + ":";
                }
            }
        }
        return "";
    }

}

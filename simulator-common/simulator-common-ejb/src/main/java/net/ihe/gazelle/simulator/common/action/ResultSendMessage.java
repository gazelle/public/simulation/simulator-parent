/*
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.action;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.simulator.common.model.ContextualInformationInstance;


/**
 *  <b>Class Description :  </b>Pair<br><br>
 *
 */
public class ResultSendMessage implements java.io.Serializable {


    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private List<ContextualInformationInstance> listCII;

    private Boolean evaluation;
    
    
    /**
     * <p>Constructor for ResultSendMessage.</p>
     */
    public ResultSendMessage(){}
    
    /**
     * <p>Constructor for ResultSendMessage.</p>
     *
     * @param listCII a {@link java.util.List} object.
     * @param evaluation a {@link java.lang.Boolean} object.
     */
    public ResultSendMessage(List<ContextualInformationInstance> listCII,
            Boolean evaluation) {
        super();
        this.listCII = listCII;
        this.evaluation = evaluation;
    }
    



    /**
     * <p>Getter for the field <code>listCII</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<ContextualInformationInstance> getListCII() {
        return listCII;
    }

    /**
     * <p>Setter for the field <code>listCII</code>.</p>
     *
     * @param listCII a {@link java.util.List} object.
     */
    public void setListCII(List<ContextualInformationInstance> listCII) {
        this.listCII = listCII;
    }

    /**
     * <p>Getter for the field <code>evaluation</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getEvaluation() {
        return evaluation;
    }

    /**
     * <p>Setter for the field <code>evaluation</code>.</p>
     *
     * @param evaluation a {@link java.lang.Boolean} object.
     */
    public void setEvaluation(Boolean evaluation) {
        this.evaluation = evaluation;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((evaluation == null) ? 0 : evaluation.hashCode());
        result = prime * result + ((listCII == null) ? 0 : listCII.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ResultSendMessage other = (ResultSendMessage) obj;
        if (evaluation == null) {
            if (other.evaluation != null)
                return false;
        } else if (!evaluation.equals(other.evaluation))
            return false;
        if (listCII == null) {
            if (other.listCII != null)
                return false;
        } else if (!listCII.equals(other.listCII))
            return false;
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "ResultSendMessage [listCII=" + listCII + ", evaluation="
                + evaluation + "]";
    }
    
    /**
     * <p>addContextualInformationInstance.</p>
     *
     * @param cii a {@link net.ihe.gazelle.simulator.common.model.ContextualInformationInstance} object.
     */
    public void addContextualInformationInstance(ContextualInformationInstance cii){
        if (cii != null){
            if (this.listCII == null){
                this.listCII = new ArrayList<ContextualInformationInstance>();
            }
            this.listCII.add(cii);
        }
    }


}

package net.ihe.gazelle.simulator.users.model;

import java.io.Serializable;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.management.RoleName;

/**
 * <p>UserRole class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("userRole")
public class UserRole implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6660549857759171329L;
	
	private String name;

	/**
	 * <p>Getter for the field <code>name</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	@RoleName 
	public String getname(){
		return this.name;
	}
	
	/**
	 * <p>Setter for the field <code>name</code>.</p>
	 *
	 * @param inName a {@link java.lang.String} object.
	 */
	public void setName(String inName){
		this.name = inName;
	}
}

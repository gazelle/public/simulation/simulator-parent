package net.ihe.gazelle.simulator.common.utils;

/**
 * <p>DatabaseUtil class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class DatabaseUtil
{

   /**
    * <p>addANDorWHERE.</p>
    *
    * @param currentQueryString a {@link java.lang.StringBuffer} object.
    * @return a {@link java.lang.StringBuffer} object.
    */
   public static StringBuffer addANDorWHERE( StringBuffer currentQueryString  )
   {
   if ( currentQueryString.indexOf("WHERE") > 0 )
   return currentQueryString.append(" AND " ) ;
   else 
   return currentQueryString.append(" WHERE " ) ;

   }
}

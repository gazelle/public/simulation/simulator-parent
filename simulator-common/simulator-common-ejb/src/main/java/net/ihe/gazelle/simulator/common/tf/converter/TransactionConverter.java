package net.ihe.gazelle.simulator.common.tf.converter;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.persistence.EntityManager;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

@BypassInterceptors
/**
 * <p>TransactionConverter class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("transactionConverter")
@org.jboss.seam.annotations.faces.Converter(forClass=Transaction.class)
public class TransactionConverter implements Serializable, Converter {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** {@inheritDoc} */
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (value != null)
		{
			Integer id = Integer.parseInt(value);
			if (id != null)
			{
				try{
					EntityManager em = EntityManagerService.provideEntityManager();
					Transaction transaction = em.find(Transaction.class, id);
					return transaction;
				}catch(NumberFormatException e){
					return null;
				}
			}
		}
		return null;
	}

	/** {@inheritDoc} */
	public String getAsString(FacesContext arg0, UIComponent arg1, Object value) {
		if (value instanceof Transaction)
			return ((Transaction) value).getId().toString();
		return null;
	}

}

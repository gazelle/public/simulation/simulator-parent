package net.ihe.gazelle.simulator.common.action;

import java.io.Serializable;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.persistence.EntityManager;
import javax.xml.soap.SOAPException;

import net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration;
import net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCUConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2InitiatorConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3InitiatorConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration;
import net.ihe.gazelle.simulator.common.configuration.model.SyslogConfiguration;
import net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration;
import net.ihe.gazelle.simulator.common.model.ConfigurationForWS;
import net.ihe.gazelle.simulator.common.model.ContextualInformation;
import net.ihe.gazelle.simulator.common.model.ContextualInformationInstance;
import net.ihe.gazelle.simulator.common.model.Message;
import net.ihe.gazelle.simulator.common.model.OIDConfiguration;
import net.ihe.gazelle.simulator.common.model.System;
import net.ihe.gazelle.simulator.common.model.TestInstance;
import net.ihe.gazelle.simulator.common.model.TestInstanceParticipants;
import net.ihe.gazelle.simulator.common.model.TestInstanceStatus;
import net.ihe.gazelle.simulator.common.model.TestStepsInstance;
import net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;


/**
 * <p>Abstract AbstractSimulatorManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class AbstractSimulatorManager implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4722287748857085529L;


	/** Logger */
	@Logger
	private static Log log;


	/**
	 * <p>startTestInstance.</p>
	 *
	 * @param gazelleTestInstanceId a {@link java.lang.String} object.
	 * @return true for a new testInstance on the simulator or false for a testInstance that is already existing
	 * @throws javax.xml.soap.SOAPException if provide argument is null or length 0
	 */
	public boolean startTestInstance(String gazelleTestInstanceId) throws SOAPException{
		if (gazelleTestInstanceId.length() == 0){
			SOAPException soapEx = new SOAPException("The length of the string describing the gazelleTestInstanceId is zero. Cannot start a test instance is this is not correctly filled.");
			throw soapEx;		
		}
		try{
		    String[] zz = gazelleTestInstanceId.split("-");
		    if (zz.length==1) throw new Exception();
			Integer.parseInt(zz[zz.length-1]) ;
		} catch(Exception e)
		{
			SOAPException soapEx = new SOAPException("The argument gazelleTestInstanceId : ("+ gazelleTestInstanceId +") could not be converted into an Integer." + e.getLocalizedMessage());
			throw soapEx;	
		}


		TestInstance testInstance=TestInstance.getTestInstanceByServerTestInstanceId(gazelleTestInstanceId);
		if(testInstance==null){
			testInstance=new TestInstance();
			testInstance.setServerTestInstanceId(gazelleTestInstanceId);
			testInstance.setTestInstanceStatus(TestInstanceStatus.getSTATUS_STARTED());
			EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
			testInstance=entityManager.merge(testInstance);
			entityManager.flush();
			return true;
		}
		return false;
	}
	
	/**
	 * <p>stopTestInstance.</p>
	 *
	 * @param testInstanceId a {@link java.lang.String} object.
	 * @return a boolean.
	 * @throws javax.xml.soap.SOAPException if any.
	 */
	public boolean stopTestInstance(@WebParam(name="testInstanceId")String testInstanceId) throws SOAPException {
	    if (testInstanceId.length() == 0){
            SOAPException soapEx = new SOAPException("The length of the string describing the testInstanceId is zero. Cannot stop a test instance is this is not correctly filled.");
            throw soapEx;       
        }
        try{
            String[] zz = testInstanceId.split("-");
            if (zz.length==1) throw new Exception();
            Integer.parseInt(zz[zz.length-1]) ;
        } catch(Exception e)
        {
            SOAPException soapEx = new SOAPException("The argument testInstanceId : ("+ testInstanceId +") could not be converted into an Integer." + e.getLocalizedMessage());
            throw soapEx;   
        }
	    TestInstance testInstance=TestInstance.getTestInstanceByServerTestInstanceId(testInstanceId);
	    if (testInstance != null){
	        testInstance.setTestInstanceStatus(TestInstanceStatus.getSTATUS_COMPLETED());
	    }
        return false;
    }

	/**
	 * <p>setTestPartnerConfigurations.</p>
	 *
	 * @param testInstanceId - id of the test instance in Gazelle
	 * @param testInstanceParticipantsList - List of participants to the test
	 * @return true if a new of partner configuration is create or false if the new partner configuration is not create.
	 * @throws javax.xml.soap.SOAPException if provide argument is null or length 0
	 * @param oidList a {@link java.util.List} object.
	 */
	@WebMethod
	public boolean setTestPartnerConfigurations(@WebParam(name="testInstanceId")String testInstanceId,
			@WebParam(name="testInstanceParticipantsList")List<TestInstanceParticipants> testInstanceParticipantsList,
			@WebParam(name="oidList")List<OIDConfiguration> oidList) throws SOAPException {
		log.info("Saving Test Partners Configurations...");
		if(testInstanceParticipantsList!=null){
			EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
			TestInstance testInstance=TestInstance.getTestInstanceByServerTestInstanceId(testInstanceId);
			if(testInstance!=null){
				log.info("Test Instance Id :"+testInstance.getId());
				for(TestInstanceParticipants testInstanceParticipants:testInstanceParticipantsList){
					if(testInstanceParticipants.getSystem()!=null){
						ActorIntegrationProfileOption actorIntegrationProfileOption=testInstanceParticipants.getActorIntegrationProfileOption();
						if(actorIntegrationProfileOption!=null){
							actorIntegrationProfileOption=ActorIntegrationProfileOption.mergeActorIntegrationProfileOption(actorIntegrationProfileOption);
							if(actorIntegrationProfileOption!=null)
								testInstanceParticipants.setActorIntegrationProfileOption(actorIntegrationProfileOption);
							else {
								SOAPException soapEx = new SOAPException("The actor integration profile option associate to the test Instance is null . Cannot start a test instance is this is not correctly filled.");
								throw soapEx;	
							}
								
						} else {
							SOAPException soapEx = new SOAPException("The actor integration profile associate to the test Instance is null . Cannot start a test instance is this is not correctly filled.");
							throw soapEx;
						}
						//System system= entityManager.find(System.class, testInstanceParticipants.getSystem());
						//System system=System.getSystemBySystemKeywordByInstitutionKeyword(testInstanceParticipants.getSystem().getKeyword(),testInstanceParticipants.getSystem().getInstitutionKeyword());
						//if(system==null){
						System system=new System();
						system.setKeyword(testInstanceParticipants.getSystem().getKeyword());
						system.setInstitutionKeyword(testInstanceParticipants.getSystem().getInstitutionKeyword());
						system.setSystemOwner(testInstanceParticipants.getSystem().getSystemOwner());
						//}
						/*
						List<HL7V2InitiatorConfiguration> hl7v2InitiatorConfigurations = testInstanceParticipants.getSystem().gethL7v2InitiatorConfigurations();
						if(hl7v2InitiatorConfigurations!=null){
						    for (HL7V2InitiatorConfiguration hl7v2InitiatorConfiguration2 : hl7v2InitiatorConfigurations) {
						        hl7v2InitiatorConfiguration2.setSystem(system);
						        hl7v2InitiatorConfiguration2 = HL7V2InitiatorConfiguration.mergeHL7V2InitiatorConfiguration(hl7v2InitiatorConfiguration2);
                                 system.addHL7v2InitiatorConfiguration(hl7v2InitiatorConfigurations);
                            }
						   
						}

						HL7V2ResponderConfiguration hl7v2ResponderConfiguration=testInstanceParticipants.getSystem().getHL7V2ResponderConfiguration();
						if(hl7v2ResponderConfiguration!=null){
							hl7v2ResponderConfiguration=HL7V2ResponderConfiguration.mergeHL7V2ResponderConfiguration(hl7v2ResponderConfiguration);
							system.setHL7V2ResponderConfiguration(hl7v2ResponderConfiguration);
						}

						HL7V3InitiatorConfiguration hl7v3InitiatorConfiguration=testInstanceParticipants.getSystem().getHL7V3InitiatorConfiguration();
						if(hl7v3InitiatorConfiguration!=null){
							hl7v3InitiatorConfiguration=HL7V3InitiatorConfiguration.mergeHL7V3InitiatorConfiguration(hl7v3InitiatorConfiguration);
							system.setHL7V3InitiatorConfiguration(hl7v3InitiatorConfiguration);
						}

						HL7V3ResponderConfiguration hl7v3ResponderConfiguration=testInstanceParticipants.getSystem().getHL7V3ResponderConfiguration();
						if(hl7v3ResponderConfiguration!=null){
							hl7v3ResponderConfiguration=HL7V3ResponderConfiguration.mergeHL7V3ResponderConfiguration(hl7v3ResponderConfiguration);
							system.setHL7V3ResponderConfiguration(hl7v3ResponderConfiguration);
						}

						DicomSCUConfiguration dicomSCUConfiguration=testInstanceParticipants.getSystem().getDicomSCUConfiguration();
						if(dicomSCUConfiguration!=null){
							dicomSCUConfiguration=DicomSCUConfiguration.mergeDicomSCUConfiguration(dicomSCUConfiguration);
							system.setDicomSCUConfiguration(dicomSCUConfiguration);
						}

						DicomSCPConfiguration dicomSCPConfiguration=testInstanceParticipants.getSystem().getDicomSCPConfiguration();
						if(dicomSCPConfiguration!=null){
							dicomSCPConfiguration=DicomSCPConfiguration.mergeDicomSCPConfiguration(dicomSCPConfiguration);
							system.setDicomSCPConfiguration(dicomSCPConfiguration);
						}


						SyslogConfiguration syslogConfiguration=testInstanceParticipants.getSystem().getSyslogConfiguration();
						if(syslogConfiguration!=null){
							syslogConfiguration=SyslogConfiguration.mergeSyslogConfiguration(syslogConfiguration);
							system.setSyslogConfiguration(syslogConfiguration);
						}

						WebServiceConfiguration webServiceConfiguration=testInstanceParticipants.getSystem().getWebServiceConfiguration();
						if(webServiceConfiguration!=null){
							webServiceConfiguration=WebServiceConfiguration.mergeWebServiceConfiguration(webServiceConfiguration);
							system.setWebServiceConfiguration(webServiceConfiguration);
						}
						
						entityManager.flush();
						*/

						system=System.mergeSystem(system);
						testInstanceParticipants.setSystem(system);
						testInstanceParticipants=entityManager.merge(testInstanceParticipants);
						testInstance.addTestInstanceParticipants(testInstanceParticipants);

					} else {
						SOAPException soapEx = new SOAPException("The systeme associate to the test Instance is null . Cannot start a test instance is this is not correctly filled.");
						throw soapEx;
					}
				}
				testInstance=entityManager.merge(testInstance);
				log.info("Test Instance Id :"+testInstance.getId());
				if (oidList != null){
				    for (OIDConfiguration oidConfiguration : oidList) {
                        System sysconf = System.getSystem(oidConfiguration.getSystem());
                        if (sysconf != null){
                            List<OIDConfiguration> loidforsys = sysconf.getOidConfigurationList();
                            if (loidforsys != null){
                                if (!loidforsys.contains(oidConfiguration)){
                                    boolean exist = false;
                                    for (OIDConfiguration oidConfiguration2 : loidforsys) {
                                        if (oidConfiguration2.getLabel().equals(oidConfiguration.getLabel())){
                                            oidConfiguration2.setOid(oidConfiguration.getOid());
                                            oidConfiguration2 = entityManager.merge(oidConfiguration2);
                                            entityManager.flush();
                                            exist = true;
                                            break;
                                        }
                                    }
                                    if (!exist){
                                        oidConfiguration.setSystem(sysconf);
                                        oidConfiguration = entityManager.merge(oidConfiguration);
                                        entityManager.flush();
                                    }
                                }
                            }
                            else{
                                oidConfiguration.setSystem(sysconf);
                                oidConfiguration = entityManager.merge(oidConfiguration);
                                entityManager.flush();
                            }
                        }
                    }
				}
				return true;
			} else {
				SOAPException soapEx = new SOAPException("The test Instance is null . Cannot start a test instance.");
				throw soapEx;
			}
		} else {
			SOAPException soapEx = new SOAPException("The list of particpants is null . Cannot start a test instance is this is not correctly filled.");
			throw soapEx;
		}
	}


	/**
	 * <p>getMessagesForTestInstance.</p>
	 *
	 * @param testInstanceId Identify the test Instance
	 * @return null if the testInstance doesn't exist or test instance if the test testInstance exist
	 */
	@WebMethod
	public List<Message> getMessagesForTestInstance(@WebParam(name="testInstanceId")String testInstanceId) {
	    /*
		TestInstance testInstance=TestInstance.getTestInstanceByServerTestInstanceId(testInstanceId);
		if(testInstance!=null){
			return testInstance.getMessageList();
		}
		*/
		return null;
	}
	
	/**
	 * <p>saveRelatedTestStepsInstance.</p>
	 *
	 * @param testInstanceId a {@link java.lang.String} object.
	 * @param testInstanceParticipantsId a {@link java.lang.String} object.
	 * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 * @param messageType a {@link java.lang.String} object.
	 * @param responderConfiguration a {@link net.ihe.gazelle.simulator.common.model.ConfigurationForWS} object.
	 * @param listContextualInformationInstanceInput a {@link java.util.List} object.
	 * @param listContextualInformationInstanceOutput a {@link java.util.List} object.
	 * @return a {@link java.lang.Integer} object.
	 */
	protected Integer saveRelatedTestStepsInstance(String testInstanceId,
            String testInstanceParticipantsId,Transaction transaction,
            String messageType,
            ConfigurationForWS responderConfiguration,
            List<ContextualInformationInstance> listContextualInformationInstanceInput,
            List<ContextualInformationInstance> listContextualInformationInstanceOutput){
	    
	    EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
	    TestInstanceParticipants testInstanceParticipants=TestInstanceParticipants.getTestInstanceParticipants(testInstanceParticipantsId);
        TestInstance testInstance = TestInstance.getTestInstanceByServerTestInstanceId(testInstanceId); 
        TestStepsInstance tsi = new TestStepsInstance();
        tsi.setTestInstance(testInstance);
        
        if (responderConfiguration != null){
            HL7V3ResponderConfiguration hl7v3conf = responderConfiguration.gethL7V3ResponderConfiguration();
            if (hl7v3conf != null){
                hl7v3conf.setSystem(testInstanceParticipants.getSystem());
                hl7v3conf = HL7V3ResponderConfiguration.mergeHL7V3ResponderConfiguration(hl7v3conf);
                tsi.sethL7V3ResponderConfiguration(hl7v3conf);
            }
            HL7V3InitiatorConfiguration hl7v3init = responderConfiguration.getHL7V3InitiatorConfiguration();
            if (hl7v3init != null){
                hl7v3init.setSystem(testInstanceParticipants.getSystem());
                hl7v3init = HL7V3InitiatorConfiguration.mergeHL7V3InitiatorConfiguration(hl7v3init);
                tsi.sethL7V3InitiatorConfiguration(hl7v3init);
            }
            HL7V2ResponderConfiguration hl7v2conf = responderConfiguration.gethL7V2ResponderConfiguration();
            if (hl7v2conf != null){
                hl7v2conf.setSystem(testInstanceParticipants.getSystem());
                hl7v2conf = HL7V2ResponderConfiguration.mergeHL7V2ResponderConfiguration(hl7v2conf);
                tsi.sethL7V2ResponderConfiguration(hl7v2conf);
            }
            HL7V2InitiatorConfiguration hl7v2init = responderConfiguration.gethL7V2InitiatorConfiguration();
            if (hl7v2init != null){
                hl7v2init.setSystem(testInstanceParticipants.getSystem());
                hl7v2init = HL7V2InitiatorConfiguration.mergeHL7V2InitiatorConfiguration(hl7v2init);
                tsi.sethL7V2InitiatorConfiguration(hl7v2init);
            }
            DicomSCPConfiguration dicomscp = responderConfiguration.getDicomSCPConfiguration();
            if (dicomscp != null){
                dicomscp.setSystem(testInstanceParticipants.getSystem());
                dicomscp = DicomSCPConfiguration.mergeDicomSCPConfiguration(dicomscp);
                tsi.setDicomSCPConfiguration(dicomscp);
            }
            DicomSCUConfiguration dicomscu = responderConfiguration.getDicomSCUConfiguration();
            if (dicomscu != null){
                dicomscu.setSystem(testInstanceParticipants.getSystem());
                dicomscu = DicomSCUConfiguration.mergeDicomSCUConfiguration(dicomscu);
                tsi.setDicomSCUConfiguration(dicomscu);
            }
            WebServiceConfiguration wsconf = responderConfiguration.getWebServiceConfiguration();
            if (wsconf != null){
                wsconf.setSystem(testInstanceParticipants.getSystem());
                wsconf = WebServiceConfiguration.mergeWebServiceConfiguration(wsconf);
                tsi.setWebServiceConfiguration(wsconf);
            }
            SyslogConfiguration sysconf = responderConfiguration.getSyslogConfiguration();
            if (sysconf != null){
                sysconf.setSystem(testInstanceParticipants.getSystem());
                sysconf = SyslogConfiguration.mergeSyslogConfiguration(sysconf);
                tsi.setSyslogConfiguration(sysconf);
            }
        }
        
        for(ContextualInformationInstance cii: listContextualInformationInstanceInput){
            cii.setForm(ContextualInformationInstance.INPUT);
            ContextualInformation ci = ContextualInformation.getContextualInformationByCI(cii.getContextualInformation());
            cii.setContextualInformation(ci);
            cii = entityManager.merge(cii);
            entityManager.flush();
        }
        
        for(ContextualInformationInstance cii: listContextualInformationInstanceOutput){
            cii.setForm(ContextualInformationInstance.OUTPUT);
            ContextualInformation ci = ContextualInformation.getContextualInformationByCI(cii.getContextualInformation());
            cii.setContextualInformation(ci);
            cii = entityManager.merge(cii);
            entityManager.flush();
        }
        
        
        tsi = entityManager.merge(tsi);
        entityManager.flush();
	    return tsi.getId();
	}
}

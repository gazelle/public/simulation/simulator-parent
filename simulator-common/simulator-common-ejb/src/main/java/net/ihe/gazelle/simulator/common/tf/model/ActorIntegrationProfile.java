/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.simulator.common.tf.model;

//JPA imports
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import javax.validation.constraints.NotNull;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Entity
/**
 * <p>ActorIntegrationProfile class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("actorIntegrationProfile")
@Table(name = "tf_actor_integration_profile", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {"actor_id","integration_profile_id"}))
@SequenceGenerator(name = "tf_actor_integration_profile_sequence", sequenceName = "tf_actor_integration_profile_id_seq", allocationSize=1)
public class ActorIntegrationProfile implements java.io.Serializable , Cloneable {


	/** Constant <code>log</code> */
	@Logger
	public static  Log log;


	/** Serial ID version of this object */
	private static final long serialVersionUID = -450911138458283760L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tf_actor_integration_profile_sequence")
	private Integer id;


	@ManyToOne
	@JoinColumn(name = "actor_id")
	private Actor actor;

	@ManyToOne
	@JoinColumn(name = "integration_profile_id")
	private IntegrationProfile integrationProfile;

	//	Constructors



	/**
	 * <p>Constructor for ActorIntegrationProfile.</p>
	 */
	public ActorIntegrationProfile() {
	}



	/**
	 * <p>Constructor for ActorIntegrationProfile.</p>
	 *
	 * @param actor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 * @param integrationProfile a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile} object.
	 */
	public ActorIntegrationProfile(Actor actor,
			IntegrationProfile integrationProfile) {
		this.actor = actor;
		this.integrationProfile = integrationProfile;
	}


	/**
	 * <p>Constructor for ActorIntegrationProfile.</p>
	 *
	 * @param inActorIntegrationProfile a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfile} object.
	 */
	public ActorIntegrationProfile( ActorIntegrationProfile inActorIntegrationProfile ) {
		if ( inActorIntegrationProfile.actor != null  ) actor = new Actor( inActorIntegrationProfile.actor )  ; 
		if ( inActorIntegrationProfile.integrationProfile != null  ) integrationProfile = new IntegrationProfile(inActorIntegrationProfile.integrationProfile ) ; 

	}




	// *********************************************************
	//  Getters and Setters : setup the DB columns properties
	// *********************************************************




	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * <p>Getter for the field <code>actor</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public Actor getActor() {
		return actor;
	}


	/**
	 * <p>Setter for the field <code>actor</code>.</p>
	 *
	 * @param actor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public void setActor(Actor actor) {
		this.actor = actor;
	}


	/**
	 * <p>Getter for the field <code>integrationProfile</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile} object.
	 */
	public IntegrationProfile getIntegrationProfile() {
		return integrationProfile;
	}


	/**
	 * <p>Setter for the field <code>integrationProfile</code>.</p>
	 *
	 * @param integrationProfile a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile} object.
	 */
	public void setIntegrationProfile(IntegrationProfile integrationProfile) {
		this.integrationProfile = integrationProfile;
	}


	/**
	 * <p>compareTo.</p>
	 *
	 * @param aip a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfile} object.
	 * @return a int.
	 */
	public int compareTo(ActorIntegrationProfile aip)
	{
		if ( aip == null ) return -1 ;


		if ( actor != null && aip.actor != null && actor.compareTo(aip.actor) == 0 )
		{
			if ( integrationProfile!= null && aip.integrationProfile!= null && integrationProfile.compareTo(aip.integrationProfile) == 0  )
			{
				return 0;

			}
			else if ( integrationProfile == null  )  return 1  ;
			else if ( aip.integrationProfile == null ) return -1 ;
			else
			{
				return integrationProfile.compareTo( aip.integrationProfile );
			}
		}
		else if ( actor == null ) return 1 ;
		else if ( aip.actor == null ) return -1 ;
		else
		{
			return actor.compareTo(aip.actor) ;
		}



	}
	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString()
	{
		return ( integrationProfile != null ? integrationProfile.toString() + " with " : "" ) + ( actor != null ? actor.toString() : "" ) ;
	}

	/**
	 * <p>findActorIntegrationProfile.</p>
	 *
	 * @param actorIntegrationProfile a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfile} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfile} object.
	 */
	public static ActorIntegrationProfile findActorIntegrationProfile(ActorIntegrationProfile actorIntegrationProfile){
		EntityManager  em = ( EntityManager) Component.getInstance("entityManager") ;
		Query q = em.createQuery("SELECT aip " +
				"FROM ActorIntegrationProfile aip " +
				"WHERE aip.actor.keyword = :inActorKeyword " +
		"AND aip.integrationProfile.keyword=:inIntegrationProfileKeyword") ;
		q.setParameter("inActorKeyword", actorIntegrationProfile.getActor().getKeyword() ) ;
		q.setParameter("inIntegrationProfileKeyword", actorIntegrationProfile.getIntegrationProfile().getKeyword() ) ;

		List<ActorIntegrationProfile> result = q.getResultList() ;
		if(result.size()>0)
			return result.get(0);
		return null;
	}

	/**
	 * <p>mergeActorIntegrationProfile.</p>
	 *
	 * @param inActorIntegrationProfile a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfile} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfile} object.
	 */
	public static ActorIntegrationProfile mergeActorIntegrationProfile(ActorIntegrationProfile inActorIntegrationProfile){
		if(inActorIntegrationProfile!=null){
			EntityManager  entityManager = ( EntityManager) Component.getInstance("entityManager") ;
			ActorIntegrationProfile actorIntegrationProfile=ActorIntegrationProfile.findActorIntegrationProfile(inActorIntegrationProfile);
			if(actorIntegrationProfile==null){
				Actor actor=Actor.mergeActor(inActorIntegrationProfile.getActor());
				if(actor!=null){
					IntegrationProfile integrationProfile=IntegrationProfile.mergeIntegrationProfile(inActorIntegrationProfile.getIntegrationProfile());
					if(integrationProfile!=null){
						actorIntegrationProfile=new ActorIntegrationProfile();
						actorIntegrationProfile.setActor(actor);
						actorIntegrationProfile.setIntegrationProfile(integrationProfile);
						actorIntegrationProfile=entityManager.merge(actorIntegrationProfile);
					}
				}
			}
			return actorIntegrationProfile;
		}
		return null;
	}



    /**
     * <p>hashCode.</p>
     *
     * @return a int.
     */
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((actor == null) ? 0 : actor.hashCode());
        result = prime
                * result
                + ((integrationProfile == null) ? 0 : integrationProfile
                        .hashCode());
        return result;
    }


    /** {@inheritDoc} */
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ActorIntegrationProfile other = (ActorIntegrationProfile) obj;
        if (actor == null) {
            if (other.actor != null)
                return false;
        } else if (!actor.equals(other.actor))
            return false;
        if (integrationProfile == null) {
            if (other.integrationProfile != null)
                return false;
        } else if (!integrationProfile.equals(other.integrationProfile))
            return false;
        return true;
    }

}

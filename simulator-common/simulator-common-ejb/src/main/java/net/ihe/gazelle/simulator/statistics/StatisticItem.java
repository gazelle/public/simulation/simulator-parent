package net.ihe.gazelle.simulator.statistics;

import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;

import java.io.Serializable;

/**
 * <p>StatisticItem class.</p>
 *
 * @author aberge
 * @version 1.0: 04/10/17
 */

public class StatisticItem implements Comparable<StatisticItem>, Serializable {

    private static final String ZERO = "0";

    private String label;
    private String percent;
    private String count;
    private int intValue;

    public StatisticItem(Object[] stat, int total) {
        createStatisticItem(stat, total);
    }

    private void createStatisticItem(Object[] stat, int total) {
        Object labelObj = stat[0];
        this.intValue = ((Number) stat[1]).intValue();
        this.count = Integer.toString(intValue);
        this.percent = getPercentage(intValue, total);
        if (labelObj instanceof Actor) {
            this.label = ((Actor) labelObj).getName();
        } else if (labelObj instanceof Domain) {
            this.label = ((Domain) labelObj).getName();
        } else if (labelObj instanceof Transaction) {
            Transaction transaction = (Transaction) labelObj;
            this.label = "[" + transaction.getKeyword() + "] " + transaction.getName();
        }
    }

    private String getPercentage(Object value, Integer total) {
        int intValue = ((Number) value).intValue();
        if (total > 0) {
            int percent = intValue * 100 / total;
            return Integer.toString(percent);
        } else {
            return ZERO;
        }
    }

    public String getLabel() {
        return label;
    }

    public String getPercent() {
        return percent;
    }

    public String getCount() {
        return count;
    }

    public int getIntValue() {
        return intValue;
    }

    @Override
    public int compareTo(StatisticItem o) {
        return o.intValue - this.intValue;
    }
}

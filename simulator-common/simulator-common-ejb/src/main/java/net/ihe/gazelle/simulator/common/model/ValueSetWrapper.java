package net.ihe.gazelle.simulator.common.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * <p>ValueSetWrapper class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlRootElement(name = "ValueSets")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValueSetWrapper {

    @XmlElementRefs(@XmlElementRef(name = "ValueSet", type = ValueSet.class))
    private List<ValueSet> valueSets;
    @XmlAttribute(name = "countOfAccessible")
    private Integer accessible;
    @XmlAttribute(name = "total")
    private Integer count;

    /**
     * <p>Constructor for ValueSetWrapper.</p>
     */
    public ValueSetWrapper(){

    }

    /**
     * <p>Constructor for ValueSetWrapper.</p>
     *
     * @param valueSetList a {@link java.util.List} object.
     */
    public ValueSetWrapper(List<ValueSet> valueSetList) {
        this.valueSets = valueSetList;
    }

    /**
     * <p>Getter for the field <code>valueSets</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<ValueSet> getValueSets() {
        return valueSets;
    }

    /**
     * <p>Setter for the field <code>valueSets</code>.</p>
     *
     * @param valueSets a {@link java.util.List} object.
     */
    public void setValueSets(List<ValueSet> valueSets) {
        this.valueSets = valueSets;
    }

    /**
     * <p>Setter for the field <code>count</code>.</p>
     *
     * @param count a {@link java.lang.Integer} object.
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * <p>Setter for the field <code>accessible</code>.</p>
     *
     * @param accessible a {@link java.lang.Integer} object.
     */
    public void setAccessible(Integer accessible) {
        this.accessible = accessible;
    }

    /**
     * <p>Getter for the field <code>accessible</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getAccessible() {
        return accessible;
    }

    /**
     * <p>Getter for the field <code>count</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getCount() {
        return count;
    }
}

package net.ihe.gazelle.simulator.ws;

import java.util.Arrays;

import javax.ejb.Stateful;
import javax.ejb.Stateless;

import net.ihe.gazelle.simulator.common.model.ApplicationConfigurationQuery;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstanceQuery;
import net.ihe.gazelle.simulator.report.TestReportData;
import net.ihe.gazelle.simulator.report.TestReportDataMessage;
import net.ihe.gazelle.simulator.report.TestReportTool;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.JndiName;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <b>Class description</b>: TestReportImpl
 *
 * This is an implementation of TestReportLocal interface. It can be used if your simulator uses the TransactionInstance entity to store the sent/received messages and the associated validation
 * results
 *
 *
 * @author Anne-Gaëlle Bergé / IHE Europe

 * @version 1.0
 */
@Stateless
@Name("TestReportImpl")
public class TestReportImpl extends TestReport implements TestReportLocal {

	private static Logger log = LoggerFactory.getLogger(TestReportImpl.class);

	/** {@inheritDoc} */
	@Override
	protected TestReportData newInstance(String testId, String test) {
		TestReportData report = new TestReportData(testId, test);
		setTestResults(report);
		ApplicationConfigurationQuery appConfQuery = new ApplicationConfigurationQuery();
		appConfQuery.variable().eq("application_name");
		String applicationName = null;
		try {
			 applicationName = appConfQuery.getUniqueResult().getValue();
		} catch (NullPointerException e) {
			log.error("No value set for application_name in app_configuration table");
		}
		appConfQuery = new ApplicationConfigurationQuery();
		appConfQuery.variable().eq("application_url");
		String applicationUrl = null;
		try {
			applicationUrl = appConfQuery.getUniqueResult().getValue();
		} catch (NullPointerException e) {
			log.error("No value set for application_url in app_configuration table");
		}
		TestReportTool tool = new TestReportTool(applicationName, applicationUrl);
		report.setTool(tool);
		return report;
	}

	/**
	 * <p>setTestResults.</p>
	 *
	 * @param report a {@link net.ihe.gazelle.simulator.report.TestReportData} object.
	 */
	protected void setTestResults(TestReportData report) {
		if (report.getTestId() == null || report.getTestId().isEmpty()) {
			log.error("received testId is null or empty !");
			return;
		}
		TransactionInstanceQuery query = new TransactionInstanceQuery();
		query.id().eq(Integer.decode(report.getTestId()));
		TransactionInstance transactionInstance = query.getUniqueResult();
		if (transactionInstance == null) {
			log.error("no message found with id " + report.getTestId());
			return;
		} else {
			report.setTest(null);
			report.setSource(null);
			report.setTarget(null);
			if (transactionInstance.getTransaction() != null) {
				report.setTest(transactionInstance.getTransaction().getKeyword());
			}
			MessageInstance request = transactionInstance.getRequest();
			TestReportDataMessage requestInfo = null;
			if (request != null) {
				report.setSource(transactionInstance.getRequest().getIssuer());
				requestInfo = new TestReportDataMessage("request", request.getType(), request.getValidationStatus(), request
						.getIssuingActor().getKeyword(), request.getId().toString());
			}
			MessageInstance response = transactionInstance.getResponse();
			TestReportDataMessage responseInfo = null;
			if (response != null) {
				report.setTarget(transactionInstance.getResponse().getIssuer());
				responseInfo = new TestReportDataMessage("response", response.getType(), response.getValidationStatus(),
						response.getIssuingActor().getKeyword(), response.getId().toString());
			}
			report.setMessages(Arrays.asList(requestInfo, responseInfo));
			report.setAcknowledgmentCode(null);
			report.setDate(transactionInstance.getTimestamp());
			ApplicationConfigurationQuery appConfQuery = new ApplicationConfigurationQuery();
			appConfQuery.variable().eq("message_permanent_link");
			try {
				report.setLogUrl(appConfQuery.getUniqueResult().getValue()
						.concat(transactionInstance.getId().toString()));
			} catch (NullPointerException e) {
				log.error("message_permanent_link variable is not defined in app_configuration table");
			}
		}

	}

}

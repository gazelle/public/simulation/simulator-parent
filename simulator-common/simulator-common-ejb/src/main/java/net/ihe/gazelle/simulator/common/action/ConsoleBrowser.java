package net.ihe.gazelle.simulator.common.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulator.common.model.ReceiverConsole;
import net.ihe.gazelle.simulator.common.model.ReceiverConsoleQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Map;

/**
 * <b>Class Description : </b>ConsoleBrowser<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 12/05/16
 */
@Name("consoleBrowser")
@Scope(ScopeType.PAGE)
public class ConsoleBrowser implements Serializable{

    private Filter<ReceiverConsole> filter;

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<ReceiverConsole> getFilter(){
        if (filter == null){
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            filter = new Filter<ReceiverConsole>(getHQLCriterionForFilter(), params);
        }
        return filter;
    }


    /**
     * <p>getHQLCriterionForFilter.</p>
     *
     * @return a {@link net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter} object.
     */
    public HQLCriterionsForFilter<ReceiverConsole> getHQLCriterionForFilter() {
        ReceiverConsoleQuery query = new ReceiverConsoleQuery();
        HQLCriterionsForFilter<ReceiverConsole> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("domain", query.domain());
        criteria.addPath("simulatedActor", query.simulatedActor());
        criteria.addPath("transaction", query.transaction());
        criteria.addPath("timestamp", query.timestamp());
        criteria.addPath("code", query.code());
        criteria.addPath("sut", query.sut());
        criteria.addPath("mtype", query.messageType());
        return criteria;
    }

    /**
     * <p>getLogs.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<ReceiverConsole> getLogs(){
        return new FilterDataModel<ReceiverConsole>(getFilter()) {
            @Override
            protected Object getId(ReceiverConsole receiverConsole) {
                return receiverConsole.getId();
            }
        };
    }

    /**
     * <p>reset.</p>
     */
    public void reset(){
        getFilter().clear();
    }
}

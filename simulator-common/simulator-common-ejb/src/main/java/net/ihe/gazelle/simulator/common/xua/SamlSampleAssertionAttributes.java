package net.ihe.gazelle.simulator.common.xua;

import java.io.Serializable;

/**
 * The Class SamlSampleAssertionAttributes.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class SamlSampleAssertionAttributes extends SamlAssertionAttributes implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1223954824205101723L;

	/**
	 * Instantiates a new saml sample assertion attributes.
	 */
	public SamlSampleAssertionAttributes() {
		super();
		setXSPASubject("Dr. Muller");
		setXSPAOrganization("Vienna AKH");
		setXSPAOrganizationId("urn:oid:1.2.3.4.5.6.7");
		setEpSOSHealthcareFacilityType("Resident Physician");
		// setHITSPClinicalSpeciality("???");
		setXSPARole("Physician");
		setXSPALocality("vienna-akh");
		setXSPAPurposeOfUse("TREATMENT");

		getXSPAPermissionsHL7().add("urn:oasis:names:tc:xspa:1.0:hl7:PRD-006");
		getXSPAPermissionsHL7().add("urn:oasis:names:tc:xspa:1.0:hl7:PRD-017");
		getXSPAPermissionsHL7().add("urn:oasis:names:tc:xspa:1.0:hl7:PRD-010");
	}

}

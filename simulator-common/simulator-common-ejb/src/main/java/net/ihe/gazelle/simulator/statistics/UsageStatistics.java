package net.ihe.gazelle.simulator.statistics;

import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionNeq;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.international.LocaleSelector;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * <p>UsageStatistics class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("usageStatisticsManager")
@Scope(ScopeType.PAGE)
public class UsageStatistics implements Serializable {

    /** Constant <code>ALL=1</code> */
    public static final int ALL = 1;
    /** Constant <code>INCLUDE=2</code> */
    public static final int INCLUDE = 2;
    /** Constant <code>EXCLUDE=3</code> */
    public static final int EXCLUDE = 3;
    /** Constant <code>YEAR=1</code> */
    public static final int YEAR = 1;
    /** Constant <code>MONTH=2</code> */
    public static final int MONTH = 2;
    /**
     * This comparator is used to sort the statistics by decreasing result
     */
    static final Comparator<Object[]> STATISTICS_COMPARATOR = new Comparator<Object[]>() {

        @Override
        public int compare(Object[] o1, Object[] o2) {
            return (((Number) o2[1]).intValue() - ((Number) o1[1]).intValue());
        }

    };
    static final Comparator<Object[]> PIE_LABEL_COMPARATOR = new Comparator<Object[]>() {

        @Override
        public int compare(Object[] o1, Object[] o2) {
            String label1 = (String) o1[0];
            String label2 = (String) o2[0];
            if (label1 == null) {
                return 1;
            } else if (label2 == null) {
                return -1;
            } else {
                return label1.compareTo(label2);
            }
        }
    };
    /**
     *
     */
    private static final long serialVersionUID = 7148070946922498828L;
    private Date startDate;
    private Date endDate;
    private HQLQueryBuilderForStatistics queryBuilder;
    private int ipFilter;
    private String ipStartsWith;
    private int selectedInterval;

    /**
     * <p>reset.</p>
     */
    @Create
    public void reset() {
        startDate = null;
        endDate = null;
        ipFilter = ALL;
        ipStartsWith = null;
        selectedInterval = YEAR;
        setQueryBuilder(true);
    }

    /**
     * <p>Setter for the field <code>queryBuilder</code>.</p>
     *
     * @param setRestrictionsOnDate a boolean.
     */
    public void setQueryBuilder(boolean setRestrictionsOnDate) {
        queryBuilder = new HQLQueryBuilderForStatistics();
        if (setRestrictionsOnDate && startDate != null) {
            queryBuilder.addRestriction(HQLRestrictions.ge("date", startDate));
        }
        if (setRestrictionsOnDate && endDate != null) {
            queryBuilder.addRestriction(HQLRestrictions.le("date", endDate));
        }
        if (ipStartsWith != null && !ipStartsWith.isEmpty()) {
            switch (ipFilter) {
                case INCLUDE:
                    queryBuilder.addRestriction(HQLRestrictions.like("caller", ipStartsWith, HQLRestrictionLikeMatchMode.START));
                    break;
                case EXCLUDE:
                    queryBuilder.addRestriction(new HQLRestrictionNeq("caller", ipStartsWith));
                    break;
                default:
            }
        }
    }

    /**
     * <p>getResultsPerValidator.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Object[]> getResultsPerValidator() {
        List<Object[]> stats = queryBuilder.getStatistics("type");
        Collections.sort(stats, STATISTICS_COMPARATOR);
        return stats;
    }

    /**
     * <p>getStatisticsPerResultStatus.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Object[]> getStatisticsPerResultStatus() {
        List<Object[]> stats = queryBuilder.getStatistics("status");

        Object[] passed = {ValidationStatus.PASSED.getStatus(), 0};
        Object[] failed = {ValidationStatus.FAILED.getStatus(), 0};

        List<Object[]> statsWithAllStatus = new ArrayList<Object[]>();
        // if stats were returned for a status, take them
        for (Object[] object : stats) {
            String label = (String) object[0];
            if (label == null) {
                continue;
            } else if (label.equals(ValidationStatus.FAILED.getStatus())) {
                failed = object;
            } else if (label.equals(ValidationStatus.PASSED.getStatus())) {
                passed = object;
            }
        }
        statsWithAllStatus.add(passed);
        statsWithAllStatus.add(failed);

        Collections.sort(statsWithAllStatus, PIE_LABEL_COMPARATOR);
        return statsWithAllStatus;
    }

    /**
     * <p>getStatisticsPerCallerIp.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Object[]> getStatisticsPerCallerIp() {
        List<Object[]> stats = queryBuilder.getStatistics("caller");
        Collections.sort(stats, STATISTICS_COMPARATOR);
        return stats;
    }

    /**
     * <p>getStatisticsPerDate.</p>
     *
     * @param interval a int.
     * @return a {@link java.util.List} object.
     */
    public List<Object[]> getStatisticsPerDate(int interval) {
        // if the startDate attribute is not valued, retrieve the date of the oldest validation for the given criteria
        if (startDate == null) {
            startDate = queryBuilder.getMinDate();
            // the first time no value register in DB
            if (getStartDate() == null) {
                startDate = new Date();
            }
        }
        // if the endDate attribute is not values, set it as today
        if (endDate == null) {
            endDate = new Date();
        }
        // recreate a query builder without the restrictions on dates
        setQueryBuilder(false);

        List<Object[]> statistics = new ArrayList<Object[]>();

        SimpleDateFormat formatYear = new SimpleDateFormat("yyyy", LocaleSelector.instance().getLocale());
        SimpleDateFormat formatMonthYear = new SimpleDateFormat("MMM yy", LocaleSelector.instance().getLocale());

        Locale locale = LocaleSelector.instance().getLocale();

        Calendar startDateAsCalendar = Calendar.getInstance(locale);
        Calendar endDateAsCalendar = Calendar.getInstance(locale);
        Calendar intervalBegin = Calendar.getInstance(locale);
        Calendar intervalEnd = Calendar.getInstance(locale);

        // set start date with the startdate at midnight
        startDateAsCalendar.setTime(startDate);
        startDateAsCalendar.set(Calendar.HOUR_OF_DAY, 0);
        startDateAsCalendar.set(Calendar.MINUTE, 0);
        startDateAsCalendar.set(Calendar.SECOND, 0);

        // set end date with the following date at midnight
        endDateAsCalendar.setTime(endDate);
        endDateAsCalendar.add(Calendar.DAY_OF_YEAR, +1);
        endDateAsCalendar.set(Calendar.HOUR_OF_DAY, 0);
        endDateAsCalendar.set(Calendar.MINUTE, 0);
        endDateAsCalendar.set(Calendar.SECOND, 0);

        intervalBegin.set(startDateAsCalendar.get(Calendar.YEAR),
                startDateAsCalendar.get(Calendar.MONTH),
                startDateAsCalendar.get(Calendar.DAY_OF_MONTH), 0, 0);

        // interval size is month
        if (interval == MONTH) {
            intervalBegin.set(Calendar.DAY_OF_MONTH, 1);
            // first interval begining of the firstMonth at the end of the current month
            intervalEnd.set(intervalBegin.get(Calendar.YEAR),
                    intervalBegin.get(Calendar.MONTH),
                    intervalBegin.get(Calendar.DAY_OF_MONTH), 0, 0);
            if (intervalEnd.get(Calendar.MONTH) == Calendar.DECEMBER) {
                intervalEnd.add(Calendar.YEAR, 1);
                intervalEnd.set(Calendar.MONTH, Calendar.JANUARY);
            } else {
                intervalEnd.add(Calendar.MONTH, 1);
            }
        } else if (interval == YEAR) {
            // first interval ends at the end of the current year
            intervalEnd.set(Calendar.DAY_OF_YEAR, 1);
            intervalEnd.set(Calendar.YEAR, startDateAsCalendar.get(Calendar.YEAR) + 1);
            intervalEnd.set(Calendar.HOUR_OF_DAY, 0);
            intervalEnd.set(Calendar.MINUTE, 0);
            intervalEnd.set(Calendar.SECOND, 0);
        }


        while (intervalBegin.before(endDateAsCalendar)) {
            setQueryBuilder(false);
            queryBuilder.addRestriction(HQLRestrictions.and(
                    HQLRestrictions.ge("date", intervalBegin.getTime()),
                    HQLRestrictions.lt("date", intervalEnd.getTime())));
            String dateLabel = null;
            if (interval == MONTH) {
                dateLabel = formatMonthYear.format(intervalBegin.getTime());
            } else {
                dateLabel = formatYear.format(intervalBegin.getTime());
            }
            Object[] stat = {dateLabel, queryBuilder.getCount()};
            statistics.add(stat);

            intervalBegin.set(intervalEnd.get(Calendar.YEAR), intervalEnd.get(Calendar.MONTH), intervalEnd.get(Calendar.DATE), intervalEnd.get(Calendar.HOUR_OF_DAY), intervalEnd.get(Calendar.MINUTE), intervalEnd.get(Calendar.SECOND));

            if (interval == MONTH) {
                if (intervalEnd.get(Calendar.MONTH) == Calendar.DECEMBER) {
                    intervalEnd.add(Calendar.YEAR, 1);
                    intervalEnd.set(Calendar.MONTH, Calendar.JANUARY);
                } else {
                    intervalEnd.add(Calendar.MONTH, 1);
                }
            } else {
                intervalEnd.add(Calendar.YEAR, 1);
            }
        }

        return statistics;
    }

    /**
     * <p>Getter for the field <code>startDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * <p>Setter for the field <code>startDate</code>.</p>
     *
     * @param startDate a {@link java.util.Date} object.
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * <p>Getter for the field <code>endDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * <p>Setter for the field <code>endDate</code>.</p>
     *
     * @param endDate a {@link java.util.Date} object.
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * <p>Getter for the field <code>ipFilter</code>.</p>
     *
     * @return a int.
     */
    public int getIpFilter() {
        return ipFilter;
    }

    /**
     * <p>Setter for the field <code>ipFilter</code>.</p>
     *
     * @param ipFilter a int.
     */
    public void setIpFilter(int ipFilter) {
        this.ipFilter = ipFilter;
    }

    /**
     * <p>Getter for the field <code>ipStartsWith</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getIpStartsWith() {
        return ipStartsWith;
    }

    /**
     * <p>Setter for the field <code>ipStartsWith</code>.</p>
     *
     * @param ipStartsWith a {@link java.lang.String} object.
     */
    public void setIpStartsWith(String ipStartsWith) {
        this.ipStartsWith = ipStartsWith;
    }

    /**
     * <p>Getter for the field <code>selectedInterval</code>.</p>
     *
     * @return a int.
     */
    public int getSelectedInterval() {
        return selectedInterval;
    }

    /**
     * <p>Setter for the field <code>selectedInterval</code>.</p>
     *
     * @param selectedInterval a int.
     */
    public void setSelectedInterval(int selectedInterval) {
        this.selectedInterval = selectedInterval;
    }

    /**
     * <p>getALL.</p>
     *
     * @return a int.
     */
    public int getALL() {
        return ALL;
    }

    /**
     * <p>getINCLUDE.</p>
     *
     * @return a int.
     */
    public int getINCLUDE() {
        return INCLUDE;
    }

    /**
     * <p>getEXCLUDE.</p>
     *
     * @return a int.
     */
    public int getEXCLUDE() {
        return EXCLUDE;
    }

    /**
     * <p>getYEAR.</p>
     *
     * @return a int.
     */
    public int getYEAR() {
        return YEAR;
    }

    /**
     * <p>getMONTH.</p>
     *
     * @return a int.
     */
    public int getMONTH() {
        return MONTH;
    }
}

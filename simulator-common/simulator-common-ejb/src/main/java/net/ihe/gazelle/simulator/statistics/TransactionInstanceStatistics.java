package net.ihe.gazelle.simulator.statistics;

import net.ihe.gazelle.simulator.message.model.TransactionInstanceQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * <p>TransactionInstanceStatistics class.</p>
 *
 * @author aberge
 * @version 1.0: 04/10/17
 */

@Name("tiStatistics")
@Scope(ScopeType.PAGE)
public class TransactionInstanceStatistics implements Serializable {

    private Integer countDomain = 0;
    private Integer countActor = 0;
    private Integer countTransaction = 0;
    private TransactionInstanceQuery query;

    @Create
    public void getTotalCount() {
        query = new TransactionInstanceQuery();
        query.domain().isNotNull();
        query.simulatedActor().isNotNull();
        query.transaction().isNotNull();
        countDomain = query.domain().getCountOnPath();
        countActor = query.simulatedActor().getCountOnPath();
        countTransaction = query.transaction().getCountOnPath();
    }

    static final Comparator<Object[]> STATISTICS_COMPARATOR = new Comparator<Object[]>() {

        @Override
        public int compare(Object[] o1, Object[] o2) {
            return (((Number) o2[1]).intValue() - ((Number) o1[1]).intValue());
        }

    };

    public List<StatisticItem> getStatsOnTransaction() {
        List<Object[]> stats = query.transaction().getStatistics();
        return buildStatisticsItems(stats, countTransaction);
    }

    public List<StatisticItem> getStatsOnSimulatedActor() {
        List<Object[]> stats = query.simulatedActor().getStatistics();
        return buildStatisticsItems(stats, countActor);
    }

    public List<StatisticItem> getStatsOnDomain() {
        List<Object[]> stats = query.domain().getStatistics();
        return buildStatisticsItems(stats, countDomain);
    }

    private List<StatisticItem> buildStatisticsItems(List<Object[]> stats, int total) {
        List<StatisticItem> items = new ArrayList<StatisticItem>();
        for (Object[] obj : stats) {
            items.add(new StatisticItem(obj, total));
        }
        Collections.sort(items);
        return items;
    }
}

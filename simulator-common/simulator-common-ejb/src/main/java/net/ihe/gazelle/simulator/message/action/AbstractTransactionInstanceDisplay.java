package net.ihe.gazelle.simulator.message.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.MessageInstanceMetadata;
import net.ihe.gazelle.simulator.message.model.MessageInstanceMetadataQuery;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstanceQuery;
import net.ihe.gazelle.simulator.statistics.ValidatorUsage;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.simulator.sut.model.SystemConfigurationQuery;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <b>Abstract class description </b>: AbstractTransactionInstanceDisplay
 * <p/>
 * This abstract class should be extended in order to display the list of
 * TransactionInstances in which the simulator has been involved
 *
 * @author Anne-Gaëlle Bergé / IHE Europe
 * @version 1.0 - August, 3rd 2012
 */
@SuppressWarnings("serial")
public abstract class AbstractTransactionInstanceDisplay
        implements
        Serializable, QueryModifier<TransactionInstance> {

    protected Filter<TransactionInstance> filter;
    protected boolean onlyMyMessages = false;
    protected TransactionInstance selectedInstance;
    private Boolean restrictAccessToMessages = null;


    /**
     * <p>Getter for the field <code>sutForReplay</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.sut.model.SystemConfiguration} object.
     */
    public SystemConfiguration getSutForReplay() {
        return sutForReplay;
    }

    /**
     * <p>Setter for the field <code>sutForReplay</code>.</p>
     *
     * @param sutForReplay a {@link net.ihe.gazelle.simulator.sut.model.SystemConfiguration} object.
     */
    public void setSutForReplay(SystemConfiguration sutForReplay) {
        this.sutForReplay = sutForReplay;
    }

    protected SystemConfiguration sutForReplay;


    /**
     * the permanent link to the given transaction instance,
     * url key to access the transaction instance is the "id" attribute of this object
     *
     * @param instance a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     *
     * @return a {@link java.lang.String} object.
     */
    public String permanentLink(TransactionInstance instance) {
        try {
            return ApplicationConfiguration.getValueOfVariable("message_permanent_link").concat(instance.getId().toString());
        } catch (NullPointerException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "message_permanent_link is not defined in app_configuration table of this " +
                    "application !");
            return null;
        }
    }


    /**
     * the permanent link to the test report (REST web service). This service is
     * implemented by this module but its location has to be defined in your web.xml,
     * http://gazelle.ihe.net/content/restful-webservices-running-jboss
     *
     * @return a {@link java.lang.String} object.
     */
    public abstract String permanentLinkToTestReport();

    /**
     * Validates and stores the result of validation of both the request and the response
     *
     * @param instance a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public void validate(TransactionInstance instance) {
        selectedInstance = instance;
        validateRequest();
        validateResponse();
    }

    /**
     * Returns an html string to display the result of the validation
     *
     * @param messageInstance a {@link net.ihe.gazelle.simulator.message.model.MessageInstance} object.
     *
     * @return a {@link java.lang.String} object.
     */
    public abstract String getPrettyFormattedResult(
            MessageInstance messageInstance);

    /**
     * Returns the list of selected transaction instances
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<TransactionInstance> getTransactionInstanceList() {
        return new FilterDataModel<TransactionInstance>(getFilter()) {
            @Override
            protected Object getId(TransactionInstance transactionInstance) {
                return transactionInstance.getId();
            }
        };
    }

    /**
     * <p>getTransactionInstanceFromUrl.</p>
     */
    public void getTransactionInstanceFromUrl() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        selectedInstance = null;
        if (urlParams != null && urlParams.containsKey("id")) {
            try {
                Integer key = Integer.parseInt(urlParams.get("id"));
                EntityManager entityManager = EntityManagerService.provideEntityManager();
                TransactionInstance instance = entityManager.find(TransactionInstance.class, key);
                // if the restrict_access_to_messages pref is on, anonymous users shall not be able to access data which are owned by a company in
                // particular
                if (getRestrictAccessToMessages()
                        && !Identity.instance().isLoggedIn() && instance.getCompanyKeyword() != null) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "You are not allowed to access those data");
                    selectedInstance = null;
                } else {
                    selectedInstance = instance;
                }
            } catch (NumberFormatException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Bad identifier format, must be an integer");
            }
        }
    }

    /**
     * This method should be called each time a message request / response is
     * validated from this page
     *
     * @param validator a {@link java.lang.String} object.
     * @param status    a {@link java.lang.String} object.
     */
    protected void addValidatorUsage(String validator, String status) {
        ValidatorUsage usage = new ValidatorUsage(new Date(), status,
                validator, "SIMULATOR");
        usage.save();
    }

    /**
     * <p>hideMessage.</p>
     *
     * @param instance a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     * @param hide     a boolean.
     */
    public void hideMessage(TransactionInstance instance, boolean hide) {
        if (instance != null) {
            instance.setVisible(!hide);
            instance.save((EntityManager) Component
                    .getInstance("entityManager"));
        }
    }


    /**
     * <p>reset.</p>
     */
    public void reset() {
        getFilter().clear();
        onlyMyMessages = false;
    }

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<TransactionInstance> getFilter() {
        if (this.filter == null) {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            filter = new Filter<TransactionInstance>(getHQLCriteriaForFilter(), params);
        }
        return this.filter;
    }

    private HQLCriterionsForFilter<TransactionInstance> getHQLCriteriaForFilter() {
        TransactionInstanceQuery query = new TransactionInstanceQuery();
        HQLCriterionsForFilter<TransactionInstance> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("transaction", query.transaction());
        criteria.addPath("timestamp", query.timestamp());
        criteria.addPath("domain", query.domain());
        criteria.addPath("simulatedActor", query.simulatedActor());
        criteria.addPath("initiator", query.request().issuingActor());
        criteria.addPath("responder", query.response().issuingActor());
        criteria.addPath("requestIssuer", query.request().issuer());
        criteria.addPath("responseIssuer", query.response().issuer());
        criteria.addQueryModifier(this);
        return criteria;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modifyQuery(HQLQueryBuilder<TransactionInstance> queryBuilder, Map<String, Object> var2) {
        TransactionInstanceQuery query = new TransactionInstanceQuery();
        if (Identity.instance().isLoggedIn()) {
            if (!Identity.instance().hasRole("admin_role") && !Identity.instance().hasRole("monitor_role")) {
                if (getRestrictAccessToMessages() || onlyMyMessages) {
                    List<String> ipList = getIpListForLoggedInUser();
                    queryBuilder.addRestriction(
                            HQLRestrictions.or(query.companyKeyword().eqRestriction(getUserCompanyKeyword()),
                                    query.request().issuerIpAddress().inRestriction(ipList)));
                } else {
                    queryBuilder.addRestriction(query.visible().eqRestriction(true));
                }
            }
        } else {
            queryBuilder.addRestriction(query.visible().eqRestriction(true));
            if (getRestrictAccessToMessages()) {
                queryBuilder.addRestriction(HQLRestrictions.and(
                        query.companyKeyword().isNullRestriction(),
                        query.request().issuerIpAddress().isNullRestriction()
                ));
            }
        }
    }

    private String getUserCompanyKeyword() {
        GazelleIdentity gazelleIdentity = GazelleIdentityImpl.instance();
        return gazelleIdentity.getOrganisationKeyword();
    }

    private void setRestrictAccessToMessagesFromPreferences() {
        Boolean prefValue = PreferenceService.getBoolean("restrict_access_to_messages");
        // If the preference does not exist, the default behaviour is to disable the restriction
        restrictAccessToMessages = (prefValue != null) ? prefValue : false;
    }

    public Boolean getRestrictAccessToMessages() {
        if (restrictAccessToMessages == null) {
            setRestrictAccessToMessagesFromPreferences();
        }
        return restrictAccessToMessages;
    }


    /**
     * <p>validateResponse.</p>
     */
    public abstract void validateResponse();

    /**
     * <p>validateRequest.</p>
     */
    public abstract void validateRequest();

    /**
     * <p>validateResponse.</p>
     *
     * @param ti a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public abstract void validateResponse(TransactionInstance ti);

    /**
     * <p>validateRequest.</p>
     *
     * @param ti a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public abstract void validateRequest(TransactionInstance ti);

    /**
     * <p>isOnlyMyMessages.</p>
     *
     * @return a boolean.
     */
    public boolean isOnlyMyMessages() {
        return onlyMyMessages;
    }

    /**
     * <p>Setter for the field <code>onlyMyMessages</code>.</p>
     *
     * @param onlyMyMessages a boolean.
     */
    public void setOnlyMyMessages(boolean onlyMyMessages) {
        this.onlyMyMessages = onlyMyMessages;
    }

    /**
     * <p>Getter for the field <code>selectedInstance</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public TransactionInstance getSelectedInstance() {
        return selectedInstance;
    }

    /**
     * <p>Setter for the field <code>selectedInstance</code>.</p>
     *
     * @param selectedInstance a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public void setSelectedInstance(TransactionInstance selectedInstance) {
        this.selectedInstance = selectedInstance;
    }

    /**
     * <p>setMessageForReplay.</p>
     *
     * @param inInstance a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public void setMessageForReplay(TransactionInstance inInstance) {
        this.selectedInstance = inInstance;
    }

    /**
     * <p>getAvailableRespondersForReplay.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SystemConfiguration> getAvailableRespondersForReplay() {
        if (selectedInstance != null) {
            SystemConfigurationQuery query = new SystemConfigurationQuery();
            query.listUsages().transaction().eq(selectedInstance.getTransaction());
            query.isAvailable().eq(true);
            if (!Identity.instance().isLoggedIn()) {
                query.isPublic().eq(true);
            } else if (!Identity.instance().hasRole("admin_role")) {
                query.addRestriction(HQLRestrictions.or(query.isPublic().eqRestriction(true),
                        query.owner().eqRestriction(Identity.instance().getCredentials().getUsername())));
            }
            query.name().order(true);
            List<SystemConfiguration> systems = query.getList();
            if (!systems.contains(sutForReplay)) {
                sutForReplay = null;
            }
            return query.getList();
        } else {
            return null;
        }
    }

    /**
     * <p>replayMessage.</p>
     */
    public abstract void replayMessage();

    /**
     * <p>cancelReplay.</p>
     */
    public void cancelReplay() {
        selectedInstance = null;
        sutForReplay = null;
    }

    /**
     * <p>onCloseMessagePopup.</p>
     */
    public void onCloseMessagePopup() {
        selectedInstance = null;
    }

    /**
     * <p>isResponseValidationStatusEqualsTo.</p>
     *
     * @param ti               a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     * @param validationStatus a {@link java.lang.String} object.
     *
     * @return a boolean.
     */
    public boolean isResponseValidationStatusEqualsTo(TransactionInstance ti, String validationStatus) {
        if (ti == null && validationStatus.isEmpty()) {
            return true;
        } else if (ti != null) {
            EntityManager em = EntityManagerService.provideEntityManager();
            TransactionInstance tiDb = em.find(TransactionInstance.class, ti.getId());
            if ((tiDb.getResponse().getValidationStatus() == null && validationStatus.isEmpty())
                    || (tiDb.getResponse().getValidationStatus() != null && tiDb.getResponse().getValidationStatus().equals(validationStatus))) {
                return true;
            }
        }
        return false;
    }

    /**
     * <p>getMessageMetadata.</p>
     *
     * @param messageInstance a {@link net.ihe.gazelle.simulator.message.model.MessageInstance} object.
     *
     * @return a {@link java.util.List} object.
     */
    public List<MessageInstanceMetadata> getMessageMetadata(MessageInstance messageInstance) {
        MessageInstanceMetadataQuery query = new MessageInstanceMetadataQuery();
        query.messageInstance().id().eq(messageInstance.getId());
        return query.getList();
    }

    /**
     * <p>displayMetadataPanel.</p>
     *
     * @param mInstance a {@link net.ihe.gazelle.simulator.message.model.MessageInstance} object.
     *
     * @return a boolean.
     */
    public boolean displayMetadataPanel(MessageInstance mInstance) {
        MessageInstanceMetadataQuery query = new MessageInstanceMetadataQuery();
        query.messageInstance().id().eq(mInstance.getId());
        return query.getCount() > 0;
    }

    public List<String> getIpListForLoggedInUser() {
        return CompanyDetailsDAO.getListIpsForCompany(getUserCompanyKeyword());
    }
}

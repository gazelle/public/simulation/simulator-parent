package net.ihe.gazelle.simulator.message.action;

import net.ihe.gazelle.simulator.message.model.CompanyDetailsQuery;

import java.util.List;

/**
 * <p>CompanyDetailsDAO class.</p>
 *
 * @author abe
 * @version 1.0: 23/02/18
 */

public class CompanyDetailsDAO {


    public static List<String> getListIpsForCompany(String companyKeyword) {
        CompanyDetailsQuery query = new CompanyDetailsQuery();
        query.companyKeyword().eq(companyKeyword);
        return query.ipList().value().getListDistinct();
    }
}

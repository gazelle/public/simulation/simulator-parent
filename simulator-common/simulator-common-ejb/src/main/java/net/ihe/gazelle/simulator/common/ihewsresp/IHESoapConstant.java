package net.ihe.gazelle.simulator.common.ihewsresp;

/**
 * Created by aberge on 22/02/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
public final class IHESoapConstant {

    /** Constant <code>BODY="body"</code> */
    public static final String BODY = "body";
    /** Constant <code>HEADER="header"</code> */
    public static final String HEADER = "header";
    /** Constant <code>HL7V3_NS="urn:hl7-org:v3"</code> */
    public static final String HL7V3_NS = "urn:hl7-org:v3";

    private IHESoapConstant(){

    }
}

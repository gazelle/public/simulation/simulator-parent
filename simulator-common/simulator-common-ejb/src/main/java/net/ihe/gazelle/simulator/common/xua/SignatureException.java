package net.ihe.gazelle.simulator.common.xua;

/**
 * <p>SignatureException class.</p>
 *
 * Use instead {@link net.ihe.gazelle.simulator.samlassertion.common.SignatureException}
 *
 * @author aberge
 * @version $Id: $Id
 */

@Deprecated
public class SignatureException extends Exception {

	private static final long serialVersionUID = -5100871848771290637L;

	/**
	 * <p>Constructor for SignatureException.</p>
	 */
	public SignatureException() {
		super();
	}

	/**
	 * <p>Constructor for SignatureException.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public SignatureException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * <p>Constructor for SignatureException.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 */
	public SignatureException(String message) {
		super(message);
	}

	/**
	 * <p>Constructor for SignatureException.</p>
	 *
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public SignatureException(Throwable cause) {
		super(cause);
	}

}

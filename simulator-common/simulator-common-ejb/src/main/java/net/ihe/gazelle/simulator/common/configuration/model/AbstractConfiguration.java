/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.simulator.common.configuration.model;

/**
 * <p>Abstract AbstractConfiguration class.</p>
 *
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, sep
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;


@MappedSuperclass
public abstract class AbstractConfiguration implements
		Serializable {

	/**
    * 
    */
	/** Logger */
	@Logger
	private static Log log;

	private static final long serialVersionUID = 1L;

	/**
    * 
    */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "configuration_id")
	@NotNull
	protected Configuration configuration;

	@Column(name = "comments", nullable = true)
	protected String comments;
	
	@ManyToOne
    @JoinColumn(name="gs_system_id", unique = false, nullable = true)
    private net.ihe.gazelle.simulator.common.model.System system;
	
	
	//--------------------------------------------

	/**
	 * <p>Setter for the field <code>system</code>.</p>
	 *
	 * @param system a {@link net.ihe.gazelle.simulator.common.model.System} object.
	 */
	public void setSystem(net.ihe.gazelle.simulator.common.model.System system) {
        this.system = system;
    }

    /**
     * <p>Getter for the field <code>system</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.model.System} object.
     */
    public net.ihe.gazelle.simulator.common.model.System getSystem() {
        return system;
    }

    /**
     * <p>Getter for the field <code>comments</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getComments() {
		return comments;
	}

	/**
	 * <p>Setter for the field <code>comments</code>.</p>
	 *
	 * @param comments a {@link java.lang.String} object.
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * <p>Getter for the field <code>configuration</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 */
	public Configuration getConfiguration() {
		return configuration;
	}

	/**
	 * <p>Setter for the field <code>configuration</code>.</p>
	 *
	 * @param configuration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 */
	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	/**
	 * <p>Constructor for AbstractConfiguration.</p>
	 */
	protected AbstractConfiguration() {

	}

	/**
	 * <p>Constructor for AbstractConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 */
	public AbstractConfiguration(Configuration inConfiguration) {
		configuration = inConfiguration;
	}

	/**
	 * <p>Constructor for AbstractConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param inComments a {@link java.lang.String} object.
	 */
	public AbstractConfiguration(Configuration inConfiguration,
			String inComments) {
		comments = inComments;
		configuration = inConfiguration;
	}

	/**
	 * <p>hashCode.</p>
	 *
	 * @return a int.
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result
				+ ((configuration == null) ? 0 : configuration.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractConfiguration other = (AbstractConfiguration) obj;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (configuration == null) {
			if (other.configuration != null)
				return false;
		} else if (!configuration.equals(other.configuration))
			return false;
		return true;
	}
	
}

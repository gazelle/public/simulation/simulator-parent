package net.ihe.gazelle.simulator.ws;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.report.TestReportData;

import org.jboss.seam.contexts.Lifecycle;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

/**
 * <p>Abstract TestReport class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class TestReport implements TestReportLocal {

	private static Log log = Logging.getLog(TestReport.class);

	/**
	 * {@inheritDoc}
	 *
	 * This is the method which will be called by TM to retrieve the test LOG
	 */
	public Response getReport(String testId, String test, HttpServletRequest request) {
		log.info("getReport");
		ResponseBuilder responseBuilder = null;
		if (testId == null || testId.isEmpty()) {
			log.error("the id parameter is null");
			responseBuilder = Response.noContent().status(404);
		} else {
			Lifecycle.beginCall();
			TestReportData report = newInstance(testId, test);
			if (report == null) {
				log.error("TestReport is null");
				responseBuilder = Response.noContent().status(404);
			} else {
				GenericEntity<TestReportData> entity = new GenericEntity<TestReportData>(report) {
				};
				responseBuilder = Response.ok(entity).status(200);
			}
			Lifecycle.endCall();
		}
		return responseBuilder.build();
	}

	/**
	 * Returns the REST URL the user has to copy and paste into TM. Key used to retrieve the application base URL in the app_configuration table is 'application_url'
	 *
	 * @param testId a {@link java.lang.Integer} object.
	 * @param test a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String buildTestReportRestUrl(Integer testId, String test) {
		if (testId == null) {
			log.error("cannot build a url with null testId");
			return null;
		} else {
			String applicationBaseUrl = ApplicationConfiguration.getValueOfVariable("application_url");
			if (applicationBaseUrl != null) {
				StringBuffer restUrl = new StringBuffer(applicationBaseUrl);
				if (applicationBaseUrl.endsWith("/")){
					restUrl.append("rest/GetReport?id=");
				} else {
					restUrl.append("/rest/GetReport?id=");
				}
				restUrl.append(testId);
				if (test != null && !test.isEmpty()) {
					restUrl.append("&test=");
					restUrl.append(test);
				}
				return restUrl.toString();
			} else {
				log.error("The application base URL is not configured !");
				return null;
			}
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * method used for testing that the application is properly configured to respond to REST requests
	 */
	public Response hello(HttpServletRequest req) {
		return Response.ok("hello").build();
	}

	/**
	 * Creates a new instance of Report and fill the attributes
	 *
	 * @param testId a {@link java.lang.String} object.
	 * @param test a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.simulator.report.TestReportData} object.
	 */
	protected abstract TestReportData newInstance(String testId, String test);

}

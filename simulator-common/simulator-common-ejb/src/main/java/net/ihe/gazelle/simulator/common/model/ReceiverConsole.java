package net.ihe.gazelle.simulator.common.model;

import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.hibernate.annotations.Type;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.Date;

/**
 * <b>Class Description : </b>ReceiverConsole<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 03/12/15
 */

@Entity
@Name("receiverConsole")
@Table(name = "cmn_receiver_console", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "cmn_receiver_console_sequence", sequenceName = "cmn_receiver_console_id_seq", allocationSize = 1)
public class ReceiverConsole implements Serializable{

    @Id
    @GeneratedValue(generator = "cmn_receiver_console_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "timestamp")
    private Date timestamp;

    @ManyToOne
    @JoinColumn(name = "simulated_actor_id")
    private Actor simulatedActor;

    @ManyToOne
    @JoinColumn(name = "transaction_id")
    private Transaction transaction;

    @ManyToOne
    @JoinColumn(name = "domain_id")
    private Domain domain;

    @Column(name = "message_type")
    private String messageType;

    @Column(name = "message_identifier")
    private String messageIdentifier;

    @Column(name = "sut")
    private String sut;

    @Column(name = "code")
    private String code;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "comment")
    private String comment;

    /**
     * <p>Constructor for ReceiverConsole.</p>
     */
    public ReceiverConsole(){

    }

    /**
     * <p>newLog.</p>
     *
     * @param actor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     * @param domain a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     * @param messageType a {@link java.lang.String} object.
     * @param messageIdentifier a {@link java.lang.String} object.
     * @param sut a {@link java.lang.String} object.
     * @param code a {@link java.lang.String} object.
     * @param comment a {@link java.lang.String} object.
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     */
    public static void newLog(Actor actor, Transaction transaction, Domain domain, String messageType,
                              String messageIdentifier, String sut, String code, String comment, EntityManager entityManager){
        ReceiverConsole log = new ReceiverConsole();
        log.setSimulatedActor(actor);
        log.setTransaction(transaction);
        log.setDomain(domain);
        log.setTimestamp(new Date());
        log.setMessageType(messageType);
        log.setMessageIdentifier(messageIdentifier);
        log.setSut(sut);
        log.setCode(code);
        log.setComment(comment);
        entityManager.merge(log);
        entityManager.flush();
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>timestamp</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * <p>Setter for the field <code>timestamp</code>.</p>
     *
     * @param timestamp a {@link java.util.Date} object.
     */
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * <p>Getter for the field <code>simulatedActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getSimulatedActor() {
        return simulatedActor;
    }

    /**
     * <p>Setter for the field <code>simulatedActor</code>.</p>
     *
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public void setSimulatedActor(Actor simulatedActor) {
        this.simulatedActor = simulatedActor;
    }

    /**
     * <p>Getter for the field <code>transaction</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public Transaction getTransaction() {
        return transaction;
    }

    /**
     * <p>Setter for the field <code>transaction</code>.</p>
     *
     * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    /**
     * <p>Getter for the field <code>domain</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public Domain getDomain() {
        return domain;
    }

    /**
     * <p>Setter for the field <code>domain</code>.</p>
     *
     * @param domain a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    /**
     * <p>Getter for the field <code>messageType</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * <p>Setter for the field <code>messageType</code>.</p>
     *
     * @param messageType a {@link java.lang.String} object.
     */
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    /**
     * <p>Getter for the field <code>messageIdentifier</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMessageIdentifier() {
        return messageIdentifier;
    }

    /**
     * <p>Setter for the field <code>messageIdentifier</code>.</p>
     *
     * @param messageIdentifier a {@link java.lang.String} object.
     */
    public void setMessageIdentifier(String messageIdentifier) {
        this.messageIdentifier = messageIdentifier;
    }

    /**
     * <p>Getter for the field <code>sut</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSut() {
        return sut;
    }

    /**
     * <p>Setter for the field <code>sut</code>.</p>
     *
     * @param sut a {@link java.lang.String} object.
     */
    public void setSut(String sut) {
        this.sut = sut;
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCode() {
        return code;
    }

    /**
     * <p>Setter for the field <code>code</code>.</p>
     *
     * @param code a {@link java.lang.String} object.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * <p>Getter for the field <code>comment</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getComment() {
        return comment;
    }

    /**
     * <p>Setter for the field <code>comment</code>.</p>
     *
     * @param comment a {@link java.lang.String} object.
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReceiverConsole)) return false;

        ReceiverConsole that = (ReceiverConsole) o;

        if (timestamp != null ? !timestamp.equals(that.timestamp) : that.timestamp != null) return false;
        return !(messageIdentifier != null ? !messageIdentifier.equals(that.messageIdentifier) : that.messageIdentifier != null);

    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        int result = timestamp != null ? timestamp.hashCode() : 0;
        result = 31 * result + (messageIdentifier != null ? messageIdentifier.hashCode() : 0);
        return result;
    }
}

package net.ihe.gazelle.simulator.message.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.message.model.CompanyDetails;
import net.ihe.gazelle.simulator.message.model.CompanyDetailsQuery;
import net.ihe.gazelle.simulator.message.model.IPAddress;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>CompanyDetailsManager class.</p>
 *
 * @author abe
 * @version 1.0: 21/02/18
 */

@Name("companyDetailsManager")
@Scope(ScopeType.PAGE)
public class CompanyDetailsManager implements Serializable{


    public final static String _255 = "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
    public final static String IP_REGEX = "(" + _255 + "\\.){3}" + _255;

    private CompanyDetails selectedCompany;
    private Filter<CompanyDetails> filter;
    private String additionalIp;
    private String searchIp;


    public CompanyDetails getSelectedCompany() {
        return selectedCompany;
    }

    public void setSelectedCompany(CompanyDetails selectedCompany) {
        this.selectedCompany = selectedCompany;
    }

    public Filter<CompanyDetails> getFilter() {
        if (filter == null) {
            filter = new Filter<CompanyDetails>(getHQLCriterions());
        }
        return filter;
    }

    private HQLCriterionsForFilter<CompanyDetails> getHQLCriterions() {
        CompanyDetailsQuery query = new CompanyDetailsQuery();
        HQLCriterionsForFilter<CompanyDetails> criteria = query.getHQLCriterionsForFilter();
        return criteria;
    }

    public FilterDataModel<CompanyDetails> getFilteredList() {
        return new FilterDataModel<CompanyDetails>(getFilter()) {
            @Override
            protected Object getId(CompanyDetails companyDetails) {
                return companyDetails.getId();
            }
        };
    }

    public String getAdditionalIp() {
        return additionalIp;
    }

    public void setAdditionalIp(String additionalIp) {
        this.additionalIp = additionalIp;
    }

    public String getSearchIp() {
        return searchIp;
    }

    public void setSearchIp(String searchIp) {
        this.searchIp = searchIp;
    }

    public void newCompanyDetails() {
        this.selectedCompany = new CompanyDetails();
    }

    public void saveDetails() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(selectedCompany);
        entityManager.flush();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Your changes have been saved");
    }

    public void deleteCompany() {
        if (selectedCompany != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            CompanyDetails companyFromDB = entityManager.find(CompanyDetails.class, selectedCompany.getId());
            entityManager.remove(companyFromDB);
            entityManager.flush();
            selectedCompany = null;
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Entry successfully deleted");
        }
    }

    public void addIPsToCompany() {
        if (additionalIp != null && !additionalIp.isEmpty()) {
            Pattern pattern = Pattern.compile(IP_REGEX);
            Matcher matcher = pattern.matcher(additionalIp);
            List<String> ipValues = new ArrayList<String>();
            while (matcher.find()) {
                ipValues.add(matcher.group(0));
            }
            if (ipValues.isEmpty()){
                FacesMessages.instance().add(StatusMessage.Severity.WARN, "No IP address has been detected in the string " + additionalIp);
            } else {
                for (String ip:ipValues){
                    if (!selectedCompany.getIpList().contains(ip)){
                        IPAddress address = new IPAddress(selectedCompany, ip);
                        selectedCompany.addIpToList(address);
                    }
                }
                saveDetails();
            }
            additionalIp = null;
            selectedCompany = null;
        }
    }
}

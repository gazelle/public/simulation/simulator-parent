package net.ihe.gazelle.simulator.samlassertion.epr;

import net.ihe.gazelle.simulator.samlassertion.common.AbstractAssertionAttributes;
import net.ihe.gazelle.simulator.samlassertion.common.AssertionProvider;
import net.ihe.gazelle.ws.client.HttpAssertionProviderClient;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;


public class EPRAssertionProvider implements AssertionProvider {

    public EPRAssertionProvider() {

    }

    @Override
    public Element getAssertion(String appliesTo, String subjectNameId, AbstractAssertionAttributes attributeStatement) {


        HttpAssertionProviderClient httpAssertionProviderClient = new HttpAssertionProviderClient();

        EPRAssertionAttributes attributes = (EPRAssertionAttributes) attributeStatement;
        String resourceId = attributes.getResourceId();
        String organizationId = attributes.getOrganizationId();

        try {
            Element assertion = httpAssertionProviderClient.getXmlAssertion(subjectNameId, organizationId, resourceId, appliesTo);
            return assertion;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (SAXException e) {
            e.printStackTrace();
            return null;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public AbstractAssertionAttributes createAssertionAttributes() {
        EPRAssertionAttributes attributes = new EPRAssertionAttributes();
        return attributes;
    }
}

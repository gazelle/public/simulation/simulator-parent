package net.ihe.gazelle.simulator.common.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration;
import net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCUConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2InitiatorConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3InitiatorConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration;
import net.ihe.gazelle.simulator.common.configuration.model.SyslogConfiguration;
import net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration;

import javax.validation.constraints.NotNull;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

/**
 * Seam and persistence object for simulator system table. A <code>System</code>
 * object encapsulates data relating to a particular system, including:
 * <ul>
 * <li>A unique system keyword.
 * <li>Owning institution and responsible party
 * <li>Applicable Configurations, for example: HL7V2 Receiver configuration.
 *
 * @author Abdallah Miladi / INRIA Rennes IHE development Project

 * @version 1.0 - 2010-02-04
 */
@XmlRootElement(name="system")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Name("system")
@Table(name = "gs_system", schema = "public")
@SequenceGenerator(name = "gs_system_sequence", sequenceName = "gs_system_id_seq", allocationSize = 1)
public class System implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 3075465360121893681L;


	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gs_system_sequence")
	@XmlElement(name="id")
	private Integer id;
	
	/**
	 * Unique key word for this system.
	 */
	@Column(name = "keyword")
	@XmlElement(name="keyword")
	private String keyword;

	/**
	 * Owning institution keyword.<br>
	 * <b>Note:</b> Not currently linked to institution table
	 */
	@Column(name = "institution_keyword")
	@XmlElement(name="institutionKeyword")
	private String institutionKeyword;

	/**
	 * Responsible party or organization.
	 */
	@Column(name = "system_owner")
	@XmlElement(name="systemOwner")
	private String systemOwner;
	
	@OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    private List<HL7V2InitiatorConfiguration> hL7v2InitiatorConfigurations;
	
	@OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    private List<HL7V2ResponderConfiguration> hL7V2ResponderConfigurations;
	
	@OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    private List<HL7V3InitiatorConfiguration> hL7V3InitiatorConfigurations;
	
	@OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    private List<HL7V3ResponderConfiguration> hL7V3ResponderConfigurations;
	
	@OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    private List<DicomSCUConfiguration> dicomSCUConfigurations;
	
	@OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    private List<DicomSCPConfiguration> dicomSCPConfigurations;
	
	@OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    private List<SyslogConfiguration> syslogConfigurations;
	
	@OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    private List<WebServiceConfiguration> webServiceConfigurations;
	
	@OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    private List<OIDConfiguration> oidConfigurations;
	
	@OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    private List<TestInstanceParticipants> testInstanceParticipants;


	@OneToMany (mappedBy="system", targetEntity=OIDConfiguration.class, fetch=FetchType.LAZY)
    private List<OIDConfiguration> oidConfigurationList;

	/**
	 * <p>Constructor for System.</p>
	 */
	public System(){

	}

	/**
	 * <p>Constructor for System.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @param institutionKeyword a {@link java.lang.String} object.
	 * @param systemOwner a {@link java.lang.String} object.
	 */
	public System(String keyword,String institutionKeyword,String systemOwner){
		this.keyword=keyword;
		this.institutionKeyword=institutionKeyword;
		this.systemOwner=systemOwner;
	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}


	/**
	 * <p>Getter for the field <code>testInstanceParticipants</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<TestInstanceParticipants> getTestInstanceParticipants() {
        return testInstanceParticipants;
    }

    /**
     * <p>Setter for the field <code>testInstanceParticipants</code>.</p>
     *
     * @param testInstanceParticipants a {@link java.util.List} object.
     */
    public void setTestInstanceParticipants(
            List<TestInstanceParticipants> testInstanceParticipants) {
        this.testInstanceParticipants = testInstanceParticipants;
    }

    /**
     * <p>Getter for the field <code>oidConfigurations</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<OIDConfiguration> getOidConfigurations() {
        return oidConfigurations;
    }

    /**
     * <p>Setter for the field <code>oidConfigurations</code>.</p>
     *
     * @param oidConfigurations a {@link java.util.List} object.
     */
    public void setOidConfigurations(List<OIDConfiguration> oidConfigurations) {
        this.oidConfigurations = oidConfigurations;
    }

    /**
     * <p>Getter for the field <code>keyword</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getKeyword() {
		return keyword;
	}

	/**
	 * <p>Setter for the field <code>keyword</code>.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	/**
	 * <p>Getter for the field <code>institutionKeyword</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getInstitutionKeyword() {
		return institutionKeyword;
	}

	/**
	 * <p>Setter for the field <code>institutionKeyword</code>.</p>
	 *
	 * @param institutionKeyword a {@link java.lang.String} object.
	 */
	public void setInstitutionKeyword(String institutionKeyword) {
		this.institutionKeyword = institutionKeyword;
	}

	/**
	 * <p>Getter for the field <code>systemOwner</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getSystemOwner() {
		return systemOwner;
	}

	/**
	 * <p>Setter for the field <code>systemOwner</code>.</p>
	 *
	 * @param systemOwner a {@link java.lang.String} object.
	 */
	public void setSystemOwner(String systemOwner) {
		this.systemOwner = systemOwner;
	}

	
	/**
	 * <p>Getter for the field <code>hL7v2InitiatorConfigurations</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<HL7V2InitiatorConfiguration> gethL7v2InitiatorConfigurations() {
        return hL7v2InitiatorConfigurations;
    }

    /**
     * <p>Setter for the field <code>hL7v2InitiatorConfigurations</code>.</p>
     *
     * @param hL7v2InitiatorConfigurations a {@link java.util.List} object.
     */
    public void sethL7v2InitiatorConfigurations(
            List<HL7V2InitiatorConfiguration> hL7v2InitiatorConfigurations) {
        this.hL7v2InitiatorConfigurations = hL7v2InitiatorConfigurations;
    }

    /**
     * <p>Getter for the field <code>hL7V2ResponderConfigurations</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HL7V2ResponderConfiguration> gethL7V2ResponderConfigurations() {
        return hL7V2ResponderConfigurations;
    }

    /**
     * <p>Setter for the field <code>hL7V2ResponderConfigurations</code>.</p>
     *
     * @param hL7V2ResponderConfigurations a {@link java.util.List} object.
     */
    public void sethL7V2ResponderConfigurations(
            List<HL7V2ResponderConfiguration> hL7V2ResponderConfigurations) {
        this.hL7V2ResponderConfigurations = hL7V2ResponderConfigurations;
    }

    /**
     * <p>Getter for the field <code>hL7V3InitiatorConfigurations</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HL7V3InitiatorConfiguration> gethL7V3InitiatorConfigurations() {
        return hL7V3InitiatorConfigurations;
    }

    /**
     * <p>Setter for the field <code>hL7V3InitiatorConfigurations</code>.</p>
     *
     * @param hL7V3InitiatorConfigurations a {@link java.util.List} object.
     */
    public void sethL7V3InitiatorConfigurations(
            List<HL7V3InitiatorConfiguration> hL7V3InitiatorConfigurations) {
        this.hL7V3InitiatorConfigurations = hL7V3InitiatorConfigurations;
    }

    /**
     * <p>Getter for the field <code>hL7V3ResponderConfigurations</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HL7V3ResponderConfiguration> gethL7V3ResponderConfigurations() {
        return hL7V3ResponderConfigurations;
    }

    /**
     * <p>Setter for the field <code>hL7V3ResponderConfigurations</code>.</p>
     *
     * @param hL7V3ResponderConfigurations a {@link java.util.List} object.
     */
    public void sethL7V3ResponderConfigurations(
            List<HL7V3ResponderConfiguration> hL7V3ResponderConfigurations) {
        this.hL7V3ResponderConfigurations = hL7V3ResponderConfigurations;
    }

    /**
     * <p>Getter for the field <code>dicomSCUConfigurations</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<DicomSCUConfiguration> getDicomSCUConfigurations() {
        return dicomSCUConfigurations;
    }

    /**
     * <p>Setter for the field <code>dicomSCUConfigurations</code>.</p>
     *
     * @param dicomSCUConfigurations a {@link java.util.List} object.
     */
    public void setDicomSCUConfigurations(
            List<DicomSCUConfiguration> dicomSCUConfigurations) {
        this.dicomSCUConfigurations = dicomSCUConfigurations;
    }

    /**
     * <p>Getter for the field <code>dicomSCPConfigurations</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<DicomSCPConfiguration> getDicomSCPConfigurations() {
        return dicomSCPConfigurations;
    }

    /**
     * <p>Setter for the field <code>dicomSCPConfigurations</code>.</p>
     *
     * @param dicomSCPConfigurations a {@link java.util.List} object.
     */
    public void setDicomSCPConfigurations(
            List<DicomSCPConfiguration> dicomSCPConfigurations) {
        this.dicomSCPConfigurations = dicomSCPConfigurations;
    }

    /**
     * <p>Getter for the field <code>syslogConfigurations</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SyslogConfiguration> getSyslogConfigurations() {
        return syslogConfigurations;
    }

    /**
     * <p>Setter for the field <code>syslogConfigurations</code>.</p>
     *
     * @param syslogConfigurations a {@link java.util.List} object.
     */
    public void setSyslogConfigurations(
            List<SyslogConfiguration> syslogConfigurations) {
        this.syslogConfigurations = syslogConfigurations;
    }

    /**
     * <p>Getter for the field <code>webServiceConfigurations</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<WebServiceConfiguration> getWebServiceConfigurations() {
        return webServiceConfigurations;
    }

    /**
     * <p>Setter for the field <code>webServiceConfigurations</code>.</p>
     *
     * @param webServiceConfigurations a {@link java.util.List} object.
     */
    public void setWebServiceConfigurations(
            List<WebServiceConfiguration> webServiceConfigurations) {
        this.webServiceConfigurations = webServiceConfigurations;
    }

    
    /**
     * <p>Getter for the field <code>oidConfigurationList</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<OIDConfiguration> getOidConfigurationList() {
        return oidConfigurationList;
    }

    /**
     * <p>Setter for the field <code>oidConfigurationList</code>.</p>
     *
     * @param oidConfigurationList a {@link java.util.List} object.
     */
    public void setOidConfigurationList(List<OIDConfiguration> oidConfigurationList) {
        this.oidConfigurationList = oidConfigurationList;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>getSystemBySystemKeywordByInstitutionKeyword.</p>
     *
     * @param systemKeyword a {@link java.lang.String} object.
     * @param institutionKeyword a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.simulator.common.model.System} object.
     */
    public static System getSystemBySystemKeywordByInstitutionKeyword(String systemKeyword,String institutionKeyword){
		EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
		Query query=entityManager.createQuery("SELECT sys " +
				"FROM System sys " +
				"WHERE sys.keyword='"+systemKeyword+"' " +
				"AND sys.institutionKeyword='"+institutionKeyword+"'");
		List<System> list=query.getResultList();
		if(list.size()>0){
			return list.get(0);
		}
		return null;	
	}
	
    /**
     * <p>hashCode.</p>
     *
     * @return a int.
     */
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((institutionKeyword == null) ? 0 : institutionKeyword
                        .hashCode());
        result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
        result = prime * result
                + ((systemOwner == null) ? 0 : systemOwner.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        System other = (System) obj;
        if (institutionKeyword == null) {
            if (other.institutionKeyword != null)
                return false;
        } else if (!institutionKeyword.equals(other.institutionKeyword))
            return false;
        if (keyword == null) {
            if (other.keyword != null)
                return false;
        } else if (!keyword.equals(other.keyword))
            return false;
        if (systemOwner == null) {
            if (other.systemOwner != null)
                return false;
        } else if (!systemOwner.equals(other.systemOwner))
            return false;
        return true;
    }
    
    /**
     * <p>getSystem.</p>
     *
     * @param sys a {@link net.ihe.gazelle.simulator.common.model.System} object.
     * @return a {@link net.ihe.gazelle.simulator.common.model.System} object.
     */
    public static System getSystem(System sys){
        EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
        Query query=entityManager.createQuery("SELECT sys FROM System sys WHERE sys.keyword=:keyword");
        query.setParameter("keyword",sys.getKeyword());
        List<System> result=query.getResultList();
        if (result != null){
            for (System ssc: result){
                if (ssc.equals(sys)){
                    return ssc;
                }
            }
        }
        return null;
    }

    /**
     * <p>mergeSystem.</p>
     *
     * @param inSystem a {@link net.ihe.gazelle.simulator.common.model.System} object.
     * @return a {@link net.ihe.gazelle.simulator.common.model.System} object.
     */
    public static System mergeSystem(System inSystem) {
        if(inSystem!=null){
            System sysconf = System.getSystem(inSystem);
            if (sysconf != null) return sysconf;
            EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
            inSystem=entityManager.merge(inSystem);
            return inSystem;
        }
        return null;
    }
    
}

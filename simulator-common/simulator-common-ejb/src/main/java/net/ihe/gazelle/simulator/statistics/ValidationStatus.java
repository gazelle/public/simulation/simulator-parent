package net.ihe.gazelle.simulator.statistics;

/**
 * <p>ValidationStatus class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public enum ValidationStatus {
	FAILED(3, "FAILED"),
	PASSED(4, "PASSED");
	
	private int level;
	private String status;
	
	ValidationStatus(int i, String status)
	{
		this.level = i;
		this.status = status;
	}
	
	/**
	 * <p>Getter for the field <code>level</code>.</p>
	 *
	 * @return a int.
	 */
	public int getLevel()
	{
		return this.level;
	}
	
	/**
	 * <p>Getter for the field <code>status</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getStatus()
	{
		return this.status;
	}
}

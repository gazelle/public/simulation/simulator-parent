/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.simulator.common.configuration.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.model.TestInstance;
import net.ihe.gazelle.simulator.common.model.TestStepsInstance;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.slf4j.LoggerFactory;


/**
 * Seam persistence object classes representing  http/https web service
 * communication end points. Encapsulating assigning authority
 *
 * @author      Abdallah Miladi / INRIA Rennes IHE development Project
 * @version 1.0 - 2010-02-05
 */
@Entity
@Name("webServiceConfiguration")
@Table(name = "cfg_web_service_configuration", schema = "public")
@SequenceGenerator(name = "cfg_web_service_configuration_sequence", sequenceName = "cfg_web_service_configuration_id_seq", allocationSize = 1)
public final class WebServiceConfiguration extends AbstractWebServiceConfiguration implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cfg_web_service_configuration_sequence")
    private Integer id;
	
	/** assigningAuthority attribute */
	@Column(name = "assigning_authority")
	private String assigningAuthority;
	
	@OneToMany(mappedBy = "webServiceConfiguration", fetch = FetchType.LAZY)
    private List<TestStepsInstance> testStepsInstances;

	// getter and setter ////////

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>testStepsInstances</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TestStepsInstance> getTestStepsInstances() {
        return testStepsInstances;
    }

    /**
     * <p>Setter for the field <code>testStepsInstances</code>.</p>
     *
     * @param testStepsInstances a {@link java.util.List} object.
     */
    public void setTestStepsInstances(List<TestStepsInstance> testStepsInstances) {
        this.testStepsInstances = testStepsInstances;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }
    
    /**
     * <p>Getter for the field <code>assigningAuthority</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAssigningAuthority()
    {
        return assigningAuthority;
    }

    /**
     * <p>Setter for the field <code>assigningAuthority</code>.</p>
     *
     * @param assigningAuthority a {@link java.lang.String} object.
     */
    public void setAssigningAuthority(String assigningAuthority)
    {
        this.assigningAuthority = assigningAuthority;
    }

    // constructor ////////

    /**
     * <p>Constructor for WebServiceConfiguration.</p>
     */
    public WebServiceConfiguration()
	{
		super();
	}

	/**
	 * <p>Constructor for WebServiceConfiguration.</p>
	 *
	 * @param inConf a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 */
	public WebServiceConfiguration(Configuration inConf)
	{
		super(inConf);
	}
	/**
	 * <p>Constructor for WebServiceConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param port a {@link java.lang.Integer} object.
	 * @param portSecured a {@link java.lang.Integer} object.
	 * @param usage a {@link java.lang.String} object.
	 * @param comment a {@link java.lang.String} object.
	 */
	public WebServiceConfiguration(Configuration inConfiguration, Integer port, Integer portSecured, String usage, String comment)
	{
		super(inConfiguration, port, portSecured, usage, comment);

	}

	/**
	 * <p>Constructor for WebServiceConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param url a {@link java.lang.String} object.
	 * @param port a {@link java.lang.Integer} object.
	 * @param portSecured a {@link java.lang.Integer} object.
	 * @param usage a {@link java.lang.String} object.
	 * @param comment a {@link java.lang.String} object.
	 */
	public WebServiceConfiguration(Configuration inConfiguration, String url, Integer port, Integer portSecured, String usage, String comment)
	{
		super(inConfiguration, url, port, portSecured, usage, comment);

	}

	/**
	 * <p>Constructor for WebServiceConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param url a {@link java.lang.String} object.
	 * @param port a {@link java.lang.Integer} object.
	 * @param portSecured a {@link java.lang.Integer} object.
	 * @param usage a {@link java.lang.String} object.
	 * @param comment a {@link java.lang.String} object.
	 * @param inAssigningAuthority a {@link java.lang.String} object.
	 */
	public WebServiceConfiguration(Configuration inConfiguration, String url, Integer port, Integer portSecured, String usage, String comment ,  String inAssigningAuthority)
	{
		super(inConfiguration, url, port, portSecured, usage, comment);
		assigningAuthority = inAssigningAuthority ;
	}

	/**
	 * <p>Constructor for WebServiceConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param inComments a {@link java.lang.String} object.
	 */
	public WebServiceConfiguration(Configuration inConfiguration, String inComments)
	{
		super(inConfiguration, inComments);

	}
	
	
	// methods ///////////////////

	/**
	 * <p>getWebServiceConfiguration.</p>
	 *
	 * @param wsconf a {@link net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration} object.
	 */
	public static WebServiceConfiguration getWebServiceConfiguration(WebServiceConfiguration wsconf){
        EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
        Query query=entityManager.createQuery("SELECT wsconf " +
                "FROM WebServiceConfiguration wsconf " +
        "WHERE wsconf.configuration=:configuration");
        query.setParameter("configuration",wsconf.getConfiguration());
        List<WebServiceConfiguration> result=query.getResultList();
        if (result != null){
            for (WebServiceConfiguration wsc: result){
                if (wsc.equals(wsconf)){
                    return wsc;
                }
            }
        }
        return null;
    }

	/**
	 * <p>mergeWebServiceConfiguration.</p>
	 *
	 * @param inWebServiceConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration} object.
	 */
	public static WebServiceConfiguration mergeWebServiceConfiguration(WebServiceConfiguration inWebServiceConfiguration) {
		if(inWebServiceConfiguration!=null){
			EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
			Configuration conf=Configuration.mergeConfiguration(inWebServiceConfiguration.getConfiguration());
			inWebServiceConfiguration.setConfiguration(conf);
			WebServiceConfiguration wsconf = WebServiceConfiguration.getWebServiceConfiguration(inWebServiceConfiguration);
			if (wsconf != null) return wsconf;
			inWebServiceConfiguration=entityManager.merge(inWebServiceConfiguration);
			return inWebServiceConfiguration;
		}
		return null;
	}
	
	
	/**
	 * Create web service url string for given {@link net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration}.
	 *
	 * @param wsConfiguration <code>WebServiceConfiguration</code>.
	 * @return string url of the form:
	 * [http://|https://]hostname:[port|securePort]/serviceName
	 */
	public static String createWSUrl(WebServiceConfiguration wsConfiguration){
	    String res = new String();
	    StringBuffer sb = new StringBuffer() ;

	    if (wsConfiguration != null){
	        if (wsConfiguration.getConfiguration() != null){
	            
	            if (wsConfiguration.getConfiguration().getIsSecured() != null){
	                sb.append(wsConfiguration.getConfiguration().getIsSecured()? "https://"  : "http://"  );
	            }
	            else{
	                sb.append("http://");
	            }
	            if (wsConfiguration.getConfiguration().getHost() != null){
	                if (wsConfiguration.getConfiguration().getHost().getIp() != null){
	                    sb.append(wsConfiguration.getConfiguration().getHost().getIp());
	                }
	            }
	            /*
	            if (networkConf != null){
	                if (networkConf.getDomainName() != null){
	                    sb.append("." + networkConf.getDomainName());
	                }
	                else{
	                    sb.append("");
	                }
	            }
	            */
	            sb.append(':');
	            if(wsConfiguration.getConfiguration().getIsSecured() != null){
	                if (wsConfiguration.getConfiguration().getIsSecured()){
                        if (wsConfiguration.getPortSecured() != null){
                            sb.append(wsConfiguration.getPortSecured());
                        }
	                }
	                else{
                        if (wsConfiguration.getPort() != null){
                            sb.append(wsConfiguration.getPort());
                        }
	                }
	            }
	            else{
                    if (wsConfiguration.getPort() != null){
                        sb.append(wsConfiguration.getPort());
                    }
	            }
	            sb.append('/');
                if (wsConfiguration.getUrl() != null){
                    sb.append(wsConfiguration.getUrl());
                }
	            res = sb.toString();
	        }
	    }
	    return res;
	}

    /**
     * <p>hashCode.</p>
     *
     * @return a int.
     */
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime
                * result
                + ((assigningAuthority == null) ? 0 : assigningAuthority
                        .hashCode());
        return result;
    }

    /** {@inheritDoc} */
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        WebServiceConfiguration other = (WebServiceConfiguration) obj;
        if (assigningAuthority == null) {
            if (other.assigningAuthority != null)
                return false;
        } else if (!assigningAuthority.equals(other.assigningAuthority))
            return false;
        return true;
    }


    /**
     * <p>getWebServiceConfigurationFiltered.</p>
     *
     * @param inSystem a {@link net.ihe.gazelle.simulator.common.model.System} object.
     * @param ti a {@link net.ihe.gazelle.simulator.common.model.TestInstance} object.
     * @param tsi a {@link net.ihe.gazelle.simulator.common.model.TestStepsInstance} object.
     * @param em a {@link javax.persistence.EntityManager} object.
     * @return a {@link java.util.List} object.
     */
    public static List<WebServiceConfiguration> getWebServiceConfigurationFiltered(net.ihe.gazelle.simulator.common.model.System inSystem, 
            TestInstance ti, TestStepsInstance tsi ,EntityManager em){
        
        HQLQueryBuilder<WebServiceConfiguration> hbuild = new HQLQueryBuilder<WebServiceConfiguration>(em, WebServiceConfiguration.class);
        if (inSystem != null) hbuild.addEq("system", inSystem);
        if (tsi != null) hbuild.addEq("testStepsInstances", tsi);
        if (ti != null) hbuild.addEq("testStepsInstances.testInstance", ti);

       return hbuild.getList();
    }
    

}


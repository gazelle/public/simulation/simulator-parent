/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.tf.model;

//JPA imports
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import javax.validation.constraints.NotNull;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;


		/**
		 *  <b>Class Description :  </b>IntegrationProfile<br><br>
		 * This class describes the Integration profile object, used by the Gazelle application. This class belongs to the Technical Framework module.
		 *
		 * IntegrationProfile possesses the following attributes :
		 * <ul>
		 * <li><b>id</b> : id corresponding to the Integration profile</li>
		 * <li><b>keyword</b> : keyword corresponding to the Integration profile</li>
		 * <li><b>name</b> : name corresponding to the Integration profile</li>
		 * <li><b>description</b> : description corresponding to the Integration profile</li>
		 * <li><b>integrationProfileType</b> : type corresponding to the Integration profile</li>
		 * </ul></br>
		 * <b>Example of Integration profile</b> : "ARI : Access to Radiology Information" is an Integration profile<br>
		 */


@Entity
@Name("integrationProfile")
@Table(name = "tf_integration_profile", schema = "public",
		uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
		@SequenceGenerator(name = "tf_integration_profile_sequence", sequenceName = "tf_integration_profile_id_seq", allocationSize=1)
		public class IntegrationProfile extends TFObject implements java.io.Serializable, Comparable<IntegrationProfile>   {


	/** Serial ID version of this object */
	private static final long serialVersionUID = -450911131561283760L;

	@Logger
	private static Log log;

	//	Attributes (existing in database as a column)
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tf_integration_profile_sequence")
	private Integer id;


	//	Constructors

	/**
	 * <p>Constructor for IntegrationProfile.</p>
	 */
	public IntegrationProfile() {
	}

	/**
	 * <p>Constructor for IntegrationProfile.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @param name a {@link java.lang.String} object.
	 */
	public IntegrationProfile(String keyword, String name) {
		this.keyword = keyword;
		this.name = name;
	}
	/**
	 * <p>Constructor for IntegrationProfile.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @param name a {@link java.lang.String} object.
	 * @param description a {@link java.lang.String} object.
	 */
	public IntegrationProfile(String keyword,
			String name, String description) {
		this.keyword = keyword;
		this.name = name;
		this.description = description;

	}

	/**
	 * <p>Constructor for IntegrationProfile.</p>
	 *
	 * @param oldIntegrationProfile a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile} object.
	 */
	public IntegrationProfile ( IntegrationProfile oldIntegrationProfile )
	{
		if (  oldIntegrationProfile.getKeyword() != null )
			this.keyword     = new String ( oldIntegrationProfile.getKeyword() ) ;
		if (  oldIntegrationProfile.getName()  != null )
			this.name        = new String ( oldIntegrationProfile.getName()    ) ;
		if ( oldIntegrationProfile.getDescription()  != null )
			this.description = new String ( oldIntegrationProfile.getDescription() ) ;

	}

	// *********************************************************
	//  Getters and Setters : setup the DB columns properties
	// *********************************************************



	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * <p>toString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String toString(){

		return keyword + "-" + name ;

	}


	/**
	 * <p>listAllIntegrationProfiles.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public static List<IntegrationProfile> listAllIntegrationProfiles(){
		return IntegrationProfile.ListAllTFObjects(IntegrationProfile.class) ;
	}

	/**
	 * <p>findIntegrationProfileWithKeyword.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile} object.
	 */
	public static IntegrationProfile findIntegrationProfileWithKeyword( String keyword )
	{
		if ( (keyword == null) || (keyword.length() == 0))
		{
			log.error("findIntegrationProfileWithKeyword - parameter given as required parameter is null");
			return null;
		}

		return IntegrationProfile.TFObjectByKeyword(IntegrationProfile.class, keyword) ;
	}


	/**
	 * <p>compareTo.</p>
	 *
	 * @param o a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile} object.
	 * @return a int.
	 */
	public int compareTo(IntegrationProfile o) {
		return this.keyword.compareTo(o.keyword) ;
	}
	
	/**
	 * <p>mergeIntegrationProfile.</p>
	 *
	 * @param inIntegrationProfile a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile} object.
	 */
	public static IntegrationProfile mergeIntegrationProfile(IntegrationProfile inIntegrationProfile){
		if(inIntegrationProfile!=null){
			IntegrationProfile integrationProfile=IntegrationProfile.findIntegrationProfileWithKeyword(inIntegrationProfile.getKeyword());
			if(integrationProfile==null){
				EntityManager  entityManager = ( EntityManager) Component.getInstance("entityManager") ;
				integrationProfile=entityManager.merge(inIntegrationProfile);
			}
			return integrationProfile;
		}
		return null;
	}
}

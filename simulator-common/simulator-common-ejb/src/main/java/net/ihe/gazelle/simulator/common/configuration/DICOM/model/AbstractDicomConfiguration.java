/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.configuration.DICOM.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import net.ihe.gazelle.simulator.common.configuration.model.AbstractConfiguration;
import net.ihe.gazelle.simulator.common.configuration.model.Configuration;




@MappedSuperclass
/**
 * <p>Abstract AbstractDicomConfiguration class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class AbstractDicomConfiguration extends AbstractConfiguration  
{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	/**
	 * ae_title attribute , Application Entity Title - The representation used to
	 * identify the DICOM nodes communicating between each other.
	 */
	@Column(name = "ae_title")
	@Pattern(regexp = "[A-Za-z0-9_]{1,16}", message = "AE Title should be formatted with letters and _")
	@NotNull
	@Size(max = 16)
	private String aeTitle;
	private static int MAX_LENGTH_FOR_AETITLE = 16 ;
	/****************************************************************************
	 * sopClass attributes <br/> The information object and the service class are
	 * the two fundamental components of DICOM.<br/> An understanding of these
	 * components makes it possible to comprehend, at least at a functional
	 * level, what DICOM does and why it is so useful.<br/> Information objects
	 * define the core contents of medical imaging, and service classes define
	 * what to do with those contents.<br/> The service classes and information
	 * objects are combined to form the functional units of DICOM.<br/> This
	 * combination is called a service-object pair, or SOP. Since DICOM is an
	 * object-oriented standard, the combination is actually called a
	 * service-object pair class, or SOP class.<br/> The SOP class is the
	 * elemental unit of DICOM; everything that DICOM does when implemented is
	 * based on the use of SOP classes.
	 */
	@ManyToOne
	@JoinColumn(name="sop_class_id" , nullable=false)
	private SopClass sopClass;

	/** Transfer role attributes */
	@Column(name = "transfer_role")
	@NotNull
	private String transferRole;

	/** modalityType attribute */
	@Column(name = "modality_type")
	private String modalityType;

	@Column(name="port")
	private Integer port ;

	@Column(name="port_secure")
	private Integer portSecured ;

	// ~ Constructors
	// //////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a new DicomConfiguration object.
	 */
	protected AbstractDicomConfiguration()
	{
		super(  ) ; 
	}

	/**
	 * <p>Constructor for AbstractDicomConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 */
	public AbstractDicomConfiguration(Configuration inConfiguration)
	{
		super(inConfiguration);
	}

	/**
	 * <p>Constructor for AbstractDicomConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param inPort a int.
	 */
	public AbstractDicomConfiguration( Configuration inConfiguration , int inPort )
	{
		super( inConfiguration ) ;
		port = inPort ;  
	}
	/**
	 * <p>Constructor for AbstractDicomConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param inAETitle a {@link java.lang.String} object.
	 * @param inPort a {@link java.lang.Integer} object.
	 */
	public AbstractDicomConfiguration( Configuration inConfiguration , String inAETitle , Integer inPort )
	{
		super( inConfiguration ) ;
		port    = inPort     ;   
		aeTitle = aeTitlePostTreatment ( inAETitle ) ;
		transferRole = " " ;
	}
	/**
	 * <p>Constructor for AbstractDicomConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param inPort a {@link java.lang.Integer} object.
	 * @param inPortSecure a {@link java.lang.Integer} object.
	 */
	public AbstractDicomConfiguration(  Configuration inConfiguration , Integer inPort , Integer inPortSecure )
	{
		super( inConfiguration ) ; 
		port        = inPort        ;   
		portSecured = inPortSecure  ;
		transferRole = " " ;
	}

	/**
	 * <p>Constructor for AbstractDicomConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param inAETitle a {@link java.lang.String} object.
	 * @param inPort a {@link java.lang.Integer} object.
	 * @param inPortSecure a {@link java.lang.Integer} object.
	 */
	public AbstractDicomConfiguration( Configuration inConfiguration , String inAETitle ,  Integer inPort , Integer inPortSecure )
	{
		super( inConfiguration ) ;
		aeTitle     = aeTitlePostTreatment ( inAETitle )    ;
		port        = inPort        ;    
		portSecured = inPortSecure  ;
		transferRole = " " ;
	}

	/**
	 * <p>Constructor for AbstractDicomConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param inAETitle a {@link java.lang.String} object.
	 * @param inPort a {@link java.lang.Integer} object.
	 * @param inPortSecure a {@link java.lang.Integer} object.
	 * @param inSopClass a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.SopClass} object.
	 * @param inTransfertRole a {@link java.lang.String} object.
	 */
	public AbstractDicomConfiguration(  Configuration inConfiguration , String inAETitle ,  Integer inPort , Integer inPortSecure, SopClass inSopClass , String inTransfertRole )
	{
		super( inConfiguration ) ;
		aeTitle     = aeTitlePostTreatment( inAETitle  )   ;
		port        = inPort        ;    
		portSecured = inPortSecure  ;
		sopClass    = inSopClass    ;
		transferRole= inTransfertRole ;
	}

	/**
	 * <p>Constructor for AbstractDicomConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param inAETitle a {@link java.lang.String} object.
	 * @param inSopClass a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.SopClass} object.
	 * @param inTransfertRole a {@link java.lang.String} object.
	 */
	public AbstractDicomConfiguration(  Configuration inConfiguration , String inAETitle ,    SopClass inSopClass , String inTransfertRole )
	{
		super( inConfiguration ) ; 
		aeTitle     = aeTitlePostTreatment( inAETitle  )   ;
		sopClass    = inSopClass    ;
		transferRole= inTransfertRole ;
	}
	/**
	 * <p>Constructor for AbstractDicomConfiguration.</p>
	 *
	 * @param inAbstractDicomConf a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.AbstractDicomConfiguration} object.
	 */
	public AbstractDicomConfiguration( AbstractDicomConfiguration inAbstractDicomConf )
	{


	}
	// ~ Methods
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////



	/**
	 * get the AE Title for the configuration
	 *
	 * @return a String aeTitle
	 */
	public String getAeTitle()
	{
		return aeTitle;
	}

	/**
	 * set the AE Title for the configuration
	 *
	 * @param inAeTitle a string
	 */
	public void setAeTitle(final String inAeTitle)
	{
		this.aeTitle = inAeTitle;
	}

	/**
	 * get the Sop class
	 *
	 * @return sopClass
	 */
	public SopClass getSopClass()
	{
		return sopClass;
	}

	/**
	 * set a new sop Class
	 *
	 * @param inSopClass a String
	 */
	public void setSopClass(final SopClass inSopClass)
	{
		this.sopClass = inSopClass;
	}

	/**
	 * get the Transfer Role for the current configuration
	 *
	 * @return transferRole a String
	 */
	public String getTransferRole()
	{
		return transferRole;
	}

	/**
	 * set a new Transfer
	 *
	 * @param inTransferRole a {@link java.lang.String} object.
	 */
	public void setTransferRole(final String inTransferRole)
	{
		this.transferRole = inTransferRole;
	}

	/**
	 * get the modality type
	 *
	 * @return modalityType
	 */
	public String getModalityType()
	{
		return modalityType;
	}

	/**
	 * set a new modality type
	 *
	 * @param inModalityType a {@link java.lang.String} object.
	 */
	public void setModalityType(final String inModalityType)
	{
		this.modalityType = inModalityType;
	}

	/**
	 * <p>Getter for the field <code>port</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getPort()
	{
		return port;
	}

	/**
	 * <p>Setter for the field <code>port</code>.</p>
	 *
	 * @param port a {@link java.lang.Integer} object.
	 */
	public void setPort(Integer port)
	{
		this.port = port;
	}

	/**
	 * <p>Getter for the field <code>portSecured</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getPortSecured()
	{
		return portSecured;
	}

	/**
	 * <p>Setter for the field <code>portSecured</code>.</p>
	 *
	 * @param portSecured a {@link java.lang.Integer} object.
	 */
	public void setPortSecured(Integer portSecured)
	{
		this.portSecured = portSecured;
	}

	private String aeTitlePostTreatment(String inAETitle)
	{
		if ( inAETitle.length() < MAX_LENGTH_FOR_AETITLE)   
			return inAETitle.toUpperCase().replaceAll("([^A-Z0-9_])", "_") ;
		else
			return inAETitle.substring(0, MAX_LENGTH_FOR_AETITLE ).toUpperCase().replaceAll("([^A-Z0-9_])", "_") ;
	}

	/**
	 * <p>hashCode.</p>
	 *
	 * @return a int.
	 */
	public int hashCode()
	{
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((aeTitle == null) ? 0 : aeTitle.hashCode());
		result = prime * result + ((modalityType == null) ? 0 : modalityType.hashCode());
		result = prime * result + ((port == null) ? 0 : port.hashCode());
		result = prime * result + ((portSecured == null) ? 0 : portSecured.hashCode());
		result = prime * result + ((sopClass == null) ? 0 : sopClass.hashCode());
		result = prime * result + ((transferRole == null) ? 0 : transferRole.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractDicomConfiguration other = (AbstractDicomConfiguration) obj;
		if (aeTitle == null)
		{
			if (other.aeTitle != null)
				return false;
		}
		else if (!aeTitle.equals(other.aeTitle))
			return false;
		if (modalityType == null)
		{
			if (other.modalityType != null)
				return false;
		}
		else if (!modalityType.equals(other.modalityType))
			return false;
		if (port == null)
		{
			if (other.port != null)
				return false;
		}
		else if (!port.equals(other.port))
			return false;
		if (portSecured == null)
		{
			if (other.portSecured != null)
				return false;
		}
		else if (!portSecured.equals(other.portSecured))
			return false;
		if (sopClass == null)
		{
			if (other.sopClass != null)
				return false;
		}
		else if (!sopClass.equals(other.sopClass))
			return false;
		if (transferRole == null)
		{
			if (other.transferRole != null)
				return false;
		}
		else if (!transferRole.equals(other.transferRole))
			return false;
		return true;
	}

	/*
	public static AbstractDicomConfiguration mergeDicomConfiguration(AbstractDicomConfiguration inAbstractDicomConfiguration){
		if(inAbstractDicomConfiguration!=null){
			EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
			Configuration conf=Configuration.mergeConfiguration(inAbstractDicomConfiguration.getConfiguration());
			inAbstractDicomConfiguration.setConfiguration(conf);
			inAbstractDicomConfiguration.setId(conf.getId());
			SopClass sopClass=inAbstractDicomConfiguration.getSopClass();
			if(sopClass!=null){
				sopClass=entityManager.merge(sopClass);
				inAbstractDicomConfiguration.setSopClass(sopClass);
			}
			inAbstractDicomConfiguration=entityManager.merge(inAbstractDicomConfiguration);
			return inAbstractDicomConfiguration;
		}
		return null;
	}
	*/


}


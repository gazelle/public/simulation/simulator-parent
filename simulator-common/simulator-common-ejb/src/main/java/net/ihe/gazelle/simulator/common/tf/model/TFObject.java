/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.tf.model;

//JPA imports
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.MappedSuperclass;
import javax.persistence.Query;
import javax.validation.constraints.Size;

import net.ihe.gazelle.hql.FilterLabel;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.jboss.seam.Component;


/**
 *  <b>Class Description :  </b>TFOject<br><br>
 * This abstract class is intended to be extended by others classes, for the purpose of implementing
 *  a technical framework object . Here is the attributes
 * <ul>
 * <li><b>keyword</b> </li>
 * <li><b>name</b> :  </li>
 * <li><b>description</b> :  </li>
 * </ul></br>
 *  and two associated columns in its table. <br>
 */

@MappedSuperclass
public abstract class TFObject {


 
	@Column(name = "keyword", unique = true, nullable = false , length=128)
	//@Length(max = 128 )
	@NotNull
	protected String keyword ;
 
	@Column(name = "name", length=128)
	//@Length(max = 128 )
	protected String name;
	
	@Column(name = "description")
	@Size(max = 2048)
	protected String description ;

	/**
	 * <p>Getter for the field <code>keyword</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getKeyword() {
		return keyword;
	}

	/**
	 * <p>Setter for the field <code>keyword</code>.</p>
	 *
	 * @param keyword a {@link java.lang.String} object.
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	/**
	 * <p>Getter for the field <code>name</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getName() {
		return name;
	}

	/**
	 * <p>Setter for the field <code>name</code>.</p>
	 *
	 * @param name a {@link java.lang.String} object.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * <p>Getter for the field <code>description</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <p>Setter for the field <code>description</code>.</p>
	 *
	 * @param description a {@link java.lang.String} object.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * <p>getSelectableLabel.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	@FilterLabel
	public String getSelectableLabel() {
		return getKeyword() + " - " + getName();
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TFObject other = (TFObject) obj;
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (keyword == null) {
			if (other.keyword != null) {
				return false;
			}
		} else if (!keyword.equals(other.keyword)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}
	
 
 
	/**
	 * <p>ListAllTFObjects.</p>
	 *
	 * @param clazz a {@link java.lang.Class} object.
	 * @param <T> a T object.
	 * @return a {@link java.util.List} object.
	 */
	@SuppressWarnings("unchecked")
    @Deprecated
	protected static <T> List<T> ListAllTFObjects(Class<T> clazz)
	{
		EntityManager  em = ( EntityManager) Component.getInstance("entityManager") ;
		
		Query q = em.createQuery("SELECT obj from " + clazz.getCanonicalName() + " obj order by obj.keyword asc") ;
		
		 return q.getResultList() ;
		
	}
	
	/**
	 * <p>TFObjectByKeyword.</p>
	 *
	 * @param clazz a {@link java.lang.Class} object.
	 * @param inKeyword a {@link java.lang.String} object.
	 * @param <T> a T object.
	 * @return a T object.
	 */
	@SuppressWarnings("unchecked")
    @Deprecated
	protected static <T> T TFObjectByKeyword(Class<T> clazz, String inKeyword )
	{
		EntityManager  em = ( EntityManager) Component.getInstance("entityManager") ;
		
		Query q = em.createQuery("SELECT obj from " + clazz.getCanonicalName() + " obj where obj.keyword = :inKeyword") ;
		q.setParameter("inKeyword", inKeyword ) ;
		
		List<T> result = q.getResultList() ;
		
		if ( result.size() > 0 )
		 return result.get(0) ;
		else return null ;
		
	}
	
	/**
	 * <p>TFObjectByKeyword.</p>
	 *
	 * @param clazz a {@link java.lang.Class} object.
	 * @param inKeyword a {@link java.lang.String} object.
	 * @param em a {@link javax.persistence.EntityManager} object.
	 * @param <T> a T object.
	 * @return a T object.
	 */
	@SuppressWarnings("unchecked")
    @Deprecated
    protected static <T> T TFObjectByKeyword(Class<T> clazz, String inKeyword, EntityManager  em)
    {
        Query q = em.createQuery("SELECT obj from " + clazz.getCanonicalName() + " obj where obj.keyword = :inKeyword") ;
        q.setParameter("inKeyword", inKeyword ) ;

        List<T> result = q.getResultList() ;

        if ( result.size() > 0 )
         return result.get(0) ;
        else return null ;

    }

}

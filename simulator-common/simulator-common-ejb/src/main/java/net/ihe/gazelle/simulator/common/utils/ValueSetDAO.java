package net.ihe.gazelle.simulator.common.utils;

import net.ihe.gazelle.simulator.common.model.ValueSet;
import net.ihe.gazelle.simulator.common.model.ValueSetQuery;
import org.jboss.seam.Component;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

/**
 * <p>ValueSetDAO class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class ValueSetDAO {

    /**
     * check if each value set from the list is accessible
     *
     * @param valueSetList a {@link java.util.List} object.
     * @return the number of accessible value sets
     * @throws java.lang.Exception if any.
     */
    public static Integer checkValueSetsAreAccessible(List<ValueSet> valueSetList) throws Exception{
        Integer accessibleValueSetsCount = 0;
        for (ValueSet vs : valueSetList) {
            if (checkValueSetIsAccessible(vs)){
                accessibleValueSetsCount ++;
            }
        }
        return accessibleValueSetsCount;
    }

    /**
     * <p>checkValueSetIsAccessible.</p>
     *
     * @param valueSet a {@link net.ihe.gazelle.simulator.common.model.ValueSet} object.
     * @return a {@link java.lang.Boolean} object.
     * @throws java.lang.Exception if any.
     */
    public static Boolean checkValueSetIsAccessible(ValueSet valueSet) throws Exception {
        boolean available = SVSConsumer.checkAvailability(valueSet.getValueSetOID());
        valueSet.setAccessible(available);
        valueSet.setLastCheck(new Date());
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        entityManager.merge(valueSet);
        entityManager.flush();
        return valueSet.getAccessible();
    }

    /**
     * <p>getAllValueSets.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public static List<ValueSet> getAllValueSets() {
        ValueSetQuery query = new ValueSetQuery();
        return query.getList();
    }

    /**
     * <p>getValueSetByKeyword.</p>
     *
     * @param keyword a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.simulator.common.model.ValueSet} object.
     */
    public static ValueSet getValueSetByKeyword(String keyword) {
        ValueSetQuery query = new ValueSetQuery();
        query.valueSetKeyword().like(keyword);
        return query.getUniqueResult();
    }
}

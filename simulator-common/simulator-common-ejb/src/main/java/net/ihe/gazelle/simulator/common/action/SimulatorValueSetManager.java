package net.ihe.gazelle.simulator.common.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.model.ValueSet;
import net.ihe.gazelle.simulator.common.model.ValueSetQuery;
import net.ihe.gazelle.simulator.common.utils.ValueSetDAO;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

/**
 * <p>SimulatorValueSetManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("simulatorValueSetManager")
@Scope(ScopeType.PAGE)
public class SimulatorValueSetManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -155225186219228794L;
    private Filter<ValueSet> filter;
    private String valueSetRepositoryUrl;
    private FilterDataModel<ValueSet> valueSets;

    /**
     * <p>init.</p>
     */
    @Create
    public void init() {
        valueSetRepositoryUrl = ApplicationConfiguration.getValueOfVariable("svs_repository_url").concat(
                "/RetrieveValueSet");
    }

    /**
     * <p>saveValueSet.</p>
     *
     * @param inValueSet a {@link net.ihe.gazelle.simulator.common.model.ValueSet} object.
     */
    public void saveValueSet(ValueSet inValueSet) {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        ValueSet valueSet = entityManager.merge(inValueSet);
        entityManager.flush();
        resetCache();
        // check if the value set is accessible in case the OID changed and does not match a value set registered in SVS Simulator
        checkValueSetIsAccessible(valueSet);
    }

    /**
     * <p>resetCache.</p>
     */
    public void resetCache() {
        ValueSetProvider.getInstance().reset();
        FacesMessages.instance().add(StatusMessage.Severity.INFO,
                "Cached value sets have been cleaned, new values will be uploaded from the SVS Repository upon request");
    }

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<ValueSet> getFilter() {
        if (filter == null) {
            ValueSetQuery query = new ValueSetQuery();
            filter = new Filter<ValueSet>(query.getHQLCriterionsForFilter());
        }
        return filter;
    }

    /**
     * <p>Getter for the field <code>valueSets</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<ValueSet> getValueSets() {
        if (valueSets == null) {
            this.valueSets = new FilterDataModel<ValueSet>(getFilter()) {
                @Override
                protected Object getId(ValueSet valueSet) {
                    return valueSet.getId();
                }
            };
        }
        return valueSets;
    }

    /**
     * <p>checkValueSetsAreAccessible.</p>
     */
    public void checkValueSetsAreAccessible() {
        List<ValueSet> valueSetList = valueSets.getAllItems(FacesContext.getCurrentInstance());
        try {
            Integer nbOfAccessibleValueSets = ValueSetDAO.checkValueSetsAreAccessible(valueSetList);
            FacesMessages.instance().add(StatusMessage.Severity.INFO, nbOfAccessibleValueSets + " out of " + valueSetList.size() + " value sets are" +
                    " accessible");
        } catch (Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
        }
    }

    /**
     * <p>checkValueSetIsAccessible.</p>
     *
     * @param valueSet a {@link net.ihe.gazelle.simulator.common.model.ValueSet} object.
     */
    public void checkValueSetIsAccessible(ValueSet valueSet) {
        try {
            Boolean status = ValueSetDAO.checkValueSetIsAccessible(valueSet);
            if (status == null){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to check accessibility of value set " + valueSet.getValueSetOID() );
            } else if (status){
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Value set with OID " + valueSet.getValueSetOID() + " is accessible");
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Value set with OID " + valueSet.getValueSetOID() + " is not accessible");
            }
        } catch (Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, valueSet.getValueSetOID() + ": " + e.getMessage());
        }
    }

    /**
     * <p>Getter for the field <code>valueSetRepositoryUrl</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getValueSetRepositoryUrl() {
        return valueSetRepositoryUrl;
    }
}

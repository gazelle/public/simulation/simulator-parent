package net.ihe.gazelle.simulator.common.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import javax.validation.constraints.NotNull;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;


@Entity
/**
 * <p>OIDConfiguration class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("oidConfiguration")
@Table(name = "tm_oid", schema = "public")
@SequenceGenerator(name = "tm_oid_sequence", sequenceName = "tm_oid_id_seq", allocationSize = 1)
public class OIDConfiguration implements Serializable, Comparable<OIDConfiguration>{

	
	@Logger
	private static Log log;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_oid_sequence")
	private Integer id;

	@Column(name = "oid",nullable=false)
	private String oid;

	@Column(name = "label",nullable=false)
	private String label;
	
	@ManyToOne
    @JoinColumn(name="system_id")
    private System system;
	
	@ManyToMany(fetch=FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinTable(name="gs_test_instance_oid", 
            joinColumns=@JoinColumn(name="oid_configuration_id"), 
            inverseJoinColumns=@JoinColumn(name="test_instance_id") ) 
    private List<TestInstance> testInstances;
	
	// constructors /////////
	
	/**
	 * <p>Constructor for OIDConfiguration.</p>
	 */
	public OIDConfiguration(){
	}

    /**
     * <p>Constructor for OIDConfiguration.</p>
     *
     * @param oid a {@link java.lang.String} object.
     * @param label a {@link java.lang.String} object.
     * @param system a {@link net.ihe.gazelle.simulator.common.model.System} object.
     */
    public OIDConfiguration(String oid, String label, System system) {
        super();
        this.oid = oid;
        this.label = label;
        this.system = system;
    }

    //-----------------------------
    
    

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>testInstances</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TestInstance> getTestInstances() {
        return testInstances;
    }

    /**
     * <p>Setter for the field <code>testInstances</code>.</p>
     *
     * @param testInstances a {@link java.util.List} object.
     */
    public void setTestInstances(List<TestInstance> testInstances) {
        this.testInstances = testInstances;
    }

    /**
     * <p>Getter for the field <code>oid</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getOid() {
        return oid;
    }


    /**
     * <p>Setter for the field <code>oid</code>.</p>
     *
     * @param oid a {@link java.lang.String} object.
     */
    public void setOid(String oid) {
        this.oid = oid;
    }


    /**
     * <p>Getter for the field <code>label</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return label;
    }


    /**
     * <p>Setter for the field <code>label</code>.</p>
     *
     * @param label a {@link java.lang.String} object.
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * <p>Setter for the field <code>system</code>.</p>
     *
     * @param system a {@link net.ihe.gazelle.simulator.common.model.System} object.
     */
    public void setSystem(System system) {
        this.system = system;
    }

    /**
     * <p>Getter for the field <code>system</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.model.System} object.
     */
    public System getSystem() {
        return system;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "OIDConfiguration [id=" + id + ", oid=" + oid + ", label="
                + label + "]";
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        result = prime * result + ((oid == null) ? 0 : oid.hashCode());
        result = prime * result + ((system == null) ? 0 : system.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        OIDConfiguration other = (OIDConfiguration) obj;
        if (label == null) {
            if (other.label != null)
                return false;
        } else if (!label.equals(other.label))
            return false;
        if (oid == null) {
            if (other.oid != null)
                return false;
        } else if (!oid.equals(other.oid))
            return false;
        if (system == null) {
            if (other.system != null)
                return false;
        } else if (!system.equals(other.system))
            return false;
        return true;
    }

    /**
     * <p>compareTo.</p>
     *
     * @param o a {@link net.ihe.gazelle.simulator.common.model.OIDConfiguration} object.
     * @return a int.
     */
    public int compareTo(OIDConfiguration o) {
        return oid.compareTo(o.oid);
    }
	
	
}

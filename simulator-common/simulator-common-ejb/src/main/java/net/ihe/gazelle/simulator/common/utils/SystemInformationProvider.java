package net.ihe.gazelle.simulator.common.utils;

/**
 * <p>SystemInformationProvider class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class SystemInformationProvider {

	/**
	 * <p>getSystemHL7InitiatorConfiguration.</p>
	 */
	public void getSystemHL7InitiatorConfiguration(){
		
	}
	
	
	/**
	 * <p>getSystemHL7ResponderConfiguration.</p>
	 */
	public void getSystemHL7ResponderConfiguration(){
		
	}
	
	/**
	 * <p>getSystemDicomSCPConfiguration.</p>
	 */
	public void getSystemDicomSCPConfiguration(){
		
	}
	
	/**
	 * <p>getSystemDicomSCUConfiguration.</p>
	 */
	public void getSystemDicomSCUConfiguration(){
		
	}
	
//	public void getSystemSysLogCongi

}

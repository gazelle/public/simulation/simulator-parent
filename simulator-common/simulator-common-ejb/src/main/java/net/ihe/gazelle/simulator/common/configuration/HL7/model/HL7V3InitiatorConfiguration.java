/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.simulator.common.configuration.HL7.model;

/**
 * <p>HL7V3InitiatorConfiguration class.</p>
 *
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, sep
 */

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.configuration.model.Configuration;
import net.ihe.gazelle.simulator.common.model.TestInstance;
import net.ihe.gazelle.simulator.common.model.TestStepsInstance;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;



@Entity
@Name("HL7V3InitiatorConfiguration")
@Table(name = "cfg_hl7_v3_initiator_configuration", schema = "public")
@SequenceGenerator(name = "cfg_hl7_v3_initiator_configuration_sequence", sequenceName = "cfg_hl7_v3_initiator_configuration_id_seq", allocationSize = 1)
public final class HL7V3InitiatorConfiguration extends AbstractHL7Configuration implements Serializable
{
   /**
    * 
    */
   private static final long serialVersionUID = 1L;

   /**
    * 
    */
  
   @Id
   @Column(name = "id", unique = true, nullable = false)
   @NotNull
   @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cfg_hl7_v3_initiator_configuration_sequence")
   private Integer id;
   
   @OneToMany(mappedBy = "hL7V3InitiatorConfiguration", fetch = FetchType.LAZY)
   private List<TestStepsInstance> testStepsInstances;
   

   /**
    * <p>Getter for the field <code>testStepsInstances</code>.</p>
    *
    * @return a {@link java.util.List} object.
    */
   public List<TestStepsInstance> getTestStepsInstances() {
    return testStepsInstances;
}

/**
 * <p>Setter for the field <code>testStepsInstances</code>.</p>
 *
 * @param testStepsInstances a {@link java.util.List} object.
 */
public void setTestStepsInstances(List<TestStepsInstance> testStepsInstances) {
    this.testStepsInstances = testStepsInstances;
}

/**
 * <p>Setter for the field <code>id</code>.</p>
 *
 * @param id a {@link java.lang.Integer} object.
 */
public void setId(Integer id) {
    this.id = id;
    }
    
    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }
    
    private HL7V3InitiatorConfiguration()
   {
      super();
      setTypeOfConfiguration(HL7V3InitiatorConfiguration.CONFIG_INITIATOR) ;  
   
   }

   /**
    * <p>Constructor for HL7V3InitiatorConfiguration.</p>
    *
    * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
    */
   public HL7V3InitiatorConfiguration( Configuration inConfiguration )
   {
      super(inConfiguration );
      setTypeOfConfiguration(HL7V3InitiatorConfiguration.CONFIG_INITIATOR) ; 
     
   }

   /**
    * <p>Constructor for HL7V3InitiatorConfiguration.</p>
    *
    * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
    * @param inComments a {@link java.lang.String} object.
    */
   public HL7V3InitiatorConfiguration( Configuration inConfiguration ,String inComments )
   {
      super(inConfiguration) ;
      comments = inComments ;
      setTypeOfConfiguration(HL7V3InitiatorConfiguration.CONFIG_INITIATOR) ;  
   

   }
   
   /**
    * <p>Constructor for HL7V3InitiatorConfiguration.</p>
    *
    * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
    * @param inSendingReceivingApplication a {@link java.lang.String} object.
    * @param inSendingReceivingFacility a {@link java.lang.String} object.
    * @param inAssigningAuthority a {@link java.lang.String} object.
    * @param inComments a {@link java.lang.String} object.
    */
   public HL7V3InitiatorConfiguration(Configuration inConfiguration, String inSendingReceivingApplication, String inSendingReceivingFacility, String inAssigningAuthority, String inComments)
   {
      super(inConfiguration, inSendingReceivingApplication, inSendingReceivingFacility, inAssigningAuthority, inComments);
   
      setTypeOfConfiguration(HL7V3InitiatorConfiguration.CONFIG_INITIATOR) ; 
   

   }
   
   /**
    * <p>getHL7V3InitiatorConfiguration.</p>
    *
    * @param hl7v3conf a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3InitiatorConfiguration} object.
    * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3InitiatorConfiguration} object.
    */
   public static HL7V3InitiatorConfiguration getHL7V3InitiatorConfiguration(HL7V3InitiatorConfiguration hl7v3conf){
       EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
       Query query=entityManager.createQuery("SELECT hl7v3conf FROM HL7V3InitiatorConfiguration hl7v3conf WHERE hl7v3conf.configuration=:configuration");
       query.setParameter("configuration",hl7v3conf.getConfiguration());
       List<HL7V3InitiatorConfiguration> result=query.getResultList();
       if (result != null){
           for (HL7V3InitiatorConfiguration ssc: result){
               if (ssc.equals(hl7v3conf)){
                   return ssc;
               }
           }
       }
       return null;
   }

   /**
    * <p>mergeHL7V3InitiatorConfiguration.</p>
    *
    * @param inHL7V3InitiatorConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3InitiatorConfiguration} object.
    * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3InitiatorConfiguration} object.
    */
   public static HL7V3InitiatorConfiguration mergeHL7V3InitiatorConfiguration(HL7V3InitiatorConfiguration inHL7V3InitiatorConfiguration) {
       if(inHL7V3InitiatorConfiguration!=null){
           EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
           Configuration conf=Configuration.mergeConfiguration(inHL7V3InitiatorConfiguration.getConfiguration());
           inHL7V3InitiatorConfiguration.setConfiguration(conf);
           HL7V3InitiatorConfiguration hl7v3conf = HL7V3InitiatorConfiguration.getHL7V3InitiatorConfiguration(inHL7V3InitiatorConfiguration);
           if (hl7v3conf != null) return hl7v3conf;
           inHL7V3InitiatorConfiguration=entityManager.merge(inHL7V3InitiatorConfiguration);
           return inHL7V3InitiatorConfiguration;
       }
       return null;
   }
   
   /**
    * <p>getHL7V3InitiatorConfigurationFiltered.</p>
    *
    * @param inSystem a {@link net.ihe.gazelle.simulator.common.model.System} object.
    * @param ti a {@link net.ihe.gazelle.simulator.common.model.TestInstance} object.
    * @param tsi a {@link net.ihe.gazelle.simulator.common.model.TestStepsInstance} object.
    * @param em a {@link javax.persistence.EntityManager} object.
    * @return a {@link java.util.List} object.
    */
   public static List<HL7V3InitiatorConfiguration> getHL7V3InitiatorConfigurationFiltered(net.ihe.gazelle.simulator.common.model.System inSystem, 
           TestInstance ti, TestStepsInstance tsi ,EntityManager em){
       
       HQLQueryBuilder<HL7V3InitiatorConfiguration> hbuild = new HQLQueryBuilder<HL7V3InitiatorConfiguration>(em, HL7V3InitiatorConfiguration.class);
       if (inSystem != null) hbuild.addEq("system", inSystem);
       if (tsi != null) hbuild.addEq("testStepsInstances", tsi);
       if (ti != null) hbuild.addEq("testStepsInstances.testInstance", ti);

      return hbuild.getList();
   }
  
   
  
}

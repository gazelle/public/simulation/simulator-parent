package net.ihe.gazelle.simulator.sut.model;



import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PersistenceException;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import net.ihe.gazelle.hql.FilterLabel;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.exception.ConstraintViolationException;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  <b>Class Description :  </b>SystemConfiguration<br><br>
 *  SystemConfiguration
 */
@Entity(name="commonSystemConfiguration")
@Name("commonSystemConfiguration")
@Table(name = "system_configuration", schema = "public", uniqueConstraints=@UniqueConstraint(columnNames="name"))
@SequenceGenerator(name = "system_configuration_sequence", sequenceName = "system_configuration_id_seq", allocationSize = 1)
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("SystemConfiguration")
public class SystemConfiguration implements Serializable, Comparable<SystemConfiguration> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5444985974251904657L;


	private static Logger log = LoggerFactory.getLogger(SystemConfiguration.class);

	//  attributes ////////////////////////////////////////////////////////////////////////////

	/** Id of this object */
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "system_configuration_sequence")
	protected Integer id;

	@Column(name = "name")
	protected String name;

	@Column(name="system_name")
	protected String systemName;

	@Column(name="url")
	@Pattern(regexp = "(^$)|(?i)(^http://.+$|^https://.+$)", message = "The provided URL is not valid" )
	protected String url;

	@ManyToMany(fetch=FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	@JoinTable(name="sys_conf_type_usages")
	private List<Usage> listUsages;

	@Column(name="owner")
	private String owner;

	@Column(name = "owner_company")
	private String ownerCompany;

	/**
	 * if not public, only users with admin role and the owner of the configuration can access it
	 */
	@Column(name="is_public")
	private Boolean isPublic;
	
	@Column(name="is_available")
	private Boolean isAvailable = true;

	// constructors ////////////////////////////////////////////////////////////////////////////

	/**
	 * <p>Constructor for SystemConfiguration.</p>
	 *
	 * @param configuration a {@link net.ihe.gazelle.simulator.sut.model.SystemConfiguration} object.
	 */
	public SystemConfiguration (SystemConfiguration configuration) {
		this.name = configuration.getName().concat("_COPY");
		this.url= configuration.getUrl();
		this.systemName = configuration.getSystemName();
	}

	// getters and setters //////////////////////////////////////////////////////////////////////

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * <p>Getter for the field <code>isAvailable</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getIsAvailable() {
		return isAvailable;
	}

	/**
	 * <p>Setter for the field <code>isAvailable</code>.</p>
	 *
	 * @param isAvailable a {@link java.lang.Boolean} object.
	 */
	public void setIsAvailable(Boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	/**
	 * <p>Getter for the field <code>name</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	@FilterLabel
	public String getName() {
		return name;
	}

	/**
	 * <p>Setter for the field <code>name</code>.</p>
	 *
	 * @param name a {@link java.lang.String} object.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * <p>Getter for the field <code>url</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * <p>Setter for the field <code>url</code>.</p>
	 *
	 * @param url a {@link java.lang.String} object.
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * <p>Setter for the field <code>systemName</code>.</p>
	 *
	 * @param systemName a {@link java.lang.String} object.
	 */
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	/**
	 * <p>Getter for the field <code>systemName</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getSystemName() {
		return systemName;
	}

	/**
	 * <p>Getter for the field <code>listUsages</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<Usage> getListUsages() {
		return listUsages;
	}

	/**
	 * <p>Setter for the field <code>listUsages</code>.</p>
	 *
	 * @param listUsages a {@link java.util.List} object.
	 */
	public void setListUsages(List<Usage> listUsages) {
		this.listUsages = listUsages;
	}

	/**
	 * <p>Constructor for SystemConfiguration.</p>
	 */
	public SystemConfiguration (){
		this.isPublic = true;
	}

    /**
     * <p>getEndpoint.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEndpoint(){
        return getUrl();
    }

	/**
	 * <p>onPersist.</p>
	 */
	@PrePersist
	public void onPersist(){
		if (Identity.instance().isLoggedIn()){
			this.owner = Identity.instance().getCredentials().getUsername();
			this.ownerCompany = GazelleIdentityImpl.instance().getOrganisationKeyword();
		}else{
			this.owner = null;
			this.ownerCompany = null;
			this.isPublic = true;
		}
	}
	// methods ///////////////////////////////////////////////////////////////////////////////////

	/**
	 * <p>deleteSystemConfiguration.</p>
	 *
	 * @param systemConfiguration - SystemConfiguration to delete
	 */
	public static void deleteSystemConfiguration(SystemConfiguration systemConfiguration){
		EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
		SystemConfiguration systemConfig = (SystemConfiguration)entityManager.find(SystemConfiguration.class, systemConfiguration.getId());
		entityManager.remove(systemConfig);
		entityManager.flush();
	}

	/**
	 * Creates or updates a system configuration
	 *
	 * @param inConfiguration : the configuration to create or merge
	 * @return a {@link net.ihe.gazelle.simulator.sut.model.SystemConfiguration} object.
	 */
	public static SystemConfiguration updateSystemConfiguration(SystemConfiguration inConfiguration)
	{
		if (inConfiguration == null)
			return null;

		EntityManager em = EntityManagerService.provideEntityManager();
		try{
			inConfiguration = em.merge(inConfiguration);
			em.flush();
			return inConfiguration;
		}catch(ConstraintViolationException cve){
			FacesMessages.instance().add(StatusMessage.Severity.WARN,"The name " + inConfiguration.getName() + " is already used, please choose another one");
			return null;
		}catch(PersistenceException pve){
			if (inConfiguration.getName() == null)
				FacesMessages.instance().add(StatusMessage.Severity.ERROR,"The name of your configuration cannot be null, please fill the appropriate field and save again");
			log.error(pve.getMessage());
			return inConfiguration;
		}catch(Exception e){
			FacesMessages.instance().add(StatusMessage.Severity.ERROR,"Unable to save configuration : " + e.getMessage());
			return inConfiguration;
		}
	}

	/**
	 * returns false if a configuration with the given name already exists
	 *
	 * @return a boolean.
	 */
	public boolean isNameValid() {
		if (this.name == null || this.name.isEmpty())
			return false;
		else
		{
			SystemConfigurationQuery query = new SystemConfigurationQuery();
			query.name().eq(this.name);
			if (this.id != null){
				query.id().neq(this.id);
			}
			return (query.getCount() == 0);
		}
	}


	/**
	 * return true if name, system name and responding gateway URL are null or empty
	 *
	 * @return a boolean.
	 */
	public boolean isEmpty() {
		return ((this.name == null || this.name.isEmpty())
				&& (this.systemName == null || this.systemName.isEmpty())
				&& (this.url == null || this.url.isEmpty()));
	}

	// hashcode and equals //////////////////////////////////////////////////////////////////////

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((systemName == null) ? 0 : systemName.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemConfiguration other = (SystemConfiguration) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (systemName == null) {
			if (other.systemName != null)
				return false;
		} else if (!systemName.equals(other.systemName))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	/**
	 * <p>Getter for the field <code>owner</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * <p>Setter for the field <code>owner</code>.</p>
	 *
	 * @param owner a {@link java.lang.String} object.
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * <p>Getter for the field <code>isPublic</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getIsPublic() {
		return isPublic;
	}

	/**
	 * <p>Setter for the field <code>isPublic</code>.</p>
	 *
	 * @param isPublic a {@link java.lang.Boolean} object.
	 */
	public void setIsPublic(Boolean isPublic) {
		this.isPublic = isPublic;
	}

	@Override
	public int compareTo(SystemConfiguration systemConfiguration) {
		if (systemConfiguration != null){
			return 0;
		} else {
			return name.compareTo(systemConfiguration.getName());
		}
	}

	public String getOwnerCompany() {
		return ownerCompany;
	}

	public void setOwnerCompany(String ownerCompany) {
		this.ownerCompany = ownerCompany;
	}
}


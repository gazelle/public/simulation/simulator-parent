package net.ihe.gazelle.simulator.samlassertion.common;

import org.w3c.dom.Element;

public interface AssertionProvider {

    Element getAssertion(String appliesTo, String subjectNameId, AbstractAssertionAttributes attributeStatement);

    AbstractAssertionAttributes createAssertionAttributes();
}

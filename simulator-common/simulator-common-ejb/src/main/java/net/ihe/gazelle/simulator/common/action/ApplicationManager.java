package net.ihe.gazelle.simulator.common.action;

import net.ihe.gazelle.common.Preferences.PreferencesKey;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.servletfilter.CSPHeaderFilter;
import net.ihe.gazelle.common.servletfilter.CSPPoliciesPreferences;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.model.ApplicationConfigurationQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.kohsuke.MetaInfServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>ApplicationManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("applicationPreferenceManager")
@Scope(ScopeType.PAGE)
@MetaInfServices(CSPPoliciesPreferences.class)
public class ApplicationManager implements Serializable, CSPPoliciesPreferences {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private static Logger log = LoggerFactory.getLogger(ApplicationManager.class);
	
	private ApplicationConfiguration preference = null;
    private Filter<ApplicationConfiguration> filter;

    private Filter<ApplicationConfiguration> getFilter(){
        if (filter == null){
            ApplicationConfigurationQuery query = new ApplicationConfigurationQuery();
            filter = new Filter<ApplicationConfiguration>(query.getHQLCriterionsForFilter());
        }
        return filter;
    }

	/**
	 * <p>getAllPreferences.</p>
	 *
	 * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
	 */
	public FilterDataModel<ApplicationConfiguration> getAllPreferences()
	{
        return new FilterDataModel<ApplicationConfiguration>(getFilter()) {
            @Override
            protected Object getId(ApplicationConfiguration applicationConfiguration) {
                return applicationConfiguration.getId();
            }
        };
	}
	
	/**
	 * <p>savePreference.</p>
	 *
	 * @param inPreference a {@link net.ihe.gazelle.simulator.common.model.ApplicationConfiguration} object.
	 */
	public void savePreference(ApplicationConfiguration inPreference)
	{
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		entityManager.merge(inPreference);
		entityManager.flush();
		preference = null;
		// reset preferences with scope application
		ApplicationConfigurationManager.instance().resetApplicationConfiguration();
	}
	
	/**
	 * <p>createNewPreference.</p>
	 */
	public void createNewPreference()
	{
		preference = new ApplicationConfiguration();
	}

	/**
	 * <p>editPreference.</p>
	 *
	 * @param pref a {@link net.ihe.gazelle.simulator.common.model.ApplicationConfiguration} object.
	 */
	public void editPreference(ApplicationConfiguration pref){
		preference = pref;
	}

	/**
	 * <p>Setter for the field <code>preference</code>.</p>
	 *
	 * @param preference a {@link net.ihe.gazelle.simulator.common.model.ApplicationConfiguration} object.
	 */
	public void setPreference(ApplicationConfiguration preference) {
		this.preference = preference;
	}

	/**
	 * <p>Getter for the field <code>preference</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.model.ApplicationConfiguration} object.
	 */
	public ApplicationConfiguration getPreference() {
		return preference;
	}

	/** {@inheritDoc} */
	@Override
	public boolean isContentPolicyActivated() {
		return ApplicationConfiguration.getBooleanValue(PreferencesKey.SECURITY_POLICIES.getFriendlyName());
	}

	/** {@inheritDoc} */
	@Override
	public Map<String, String> getHttpSecurityPolicies() {
		HashMap<String, String> headers = new HashMap<>();
		headers.put(PreferencesKey.SECURITY_POLICIES.getFriendlyName(),
				String.valueOf(ApplicationConfiguration.getBooleanValue(PreferencesKey.SECURITY_POLICIES.getFriendlyName())));
		headers.put(PreferencesKey.X_FRAME_OPTIONS.getFriendlyName(),
				ApplicationConfiguration.getValueOfVariable(PreferencesKey.X_FRAME_OPTIONS.getFriendlyName()));
		headers.put(PreferencesKey.CACHE_CONTROL.getFriendlyName(),
				ApplicationConfiguration.getValueOfVariable(PreferencesKey.CACHE_CONTROL.getFriendlyName()));
		headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName(),
				ApplicationConfiguration.getValueOfVariable(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName()));
		headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName(),
				ApplicationConfiguration.getValueOfVariable(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName()));
		headers.put(PreferencesKey.STRICT_TRANSPORT_SECURITY.getFriendlyName(),
				ApplicationConfiguration.getValueOfVariable(PreferencesKey.STRICT_TRANSPORT_SECURITY.getFriendlyName()));
		headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_CHROME.getFriendlyName(),
				ApplicationConfiguration.getValueOfVariable(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName()));
		headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY_CHROME.getFriendlyName(),
				ApplicationConfiguration.getValueOfVariable(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName()));
		return headers;
	}

	/** {@inheritDoc} */
	@Override
	public boolean getSqlInjectionFilterSwitch() {
		return ApplicationConfiguration.getBooleanValue(PreferencesKey.SQL_INJECTION_FILTER_SWITCH.getFriendlyName());
	}
	
	/**
	 * <p>resetHttpHeaders.</p>
	 */
	public void resetHttpHeaders() {
		log.info("Reset http headers to default values");
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        ApplicationConfiguration ap;

        for (PreferencesKey preference : PreferencesKey.values()) {
			ApplicationConfigurationQuery applicationConfigurationQuery = new ApplicationConfigurationQuery();
			String friendlyName = preference.getFriendlyName();
			applicationConfigurationQuery.variable().eq(friendlyName);
			ap = applicationConfigurationQuery.getUniqueResult();
			if (ap == null) {
				ap = new ApplicationConfiguration();
			}
			ap.setVariable(friendlyName);
			ap.setValue(preference.getDefaultValue());
			entityManager.merge(ap);
			entityManager.flush();
		}
		CSPHeaderFilter.clearCache();
		FacesMessages.instance().add(StatusMessage.Severity.INFO, "Default HTTP headers values have been restored");
	}
	
	/**
	 * <p>updateHttpHeaders.</p>
	 */
	public void updateHttpHeaders(){
		CSPHeaderFilter.clearCache();
		FacesMessages.instance().add(StatusMessage.Severity.INFO, "HTTP headers values have been updated ");
	}
	
}

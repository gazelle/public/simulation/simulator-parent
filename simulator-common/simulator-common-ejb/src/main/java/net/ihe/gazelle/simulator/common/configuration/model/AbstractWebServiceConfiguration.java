/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.simulator.common.configuration.model;



import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;


@MappedSuperclass
/**
 * <p>Abstract AbstractWebServiceConfiguration class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class AbstractWebServiceConfiguration extends AbstractConfiguration implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */

	@Column(name = "url",nullable=false )
	private String url ;

	@Column(name="port")
	private Integer port ;

	@Column(name="port_secured")
	private Integer portSecured ;

	@Column(name = "usage" )
	private String usage ;

	/**
	 * <p>Constructor for AbstractWebServiceConfiguration.</p>
	 */
	protected AbstractWebServiceConfiguration()
	{
		super() ;
	}


	/**
	 * <p>Constructor for AbstractWebServiceConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param inComments a {@link java.lang.String} object.
	 */
	public AbstractWebServiceConfiguration( Configuration inConfiguration ,String inComments )
	{
		super(inConfiguration) ;
		comments = inComments ;
	}

	/**
	 * <p>Constructor for AbstractWebServiceConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 */
	public AbstractWebServiceConfiguration( Configuration inConfiguration  )
	{
		super(inConfiguration) ;
	}

	/**
	 * <p>Constructor for AbstractWebServiceConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param url a {@link java.lang.String} object.
	 * @param port a {@link java.lang.Integer} object.
	 * @param portSecured a {@link java.lang.Integer} object.
	 * @param usage a {@link java.lang.String} object.
	 * @param comment a {@link java.lang.String} object.
	 */
	public AbstractWebServiceConfiguration( Configuration inConfiguration , String url , Integer port , Integer portSecured , String usage , String comment )
	{
		super(inConfiguration) ;
		this.url         = url         ;
		this.port        = port        ;
		this.portSecured = portSecured ;
		this.usage       = usage       ;
		this.comments    = comment     ;
	}
	/**
	 * <p>Constructor for AbstractWebServiceConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param port a {@link java.lang.Integer} object.
	 * @param portSecured a {@link java.lang.Integer} object.
	 * @param usage a {@link java.lang.String} object.
	 * @param comment a {@link java.lang.String} object.
	 */
	public AbstractWebServiceConfiguration( Configuration inConfiguration ,  Integer port , Integer portSecured , String usage , String comment )
	{
		super(inConfiguration) ; 
		this.port        = port        ;
		this.portSecured = portSecured ;
		this.usage       = usage       ;
		this.comments    = comment     ;
	}
	/**
	 * <p>Getter for the field <code>url</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUrl()
	{
		return url;
	}
	/**
	 * <p>Setter for the field <code>url</code>.</p>
	 *
	 * @param url a {@link java.lang.String} object.
	 */
	public void setUrl(String url)
	{
		this.url = url;
	}
	/**
	 * <p>Getter for the field <code>port</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getPort()
	{
		return port;
	}
	/**
	 * <p>Setter for the field <code>port</code>.</p>
	 *
	 * @param port a {@link java.lang.Integer} object.
	 */
	public void setPort(Integer port)
	{
		this.port = port;
	}

	/**
	 * <p>Getter for the field <code>portSecured</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getPortSecured()
	{
		return portSecured;
	}
	/**
	 * <p>Setter for the field <code>portSecured</code>.</p>
	 *
	 * @param portSecured a {@link java.lang.Integer} object.
	 */
	public void setPortSecured(Integer portSecured)
	{
		this.portSecured = portSecured;
	}

	/**
	 * <p>Getter for the field <code>usage</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUsage()
	{
		return usage;
	}
	/**
	 * <p>Setter for the field <code>usage</code>.</p>
	 *
	 * @param usage a {@link java.lang.String} object.
	 */
	public void setUsage(String usage)
	{
		this.usage = usage;
	}


	/**
	 * <p>hashCode.</p>
	 *
	 * @return a int.
	 */
	public int hashCode()
	{
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((port == null) ? 0 : port.hashCode());
		result = prime * result + ((portSecured == null) ? 0 : portSecured.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		result = prime * result + ((usage == null) ? 0 : usage.hashCode());
		return result;
	}


	/** {@inheritDoc} */
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractWebServiceConfiguration other = (AbstractWebServiceConfiguration) obj;
		if (port == null)
		{
			if (other.port != null)
				return false;
		}
		else if (!port.equals(other.port))
			return false;
		if (portSecured == null)
		{
			if (other.portSecured != null)
				return false;
		}
		else if (!portSecured.equals(other.portSecured))
			return false;
		if (url == null)
		{
			if (other.url != null)
				return false;
		}
		else if (!url.equals(other.url))
			return false;
		if (usage == null)
		{
			if (other.usage != null)
				return false;
		}
		else if (!usage.equals(other.usage))
			return false;
		return true;
	}


}

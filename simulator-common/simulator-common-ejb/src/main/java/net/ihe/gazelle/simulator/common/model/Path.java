package net.ihe.gazelle.simulator.common.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import javax.validation.constraints.NotNull;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;


@Entity
/**
 * <p>Path class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("path")
@Table(name = "tm_path", schema = "public")
@SequenceGenerator(name = "tm_path_sequence", sequenceName = "tm_path_id_seq", allocationSize = 1)
public class Path implements Serializable, Comparable<Path>{

	
	@Logger
	private static Log log;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_path_sequence")
	private Integer id;

	@Column(name = "keyword",nullable=false, unique=true)
	private String keyword;

	@Column(name = "description",nullable=false)
	private String description;
	
	@Column(name = "type",nullable=true)
	private String type;
	
	@OneToMany(mappedBy = "path", fetch = FetchType.LAZY)
    private List<ContextualInformation> contextualInformations;
	
	// constructors /////////
	
	/**
	 * <p>Constructor for Path.</p>
	 */
	public Path(){
	}
	
    /**
     * <p>Constructor for Path.</p>
     *
     * @param keyword a {@link java.lang.String} object.
     * @param description a {@link java.lang.String} object.
     * @param type a {@link java.lang.String} object.
     */
    public Path(String keyword, String description, String type) {
        super();
        this.keyword = keyword;
        this.description = description;
        this.type = type;
    }


    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>contextualInformations</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<ContextualInformation> getContextualInformations() {
        return contextualInformations;
    }

    /**
     * <p>Setter for the field <code>contextualInformations</code>.</p>
     *
     * @param contextualInformations a {@link java.util.List} object.
     */
    public void setContextualInformations(
            List<ContextualInformation> contextualInformations) {
        this.contextualInformations = contextualInformations;
    }

    /**
     * <p>Getter for the field <code>keyword</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * <p>Setter for the field <code>keyword</code>.</p>
     *
     * @param keyword a {@link java.lang.String} object.
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * <p>Getter for the field <code>description</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>Setter for the field <code>description</code>.</p>
     *
     * @param description a {@link java.lang.String} object.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the field <code>type</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getType() {
        return type;
    }

    /**
     * <p>Setter for the field <code>type</code>.</p>
     *
     * @param type a {@link java.lang.String} object.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * <p>hashCode.</p>
     *
     * @return a int.
     */
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Path other = (Path) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (keyword == null) {
            if (other.keyword != null)
                return false;
        } else if (!keyword.equals(other.keyword))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "Path [id=" + id + ", keyword=" + keyword + ", description="
                + description + ", type=" + type + "]";
    }

    /**
     * <p>compareTo.</p>
     *
     * @param o a {@link net.ihe.gazelle.simulator.common.model.Path} object.
     * @return a int.
     */
    public int compareTo(Path o) {
        return keyword.compareTo(o.keyword);
    }
    
    /**
     * <p>getPathByKeyword.</p>
     *
     * @param keyword a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.simulator.common.model.Path} object.
     */
    public static Path getPathByKeyword(String keyword){
        EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
        Query query=entityManager.createQuery("SELECT path FROM Path path WHERE path.keyword=:kw");
        query.setParameter("kw", keyword);
        List<Path> list=query.getResultList();
        if (list != null){
            if(list.size()>0){
                return list.get(0);
            }
        }
        return null;
    }
	
	
}

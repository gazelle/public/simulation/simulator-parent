package net.ihe.gazelle.simulator.common.ihewsresp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by aberge on 15/04/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class WSAddressingHandler implements SOAPHandler<SOAPMessageContext> {

	private static Logger log = LoggerFactory.getLogger(WSAddressingHandler.class);
	private static final String NS_WSA_ADDRESSING = "http://www.w3.org/2005/08/addressing";
	private static final String PREFIX_WSA_ADDRESSING = "wsa";
	private static final String REPLY_TO = "ReplyTo";
	private static final String FROM = "From";
	private static final String MESSAGE_ID = "MessageID";
	private static final String SECURITY_LOCAL = "Security";
	private static final String NS_SECURITY = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	private static final String PREFIX_SECURITY = "wsse";

	/** {@inheritDoc} */
	@Override public Set<QName> getHeaders() {
		final HashSet<QName> headers = new HashSet<QName>();
		headers.add(new QName(NS_WSA_ADDRESSING, REPLY_TO, PREFIX_WSA_ADDRESSING));
		headers.add(new QName(NS_WSA_ADDRESSING, FROM, PREFIX_WSA_ADDRESSING));
		headers.add(new QName(NS_WSA_ADDRESSING, MESSAGE_ID, PREFIX_WSA_ADDRESSING));
		headers.add(getWssSecurityHeader());
		return headers;
	}

	private QName getWssSecurityHeader() {
		return new QName(NS_SECURITY, SECURITY_LOCAL, PREFIX_SECURITY);
	}


	/** {@inheritDoc} */
	@Override public boolean handleMessage(SOAPMessageContext context) {
		return true;
	}

	/** {@inheritDoc} */
	@Override public boolean handleFault(SOAPMessageContext context) {
		return false;
	}

	/** {@inheritDoc} */
	@Override public void close(MessageContext context) {
	}
}

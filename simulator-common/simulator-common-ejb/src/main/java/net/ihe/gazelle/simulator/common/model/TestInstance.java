package net.ihe.gazelle.simulator.common.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile;
import net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption;
import net.ihe.gazelle.simulator.common.utils.AndOrWhere;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import javax.validation.constraints.NotNull;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

/**
 *  Seam and persistence object for test instance table.
 *  A <code>TestInstance</code> object encapsulates information relating to a
 *  particular test instance including:
 *  <ul>
 *  <li>A unique test instance ID
 *  <li>test status
 *  <li>links to participants in the test
 *  <li>links to message generated during the test.
 *  </ul>  <p>
 *  <b>Note:</b> Test status is not currently implemented or persisted.
 *
 * @author      Abdallah Miladi / INRIA Rennes IHE development Project

 * @version     1.0 - 2009-12-10
 */

@Entity
@Name("testInstance")
@Table(name = "gs_test_instance", schema = "public")
@SequenceGenerator(name = "gs_test_instance_sequence", sequenceName = "gs_test_instance_id_seq", allocationSize = 1)
public class TestInstance implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = -3095343732946301370L;

    @Logger
    private static Log log;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="gs_test_instance_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "test_instance_status_id")
    private TestInstanceStatus testInstanceStatus;

    /**
	 * A unique id for this test instance. A string, but must parse to an integer.
	 */
    @Column(name="server_test_instance_id",unique=true,nullable=false)
    private String serverTestInstanceId;

    @ManyToMany(fetch=FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    @JoinTable(name="gs_test_instance_test_instance_participants", 
            joinColumns=@JoinColumn(name="test_instance_id"), 
            inverseJoinColumns=@JoinColumn(name="test_instance_participants_id") ) 
    private List<TestInstanceParticipants> testInstanceParticipants;
    
    @ManyToMany(fetch=FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinTable(name="gs_test_instance_oid", 
            joinColumns=@JoinColumn(name="test_instance_id"), 
            inverseJoinColumns=@JoinColumn(name="oid_configuration_id") ) 
    private List<OIDConfiguration> oidConfigurations;

    @OneToMany(mappedBy = "testInstance", fetch = FetchType.LAZY)
    private List<TestStepsInstance> testStepsInstances;


    /**
     * <p>Constructor for TestInstance.</p>
     */
    public TestInstance(){

    }



    /**
     * <p>Getter for the field <code>oidConfigurations</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<OIDConfiguration> getOidConfigurations() {
        return oidConfigurations;
    }



    /**
     * <p>Setter for the field <code>oidConfigurations</code>.</p>
     *
     * @param oidConfigurations a {@link java.util.List} object.
     */
    public void setOidConfigurations(List<OIDConfiguration> oidConfigurations) {
        this.oidConfigurations = oidConfigurations;
    }



    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    /**
     * <p>Setter for the field <code>testStepsInstances</code>.</p>
     *
     * @param testStepsInstances a {@link java.util.List} object.
     */
    public void setTestStepsInstances(List<TestStepsInstance> testStepsInstances) {
        this.testStepsInstances = testStepsInstances;
    }


    /**
     * <p>Getter for the field <code>testStepsInstances</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TestStepsInstance> getTestStepsInstances() {
        return testStepsInstances;
    }
    
    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }
    /**
     * <p>Getter for the field <code>testInstanceStatus</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.model.TestInstanceStatus} object.
     */
    public TestInstanceStatus getTestInstanceStatus() {
        return testInstanceStatus;
    }

    /**
     * <p>Setter for the field <code>testInstanceStatus</code>.</p>
     *
     * @param testInstanceStatus a {@link net.ihe.gazelle.simulator.common.model.TestInstanceStatus} object.
     */
    public void setTestInstanceStatus(TestInstanceStatus testInstanceStatus) {
        this.testInstanceStatus = testInstanceStatus;
    }

    /**
     * <p>Getter for the field <code>serverTestInstanceId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getServerTestInstanceId() {
        return serverTestInstanceId;
    }

    /**
     * <p>Setter for the field <code>serverTestInstanceId</code>.</p>
     *
     * @param serverTestInstanceId a {@link java.lang.String} object.
     */
    public void setServerTestInstanceId(String serverTestInstanceId) {
        this.serverTestInstanceId = serverTestInstanceId;
    }

    /**
     * <p>Getter for the field <code>testInstanceParticipants</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TestInstanceParticipants> getTestInstanceParticipants() {
        return testInstanceParticipants;
    }

    /**
     * <p>Setter for the field <code>testInstanceParticipants</code>.</p>
     *
     * @param testInstanceParticipants a {@link java.util.List} object.
     */
    public void setTestInstanceParticipants(
            List<TestInstanceParticipants> testInstanceParticipants) {
        this.testInstanceParticipants = testInstanceParticipants;
    }

    /**
     * Gets TestInstance for passed Instance ID.
     *
     * @param inTestInstanceId string Instance ID to select
     * @return <code>TestInstance</code> object, <code>null</code> on error.
     */
    @SuppressWarnings("unchecked")
    public static TestInstance getTestInstanceByServerTestInstanceId(String inTestInstanceId) {
        EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
        Query query=entityManager.createQuery("SELECT ti FROM TestInstance ti WHERE ti.serverTestInstanceId=:inTestInstanceId");
        query.setParameter("inTestInstanceId", inTestInstanceId);
        List<TestInstance> list=query.getResultList();
        if(list.size()>0)
            return list.get(0);
        return null;
    }

    /**
     * Gets list of all Test Instances on file.
     *
     * @return <code>List</code> of <code>TestInstance</code> objects, will be
     * empty if no instances are on file.
     */
    @SuppressWarnings("unchecked")
    public static List<TestInstance> getListOfTestInstance(){
        EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
        Query query=entityManager.createQuery("SELECT ti FROM TestInstance ti");
        List<TestInstance> list=(List<TestInstance>)(query.getResultList());
        log.info("list.size() = " + list.size());
        return list;
    }


    /**
     * Adds passed <code>TestInstanceParticipants</code> to those designated
     * as being on this test instance.
     *
     * @param inTestInstanceParticipants <code>TestInstanceParticipants</code>
     * object to add.
     */
    public void addTestInstanceParticipants(TestInstanceParticipants inTestInstanceParticipants){
        if(testInstanceParticipants==null) testInstanceParticipants=new ArrayList<TestInstanceParticipants>();
        testInstanceParticipants.add(inTestInstanceParticipants);
    }


    

    /**
     * <p>hashCode.</p>
     *
     * @return a int.
     */
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((serverTestInstanceId == null) ? 0 : serverTestInstanceId
                        .hashCode());
        result = prime
                * result
                + ((testInstanceParticipants == null) ? 0
                        : testInstanceParticipants.hashCode());
        result = prime
                * result
                + ((testInstanceStatus == null) ? 0 : testInstanceStatus
                        .hashCode());
        return result;
    }


    /** {@inheritDoc} */
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TestInstance other = (TestInstance) obj;
        if (serverTestInstanceId == null) {
            if (other.serverTestInstanceId != null)
                return false;
        } else if (!serverTestInstanceId.equals(other.serverTestInstanceId))
            return false;
        if (testInstanceParticipants == null) {
            if (other.testInstanceParticipants != null)
                return false;
        } else if (!testInstanceParticipants
                .equals(other.testInstanceParticipants))
            return false;
        if (testInstanceStatus == null) {
            if (other.testInstanceStatus != null)
                return false;
        } else if (!testInstanceStatus.equals(other.testInstanceStatus))
            return false;
        return true;
    }


    /**
     * <p>getTestInstanceFiltred.</p>
     *
     * @param inActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param inIntegrationProfile a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile} object.
     * @param inIntegrationProfileOption a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption} object.
     * @return a {@link java.util.List} object.
     */
    public static List<TestInstance> getTestInstanceFiltred(Actor inActor,
                                                            IntegrationProfile inIntegrationProfile,
                                                            IntegrationProfileOption inIntegrationProfileOption){
        EntityManager em = EntityManagerService.provideEntityManager();
        Query query ;
        StringBuffer queryString = new StringBuffer() ;
        HashSet < String> prefixes = new HashSet<String>();
        HashSet < String> prefixesUsed = new HashSet<String>();
        HashMap<String ,String > mapOfJoin = new HashMap<String, String>() ;
        HashMap<String , Object > mapOfParameters = new HashMap<String, Object>() ;


        if (inActor != null){
            AndOrWhere.addANDorWHERE(queryString) ;
            prefixes.add("TestInstance ti");
            mapOfJoin.put("TestInstance ti", "join ti.testInstanceParticipants tip" ) ;
            queryString.append( "tip.actorIntegrationProfileOption.actorIntegrationProfile.actor=:inActor" );
            mapOfParameters.put("inActor", inActor);
        }
        
        if (inIntegrationProfile != null){
            AndOrWhere.addANDorWHERE(queryString) ;
            prefixes.add("TestInstance ti");
            mapOfJoin.put("TestInstance ti", "join ti.testInstanceParticipants tip" ) ;
            queryString.append( "tip.actorIntegrationProfileOption.actorIntegrationProfile.integrationProfile=:inIntegrationProfile" );
            mapOfParameters.put("inIntegrationProfile", inIntegrationProfile);
        }
        
        if (inIntegrationProfileOption != null){
            AndOrWhere.addANDorWHERE(queryString) ;
            prefixes.add("TestInstance ti");
            mapOfJoin.put("TestInstance ti", "join ti.testInstanceParticipants tip" ) ;
            queryString.append( "tip.actorIntegrationProfileOption.integrationProfileOption=:inIntegrationProfileOption" );
            mapOfParameters.put("inIntegrationProfileOption", inIntegrationProfileOption);
        }



        prefixes.add("TestInstance ti");

        List<String> listOfPrefixes = new ArrayList<String>( prefixes ) ;
        StringBuffer firstPartOfQuery = new StringBuffer() ;

        for ( int i= 0 ; i< listOfPrefixes.size() ; i++  )
        {
            if ( i == 0 ) 
            {
                firstPartOfQuery.append("SELECT distinct ti FROM "    ) ;
            }

            if ( !prefixesUsed.contains(listOfPrefixes.get(i)) )
            {
                if ( prefixesUsed.size() > 0 ) firstPartOfQuery.append(" , "  ) ;

                firstPartOfQuery.append( listOfPrefixes.get(i) ) ;
                if (mapOfJoin.containsKey(listOfPrefixes.get(i)))
                    firstPartOfQuery.append(" " + mapOfJoin.get(listOfPrefixes.get(i)) +" " ) ;
                prefixesUsed.add( listOfPrefixes.get(i) ) ;
            }
        }

        queryString.insert(0, firstPartOfQuery ) ;
        queryString.append( " ORDER BY ti.serverTestInstanceId ASC" );
        if ( queryString.toString().trim().length() == 0 ) return null ;
        query = em.createQuery(queryString.toString()) ;
        List<String> listOfParameters =  new ArrayList<String>( mapOfParameters.keySet() ) ;
        for ( String param : listOfParameters ){
            query.setParameter( param , mapOfParameters.get(param) ) ;
        }
        List<TestInstance> listOfTestInstance = ( List<TestInstance> )query.getResultList(); 
        return listOfTestInstance ;
    }


}

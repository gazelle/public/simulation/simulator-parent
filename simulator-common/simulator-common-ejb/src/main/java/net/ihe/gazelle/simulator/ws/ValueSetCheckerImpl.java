package net.ihe.gazelle.simulator.ws;

import net.ihe.gazelle.simulator.common.model.ValueSet;
import net.ihe.gazelle.simulator.common.model.ValueSetWrapper;
import net.ihe.gazelle.simulator.common.utils.ValueSetDAO;
import org.jboss.seam.contexts.Lifecycle;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import java.util.List;

@Stateless
/**
 * <p>ValueSetCheckerImpl class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class ValueSetCheckerImpl implements ValueSetCheckerApi {


    /** {@inheritDoc} */
    @Override
    public Response checkAllValueSets() {
        Lifecycle.beginCall();
        List<ValueSet> valueSetList = ValueSetDAO.getAllValueSets();
        if (valueSetList.isEmpty()){
            Lifecycle.endCall();
            javax.ws.rs.core.Response.ResponseBuilder responseBuilder = javax.ws.rs.core.Response.noContent().status(404);
            responseBuilder.header("Warning", "Warning: 199 PatientManager \" No value set defined in this application \"");
            return responseBuilder.build();
        } else {
            try {
                Integer nbOfAccessibleValueSets = ValueSetDAO.checkValueSetsAreAccessible(valueSetList);
                ValueSetWrapper wrapper = new ValueSetWrapper(valueSetList);
                wrapper.setAccessible(nbOfAccessibleValueSets);
                wrapper.setCount(valueSetList.size());
                Response.ResponseBuilder builder = Response.ok(wrapper);
                Lifecycle.endCall();
                return builder.build();
            } catch (Exception e) {
                Lifecycle.endCall();
                javax.ws.rs.core.Response.ResponseBuilder responseBuilder = javax.ws.rs.core.Response.noContent().status(404);
                responseBuilder.header("Warning", "Warning: 199 PatientManager \"" + e.getMessage() + "\"");
                return responseBuilder.build();
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public Response checkValueSet(String keyword) {
        Lifecycle.beginCall();
        ValueSet valueSet = ValueSetDAO.getValueSetByKeyword(keyword);
        if (valueSet == null){
            javax.ws.rs.core.Response.ResponseBuilder responseBuilder = javax.ws.rs.core.Response.noContent().status(404);
            responseBuilder.header("Warning", "Warning: 199 PatientManager \" Value set with keyword" + keyword + " not found \"");
            Lifecycle.endCall();
            return responseBuilder.build();
        } else {
            try {
                ValueSetDAO.checkValueSetIsAccessible(valueSet);
                Response.ResponseBuilder builder = Response.ok(true);
                Lifecycle.endCall();
                return builder.build();
            } catch (Exception e) {
                Lifecycle.endCall();
                javax.ws.rs.core.Response.ResponseBuilder responseBuilder = javax.ws.rs.core.Response.noContent().status(404);
                responseBuilder.header("Warning", "Warning: 199 PatientManager \"" + e.getMessage() + "\"");
                return responseBuilder.build();
            }
        }
    }
}

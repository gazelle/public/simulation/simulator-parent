package net.ihe.gazelle.simulator.common.action;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.richfaces.model.TreeNode;
import org.richfaces.model.TreeNodeImpl;




/**
 * <p>GazelleTreeNodeImpl class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class GazelleTreeNodeImpl <T> extends TreeNodeImpl implements Serializable {
		
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private T data;

		
		/**
		 * <p>Getter for the field <code>data</code>.</p>
		 *
		 * @return a T object.
		 */
		public T getData() {
			return data;
		}
	
		
	
		/**
		 * <p>Setter for the field <code>data</code>.</p>
		 *
		 * @param data a T object.
		 */
		public void setData(T data) {
			this.data = data;
		}
	
		

		
	
	}

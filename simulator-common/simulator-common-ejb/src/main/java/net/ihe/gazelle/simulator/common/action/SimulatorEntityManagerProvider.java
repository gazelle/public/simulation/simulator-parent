package net.ihe.gazelle.simulator.common.action;

import net.ihe.gazelle.hql.providers.detached.AbstractEntityManagerProvider;

import org.jboss.seam.contexts.Contexts;
import org.kohsuke.MetaInfServices;

/**
 * <p>SimulatorEntityManagerProvider class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@MetaInfServices(net.ihe.gazelle.hql.providers.EntityManagerProvider.class)
public class SimulatorEntityManagerProvider extends AbstractEntityManagerProvider {

	private static final int LIGHT_WEIGHT = -100;
	private static final int HEAVY_WEIGHT = 100;


	/** {@inheritDoc} */
	@Override
	public Integer getWeight() {
		if (Contexts.isApplicationContextActive()) {
			return HEAVY_WEIGHT;
		} else {
			return LIGHT_WEIGHT;
		}
	}

	/** {@inheritDoc} */
	@Override
	public String getHibernateConfigPath() {
		return "META-INF/hibernate.cfg.xml";
	}

}

package net.ihe.gazelle.simulator.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <p>TestReportTool class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Tool")
public class TestReportTool {
	@XmlElement(name = "Name")
	private String name;
	@XmlElement(name = "URL")
	private String url;

	/**
	 * <p>Constructor for TestReportTool.</p>
	 */
	public TestReportTool(){
		
	}
	
	/**
	 * <p>Constructor for TestReportTool.</p>
	 *
	 * @param inName a {@link java.lang.String} object.
	 * @param inURL a {@link java.lang.String} object.
	 */
	public TestReportTool(String inName, String inURL) {
		this.name = inName;
		this.url = inURL;
	}

	/**
	 * <p>Getter for the field <code>name</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getName() {
		return name;
	}

	/**
	 * <p>Setter for the field <code>name</code>.</p>
	 *
	 * @param name a {@link java.lang.String} object.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * <p>Getter for the field <code>url</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * <p>Setter for the field <code>url</code>.</p>
	 *
	 * @param url a {@link java.lang.String} object.
	 */
	public void setUrl(String url) {
		this.url = url;
	}
}

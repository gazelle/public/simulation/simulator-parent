package net.ihe.gazelle.simulator.samlassertion.common;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


public abstract class AbstractAssertionAttributes<T extends AssertionAttributeType> implements Serializable {

    protected static final String NS_XS = "http://www.w3.org/2001/XMLSchema";
    protected static final String NS_XSI = "http://www.w3.org/2001/XMLSchema-instance";

    /**
     * The Constant NS_SAML2.
     */
    protected static final String NS_SAML2 = "urn:oasis:names:tc:SAML:2.0:assertion";

    private static final long serialVersionUID = 2893478078360476453L;


    /**
     * Gets the value.
     *
     * @param type the type
     * @return the value
     */
    public String getValue(T type) {
        if (getDelegate().get(type) == null) {
            return null;
        } else {
            if (getDelegate().get(type).size() == 0) {
                return null;
            } else {
                return getDelegate().get(type).get(0);
            }
        }
    }

    /**
     * Gets the values.
     *
     * @param type the type
     * @return the values
     */
    public List<String> getValues(T type) {
        checkType(type);
        return getDelegate().get(type);
    }

    /**
     * Sets the value.
     *
     * @param type  the type
     * @param value the value
     */
    public void setValue(T type, String value) {
        checkType(type);
        if (getDelegate().get(type).size() == 0) {
            getDelegate().get(type).add(value);
        } else {
            getDelegate().get(type).set(0, value);
        }
    }

    /**
     * Check type.<br>
     * Avoid NPE
     *
     * @param type the type
     */
    private void checkType(T type) {
        if (getDelegate().get(type) == null) {
            getDelegate().put(type, new ArrayList<String>(1));
        }
    }

    public abstract Map<T, List<String>> getDelegate();

    /**
     * Creates the element.
     *
     * @param document
     * @return the element
     */
    protected Element getDOMElement(String prefix) throws SignatureException {
        DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
        fabrique.setNamespaceAware(true);
        DocumentBuilder constructeur;
        try {
            constructeur = fabrique.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new SignatureException(e);
        }
        Document document = constructeur.newDocument();

        Element attributeStatement = document.createElementNS(NS_SAML2, prefix + "AttributeStatement");

        Set<Map.Entry<T, List<String>>> entrySet = getDelegate().entrySet();
        for (Map.Entry<T, List<String>> entry : entrySet) {
            AssertionAttributeType key = entry.getKey();
            List<String> values = entry.getValue();

            Element attribute = document.createElementNS(NS_SAML2, prefix + "Attribute");
            attributeStatement.appendChild(attribute);

            attribute.setAttribute("FriendlyName", key.getFriendlyName());
            attribute.setAttribute("Name", key.getName());
            attribute.setAttribute("NameFormat", key.getType());

            for (String value : values) {
                Element valueElement = document.createElementNS(NS_SAML2, prefix + "AttributeValue");
                valueElement.setAttribute("xmlns:xs", NS_XS);
                valueElement.setAttribute("xmlns:xsi", NS_XSI);
                valueElement.setAttribute("xsi:type", key.getXsiType());
                attribute.appendChild(valueElement);
                valueElement.setTextContent(value);
            }

        }

        return attributeStatement;
    }
}

/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.simulator.common.configuration.HL7.model;

/**
 * <p>HL7V3ResponderConfiguration class.</p>
 *
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, sep
 */

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.configuration.model.Configuration;
import net.ihe.gazelle.simulator.common.model.TestInstance;
import net.ihe.gazelle.simulator.common.model.TestStepsInstance;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;



@Entity
@Name("hl7V3ResponderConfiguration")
@Table(name = "cfg_hl7_v3_responder_configuration", schema = "public")
@SequenceGenerator(name = "cfg_hl7_v3_responder_configuration_sequence", sequenceName = "cfg_hl7_v3_responder_configuration_id_seq", allocationSize = 1)
public final class HL7V3ResponderConfiguration extends AbstractHL7Configuration implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cfg_hl7_v3_responder_configuration_sequence")
    private Integer id;

	@Column(name = "url",nullable=false )
	private String url ;

	@Column(name="port")
	private Integer port ;

	@Column(name="port_secured")
	private Integer portSecured ;

	@Column(name = "usage" )
	private String usage ;

	@OneToMany(mappedBy = "hL7V3ResponderConfiguration", fetch = FetchType.LAZY)
    private List<TestStepsInstance> testStepsInstances;


	/**
	 * <p>Getter for the field <code>testStepsInstances</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<TestStepsInstance> getTestStepsInstances() {
        return testStepsInstances;
    }

    /**
     * <p>Setter for the field <code>testStepsInstances</code>.</p>
     *
     * @param testStepsInstances a {@link java.util.List} object.
     */
    public void setTestStepsInstances(List<TestStepsInstance> testStepsInstances) {
        this.testStepsInstances = testStepsInstances;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Getter for the field <code>url</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUrl()
	{
		return url;
	}

	/**
	 * <p>Setter for the field <code>url</code>.</p>
	 *
	 * @param url a {@link java.lang.String} object.
	 */
	public void setUrl(String url)
	{
		this.url = url;
	}

	/**
	 * <p>Getter for the field <code>port</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getPort()
	{
		return port;
	}

	/**
	 * <p>Setter for the field <code>port</code>.</p>
	 *
	 * @param port a {@link java.lang.Integer} object.
	 */
	public void setPort(Integer port)
	{
		this.port = port;
	}

	/**
	 * <p>Getter for the field <code>portSecured</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getPortSecured()
	{
		return portSecured;
	}

	/**
	 * <p>Setter for the field <code>portSecured</code>.</p>
	 *
	 * @param portSecured a {@link java.lang.Integer} object.
	 */
	public void setPortSecured(Integer portSecured)
	{
		this.portSecured = portSecured;
	}

	/**
	 * <p>Getter for the field <code>usage</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUsage()
	{
		return usage;
	}

	/**
	 * <p>Setter for the field <code>usage</code>.</p>
	 *
	 * @param usage a {@link java.lang.String} object.
	 */
	public void setUsage(String usage)
	{
		this.usage = usage;
	}

	private HL7V3ResponderConfiguration()
	{
		super();
		setTypeOfConfiguration(HL7V3ResponderConfiguration.CONFIG_RESPONDER) ;  
	}

	/**
	 * <p>Constructor for HL7V3ResponderConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 */
	public HL7V3ResponderConfiguration( Configuration inConfiguration )
	{
		super(inConfiguration );
		setTypeOfConfiguration(HL7V3ResponderConfiguration.CONFIG_RESPONDER) ;  

	}

	/**
	 * <p>Constructor for HL7V3ResponderConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param inComments a {@link java.lang.String} object.
	 */
	public HL7V3ResponderConfiguration( Configuration inConfiguration ,String inComments )
	{
		super(inConfiguration) ;
		comments = inComments ;
		setTypeOfConfiguration(HL7V3ResponderConfiguration.CONFIG_RESPONDER) ;  

	}

	/**
	 * <p>Constructor for HL7V3ResponderConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param inSendingReceivingApplication a {@link java.lang.String} object.
	 * @param inSendingReceivingFacility a {@link java.lang.String} object.
	 * @param inAssigningAuthority a {@link java.lang.String} object.
	 * @param inComments a {@link java.lang.String} object.
	 */
	public HL7V3ResponderConfiguration(Configuration inConfiguration, String inSendingReceivingApplication, String inSendingReceivingFacility, String inAssigningAuthority, String inComments)
	{
		super(inConfiguration, inSendingReceivingApplication, inSendingReceivingFacility, inAssigningAuthority, inComments);
		setTypeOfConfiguration(HL7V3ResponderConfiguration.CONFIG_RESPONDER) ;  

	}

	/**
	 * <p>Constructor for HL7V3ResponderConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param port a {@link java.lang.Integer} object.
	 * @param portSecured a {@link java.lang.Integer} object.
	 * @param usage a {@link java.lang.String} object.
	 * @param comment a {@link java.lang.String} object.
	 */
	public HL7V3ResponderConfiguration( Configuration inConfiguration ,  Integer port , Integer portSecured , String usage , String comment )
	{
		super(inConfiguration) ; 
		this.port        = port        ;
		this.portSecured = portSecured ;
		this.usage       = usage       ;
		this.comments    = comment     ;

		setTypeOfConfiguration(HL7V3ResponderConfiguration.CONFIG_RESPONDER) ;  

	} 

	/**
	 * <p>Constructor for HL7V3ResponderConfiguration.</p>
	 *
	 * @param inConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.Configuration} object.
	 * @param url a {@link java.lang.String} object.
	 * @param port a {@link java.lang.Integer} object.
	 * @param portSecured a {@link java.lang.Integer} object.
	 * @param usage a {@link java.lang.String} object.
	 * @param inSendingReceivingApplication a {@link java.lang.String} object.
	 * @param inSendingReceivingFacility a {@link java.lang.String} object.
	 * @param inAssigningAuthority a {@link java.lang.String} object.
	 * @param inComments a {@link java.lang.String} object.
	 */
	public HL7V3ResponderConfiguration(Configuration inConfiguration,  String url , Integer port , Integer portSecured , String usage ,  String inSendingReceivingApplication, String inSendingReceivingFacility, String inAssigningAuthority, String inComments)
	{
		super(inConfiguration, inSendingReceivingApplication, inSendingReceivingFacility, inAssigningAuthority, inComments);

		this.url = url ;
		this.port = port ;
		this.portSecured = portSecured ;
		this.usage = usage  ;

		setTypeOfConfiguration(HL7V3ResponderConfiguration.CONFIG_RESPONDER) ;  

	}

	/** {@inheritDoc} */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((port == null) ? 0 : port.hashCode());
		result = prime * result + ((portSecured == null) ? 0 : portSecured.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		result = prime * result + ((usage == null) ? 0 : usage.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HL7V3ResponderConfiguration other = (HL7V3ResponderConfiguration) obj;
		if (port == null)
		{
			if (other.port != null)
				return false;
		}
		else if (!port.equals(other.port))
			return false;
		if (portSecured == null)
		{
			if (other.portSecured != null)
				return false;
		}
		else if (!portSecured.equals(other.portSecured))
			return false;
		if (url == null)
		{
			if (other.url != null)
				return false;
		}
		else if (!url.equals(other.url))
			return false;
		if (usage == null)
		{
			if (other.usage != null)
				return false;
		}
		else if (!usage.equals(other.usage))
			return false;
		return true;
	}
	
	/**
	 * <p>getHL7V3ResponderConfiguration.</p>
	 *
	 * @param hl7v3conf a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration} object.
	 * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration} object.
	 */
	public static HL7V3ResponderConfiguration getHL7V3ResponderConfiguration(HL7V3ResponderConfiguration hl7v3conf){
        EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
        Query query=entityManager.createQuery("SELECT hl7v3conf FROM HL7V3ResponderConfiguration hl7v3conf WHERE hl7v3conf.configuration=:configuration");
        query.setParameter("configuration",hl7v3conf.getConfiguration());
        List<HL7V3ResponderConfiguration> result=query.getResultList();
        if (result != null){
            for (HL7V3ResponderConfiguration ssc: result){
                if (ssc.equals(hl7v3conf)){
                    return ssc;
                }
            }
        }
        return null;
    }

    /**
     * <p>mergeHL7V3ResponderConfiguration.</p>
     *
     * @param inHL7V3ResponderConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration} object.
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration} object.
     */
    public static HL7V3ResponderConfiguration mergeHL7V3ResponderConfiguration(HL7V3ResponderConfiguration inHL7V3ResponderConfiguration) {
        if(inHL7V3ResponderConfiguration!=null){
            EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
            Configuration conf=Configuration.mergeConfiguration(inHL7V3ResponderConfiguration.getConfiguration());
            inHL7V3ResponderConfiguration.setConfiguration(conf);
            HL7V3ResponderConfiguration hl7v3conf = HL7V3ResponderConfiguration.getHL7V3ResponderConfiguration(inHL7V3ResponderConfiguration);
            if (hl7v3conf != null) return hl7v3conf;
            inHL7V3ResponderConfiguration=entityManager.merge(inHL7V3ResponderConfiguration);
            return inHL7V3ResponderConfiguration;
        }
        return null;
    }
    
    /**
     * <p>createWSUrl.</p>
     *
     * @param wsConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration} object.
     * @return a {@link java.lang.String} object.
     */
    public static String createWSUrl(HL7V3ResponderConfiguration wsConfiguration){
        String res = new String();
        StringBuffer sb = new StringBuffer() ;

        if (wsConfiguration != null){
            if (wsConfiguration.getConfiguration() != null){
                
                if (wsConfiguration.getConfiguration().getIsSecured() != null){
                    sb.append(wsConfiguration.getConfiguration().getIsSecured()? "https://"  : "http://"  );
                }
                else{
                    sb.append("http://");
                }
                if (wsConfiguration.getConfiguration().getHost() != null){
                    if (wsConfiguration.getConfiguration().getHost().getIp() != null){
                        sb.append(wsConfiguration.getConfiguration().getHost().getIp());
                    }
                }
                /*
                if (networkConf != null){
                    if (networkConf.getDomainName() != null){
                        sb.append("." + networkConf.getDomainName());
                    }
                    else{
                        sb.append("");
                    }
                }
                */
                sb.append(':');
                if(wsConfiguration.getConfiguration().getIsSecured() != null){
                    if (wsConfiguration.getConfiguration().getIsSecured()){
                        if (wsConfiguration.getPortSecured() != null){
                            sb.append(wsConfiguration.getPortSecured());
                        }
                    }
                    else{
                        if (wsConfiguration.getPort() != null){
                            sb.append(wsConfiguration.getPort());
                        }
                    }
                }
                else{
                    if (wsConfiguration.getPort() != null){
                        sb.append(wsConfiguration.getPort());
                    }
                }
                sb.append('/');
                if (wsConfiguration.getUrl() != null){
                    sb.append(wsConfiguration.getUrl());
                }
                res = sb.toString();
            }
        }
        return res;
    }

    /**
     * <p>getHL7V3ResponderConfigurationFiltered.</p>
     *
     * @param inSystem a {@link net.ihe.gazelle.simulator.common.model.System} object.
     * @param ti a {@link net.ihe.gazelle.simulator.common.model.TestInstance} object.
     * @param tsi a {@link net.ihe.gazelle.simulator.common.model.TestStepsInstance} object.
     * @param em a {@link javax.persistence.EntityManager} object.
     * @return a {@link java.util.List} object.
     */
    public static List<HL7V3ResponderConfiguration> getHL7V3ResponderConfigurationFiltered(net.ihe.gazelle.simulator.common.model.System inSystem, 
            TestInstance ti, TestStepsInstance tsi ,EntityManager em){
        
        HQLQueryBuilder<HL7V3ResponderConfiguration> hbuild = new HQLQueryBuilder<HL7V3ResponderConfiguration>(em, HL7V3ResponderConfiguration.class);
        if (inSystem != null) hbuild.addEq("system", inSystem);
        if (tsi != null) hbuild.addEq("testStepsInstances", tsi);
        if (ti != null) hbuild.addEq("testStepsInstances.testInstance", ti);

       return hbuild.getList();
    }





}

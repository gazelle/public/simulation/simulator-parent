package net.ihe.gazelle.simulator.common.action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.poi.ss.formula.functions.T;
import org.richfaces.model.TreeNode;

import com.google.common.collect.Iterators;

/**
 * <p>GazelleTypedTreeNode class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class GazelleTypedTreeNode<T> implements TreeNode, Serializable {
	 
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 2279390728623928406L;
	private T data;
    private LinkedHashMap<Object, GazelleTypedTreeNode<T>> children = new LinkedHashMap<Object, GazelleTypedTreeNode<T>>();
    private List<Object> keys = new ArrayList<Object>();
 
    /**
     * <p>Constructor for GazelleTypedTreeNode.</p>
     */
    public GazelleTypedTreeNode() {
    }
 
    /**
     * <p>Constructor for GazelleTypedTreeNode.</p>
     *
     * @param data a {@link org.apache.poi.ss.formula.functions.T} object.
     */
    public GazelleTypedTreeNode(T data) {
        this.data = data;
    }
 
    /** {@inheritDoc} */
    @Override
    public GazelleTypedTreeNode<T> getChild(Object key) {
        return children.get(key);
    }
 
    /** {@inheritDoc} */
    @Override
    public int indexOf(Object key) {
        return keys.indexOf(key);
    }
 
    /** {@inheritDoc} */
    @Override
    public Iterator<Object> getChildrenKeysIterator() {
        if (isLeaf()) {
            return Iterators.emptyIterator();
        }
        return Iterators.unmodifiableIterator(keys.iterator());
    }
 
    /** {@inheritDoc} */
    @Override
    public boolean isLeaf() {
        return children.isEmpty();
    }
 
    /** {@inheritDoc} */
    @Override
    public void addChild(Object key, TreeNode child) {
        addChild(-1, key, child);
    }
 
    /** {@inheritDoc} */
    @Override
    public void insertChild(int idx, Object key, TreeNode child) {
        addChild(idx, key, child);
    }
 
    /** {@inheritDoc} */
    @Override
    public void removeChild(Object key) {
        children.remove(key);
        keys.remove(key);
    }
 
    ///////////////////////////
    // Non Interface Methods //
    ///////////////////////////
    /**
     * <p>Getter for the field <code>data</code>.</p>
     *
     * @return a {@link org.apache.poi.ss.formula.functions.T} object.
     */
    public T getData() {
        return data;
    }
 
    /**
     * <p>Setter for the field <code>data</code>.</p>
     *
     * @param data a {@link org.apache.poi.ss.formula.functions.T} object.
     */
    public void setData(T data) {
        this.data = data;
    }
 
    /**
     * <p>Getter for the field <code>keys</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Object> getKeys() {
        return Collections.unmodifiableList(keys);
    }
 
    @SuppressWarnings("unchecked")
    private void addChild(int idx, Object key, TreeNode child) {
        if (child instanceof GazelleTypedTreeNode) {
            if (idx != -1) {
                keys.add(idx, key);
            } else {
                keys.add(key);
            }
            children.put(key, (GazelleTypedTreeNode<T>) child);
        } else {
            throw new ClassCastException("The child is not a TreeNodeImpl<T>class object.");
        }
    }
 
    /**
     * <p>Getter for the field <code>children</code>.</p>
     *
     * @return a {@link java.util.LinkedHashMap} object.
     */
    public LinkedHashMap<Object, GazelleTypedTreeNode<T>> getChildren() {
        return children;
    }
}

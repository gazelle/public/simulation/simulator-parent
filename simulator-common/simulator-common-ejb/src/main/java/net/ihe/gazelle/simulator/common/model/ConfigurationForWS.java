package net.ihe.gazelle.simulator.common.model;

import net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration;
import net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCUConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2InitiatorConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3InitiatorConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration;
import net.ihe.gazelle.simulator.common.configuration.model.SyslogConfiguration;
import net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration;

/**
 * <p>ConfigurationForWS class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class ConfigurationForWS {
    
    /**
     * <p>Constructor for ConfigurationForWS.</p>
     */
    public ConfigurationForWS(){}
    
    private HL7V2InitiatorConfiguration hL7V2InitiatorConfiguration;
    
    private HL7V2ResponderConfiguration hL7V2ResponderConfiguration;
    
    private HL7V3InitiatorConfiguration HL7V3InitiatorConfiguration;
    
    private HL7V3ResponderConfiguration hL7V3ResponderConfiguration;
    
    private DicomSCPConfiguration dicomSCPConfiguration;
    
    private DicomSCUConfiguration dicomSCUConfiguration;
    
    private SyslogConfiguration syslogConfiguration;
    
    private WebServiceConfiguration webServiceConfiguration;

    /**
     * <p>Constructor for ConfigurationForWS.</p>
     *
     * @param hL7V2InitiatorConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2InitiatorConfiguration} object.
     * @param hL7V2ResponderConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration} object.
     * @param hL7V3InitiatorConfiguration a net$ihe$gazelle$simulator$common$configuration$HL7$model$HL7V3InitiatorConfiguration object.
     * @param hL7V3ResponderConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration} object.
     * @param dicomSCPConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration} object.
     * @param dicomSCUConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCUConfiguration} object.
     * @param syslogConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.SyslogConfiguration} object.
     * @param webServiceConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration} object.
     */
    public ConfigurationForWS(
            HL7V2InitiatorConfiguration hL7V2InitiatorConfiguration,
            HL7V2ResponderConfiguration hL7V2ResponderConfiguration,
            net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3InitiatorConfiguration hL7V3InitiatorConfiguration,
            HL7V3ResponderConfiguration hL7V3ResponderConfiguration,
            DicomSCPConfiguration dicomSCPConfiguration,
            DicomSCUConfiguration dicomSCUConfiguration,
            SyslogConfiguration syslogConfiguration,
            WebServiceConfiguration webServiceConfiguration) {
        super();
        this.hL7V2InitiatorConfiguration = hL7V2InitiatorConfiguration;
        this.hL7V2ResponderConfiguration = hL7V2ResponderConfiguration;
        HL7V3InitiatorConfiguration = hL7V3InitiatorConfiguration;
        this.hL7V3ResponderConfiguration = hL7V3ResponderConfiguration;
        this.dicomSCPConfiguration = dicomSCPConfiguration;
        this.dicomSCUConfiguration = dicomSCUConfiguration;
        this.syslogConfiguration = syslogConfiguration;
        this.webServiceConfiguration = webServiceConfiguration;
    }

    /**
     * <p>Getter for the field <code>hL7V2InitiatorConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2InitiatorConfiguration} object.
     */
    public HL7V2InitiatorConfiguration gethL7V2InitiatorConfiguration() {
        return hL7V2InitiatorConfiguration;
    }

    /**
     * <p>Setter for the field <code>hL7V2InitiatorConfiguration</code>.</p>
     *
     * @param hL7V2InitiatorConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2InitiatorConfiguration} object.
     */
    public void sethL7V2InitiatorConfiguration(
            HL7V2InitiatorConfiguration hL7V2InitiatorConfiguration) {
        this.hL7V2InitiatorConfiguration = hL7V2InitiatorConfiguration;
    }

    /**
     * <p>Getter for the field <code>hL7V2ResponderConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration} object.
     */
    public HL7V2ResponderConfiguration gethL7V2ResponderConfiguration() {
        return hL7V2ResponderConfiguration;
    }

    /**
     * <p>Setter for the field <code>hL7V2ResponderConfiguration</code>.</p>
     *
     * @param hL7V2ResponderConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration} object.
     */
    public void sethL7V2ResponderConfiguration(
            HL7V2ResponderConfiguration hL7V2ResponderConfiguration) {
        this.hL7V2ResponderConfiguration = hL7V2ResponderConfiguration;
    }

    /**
     * <p>getHL7V3InitiatorConfiguration.</p>
     *
     * @return a net$ihe$gazelle$simulator$common$configuration$HL7$model$HL7V3InitiatorConfiguration object.
     */
    public HL7V3InitiatorConfiguration getHL7V3InitiatorConfiguration() {
        return HL7V3InitiatorConfiguration;
    }

    /**
     * <p>setHL7V3InitiatorConfiguration.</p>
     *
     * @param hL7V3InitiatorConfiguration a net$ihe$gazelle$simulator$common$configuration$HL7$model$HL7V3InitiatorConfiguration object.
     */
    public void setHL7V3InitiatorConfiguration(
            HL7V3InitiatorConfiguration hL7V3InitiatorConfiguration) {
        HL7V3InitiatorConfiguration = hL7V3InitiatorConfiguration;
    }

    /**
     * <p>Getter for the field <code>hL7V3ResponderConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration} object.
     */
    public HL7V3ResponderConfiguration gethL7V3ResponderConfiguration() {
        return hL7V3ResponderConfiguration;
    }

    /**
     * <p>Setter for the field <code>hL7V3ResponderConfiguration</code>.</p>
     *
     * @param hL7V3ResponderConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration} object.
     */
    public void sethL7V3ResponderConfiguration(
            HL7V3ResponderConfiguration hL7V3ResponderConfiguration) {
        this.hL7V3ResponderConfiguration = hL7V3ResponderConfiguration;
    }

    /**
     * <p>Getter for the field <code>dicomSCPConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration} object.
     */
    public DicomSCPConfiguration getDicomSCPConfiguration() {
        return dicomSCPConfiguration;
    }

    /**
     * <p>Setter for the field <code>dicomSCPConfiguration</code>.</p>
     *
     * @param dicomSCPConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration} object.
     */
    public void setDicomSCPConfiguration(DicomSCPConfiguration dicomSCPConfiguration) {
        this.dicomSCPConfiguration = dicomSCPConfiguration;
    }

    /**
     * <p>Getter for the field <code>dicomSCUConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCUConfiguration} object.
     */
    public DicomSCUConfiguration getDicomSCUConfiguration() {
        return dicomSCUConfiguration;
    }

    /**
     * <p>Setter for the field <code>dicomSCUConfiguration</code>.</p>
     *
     * @param dicomSCUConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCUConfiguration} object.
     */
    public void setDicomSCUConfiguration(DicomSCUConfiguration dicomSCUConfiguration) {
        this.dicomSCUConfiguration = dicomSCUConfiguration;
    }

    /**
     * <p>Getter for the field <code>syslogConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.SyslogConfiguration} object.
     */
    public SyslogConfiguration getSyslogConfiguration() {
        return syslogConfiguration;
    }

    /**
     * <p>Setter for the field <code>syslogConfiguration</code>.</p>
     *
     * @param syslogConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.SyslogConfiguration} object.
     */
    public void setSyslogConfiguration(SyslogConfiguration syslogConfiguration) {
        this.syslogConfiguration = syslogConfiguration;
    }

    /**
     * <p>Getter for the field <code>webServiceConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration} object.
     */
    public WebServiceConfiguration getWebServiceConfiguration() {
        return webServiceConfiguration;
    }

    /**
     * <p>Setter for the field <code>webServiceConfiguration</code>.</p>
     *
     * @param webServiceConfiguration a {@link net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration} object.
     */
    public void setWebServiceConfiguration(
            WebServiceConfiguration webServiceConfiguration) {
        this.webServiceConfiguration = webServiceConfiguration;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((HL7V3InitiatorConfiguration == null) ? 0
                        : HL7V3InitiatorConfiguration.hashCode());
        result = prime
                * result
                + ((dicomSCPConfiguration == null) ? 0 : dicomSCPConfiguration
                        .hashCode());
        result = prime
                * result
                + ((dicomSCUConfiguration == null) ? 0 : dicomSCUConfiguration
                        .hashCode());
        result = prime
                * result
                + ((hL7V2InitiatorConfiguration == null) ? 0
                        : hL7V2InitiatorConfiguration.hashCode());
        result = prime
                * result
                + ((hL7V2ResponderConfiguration == null) ? 0
                        : hL7V2ResponderConfiguration.hashCode());
        result = prime
                * result
                + ((hL7V3ResponderConfiguration == null) ? 0
                        : hL7V3ResponderConfiguration.hashCode());
        result = prime
                * result
                + ((syslogConfiguration == null) ? 0 : syslogConfiguration
                        .hashCode());
        result = prime
                * result
                + ((webServiceConfiguration == null) ? 0
                        : webServiceConfiguration.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ConfigurationForWS other = (ConfigurationForWS) obj;
        if (HL7V3InitiatorConfiguration == null) {
            if (other.HL7V3InitiatorConfiguration != null)
                return false;
        } else if (!HL7V3InitiatorConfiguration
                .equals(other.HL7V3InitiatorConfiguration))
            return false;
        if (dicomSCPConfiguration == null) {
            if (other.dicomSCPConfiguration != null)
                return false;
        } else if (!dicomSCPConfiguration.equals(other.dicomSCPConfiguration))
            return false;
        if (dicomSCUConfiguration == null) {
            if (other.dicomSCUConfiguration != null)
                return false;
        } else if (!dicomSCUConfiguration.equals(other.dicomSCUConfiguration))
            return false;
        if (hL7V2InitiatorConfiguration == null) {
            if (other.hL7V2InitiatorConfiguration != null)
                return false;
        } else if (!hL7V2InitiatorConfiguration
                .equals(other.hL7V2InitiatorConfiguration))
            return false;
        if (hL7V2ResponderConfiguration == null) {
            if (other.hL7V2ResponderConfiguration != null)
                return false;
        } else if (!hL7V2ResponderConfiguration
                .equals(other.hL7V2ResponderConfiguration))
            return false;
        if (hL7V3ResponderConfiguration == null) {
            if (other.hL7V3ResponderConfiguration != null)
                return false;
        } else if (!hL7V3ResponderConfiguration
                .equals(other.hL7V3ResponderConfiguration))
            return false;
        if (syslogConfiguration == null) {
            if (other.syslogConfiguration != null)
                return false;
        } else if (!syslogConfiguration.equals(other.syslogConfiguration))
            return false;
        if (webServiceConfiguration == null) {
            if (other.webServiceConfiguration != null)
                return false;
        } else if (!webServiceConfiguration
                .equals(other.webServiceConfiguration))
            return false;
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "ConfigurationForWS [hL7V2InitiatorConfiguration="
                + hL7V2InitiatorConfiguration
                + ", hL7V2ResponderConfiguration="
                + hL7V2ResponderConfiguration
                + ", HL7V3InitiatorConfiguration="
                + HL7V3InitiatorConfiguration
                + ", hL7V3ResponderConfiguration="
                + hL7V3ResponderConfiguration + ", dicomSCPConfiguration="
                + dicomSCPConfiguration + ", dicomSCUConfiguration="
                + dicomSCUConfiguration + ", syslogConfiguration="
                + syslogConfiguration + ", webServiceConfiguration="
                + webServiceConfiguration + "]";
    }
    
}

package net.ihe.gazelle.simulator.common.action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.EntityManager;

import net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration;
import net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCUConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2InitiatorConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3InitiatorConfiguration;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration;
import net.ihe.gazelle.simulator.common.configuration.model.SyslogConfiguration;
import net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration;
import net.ihe.gazelle.simulator.common.model.TestInstance;
import net.ihe.gazelle.simulator.common.model.TestInstanceParticipants;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile;
import net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.JndiName;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;
import org.slf4j.LoggerFactory;


/**
 * Seam manager bean for testInstances page.
 *
 * @author abderrazek boufahja / INRIA Rennes IHE development Project
 * @version     1.0 - 2010-05-27
 */
@Stateful
@Name("testInstanceManager")
@Scope(ScopeType.SESSION)
public class TestInstanceManager implements TestInstanceManagerLocal, Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(TestInstanceManager.class);

    private TestInstance selectedTestInstance;
	
	private TestInstanceParticipants selectedTestInstanceParticipants;
	
	private IntegrationProfile selectedIntegrationProfile;
    
    private Actor selectedActor;
    
    private IntegrationProfileOption selectedIntegrationProfileOption;
    
    private HL7V2InitiatorConfiguration selectedHL7V2InitiatorConfiguration;
    
    private HL7V2ResponderConfiguration selectedHL7V2ResponderConfiguration;
    
    private HL7V3InitiatorConfiguration selectedHL7V3InitiatorConfiguration;
    
    private HL7V3ResponderConfiguration selectedHL7V3ResponderConfiguration;
    
    private DicomSCPConfiguration selectedDicomSCPConfiguration;
    
    private DicomSCUConfiguration selectedDicomSCUConfiguration;
    
    private WebServiceConfiguration selectedWebServiceConfiguration;
    
    private boolean showSystemProperties = true;
    
    private boolean showHL7V2InitiatorProperties = false;
    private boolean showHL7V2ResponderProperties = false;
    private boolean showHL7V3InitiatorProperties = false;
    private boolean showHL7V3ResponderProperties = false;
    private boolean showDicomSCPProperties = false;
    private boolean showDicomSCUProperties = false;
    private boolean showWebServiceProperties = false;
    
    /**
     * <p>isShowSystemProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowSystemProperties() {
        return showSystemProperties;
    }

    /** {@inheritDoc} */
    public void setShowSystemProperties(boolean showSystemProperties) {
        this.showSystemProperties = showSystemProperties;
    }

    /**
     * <p>isShowHL7V2InitiatorProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowHL7V2InitiatorProperties() {
        return showHL7V2InitiatorProperties;
    }

    /** {@inheritDoc} */
    public void setShowHL7V2InitiatorProperties(boolean showHL7V2InitiatorProperties) {
        this.showHL7V2InitiatorProperties = showHL7V2InitiatorProperties;
    }

    /**
     * <p>isShowHL7V2ResponderProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowHL7V2ResponderProperties() {
        return showHL7V2ResponderProperties;
    }

    /** {@inheritDoc} */
    public void setShowHL7V2ResponderProperties(boolean showHL7V2ResponderProperties) {
        this.showHL7V2ResponderProperties = showHL7V2ResponderProperties;
    }

    /**
     * <p>isShowHL7V3InitiatorProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowHL7V3InitiatorProperties() {
        return showHL7V3InitiatorProperties;
    }

    /** {@inheritDoc} */
    public void setShowHL7V3InitiatorProperties(boolean showHL7V3InitiatorProperties) {
        this.showHL7V3InitiatorProperties = showHL7V3InitiatorProperties;
    }

    /**
     * <p>isShowHL7V3ResponderProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowHL7V3ResponderProperties() {
        return showHL7V3ResponderProperties;
    }

    /** {@inheritDoc} */
    public void setShowHL7V3ResponderProperties(boolean showHL7V3ResponderProperties) {
        this.showHL7V3ResponderProperties = showHL7V3ResponderProperties;
    }

    /**
     * <p>isShowDicomSCPProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowDicomSCPProperties() {
        return showDicomSCPProperties;
    }

    /** {@inheritDoc} */
    public void setShowDicomSCPProperties(boolean showDicomSCPProperties) {
        this.showDicomSCPProperties = showDicomSCPProperties;
    }

    /**
     * <p>isShowDicomSCUProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowDicomSCUProperties() {
        return showDicomSCUProperties;
    }

    /** {@inheritDoc} */
    public void setShowDicomSCUProperties(boolean showDicomSCUProperties) {
        this.showDicomSCUProperties = showDicomSCUProperties;
    }

    /**
     * <p>isShowWebServiceProperties.</p>
     *
     * @return a boolean.
     */
    public boolean isShowWebServiceProperties() {
        return showWebServiceProperties;
    }

    /** {@inheritDoc} */
    public void setShowWebServiceProperties(boolean showWebServiceProperties) {
        this.showWebServiceProperties = showWebServiceProperties;
    }


    /**
     * <p>Getter for the field <code>selectedHL7V2InitiatorConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2InitiatorConfiguration} object.
     */
    public HL7V2InitiatorConfiguration getSelectedHL7V2InitiatorConfiguration() {
        return selectedHL7V2InitiatorConfiguration;
    }

    /** {@inheritDoc} */
    public void setSelectedHL7V2InitiatorConfiguration(
            HL7V2InitiatorConfiguration selectedHL7V2InitiatorConfiguration) {
        this.selectedHL7V2InitiatorConfiguration = selectedHL7V2InitiatorConfiguration;
    }

    /**
     * <p>Getter for the field <code>selectedHL7V2ResponderConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V2ResponderConfiguration} object.
     */
    public HL7V2ResponderConfiguration getSelectedHL7V2ResponderConfiguration() {
        return selectedHL7V2ResponderConfiguration;
    }

    /** {@inheritDoc} */
    public void setSelectedHL7V2ResponderConfiguration(
            HL7V2ResponderConfiguration selectedHL7V2ResponderConfiguration) {
        this.selectedHL7V2ResponderConfiguration = selectedHL7V2ResponderConfiguration;
    }

    /**
     * <p>Getter for the field <code>selectedHL7V3InitiatorConfiguration</code>.</p>
     *
     * @return a net$ihe$gazelle$simulator$common$configuration$HL7$model$HL7V3InitiatorConfiguration object.
     */
    public HL7V3InitiatorConfiguration getSelectedHL7V3InitiatorConfiguration() {
        return selectedHL7V3InitiatorConfiguration;
    }

    /**
     * <p>Setter for the field <code>selectedHL7V3InitiatorConfiguration</code>.</p>
     *
     * @param selectedHL7V3InitiatorConfiguration a net$ihe$gazelle$simulator$common$configuration$HL7$model$HL7V3InitiatorConfiguration object.
     */
    public void setSelectedHL7V3InitiatorConfiguration(
            HL7V3InitiatorConfiguration selectedHL7V3InitiatorConfiguration) {
        this.selectedHL7V3InitiatorConfiguration = selectedHL7V3InitiatorConfiguration;
    }

    /**
     * <p>Getter for the field <code>selectedHL7V3ResponderConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration} object.
     */
    public HL7V3ResponderConfiguration getSelectedHL7V3ResponderConfiguration() {
        return selectedHL7V3ResponderConfiguration;
    }

    /** {@inheritDoc} */
    public void setSelectedHL7V3ResponderConfiguration(
            HL7V3ResponderConfiguration selectedHL7V3ResponderConfiguration) {
        this.selectedHL7V3ResponderConfiguration = selectedHL7V3ResponderConfiguration;
    }

    /**
     * <p>Getter for the field <code>selectedDicomSCPConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCPConfiguration} object.
     */
    public DicomSCPConfiguration getSelectedDicomSCPConfiguration() {
        return selectedDicomSCPConfiguration;
    }

    /** {@inheritDoc} */
    public void setSelectedDicomSCPConfiguration(
            DicomSCPConfiguration selectedDicomSCPConfiguration) {
        this.selectedDicomSCPConfiguration = selectedDicomSCPConfiguration;
    }

    /**
     * <p>Getter for the field <code>selectedDicomSCUConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.DICOM.model.DicomSCUConfiguration} object.
     */
    public DicomSCUConfiguration getSelectedDicomSCUConfiguration() {
        return selectedDicomSCUConfiguration;
    }

    /** {@inheritDoc} */
    public void setSelectedDicomSCUConfiguration(
            DicomSCUConfiguration selectedDicomSCUConfiguration) {
        this.selectedDicomSCUConfiguration = selectedDicomSCUConfiguration;
    }

    /**
     * <p>Getter for the field <code>selectedWebServiceConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.configuration.model.WebServiceConfiguration} object.
     */
    public WebServiceConfiguration getSelectedWebServiceConfiguration() {
        return selectedWebServiceConfiguration;
    }

    /** {@inheritDoc} */
    public void setSelectedWebServiceConfiguration(
            WebServiceConfiguration selectedWebServiceConfiguration) {
        this.selectedWebServiceConfiguration = selectedWebServiceConfiguration;
    }

    
	/** {@inheritDoc} */
	public void setSelectedIntegrationProfileOption(
            IntegrationProfileOption selectedIntegrationProfileOption) {
        this.selectedIntegrationProfileOption = selectedIntegrationProfileOption;
    }

    /**
     * <p>Getter for the field <code>selectedIntegrationProfileOption</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfileOption} object.
     */
    public IntegrationProfileOption getSelectedIntegrationProfileOption() {
        return selectedIntegrationProfileOption;
    }

    /** {@inheritDoc} */
    public void setSelectedActor(Actor selectedActor) {
        this.selectedActor = selectedActor;
    }

    /**
     * <p>Getter for the field <code>selectedActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getSelectedActor() {
        return selectedActor;
    }

    /** {@inheritDoc} */
    public void setSelectedIntegrationProfile(IntegrationProfile selectedIntegrationProfile) {
        this.selectedIntegrationProfile = selectedIntegrationProfile;
    }

    /**
     * <p>Getter for the field <code>selectedIntegrationProfile</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile} object.
     */
    public IntegrationProfile getSelectedIntegrationProfile() {
        return selectedIntegrationProfile;
    }

    /** {@inheritDoc} */
    public void setSelectedTestInstanceParticipants(TestInstanceParticipants selectedTestInstanceParticipants){
		this.selectedTestInstanceParticipants = selectedTestInstanceParticipants;
	}

	/**
	 * <p>Getter for the field <code>selectedTestInstanceParticipants</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.model.TestInstanceParticipants} object.
	 */
	public TestInstanceParticipants getSelectedTestInstanceParticipants(){
		return selectedTestInstanceParticipants;
	}

	/** {@inheritDoc} */
	public void setSelectedTestInstance(TestInstance selectedTestInstance){
		this.selectedTestInstance = selectedTestInstance;
	}

	/**
	 * <p>Getter for the field <code>selectedTestInstance</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.model.TestInstance} object.
	 */
	public TestInstance getSelectedTestInstance(){
		return selectedTestInstance;
	}

	/**
	 * <p>getListOfTestInstances.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<TestInstance> getListOfTestInstances(){
		return TestInstance.getTestInstanceFiltred(selectedActor, selectedIntegrationProfile, selectedIntegrationProfileOption);
	}
	
	/**
	 * <p>getListOfPartnerTree.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.action.GazelleTreeNodeImpl} object.
	 */
	public GazelleTreeNodeImpl<Object> getListOfPartnerTree(){
	    EntityManager em=(EntityManager)Component.getInstance("entityManager");
		GazelleTreeNodeImpl<Object> rootNode = new GazelleTreeNodeImpl<Object>();
		List<TestInstanceParticipants> listTIP= this.getListOfTIPOfCurrentTestInstance();
		if (listTIP != null){
			int i = 0;
			for(TestInstanceParticipants testInstanceParticipants:listTIP){
				GazelleTreeNodeImpl<Object> OTypeTreeNode=new GazelleTreeNodeImpl<Object>();
				OTypeTreeNode.setData(testInstanceParticipants);
				
				int j = 0;
				//List<DicomSCPConfiguration> ldscp = testInstanceParticipants.getSystem().getDicomSCPConfigurations();
				List<DicomSCPConfiguration> ldscp = DicomSCPConfiguration.getDicomSCPConfigurationFiltered(
				        testInstanceParticipants.getSystem(), selectedTestInstance, null, em); 
				for (DicomSCPConfiguration dicomSCPConfiguration : ldscp) {
				    GazelleTreeNodeImpl<Object> ACTreeNode=new GazelleTreeNodeImpl<Object>();
				    ACTreeNode.setData(dicomSCPConfiguration);
				    OTypeTreeNode.addChild(j, ACTreeNode);
	                j++;
                }
				
				//List<DicomSCUConfiguration> ldscu = testInstanceParticipants.getSystem().getDicomSCUConfigurations();
				List<DicomSCUConfiguration> ldscu = DicomSCUConfiguration.getDicomSCUConfigurationFiltered(
				        testInstanceParticipants.getSystem(), selectedTestInstance, null, em);
                for (DicomSCUConfiguration dicomSCUConfiguration : ldscu) {
                    GazelleTreeNodeImpl<Object> ACTreeNode=new GazelleTreeNodeImpl<Object>();
                    ACTreeNode.setData(dicomSCUConfiguration);
                    OTypeTreeNode.addChild(j, ACTreeNode);
                    j++;
                }
                
                //List<HL7V2InitiatorConfiguration> lhl7in = testInstanceParticipants.getSystem().gethL7v2InitiatorConfigurations();
                List<HL7V2InitiatorConfiguration> lhl7in = HL7V2InitiatorConfiguration.getHL7V2InitiatorConfigurationFiltered(
                        testInstanceParticipants.getSystem(), selectedTestInstance, null, em);
                for (HL7V2InitiatorConfiguration hl7v2InitiatorConfiguration : lhl7in) {
                    GazelleTreeNodeImpl<Object> ACTreeNode=new GazelleTreeNodeImpl<Object>();
                    ACTreeNode.setData(hl7v2InitiatorConfiguration);
                    OTypeTreeNode.addChild(j, ACTreeNode);
                    j++;
                }
                
                //List<HL7V2ResponderConfiguration> lhl7resp = testInstanceParticipants.getSystem().gethL7V2ResponderConfigurations();
                List<HL7V2ResponderConfiguration> lhl7resp = HL7V2ResponderConfiguration.getHL7V2ResponderConfigurationFiltered(
                        testInstanceParticipants.getSystem(), selectedTestInstance, null, em);
                for (HL7V2ResponderConfiguration hl7v2ResponderConfiguration : lhl7resp) {
                    GazelleTreeNodeImpl<Object> ACTreeNode=new GazelleTreeNodeImpl<Object>();
                    ACTreeNode.setData(hl7v2ResponderConfiguration);
                    OTypeTreeNode.addChild(j, ACTreeNode);
                    j++;
                }
                
                //List<HL7V3InitiatorConfiguration> lhl7v3in = testInstanceParticipants.getSystem().gethL7V3InitiatorConfigurations();
                List<HL7V3InitiatorConfiguration> lhl7v3in = HL7V3InitiatorConfiguration.getHL7V3InitiatorConfigurationFiltered(
                        testInstanceParticipants.getSystem(), selectedTestInstance, null, em);
                for (HL7V3InitiatorConfiguration hl7v3InitiatorConfiguration : lhl7v3in) {
                    GazelleTreeNodeImpl<Object> ACTreeNode=new GazelleTreeNodeImpl<Object>();
                    ACTreeNode.setData(hl7v3InitiatorConfiguration);
                    OTypeTreeNode.addChild(j, ACTreeNode);
                    j++;
                }
                
                //List<HL7V3ResponderConfiguration> lhl7v3resp = testInstanceParticipants.getSystem().gethL7V3ResponderConfigurations();
                List<HL7V3ResponderConfiguration> lhl7v3resp = HL7V3ResponderConfiguration.getHL7V3ResponderConfigurationFiltered(
                        testInstanceParticipants.getSystem(), selectedTestInstance, null, em);
                for (HL7V3ResponderConfiguration hl7v3ResponderConfiguration : lhl7v3resp) {
                    GazelleTreeNodeImpl<Object> ACTreeNode=new GazelleTreeNodeImpl<Object>();
                    ACTreeNode.setData(hl7v3ResponderConfiguration);
                    OTypeTreeNode.addChild(j, ACTreeNode);
                    j++;
                }
                
                //List<WebServiceConfiguration> lws = testInstanceParticipants.getSystem().getWebServiceConfigurations();
                List<WebServiceConfiguration> lws = WebServiceConfiguration.getWebServiceConfigurationFiltered(
                        testInstanceParticipants.getSystem(), selectedTestInstance, null, em);
                for (WebServiceConfiguration webServiceConfiguration : lws) {
                	GazelleTreeNodeImpl<Object> ACTreeNode=new GazelleTreeNodeImpl<Object>();
                    ACTreeNode.setData(webServiceConfiguration);
                    OTypeTreeNode.addChild(j, ACTreeNode);
                    j++;
                }
                
                //List<SyslogConfiguration> lsyslog = testInstanceParticipants.getSystem().getSyslogConfigurations();
                List<SyslogConfiguration> lsyslog = SyslogConfiguration.getSyslogConfigurationFiltered(
                        testInstanceParticipants.getSystem(), selectedTestInstance, null, em);
                for (SyslogConfiguration syslogConfiguration : lsyslog) {
                    GazelleTreeNodeImpl<Object> ACTreeNode=new GazelleTreeNodeImpl<Object>();
                    ACTreeNode.setData(syslogConfiguration);
                    OTypeTreeNode.addChild(j, ACTreeNode);
                    j++;
                }
                
				rootNode.addChild(i, OTypeTreeNode);
				i++;
			}
		}
		return rootNode;
	}
	
	/**
	 * Gets partners for currently selected test instance
	 * @return <code>List</code> of
	 * {@link net.ihe.gazelle.simulator.common.model.TestInstanceParticipants
	 * TestInstanceParticipants} objects
	 */
	private List<TestInstanceParticipants> getListOfTIPOfCurrentTestInstance(){
		List<TestInstanceParticipants> res = new ArrayList<TestInstanceParticipants>();
		if (this.selectedTestInstance != null){
			res = TestInstanceParticipants.getTestInstanceParticipantsFiltered(selectedTestInstance);
		}
		return res;
	}
	
	/**
	 * <p>initializeVariables.</p>
	 */
	public void initializeVariables(){
		this.selectedTestInstanceParticipants = null;
	}
	
	/** {@inheritDoc} */
	public void optionChanged(ValueChangeEvent event){
		initializeVariables();
	}
	
	/**
	 * <p>getPossibleIntegrationProfiles.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<IntegrationProfile> getPossibleIntegrationProfiles(){
        return IntegrationProfile.listAllIntegrationProfiles();
    }
    
    /**
     * <p>getPossibleActors.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Actor> getPossibleActors(){
        List<Actor> actorL = null;
        if (this.selectedIntegrationProfile != null) {
            List<ActorIntegrationProfileOption> listOfTestParticipants = ActorIntegrationProfileOption.getActorIntegrationProfileOptionFiltered(null, 
                    this.selectedIntegrationProfile, null,null);
            
            HashSet<Actor> setOfActor = new HashSet<Actor>();
            if (listOfTestParticipants == null)
            {
                return null;
            }
            for (ActorIntegrationProfileOption tp : listOfTestParticipants) {
                setOfActor.add(tp.getActorIntegrationProfile().getActor());
            }

            actorL = new ArrayList<Actor>(setOfActor);
            Collections.sort(actorL);

        } 
        return actorL;
    }
    
    /**
     * <p>getPossibleOptions.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<IntegrationProfileOption> getPossibleOptions(){
        List<IntegrationProfileOption> ipOL = null;
        if (this.selectedIntegrationProfile != null && this.selectedActor != null) {
            List<ActorIntegrationProfileOption> listOfTestParticipants = ActorIntegrationProfileOption.getActorIntegrationProfileOptionFiltered(null,
                    this.selectedIntegrationProfile, 
                    null,
                    this.selectedActor);

            HashSet<IntegrationProfileOption> setOfOption = new HashSet<IntegrationProfileOption>();
            if (listOfTestParticipants == null)
                return null;
            for (ActorIntegrationProfileOption tp : listOfTestParticipants) {
                setOfOption.add(tp.getIntegrationProfileOption());
            }

            ipOL = new ArrayList<IntegrationProfileOption>(setOfOption);
            Collections.sort(ipOL);

        } 
        else {
            LOG.info(" actor and ip is null ");
        }
        return ipOL;
    }
    
    /** {@inheritDoc} */
    public void resetSelectionValues(int i){
        if (i == 1){
            selectedIntegrationProfile = null;
            selectedActor = null;
            selectedIntegrationProfileOption = null;
            selectedTestInstance = null;
        }
        if (i == 2){
            selectedActor = null;
            selectedIntegrationProfileOption = null;
            selectedTestInstance = null;
        }
        if (i == 3){
            selectedIntegrationProfileOption = null;
            selectedTestInstance = null;
        }
        if (i == 4){
            selectedTestInstance = null;
        }
    }
    
    /** {@inheritDoc} */
    public void initSelectedDicomSCPConfiguration(DicomSCPConfiguration dsc){
        this.resetSelectedConfigOrSystem();
        this.resetShowProperties();
        this.selectedDicomSCPConfiguration = dsc;
        this.showDicomSCPProperties = true;
    }
    
    /** {@inheritDoc} */
    public void initSelectedDicomSCUConfiguration(DicomSCUConfiguration dsc){
        this.resetSelectedConfigOrSystem();
        this.resetShowProperties();
        this.selectedDicomSCUConfiguration = dsc;
        this.showDicomSCUProperties = true;
    }
    
    /** {@inheritDoc} */
    public void initSelectedHL7V2InitiatorConfiguration(HL7V2InitiatorConfiguration dsc){
        this.resetSelectedConfigOrSystem();
        this.resetShowProperties();
        this.selectedHL7V2InitiatorConfiguration = dsc;
        this.showHL7V2InitiatorProperties = true;
    }
    
    /** {@inheritDoc} */
    public void initSelectedHL7V2ResponderConfiguration(HL7V2ResponderConfiguration dsc){
        this.resetSelectedConfigOrSystem();
        this.resetShowProperties();
        this.selectedHL7V2ResponderConfiguration = dsc;
        this.showHL7V2ResponderProperties = true;
    }
    
    /**
     * <p>initSelectedHL7V3InitiatorConfiguration.</p>
     *
     * @param dsc a net$ihe$gazelle$simulator$common$configuration$HL7$model$HL7V3InitiatorConfiguration object.
     */
    public void initSelectedHL7V3InitiatorConfiguration(HL7V3InitiatorConfiguration dsc){
        this.resetSelectedConfigOrSystem();
        this.resetShowProperties();
        this.selectedHL7V3InitiatorConfiguration = dsc;
        this.showHL7V3InitiatorProperties = true;
    }
    
    /** {@inheritDoc} */
    public void initSelectedHL7V3ResponderConfiguration(HL7V3ResponderConfiguration dsc){
        this.resetSelectedConfigOrSystem();
        this.resetShowProperties();
        this.selectedHL7V3ResponderConfiguration = dsc;
        this.showHL7V3ResponderProperties = true;
    }
    
    /** {@inheritDoc} */
    public void initSelectedWebServiceConfiguration(WebServiceConfiguration dsc){
        this.resetSelectedConfigOrSystem();
        this.resetShowProperties();
        this.selectedWebServiceConfiguration = dsc;
        this.showWebServiceProperties = true;
    }
    
    /** {@inheritDoc} */
    public void initSelectedTestInstanceParticipants(TestInstanceParticipants tip){
        this.resetSelectedConfigOrSystem();
        this.resetShowProperties();
        this.selectedTestInstanceParticipants = tip;
        this.showSystemProperties = true;
    }
    
    private void resetShowProperties(){
        this.showDicomSCPProperties = false;
        this.showDicomSCUProperties = false;
        this.showHL7V2InitiatorProperties = false;
        this.showHL7V2ResponderProperties = false;
        this.showHL7V3InitiatorProperties = false;
        this.showHL7V3ResponderProperties = false;
        this.showSystemProperties = false;
        this.showWebServiceProperties = false;
    }
    
    private void resetSelectedConfigOrSystem(){
        this.selectedTestInstanceParticipants = null;
        this.selectedDicomSCPConfiguration = null;
        this.selectedDicomSCUConfiguration = null;
        this.selectedHL7V2InitiatorConfiguration = null;
        this.selectedHL7V2ResponderConfiguration = null;
        this.selectedHL7V3InitiatorConfiguration = null;
        this.selectedHL7V3ResponderConfiguration = null;
        this.selectedWebServiceConfiguration = null;
    }
    
    /**
     * <p>initTestInstanceView.</p>
     */
    public void initTestInstanceView(){
        this.resetShowProperties();
    }
    

	
	/**
	 * <p>destroy.</p>
	 */
	@Destroy
	@Remove
	public void destroy()
	{
		LOG.info("destroy");
	}

}

package net.ihe.gazelle.simulator.samlassertion.epsos;

import net.ihe.gazelle.simulator.samlassertion.common.AssertionAttributeType;

public enum EpSOSAssertionAttributeType implements AssertionAttributeType {

    /**
     * The XSPA subject.
     */
    XSPASubject("XSPA subject",

            "urn:oasis:names:tc:xacml:1.0:subject:subject-id",

            "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",

            "xs:string"),

    /**
     * The XSPA subject.
     */
    XSPASubjectTRC("XSPA subject",

            "urn:oasis:names:tc:xacml:1.0:resource:resource-id",

            "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",

            "xs:string"),

    /**
     * The XSPA role.
     */
    XSPARole("XSPA role",

            "urn:oasis:names:tc:xacml:2.0:subject:role",

            "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",

            "xs:string"),

    /**
     * The HITSP clinical speciality.
     */
    HITSPClinicalSpeciality("HITSP Clinical Speciality",

            "urn:epsos:names:wp3.4:subject:clinical-speciality",

            "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",

            "xs:string"),

    /**
     * The XSPA organization.
     */
    XSPAOrganization("XSPA Organization",

            "urn:oasis:names:tc:xspa:1.0:subject:organization",

            "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",

            "xs:string"),

    /**
     * The XSPA organization id.
     */
    XSPAOrganizationId("XSPA Organization Id",

            "urn:oasis:names:tc:xspa:1.0:subject:organization-id",

            "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",

            "xs:anyURI"),

    /**
     * The epSOS Healthcare Facility Type.
     */
    epSOSHealthcareFacilityType("epSOS Healthcare Facility Type",

            "urn:epsos:names:wp3.4:subject:healthcare-facility-type",

            "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",

            "xs:string"),

    /**
     * The XSPA purpose of use.
     */
    XSPAPurposeOfUse("XSPA Purpose Of Use",

            "urn:oasis:names:tc:xspa:1.0:subject:purposeofuse",

            "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",

            "xs:string"),

    /**
     * The XSPA locality.
     */
    XSPALocality("XSPA Locality",

            "urn:oasis:names:tc:xspa:1.0:environment:locality",

            "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",

            "xs:string"),

    /**
     * The XSPA permissions according with Hl7.
     */
    XSPAPermissionsHL7("XSPA permissions according with Hl7",

            "urn:oasis:names:tc:xspa:1.0:subject:hl7:permission",

            "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",

            "xs:string");

    /**
     * The friendly name.
     */
    private String friendlyName;

    /**
     * The name.
     */
    private String name;

    /**
     * The type.
     */
    private String type;

    /**
     * The xsi type.
     */
    private String xsiType;


    /**
     * Instantiates a new saml assertion attribute type.
     *
     * @param friendlyName the friendly name
     * @param name         the name
     * @param type         the type
     */
    EpSOSAssertionAttributeType(String friendlyName, String name, String type, String xsiType) {
        this.friendlyName = friendlyName;
        this.name = name;
        this.type = type;
        this.xsiType = xsiType;
    }

    /**
     * Gets the friendly name.
     *
     * @return the friendly name
     */
    public String getFriendlyName() {
        return friendlyName;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Gets the xsi type.
     *
     * @return the xsi type
     */
    public String getXsiType() {
        return xsiType;
    }


    /**
     * <p>byFriendlyName.</p>
     *
     * @param friendlyName a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.simulator.samlassertion.epsos.EpSOSAssertionAttributeType} object.
     */
    public static EpSOSAssertionAttributeType byFriendlyName(String friendlyName) {
        EpSOSAssertionAttributeType result = null;
        for (EpSOSAssertionAttributeType assertionAttributeType : EpSOSAssertionAttributeType.values()) {
            if (assertionAttributeType.getFriendlyName().equals(friendlyName)) {
                result = assertionAttributeType;
            }
        }
        return result;
    }
}

package net.ihe.gazelle.simulator.common.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.jboss.seam.annotations.Name;


@Entity
/**
 * <p>ContextualInformationInstance class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("contextualInformationInstance")
@Table(name = "gs_contextual_information_instance", schema = "public")
@SequenceGenerator(name = "gs_contextual_information_instance_sequence", sequenceName = "gs_contextual_information_instance_id_seq", allocationSize = 1)
public class ContextualInformationInstance implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** Constant <code>INPUT="input"</code> */
	public static final String INPUT = "input";
	
	/** Constant <code>OUTPUT="output"</code> */
	public static final String OUTPUT = "output";

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gs_contextual_information_instance_sequence")
	private Integer id;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name= "contextual_information_id",nullable=false)
	private ContextualInformation contextualInformation;

	@Column(name = "value")
	private String value;
	
	@ManyToOne
    @JoinColumn(name= "test_steps_instance_id",nullable=false)
    private TestStepsInstance testStepsInstance;
	
	@Column(name = "form")
    private String form;

    /**
     * <p>Constructor for ContextualInformationInstance.</p>
     */
    public ContextualInformationInstance(){

	}

	/**
	 * <p>Constructor for ContextualInformationInstance.</p>
	 *
	 * @param contextualInformation a {@link net.ihe.gazelle.simulator.common.model.ContextualInformation} object.
	 * @param value a {@link java.lang.String} object.
	 */
	public ContextualInformationInstance(ContextualInformation contextualInformation,String value){
		this.contextualInformation=contextualInformation;
		this.value=value;
	}

	/**
	 * <p>Constructor for ContextualInformationInstance.</p>
	 *
	 * @param inContextualInformationInstance a {@link net.ihe.gazelle.simulator.common.model.ContextualInformationInstance} object.
	 */
	public ContextualInformationInstance(ContextualInformationInstance inContextualInformationInstance){
		if(inContextualInformationInstance.getContextualInformation()!=null) this.contextualInformation = inContextualInformationInstance.getContextualInformation();
		if(inContextualInformationInstance.getValue() !=null) this.value  = inContextualInformationInstance.getValue();
	}
	
	/**
	 * <p>Setter for the field <code>testStepsInstance</code>.</p>
	 *
	 * @param testStepsInstance a {@link net.ihe.gazelle.simulator.common.model.TestStepsInstance} object.
	 */
	public void setTestStepsInstance(TestStepsInstance testStepsInstance) {
        this.testStepsInstance = testStepsInstance;
    }

    /**
     * <p>Getter for the field <code>testStepsInstance</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.model.TestStepsInstance} object.
     */
    public TestStepsInstance getTestStepsInstance() {
        return testStepsInstance;
    }

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * <p>Getter for the field <code>contextualInformation</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.model.ContextualInformation} object.
	 */
	public ContextualInformation getContextualInformation() {
		return contextualInformation;
	}

	/**
	 * <p>Setter for the field <code>contextualInformation</code>.</p>
	 *
	 * @param contextualInformation a {@link net.ihe.gazelle.simulator.common.model.ContextualInformation} object.
	 */
	public void setContextualInformation(ContextualInformation contextualInformation) {
		this.contextualInformation = contextualInformation;
	}

	/**
	 * <p>Getter for the field <code>value</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <p>Setter for the field <code>value</code>.</p>
	 *
	 * @param value a {@link java.lang.String} object.
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	 /**
	  * <p>Getter for the field <code>form</code>.</p>
	  *
	  * @return a {@link java.lang.String} object.
	  */
	 public String getForm() {
        return form;
    }

    /**
     * <p>Setter for the field <code>form</code>.</p>
     *
     * @param form a {@link java.lang.String} object.
     */
    public void setForm(String form) {
        this.form = form;
    }


	/**
	 * <p>getContextualInformationInstanceListForContextualInformation.</p>
	 *
	 * @param inContextualInformation a {@link net.ihe.gazelle.simulator.common.model.ContextualInformation} object.
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link java.util.List} object.
	 */
	public static List<ContextualInformationInstance> getContextualInformationInstanceListForContextualInformation(
			ContextualInformation inContextualInformation,
			EntityManager entityManager) {
		if(inContextualInformation!=null){
			Query query = entityManager.createQuery("SELECT DISTINCT cii FROM ContextualInformationInstance cii WHERE cii.contextualInformation=:inContextualInformation");
			query.setParameter("inContextualInformation", inContextualInformation);
			List<ContextualInformationInstance> list=query.getResultList();
			if(list.size()>0)
				return list;
		}
		return null;
	}
	
	//------ hashcode and equals ----------------------------------------

	/**
	 * <p>hashCode.</p>
	 *
	 * @return a int.
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((contextualInformation == null) ? 0 : contextualInformation
						.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContextualInformationInstance other = (ContextualInformationInstance) obj;
		if (contextualInformation == null) {
			if (other.contextualInformation != null)
				return false;
		} else if (!contextualInformation.equals(other.contextualInformation))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
	//------ toString ----------------------------------------

	/** {@inheritDoc} */
	@Override
    public String toString() {
        return "ContextualInformationInstance [id=" + id
                + ", contextualInformation=" + contextualInformation
                + ", value=" + value + ", testStepsInstance="
                + testStepsInstance + "]";
    }

}

package net.ihe.gazelle.simulator.common.utils;



import java.io.Serializable;

import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;





/**
 * <b>Class Description : </b>ExceptionLogging<br>
 * <br>
 * This class manages the exception methods actions. It corresponds to the Business Layer.
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, March 4th
 */
public class ExceptionLogging implements Serializable {


	/** Serial ID version of this object */
	private static final long serialVersionUID = -450973131258793760L;

	 
    

	/**
	 * <p>logException.</p>
	 *
	 * @param e a {@link java.lang.Exception} object.
	 * @param log a {@link org.jboss.seam.log.Log} object.
	 */
	public static void logException (Exception e, Log log )
	{
		
		
		log.fatal("-----------------------------------------------");
		log.fatal("-----------------------------------------------");
		log.fatal("   Logging Exception...(" + e.getClass() +" )"  );
		log.fatal("-----------------------------------------------");
		log.fatal("-----------------------------------------------");
		log.fatal("");
		
		String usersInfo = Identity.instance().getCredentials().getUsername() ;
		log.fatal("User information  = "+ usersInfo );
		
		log.fatal("Exception Type = "   + e.getClass()  );
		log.fatal("Exception message = "+ e.getMessage());
		//LOG.fatal("Localized message = " + e.getLocalizedMessage());
		
		 e.printStackTrace();
		/* 
		LOG.info("Send email to admin...");
		try {
		   Renderer.instance().render("/email/errorStackTraceEmail.xhtml");
        	StatusMessages.instance().addFromResourceBundle("gazelle.common.faces.email.exception");
            
        
		}
        catch (final Exception emailException) {
            LOG.error("Error sending exception email", emailException);
       
        }
        */
	}
	

}


package net.ihe.gazelle.simulator.samlassertion.common;

/**
 * <p>SignatureException class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class SignatureException extends Exception {

    private static final long serialVersionUID = -5100871848771290637L;

    /**
     * <p>Constructor for SignatureException.</p>
     */
    public SignatureException() {
        super();
    }

    /**
     * <p>Constructor for SignatureException.</p>
     *
     * @param message a {@link String} object.
     * @param cause   a {@link Throwable} object.
     */
    public SignatureException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * <p>Constructor for SignatureException.</p>
     *
     * @param message a {@link String} object.
     */
    public SignatureException(String message) {
        super(message);
    }

    /**
     * <p>Constructor for SignatureException.</p>
     *
     * @param cause a {@link Throwable} object.
     */
    public SignatureException(Throwable cause) {
        super(cause);
    }

}

package net.ihe.gazelle.simulator.common.utils;

/**
 * <p>AndOrWhere class.</p>
 *
 * @author bderrazek aboufahja
 * @version $Id: $Id
 */
public class AndOrWhere {
    
    /**
     * <p>Constructor for AndOrWhere.</p>
     */
    public AndOrWhere(){
    }
    
    /**
     * <p>addANDorWHERE.</p>
     *
     * @param currentQueryString a {@link java.lang.StringBuffer} object.
     * @return a {@link java.lang.StringBuffer} object.
     */
    public static StringBuffer addANDorWHERE( StringBuffer currentQueryString  )
    {
        if ( currentQueryString.indexOf("WHERE") > 0 )
            return currentQueryString.append(" AND " ) ;
        else 
            return currentQueryString.append(" WHERE " ) ;

    }

}

package net.ihe.gazelle.simulator.common.ihewsinit;

import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.EStandard;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.SOAPMessage;
import java.io.File;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;

public class SoapWebServiceClientTest extends SoapWebServiceClient{

    @Test
    public void testGetFirstElementChildWithWhiteSpace(){
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        //API to obtain DOM Document instance
        DocumentBuilder builder;

        try {
            String xmlString = FileUtils.readFileToString(new File("src/test/resource/testTagWithWhiteSpaces.xml"), StandardCharsets.UTF_8.toString());
            //Create DocumentBuilder with default configuration
            builder = dbf.newDocumentBuilder();

            //Parse the content to Document object
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            Node node = doc.getFirstChild();
            Node firstBodyNode = getFirstElementChild(node.getChildNodes().item(3).getChildNodes());
            Assert.assertEquals("ns2:TEST_TAG", firstBodyNode.getNodeName());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testGetFirstElementChildWithoutWhiteSpace(){
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        //API to obtain DOM Document instance
        DocumentBuilder builder;
        try {
            String xmlString = FileUtils.readFileToString(new File("src/test/resource/testTagWithoutWhiteSpaces.xml"), StandardCharsets.UTF_8.toString());
            //Create DocumentBuilder with default configuration
            builder = dbf.newDocumentBuilder();

            //Parse the content to Document object
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            Node node = doc.getFirstChild();
            Node firstBodyNode = getFirstElementChild(node.getChildNodes().item(3).getChildNodes());
            Assert.assertEquals("ns2:TEST_TAG", firstBodyNode.getNodeName());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Override
    protected Element getAssertionFromSTS(String username, String password) {
        return null;
    }

    @Override
    protected void appendAssertionToSoapHeader(SOAPMessage msg, Element assertion) {

    }

    @Override
    public Domain getDomainForTransactionInstance() {
        return null;
    }

    @Override
    public String getIssuerName() {
        return null;
    }

    @Override
    protected QName getServiceQName() {
        return null;
    }

    @Override
    protected QName getPortQName() {
        return null;
    }

    @Override
    protected Actor getSimulatedActor() {
        return null;
    }

    @Override
    protected Transaction getSimulatedTransaction() {
        return null;
    }

    @Override
    protected Actor getSutActor() {
        return null;
    }

    @Override
    protected EStandard getStandard() {
        return null;
    }
}

package net.ihe.gazelle.simulator.common.ihewsinit;

import org.testng.annotations.Test;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import static org.testng.Assert.*;

/**
 * Tests for {@link SoapMessageConverter}
 */
public class SoapMessageConverterTest {

    /**
     * Test conversion of a Soap Message to a byte array.
     */
    @Test
    public void testSoapMessageToByteArray() throws SOAPException {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        assertNotNull(SoapMessageConverter.soapMessageToByteArray(soapMessage));
    }
}
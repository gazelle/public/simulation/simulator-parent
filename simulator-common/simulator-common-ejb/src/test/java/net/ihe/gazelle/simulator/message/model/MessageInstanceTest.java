package net.ihe.gazelle.simulator.message.model;

import org.junit.Assert;
import org.junit.Test;

public class MessageInstanceTest {

    private MessageInstance messageInstance = new MessageInstance();
    private String inputString = "Test PDQm messages";

    /**
     * Test the get payload
     */
    @Test
    public void testGetPayload() {
        byte[] byteMessage = inputString.getBytes();
        messageInstance.setPayload(byteMessage);
        Assert.assertEquals("The getPayload shall return a byte message with the inputString value", messageInstance.getPayload(), byteMessage);
    }

    /**
     * Test the set payload
     */
    @Test
    public void testSetPayload() {
        byte[] byteMessage = inputString.getBytes();
        messageInstance.setPayload(byteMessage);
        Assert.assertEquals("The setPayload shall initiate a byte message with the inputString value", messageInstance.getPayload(), byteMessage);
    }



}